// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicConferenceCalendarHeader } from './IonicConferenceCalendarHeader';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

@NgModule({
    declarations: [
        IonicConferenceCalendarHeader,
    ],
    imports: [
        IonicPageModule.forChild(IonicConferenceCalendarHeader),
    ],
    exports: [
        IonicConferenceCalendarHeader
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class IonicConferenceCalendarHeaderModule {}
