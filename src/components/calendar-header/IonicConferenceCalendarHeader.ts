// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import * as moment from "moment";
import { Constants } from "../../common/Constants";

import { Component, ViewChildren, ChangeDetectorRef, QueryList, ElementRef, ViewChild, Input, Output, EventEmitter, SimpleChanges, OnInit, OnDestroy } from "@angular/core";
import { Events, PickerController, AlertController, Alert, PickerColumn, Picker } from "ionic-angular";

const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

const monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

const Names = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];

interface IEventYear {
    year: number;
    months: IEventMonth[];
    dates: Date[];
}

interface IEventMonth {
    month: number;
    monthName: string;
    days: number[];
    dates: Date[];
}

@Component({
    selector: "ionic-conference-calendar-header",
    template: `
        <div class="header-calendar-wrapper">
            <div (click)="openDatePicker()">
                <div *ngIf="viewType === 'days'; else month_datepicker_view" style="display: inline-flex">
                    <div class="datepicker-headline">{{ getMonthName() }}</div>
                    <ion-icon name="ios-arrow-down" style="margin-left: 10%;margin-top: 7%;font-weight: bolder;font-size: 18px;"></ion-icon>
                </div>

                <ng-template #month_datepicker_view>
                    <div class="datepicker-headline">{{ activeYear }}</div>
                </ng-template>
            </div>

            <div #selectionWrapper class="selection-wrapper">
                <div #selectionSlider *ngIf="years.length" [style.width]="getSliderWidth() + 'px'" class="selection-slider">
                    <div class="selection-slider-padding"></div>

                    <div *ngIf="viewType === 'days'; else month_selection_view">
                        <div *ngFor="let d of getDaysInMonth(); let i = index;" class="holder">
                            <span class="dayName">{{ dateNames[i] }}</span>
                            <div #selection [ngClass]="{ active: d == activeDay }" (click)="setActiveDay(d)" class="selection">
                                {{ d }}
                            </div>
                        </div>
                    </div>

                    <ng-template #month_selection_view>
                        <div #selection *ngFor="let m of getMonthsInYear()" [ngClass]="{ active: m == activeMonth }" (click)="setActiveMonth(m)" class="selection">
                            {{ getMonthShortName(m) }}
                        </div>
                    </ng-template>

                    <div class="selection-slider-padding"></div>
                </div>
            </div>
        </div>
    `,
})
export class IonicConferenceCalendarHeader implements OnDestroy {
    private margin: number = 0.5;

    private selectionWidth: number = 64;

    private years: IEventYear[] = [];

    private pickerYearColumn: PickerColumn;

    private pickerMonthColumn: PickerColumn;

    public sliderWidth: number;

    public activeYear: number;

    public activeMonth: number;

    public activeDay: number;

    @Output("change") change = new EventEmitter<string | Date>();

    public dateNames: Array<string> = [];

    @Input("dates") dates: Array<string | Date> = [];

    @Input("date") date: string | Date;

    @Input("view") viewType: "days" | "months" = "days";

    @ViewChild("selectionWrapper") selectionWrapper: ElementRef;

    @ViewChild("selectionSlider") selectionSlider: ElementRef;

    @ViewChildren("selection") selections: QueryList<ElementRef>;

    constructor(private pickerController: PickerController, public cds: ChangeDetectorRef, public events: Events) {
        let newDate: Date = new Date(Date.now());
        newDate.setMonth(newDate.getMonth());
        let dt = moment(newDate);
        this.setActiveDay(dt.utc().date(), dt.utc().month(), dt.utc().year(), true);
        setTimeout(() => {
               this.getDayNamesInMoth();
        }, 300);
    }

    public ngOnChanges(changes: SimpleChanges) {
        let activeDateChanged = false;
        let datesChanged = false;

        if (!this.pickerMonthColumn) {
            this.pickerMonthColumn = {
                name: "Month",
                align: "center",
                options: [],
            };

            this.pickerYearColumn = {
                name: "Year",
                align: "center",
                options: [],
            };
        }
        let updateDayNames: boolean = true;

        if (changes.dates) {
            updateDayNames = false;
            this.expandEvents();

            if (!changes.date) {
                this.setActiveYear(this.years[0].year, false);
            }
        } else {
            this.getDayNamesInMoth();
        }

        if (changes.date) {
            updateDayNames = false;
            var dt = moment(changes.date.currentValue);
            if (null != this.date) {
                
            } else {
                let newDate: Date = new Date(changes.date.currentValue);
                dt = moment(newDate);
            }
            this.setActiveDay(dt.utc().date(), dt.utc().month(), dt.utc().year(), false);
        } else {
            this.getDayNamesInMoth();
        }
        setTimeout(() => {
            if (updateDayNames) {
                this.cds.detectChanges();
            //   this.getDayNamesInMoth();
            }
        }, 300);
    }

    public ngOnDestroy(): void {
        console.log("ngOnDestroy");
        this.events.unsubscribe(Constants.EVENT_GO_TODAY);
    }

    public getSliderWidth(): number {
        let selectionWrapperWidth = (<HTMLDivElement>this.selectionWrapper.nativeElement).offsetWidth;

        let possibleSelections = 0;

        if (this.viewType === "days") {
            possibleSelections = this.getDaysInMonth().length;
        } else {
            possibleSelections = this.getMonthsInYear().length;
        }

        return Math.max((selectionWrapperWidth + this.selectionWidth + this.selectionWidth * this.margin) * possibleSelections);
    }

    /**
     * Takes all this.dates and expands into years array
     */
    private expandEvents() {
        this.years = [];

        this.dates.forEach((dt) => {
            let date = moment(dt);

            let nDay = date.utc().date();
            let nYear = date.utc().year();
            let nMonth = date.utc().month();
            let monthName = monthNames[nMonth];

            let year = this.years.filter((yr) => {
                return yr.year === nYear;
            })[0];

            if (!year) {
                year = {
                    year: nYear,
                    dates: [],
                    months: [],
                };

                this.years.push(year);
            }

            year.dates.push(date.toDate());

            let month = year.months.filter((m) => {
                return m.month === nMonth;
            })[0];

            if (!month) {
                month = {
                    month: nMonth,
                    monthName: monthName,
                    dates: [],
                    days: [],
                };

                year.months.push(month);
            }

            month.dates.push(date.toDate());

            if (month.days.indexOf(nDay) == -1) {
                month.days.push(nDay);
            }

            month.days = month.days.sort((a, b) => {
                if (a > b) {
                    return 1;
                } else if (b > a) {
                    return -1;
                }

                return 0;
            });

            year.months = year.months.sort((a, b) => {
                if (a.month > b.month) {
                    return 1;
                } else if (b.month > a.month) {
                    return -1;
                }

                return 0;
            });
        });

        this.years = this.years.sort((a, b) => {
            if (a.year > b.year) {
                return 1;
            } else if (b.year > a.year) {
                return -1;
            }

            return 0;
        });

        this.years.forEach((yr) => {
            this.pickerYearColumn.options.push({
                value: yr.year,
                text: yr.year.toString(),
            });
        });
    }

    public getMonthsInYear(y: number = this.activeYear): number[] {
        return this.getYear(y).months.map((m) => m.month);
    }

    public getDaysInMonth(m: number = this.activeMonth, y: number = this.activeYear): number[] {
        return this.getMonth(m, y).days;
    }

    public getDayNamesInMoth(year: number = this.activeYear, month: number = this.activeMonth) {
        this.dateNames = [];
        let date = new Date(Date.now());
        let firstDay = new Date(year, month, 1);
        let firstDayNumber = firstDay.getDay();
        let index: number = firstDayNumber;
        let days = new Date(year, month, 0).getDate();
        let arr = Array.from(Array(days).keys());
       // arr = arr.slice(firstDay.getDate(), arr.length);
        // console.log("days", arr, firstDayNumber);
        arr.forEach((day: number) => {
            this.dateNames.push(Names[index]);
            if (index === 6) {
                index = 0;
            } else {
                index += 1;
            }
        });
    }

    private getYear(y: number = this.activeYear) {
        return this.years.filter((yr) => {
            return yr.year === y;
        })[0];
    }

    private getMonth(m: number = this.activeMonth, y: number = this.activeYear) {
        let year = this.getYear(y);

        return year.months.filter((mn) => {
            return mn.month === m;
        })[0];
    }

    public setActiveMonth(m: number, y: number = this.activeYear, emitEvent: boolean = true) {
        this.activeYear = y;
        this.activeMonth = m;
        // console.log("setActiveMonth", this.activeMonth, m);
        this.setActiveDay(this.getMonth(m, y).days[0], m, y, emitEvent);
    }

    public setActiveYear(y: number, emitEvent: boolean = true) {
        this.activeYear = y;
        let year = this.getYear(y);
        this.setActiveMonth(this.getYear(y).months[0].month, y, emitEvent);
    }

    public setActiveDay(d: number, m: number = this.activeMonth, y: number = this.activeYear, emitEvent: boolean = true) {
        this.activeYear = y;
        this.activeMonth = m;
        this.activeDay = d;

        setTimeout(() => {
            this.updateSelectionScrollPosition();
        }, 200);

        if (emitEvent) {
            let ds = this.getDateString();
            this.change.emit(ds);
        }
    }

    public getSelectionIndex() {
        if (this.viewType === "days") {
           // this.getDayNamesInMoth();
            return this.getDaysInMonth().indexOf(this.activeDay);
        } else {
            return this.getMonthsInYear().indexOf(this.activeMonth);
        }
    }

    public updateSelectionScrollPosition() {
        if (this.selections) {
            let idx = this.getSelectionIndex();
            let day = this.selections.toArray()[idx];
            if (null != day) {
                let offset = (<HTMLDivElement>day.nativeElement).offsetLeft - this.selectionWrapper.nativeElement.offsetWidth / 2 + this.selectionWidth / 2;

                this.scrollTo(offset);
            }
        }
    }

    public getMonthName(m: number = this.activeMonth) {
        return monthNames[m];
    }

    public getMonthShortName(m: number = this.activeMonth) {
        return monthShortNames[m];
    }

    public openDatePicker() {
        this.updatePickerMonthOptions(this.years[0].year);

        // Short circuit if no meaningful options exist
        if (this.viewType === "days") {
            if (this.years.length < 2 && this.pickerMonthColumn.options.length < 2) {
                return;
            }
        } else {
            if (this.years.length < 2) {
                return;
            }
        }

        let columns: PickerColumn[] = [];

        if (this.years.length > 1) {
            columns.push(this.pickerYearColumn);
        }

        if (this.viewType === "days") {
            columns.push(this.pickerMonthColumn);
        }

        let picker = this.pickerController.create({
            columns: columns,
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Done",
                    handler: (data: any) => {
                        if (data.Month) {
                            this.setActiveMonth(data.Month.value, data.Year ? data.Year.value : this.years[0].year);
                        } else {
                            this.setActiveMonth(this.getMonthsInYear(data.Year.value)[0], data.Year.value);
                        }
                    },
                },
            ],
        });

        if (this.viewType === "days") {
            let yr = this.years[0].year;

            /**
             * July, 15 2017
             *
             * Hacked the shit out of this. As of this date, current ControlPicker does not support dynamic column items.
             * If they update this, this should be unhacked.
             *
             * Didn't mean to introduce lavaflow this early.
             */

            picker.ionChange.subscribe((change: any) => {
                if (change.Year && change.Year.value !== yr) {
                    yr = change.Year.value;

                    this.updatePickerMonthOptions(yr);

                    setTimeout(() => {
                        (<any>picker)._cmp._component._cols._results.forEach((r: any) => {
                            r.col.prevSelected = null;
                        });

                        picker.refresh();
                    });
                }
            });
        }

        picker.present();

        setTimeout(() => {
            (<any>picker)._cmp._component._cols._results.forEach((r: any) => {
                r.lastIndex = 0;
                r.col.selectedIndex = 0;
                r.col.prevSelected = null;
            });

            picker.refresh();
        });
    }

    private updatePickerMonthOptions(year: number) {
        this.pickerMonthColumn.options.length = 0;

        this.getYear(year).months.forEach((mn) => {
            this.pickerMonthColumn.options.push({
                value: mn.month,
                text: mn.monthName,
            });
        });
    }

    private getDateString(date: string | Date = null) {
        if (!date) {
            return `${this.activeYear}-${this.padLeft(this.activeMonth + 1)}-${this.padLeft(this.activeDay)}`;
        }

        let dt = moment(date);

        let day = dt.utc().date();
        let month = dt.utc().month();
        let year = dt.utc().year();

        return `${year}-${this.padLeft(month + 1)}-${this.padLeft(day)}`;
    }

    private scrollTo(to: any) {
        let ele = <HTMLDivElement>this.selectionWrapper.nativeElement;

        ele.scrollLeft = to;
    }

    private padLeft(n: number) {
        let str = n.toString();
        let pad = "00";
        return pad.substr(0, pad.length - str.length) + str;
    }
}
