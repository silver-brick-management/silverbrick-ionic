// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Component, ElementRef, Input, OnInit, ChangeDetectionStrategy, OnChanges } from '@angular/core';

@Component({
    selector: 'letter-avatar',
    templateUrl: 'letter-avatar.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LetterAvatarComponent implements OnInit, OnChanges {
    @Input() text: string;
    @Input() size: number;
    @Input() color: string;
    @Input() border: string;
    @Input() isSquare: boolean;
    @Input() fontColor: string;
    @Input() background: string;
    @Input() fixedColor: boolean;

    public props: any = null;
    private _el: HTMLElement;

    constructor(el: ElementRef) {
        // Init props and element
        this.props = new Object();
        this._el = el.nativeElement;
    }

    test(): void {
        this.generateLetter();
    }

    generateLetter(): void {
        // Init default values if non provided
        this.text = this.text ? this.text : '?';
        this.size = this.size ? this.size : 100;
        this.isSquare = this.isSquare ? true : false;
        this.fixedColor = (this.fixedColor == null) ? true : this.fixedColor; // Defaulting to true
        this.background = this.background ? this.background : null;
        this.fontColor = this.fontColor ? this.fontColor : '#FFFFFF';
        this.border = this.border ? this.border : "1px solid #d3d3d3";

        // Generate dynamic values
        const fontSize: number = (39 * this.size) / 100;
        const textArray: string[] = this.text.split(' ');
        let letter: string = textArray[0].substr(0, 1) + '' + (textArray.length > 1 ? textArray[1].substr(0, 1) : '');
        letter = letter.toUpperCase();

        // Set style properties
        this.props['size'] = this.size + 'px';
        this.props['lineheight'] = this.size + 'px';
        this.props['letter'] = letter;
        this.props['fontSize'] = fontSize + 'px';
        this.props['textalign'] = 'center';
        this.props['border'] = this.border;
        if (this.isSquare) {
            this.props['borderradius'] = '0%';
        } else {
            this.props['borderradius'] = '50%';
        }
        if (this.fixedColor && !this.background) {
            this.props['background'] = this.background || this.colorize(letter);
        } else {
            this.props['background'] = this.background || this.getRandomColor();
        }
    }

    getRandomColor(): string {
        const letters: string[] = '0123456789ABCDEF'.split('');
        let color: string = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    colorize(str: string): string {
        let color: string = ';';
        for (let i = 0, hash = 0; i < str.length; hash = str.charCodeAt(i++) + ((hash << 5) - hash)) {
            color = Math.floor(Math.abs((Math.sin(hash) * 10000) % 1 * 16777216)).toString(16);
        }
        return '#' + Array(6 - color.length + 1).join('0') + color;
    }

    ngOnInit(): void {
        this.generateLetter();
    }

    ngOnChanges(...args: any[]): void {
        this.generateLetter();
    }
}
