// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarHeaderComponent } from './calendar-header.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
    CalendarHeaderComponent,
  ],
  imports: [
    IonicPageModule.forChild(CalendarHeaderComponent),
  ],
  exports: [
    CalendarHeaderComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
  })
export class CalendarHeaderComponentModule {}
