// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef, Input, Output, EventEmitter } from "@angular/core";


import { AppState } from "../../app/app.state";

import { Constants, Urls, colors } from "../../common/Constants";

import { IUserState, UserStateType, IOrgState, OrgStateType, SessionStateType, ISessionState } from "../../interfaces/IStoreState";
import { Store } from "@ngrx/store";

import { SessionActions } from "../../store/actions/SessionActions";

import { OrgActions } from "../../store/actions/OrgActions";

import { SessionData } from "../../providers/SessionData";
import { FirebaseAuth } from '../../services/FirebaseAuth';
import { Controls } from "../../providers/Controls";
import { UserActions } from "../../store/actions/UserActions";

@Component({
    selector: "mwl-demo-utils-calendar-header",
    templateUrl: "calendar-header.component.html"
})
export class CalendarHeaderComponent {
    @Input() view: string;

    @Input() viewDate: Date;

    @Input() locale: string = "en";

    @Output() viewChange: EventEmitter<string> = new EventEmitter();

    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
}