// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable, Component, Input, OnInit, OnChanges, OnDestroy, ViewChild, Output, Optional, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import {
    CalendarDayViewBeforeRenderEvent,
    CalendarMonthViewBeforeRenderEvent,
    CalendarWeekViewBeforeRenderEvent,
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay,
    CalendarWeekViewEvent
} from "angular-calendar";
import {
    getMonth,
    startOfMonth,
    startOfWeek,
    startOfDay,
    endOfWeek,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours } from "date-fns";
import RRule from 'rrule';
// import { NgxSmartModalService, NgxSmartModalConfig } from "ngx-smart-modal";
import { Subject } from "rxjs/Subject";
import { ViewPeriod } from 'calendar-utils';

// Local modules
import { AppState } from "../../../app/app.state";
import { Constants, Urls, colors } from "../../../common/Constants";
import { IError } from "../../../interfaces/IError";
import {
    IUser,
    BeforeCalendarLoadMonth,
    BeforeCalendarLoadWeek,
    RecurringEvent,
    IOrgBasic,
    ITask
} from "../../../shared/SilverBrickTypes";
import { IUserState, UserStateType, IOrgState, OrgStateType, SessionStateType, ISessionState } from "../../../interfaces/IStoreState";
import { SessionActions } from "../../../store/actions/SessionActions";
import { UserActions } from "../../../store/actions/UserActions";

import { OrgActions } from "../../../store/actions/OrgActions";
import { UserUtil } from "../../../common/UserUtil";
import { IPaginateQueryStrings, IGetBldgHelper } from "../../../shared/IParams";
// import { UserDetailComponent } from '../components/user-details/user-details.component';
import { SessionData } from "../../../providers/SessionData";
import { FirebaseAuth } from '../../../services/FirebaseAuth';
import { Controls } from '../../../providers/Controls';
export declare type WeekdayStr = 'SU' |'MO' | 'TU' | 'WE' | 'TH' | 'FR' | 'SA';

@Component({
    selector: "main-calendar",
    templateUrl: "./main-calendar.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainCalendarComponent {
    @Input() TaskToggle: string;
    private _sessionStateSubscription: Subscription;
    private _commStateSubscription: Subscription;
    private _userStateSubscription: Subscription;
    private _usersSubscription: Subscription;
    private _screenSize: number = window.screen.width;

    public UserState$: Observable<IUserState>;
    public uidOrEmail: string = "";
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public tasks$: Observable<Array<ITask>>;
    public SessionState$: Observable<ISessionState>;
    public OrgState$: Observable<IOrgState>;
    public ListView: boolean = false;
    public CloseButton: string = 'Will Do';
    public ModalTitle: string = 'Setup Payment Info?';
    public ModalMessage: string = '';
    public view: string = "week";
    public SelectedDate: Date = null;
    public profile$: Observable<IUser>;
    public WeekDays: WeekdayStr[] = ['SU','MO', 'TU', 'WE', 'TH', 'FR','SA'];
    public DurationOptions: string[] = ['day(s)', 'week(s)', 'month(s)', 'year(s)'];
    public FrequencyOptions: string[] = ['Daily', 'Weekly', 'Monthly', 'Yearly'];
    public actions: CalendarEventAction[] = [
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
        onClick: ({ event, date }: { event: RecurringEvent, date: Date }): void => {
           // this.RecurringTask(event.task, date);
        }
    },
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-times"></i>',
        onClick: ({ event }: { event: RecurringEvent }): void => {
            this.allEvents = this.allEvents.filter(iEvent => iEvent !== event);
            this.handleEvent("Deleted", event);
        }
    }
    ];
    public recurringEvents: RecurringEvent[] = [
        // {
        //     title: 'Trash (68 4th Place)',
        //     color: colors.yellow,
        //     rrule: {
        //         freq: RRule.MONTHLY,
        //         bymonthday: 5,
        //     },
        //     start: new Date(),
        //     actions: this.actions,
        //     resizable: {
        //         beforeStart: true,
        //         afterEnd: true
        //     },
        //     displayTitle: 'Trash (68 4th Place)'
        // }
    ];

    public viewDate: Date = new Date(Date.now());

    public modalData: {
        action: string;
        event: RecurringEvent;
    };

    

    public eventActions: CalendarEventAction[] = [
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
        onClick: ({ event }: { event: RecurringEvent }): void => {
        }
    },
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-times"></i>',
        onClick: ({ event }: { event: RecurringEvent }): void => {
            this.allEvents = this.allEvents.filter(iEvent => iEvent !== event);
            this.handleEvent("Deleted", event);
        }
    }
    ];

    public refresh: Subject<any> = new Subject();

    public allEvents: RecurringEvent[] = [];
    public activeDayIsOpen: boolean = false;
    public isNewTask: boolean = false;
    public updateParams: BeforeCalendarLoadMonth | BeforeCalendarLoadWeek = null;

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    constructor(
        public controls: Controls,
        public orgActions: OrgActions,
        public sessionData: SessionData,
        public userActions: UserActions,
        public firebaseAuth: FirebaseAuth,
        public store: Store<AppState>,
        private cdr: ChangeDetectorRef,
        public sessionActions: SessionActions) {
        this.error$ = this.store.select(state => state.usersState.error);
        this.loading$ = this.store.select(state => state.sessionState.loading);
        this.OrgState$ = this.store.select(state => state.orgState);
        this.SessionState$ = this.store.select(state => state.sessionState);
        this.tasks$ = this.store.select(state => state.sessionState.tasks);
        this.UserState$ = this.store.select(state => state.usersState);
        this.profile$ = this.store.select(state => state.authState.profile);
    }

    ngOnInit(): void {
        this.Initialize();
    }

    ngOnDestroy(): void {
        // this.Unitialize();
    }

    ionViewDidLeave(): void {
        // this.Unitialize();
    }

    public async Initialize(): Promise<void> {
        if ((null != this.sessionData.user)) {
            if (!this.sessionData.user.isAdmin) {
                if (this.sessionData.user.role === "Tenant") {
                } else {

                    let bldgHelper: IGetBldgHelper = {
                        orgID: this.sessionData.org.id,
                        buildingID: this.sessionData.org.buildings[0].id
                    };
                    console.log("bldgHelper", bldgHelper);
                    this.store.dispatch(this.sessionActions.GetRecurringStaffTasks(this.sessionData.user.uid));

                }
            } else {
                console.log("GetAllHistory");
                // this.store.dispatch(this._sessionActions.GetAllHistory());
                this.store.dispatch(this.sessionActions.GetRecurringTasks());
            }
        }

        this._sessionStateSubscription = this.SessionState$.subscribe(async (state: ISessionState) => {
            if (null != state) {
                switch (state.type) {
                    case SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED:
                    {
                        this.cdr.detectChanges();
                        break;
                    }

                    case SessionStateType.GET_RECURRING_TASKS_SUCCEEDED:
                    {
                        this.recurringEvents = [];
                        console.log("SessionStateType", SessionStateType[state.type]);
                        let eventAction = state.recurringTasks.forEach((task: ITask) => {
                            let newEvent: RecurringEvent = {
                                start: new Date(task.startDate),
                                title: `${ task.subject }\n(${ task.buildingName })`,
                                actions: this.actions,
                                color: colors.blue,
                                resizable: {
                                    beforeStart: true,
                                    afterEnd: true
                                },
                                meta: `${ task.subject }\n(${ task.buildingName })`,
                                task: task,
                                displayTitle: `${ task.subject }\n(${ task.buildingName })`
                            };
                            switch (task.durationAmount) {
                                case this.DurationOptions[0]:
                                {
                                    let endDate: Date = new Date(task.startDate);
 
                                    endDate.setDate(endDate.getDate() + Number(task.durationQuantity));
                                    newEvent.end = endDate;
                                    console.log("Daily", endDate);
                                    break;
                                }

                                case this.DurationOptions[1]:
                                {
                                    let endDate: Date = new Date(task.startDate);
                                    endDate.setDate(endDate.getDate() + (Number(task.durationQuantity) * 7));
                                    newEvent.end = endDate;
                                    console.log("Weekly", endDate);
                                    break;
                                }

                                case this.DurationOptions[2]:
                                {
                                    let date: Date = new Date(task.startDate);
                                    let currentDate = date.getDate();
                                    // Set to day 1 to avoid forward
                                    date.setDate(1);
                                    console.log("date", date, Number(task.durationQuantity));
                                    // Increase month by 1
                                    date.setMonth(date.getMonth() + Number(task.durationQuantity) + 1);
                                    // Get max # of days in this new month
                                    let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
                                    // Set the date to the minimum of current date of days in month
                                    date.setDate(Math.min(currentDate, daysInMonth));
                                    // date.setMonth(date.getMonth() + task.durationQuantity);
                                    newEvent.end = date;
                                    
                                    break;
                                }

                                case this.DurationOptions[3]:
                                {
                                    let endDate: Date = new Date(task.startDate);
                                    endDate.setFullYear(endDate.getFullYear() + Number(task.durationQuantity));
                                    newEvent.end = endDate;
                                    console.log("Yearly", endDate);
                                    break;
                                }
                                
                                default:
                                {
                                    break;
                                }
                            }
                            switch (task.repeatInterval) {
                                case "Daily":
                                {
                                    newEvent.rrule = {
                                        freq: RRule.DAILY,
                                        interval: task.repeatQuantity,
                                    };
                                    console.log("Daily", newEvent.rrule);
                                    break;
                                }

                                case "Weekly":
                                {
                                    let newWeeklyMonthlyDays = [];
                                    if (null != task.monthlyWeekDays) {
                                        for (let day of task.monthlyWeekDays) {
                                            newWeeklyMonthlyDays.push(day - 1);
                                        }
                                        newEvent.rrule = {
                                            freq: RRule.WEEKLY,
                                            interval: task.repeatQuantity,
                                            byweekday: newWeeklyMonthlyDays
                                        };
                                    }
                                    console.log("Weekly", newEvent.rrule);
                                    break;
                                }

                                case "Monthly":
                                {
                                    
                                    if (task.monthlyConvention === 'Day of month') {
                                        let newMonthlyDays = [];
                                        for (let day of task.monthlyDays) {
                                            newMonthlyDays.push(day + 1);
                                        }
                                        newEvent.rrule = {
                                            freq: RRule.MONTHLY,
                                            interval: task.repeatQuantity,
                                            bymonthday: newMonthlyDays
                                        };
                                    } else {
                                        let weekCount = 1;
                                        let dayCount = 1;
                                        newEvent.rrule = {
                                            freq: RRule.MONTHLY,
                                            interval: task.repeatQuantity,
                                            byweekday: []
                                        };
                                        if (null != task.monthlyNWeekDays) {
                                            for (let week of task.monthlyNWeekDays) {
                                                if (null != week) {
                                                    for (let day of week) {
                                                        newEvent.rrule.byweekday.push(RRule[this.WeekDays[day]].nth(weekCount));
                                                    }
                                                }
                                                weekCount = weekCount + 1;
                                                dayCount = 1;
                                            }
                                        }
                                        
                                    }
                                    console.log("Monthly", newEvent.rrule);
                                    break;
                                }

                                case "Yearly":
                                {
                                    let newMonthlyDays = [];
                                    for (let day of task.monthlyDays) {
                                        newMonthlyDays.push(day + 1);
                                    }
                                    newEvent.rrule = {
                                        freq: RRule.YEARLY,
                                        interval: task.repeatQuantity,
                                        bymonthday: newMonthlyDays,
                                        bymonth: new Date(task.startDate).getMonth()
                                    };
                                    console.log("Yearly", newEvent.rrule);
                                    break;
                                }
                                
                                default:
                                {
                                    break;
                                }
                            }
                            this.recurringEvents.push(newEvent);
                        });
                        await eventAction;
                        setTimeout(() => {
                            this.updateCalendarEvents();
                        }, 700);
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
        },
        (error: any) => {
            console.log("Error - " + error);
        });

        this._userStateSubscription = this.UserState$
        .subscribe((state: IUserState) => {
            if (null != state) {
                switch (state.type) {


                    default:
                    {
                        break;
                    }
                }
            }
        });
    }

    public Unitialize(): void {
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }

        if (null != this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
    }

    public dayClicked({ date, allEvents }: { date: Date; allEvents: RecurringEvent[] }): void {
        if (this.isNewTask) {
            if (isSameMonth(date, this.viewDate)) {
                if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true)) {
                    console.log("isSame", date);
                    this.RecurringTask(null, date);
                    this.activeDayIsOpen = false;
                } else {
                    this.activeDayIsOpen = true;
                    this.viewDate = date;
                }
            }
        } else {
            this.viewDate = date;
        }
    }

    public RecurringTask(task: ITask = null, date: Date = null): void {
        
    }

    public eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        // this.handleEvent("Dropped or resized", event);
        this.refresh.next();
    }

    public handleEvent(action: string, event: RecurringEvent): void {
        // window.location.href = event.meta;
        // this.modalData = { event, action };
        // this.modal.open(this.modalContent, { size: 'lg' });
    }

    public addEvent(): void {
        this.allEvents.push({
            title: "New event",
            start: startOfDay(new Date()),
            end: endOfDay(new Date()),
            color: colors.red,
            draggable: true,
            resizable: {
                beforeStart: true,
                afterEnd: true
            }
        });
        this.refresh.next();
    }

    public ToggleView(): void {
        this.ListView = !this.ListView;
    }

    handleTaskDetailResponse(response: any): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
  
                break;
            }

            default:
            {
                break;
            }
        }
    }



    public updateCalendarEvents($event: BeforeCalendarLoadMonth | BeforeCalendarLoadWeek = null): void {
        if ((null != this.TaskToggle) && (this.TaskToggle === "calendar")) {
            console.log("updateCalendarEvents", $event);

            this.allEvents = [];
            const startOfPeriod: any = {
                month: startOfMonth,
                week: startOfWeek,
                day: startOfDay
            };

            const endOfPeriod: any = {
                month: endOfMonth,
                week: endOfWeek,
                day: endOfDay
            };
            let date = this.viewDate;
            let endDate = this.viewDate;
            for (let event of this.recurringEvents) {
                if (null != event.end) {
                    endDate = event.end;
                }
                const rule: RRule = new RRule(
                    Object.assign({}, event.rrule, {
                        dtstart: startOfPeriod[this.view](date),
                        until: endOfPeriod[this.view](endDate)
                    })
                    );
                let startHours: number = Number(event.task.startTime.split(":", 2)[0]);
                let endHours: number = Number(event.task.endTime.split(":", 2)[0]);
                for (let date of rule.all()) {
                    this.allEvents.push(
                        Object.assign({}, event, {
                            start: new Date(date.getFullYear(), date.getMonth(), date.getDate(), startHours),
                            end: new Date(date.getFullYear(), date.getMonth(), date.getDate(), endHours)
                        })
                        );
                    // console.log("this.allEvents", rule, this.allEvents);
                }
            };
            this.cdr.detectChanges();
        }
    }

    private _getMultiplier(task: ITask): void {

    }
}