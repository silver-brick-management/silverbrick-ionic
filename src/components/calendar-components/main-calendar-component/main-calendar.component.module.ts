// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarHeaderComponentModule } from '../calendar-header.component.module';
import { MainCalendarComponent } from './main-calendar.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CalendarModule } from 'angular-calendar';

@NgModule({
    declarations: [
        MainCalendarComponent,
    ],
    imports: [
        CalendarModule,
        CalendarHeaderComponentModule,
        IonicPageModule.forChild(MainCalendarComponent),
    ],
    exports: [
        MainCalendarComponent
    ]
})
export class MainCalendarComponentModule {}
