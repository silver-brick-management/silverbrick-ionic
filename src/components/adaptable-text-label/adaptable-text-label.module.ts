// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdaptableTextLabelComponent } from './adaptable-text-label';
import { ElasticModule } from 'ng-elastic';

@NgModule({
  declarations: [
    AdaptableTextLabelComponent,
  ],
  imports: [
    IonicPageModule.forChild(AdaptableTextLabelComponent),
    ElasticModule
  ],
  exports: [
    AdaptableTextLabelComponent
  ]
})
export class AdaptableTextLabelComponentModule {}
