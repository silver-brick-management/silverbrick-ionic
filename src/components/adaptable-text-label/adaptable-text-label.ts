// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'adaptable-text-label',
  templateUrl: 'adaptable-text-label.html'
})
export class AdaptableTextLabelComponent {
    private _value: string;

    @Input() public IsEditMode: boolean;
    @Input() public Header: string;
    @Input() public PlaceHolder: string;
    @Input() public KeyboardType: string;
    @Input() get Value(): string {
        return this._value;
    }

    set Value(val: string) {
        if (null != val) {
            this._value = val;

            // Trigger that this value is updated.
            this.ValueChange.emit(this.Value);
        }
        else {
            // Undefined is an invalid in this accessor,
            // set to null
            val = null;
        }
    }

    @Input() public InvalidText: string;
    @Input() public ShouldShowInvalidText: boolean;

    @Output() public ValueChange: EventEmitter<string> = new EventEmitter<string>();
    @Input() public IconName: string;

    @Input() public MaxInputLength: number;
    @Input() public UseTextArea: boolean;

    constructor() {
        console.log('Hello AdaptableTextLabelComponent Component');
    }

    ionViewWillEnter(): void {
        this._value = null;
    }

    ionViewDidLeave(): void {
        this._value = null;
    }

    get CharactersLeft(): number {
        if (null != this.MaxInputLength) {
            let toSubtract: number = (null != this.Value) ? this.Value.length : 0 /* Subtract no characters */;

            return this.MaxInputLength - toSubtract;
        }
        else {
            return 0;
        }
    }

}
