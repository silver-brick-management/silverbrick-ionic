// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageControlComponent } from './message-control';
import { LetterAvatarModule } from '../letter-avatar/letter-avatar.module'

@NgModule({
    declarations: [
        MessageControlComponent,
    ],
    imports: [
        LetterAvatarModule,
        IonicPageModule.forChild(MessageControlComponent),
    ],
    exports: [
        MessageControlComponent
    ]
})
export class MessageControlModule {}
