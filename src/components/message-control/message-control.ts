// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from "@ngrx/store";
import { Subscription, Observable } from "rxjs/Rx";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { Events, Content, AlertController } from "ionic-angular";
import { Component, Input, OnInit, OnDestroy } from "@angular/core";

// Local modules
import { AppState } from "../../app/app.state";
import { IError } from "../../interfaces/IError";
import { MessageStateType } from "../../interfaces/IStoreState";
import { MessageActions } from "../../store/actions/MessageActions";
import { IMessage } from "../../shared/SilverBrickTypes";
import { Constants } from "../../common/Constants";
import { Controls } from "../../providers/Controls";
import { SessionData } from "../../providers/SessionData";

@Component({
    selector: "message-control",
    templateUrl: "message-control.html"
})
export class MessageControlComponent implements OnInit, OnDestroy {
    @Input() content: Content;
    @Input() RoomID: string;

    public Error: IError;
    public IsLoading: boolean;
    public HasMoreMessages: boolean;
    public Messages$: Observable<IMessage[]>;
    public SocketReconnected: boolean = false;
    private _messageStateSubscription: Subscription;

    constructor(public events: Events, public controls: Controls, public store: Store<AppState>, public messageActions: MessageActions, public photoViewer: PhotoViewer, public alertCtrl: AlertController, public sessionData: SessionData) {
        // Init message array (other variables are init in ngOnInit)
        this.controls.SetContent(Constants.STR_LOADING);
        this.Messages$ = this.store.select(state => state.messageState.messages);
    }

    ngOnInit(): void {
        // For the swipe guesture, inform parent control the component is ready

        // Using subscription model over the observable model to detect state for scroll logic
        this._messageStateSubscription = this.store
            .select(state => state.messageState)
            .subscribe(messageState => {
                if (messageState) {
                    // Set params
                    this.Error = messageState.error;
                    this.IsLoading = messageState.loading;
                    this.HasMoreMessages = messageState.hasMoreMessages;
                    // Additional logic (scroll)
                    switch (messageState.type) {
                        case MessageStateType.LOADING: 
                        {
                            console.log("Loading");
                            // this.controls.ShowLoading();
                            this.SocketReconnected = true;
                            break;
                        }

                        case MessageStateType.JOIN_SUCCEEDED: {
                            // We should scoll to the bottom on all initial load
                            // TODO: Add logic to remove the loading UI after the scroll.
                            // So might initially have a detached loading flag that eventually
                            // is connected to the state loading flag
                            setTimeout(() => {
                                this.SocketReconnected = true;
                                // this.controls.DismissLoading();
                                // this.content.scrollToBottom(1);
                            }, 1000);
                            break;
                        }

                        case MessageStateType.ADD_MESSAGE_SUCCESS: {
                            // Only auto scroll if the user is at the bottom
                            // Do not auto scroll if the user has scroll up reading older chats
                            console.log("Proper add chat scroll logic coming soon");
                            setTimeout(() => {
                                this.SocketReconnected = true;
                                // this.controls.DismissLoading();
                                // this.content.scrollToBottom(1);
                            }, 1000);
                            break;
                        }

                        case MessageStateType.MESSAGE_STATE_FAILED: {
                            setTimeout(() => {
                                if (!this.SocketReconnected && messageState.error != null && messageState.type === MessageStateType.MESSAGE_STATE_FAILED) {
                                    // this.controls.DismissLoading();
                                    // Prep error message
                                    const title = "Please Reload Chat Room";
                                    const message = "To make sure your chat is up to date, please exit and re-enter the chat room to reload chat.";
                                    this.alertCtrl
                                        .create({
                                            title: title,
                                            subTitle: message,
                                            buttons: ["Ok"]
                                        })
                                        .present();
                                }
                            }, 2000);
                            break;
                        }

                        case MessageStateType.GET_MESSAGES_SUCCESS: 
                        {
                            setTimeout(() => {
                                // this.controls.DismissLoading();
                                if (messageState.isInitialLoad) {
                                    // this.content.scrollToBottom(1);
                                }
                            }, 2000);
                            break;
                        }

                        case MessageStateType.NONE:
                        default: 
                        {
                            setTimeout(() => {
                                // this.controls.DismissLoading();
                                if (!messageState.isInitialLoad) {
                                    // this.content.scrollToBottom(1);
                                }
                            }, 2000);
                            break;
                        }
                    }
                } else {
                    // Chat state is undefined for some reason
                }
            });
        // Clean up the chat state before joining a room
        this.store.dispatch(this.messageActions.LeaveMessageRoom());
        this.store.dispatch(this.messageActions.JoinMessageRoom(this.RoomID));
        // Set up socket reconnect listener
        this._reconnectReJoin();
    }

    ngOnDestroy(): void {
        console.log("On Destroy MessageControl Component");
        this.store.dispatch(this.messageActions.LeaveMessageRoom());
        if (this._messageStateSubscription != null) {
            this._messageStateSubscription.unsubscribe();
            this._messageStateSubscription = null;
        }
        this.SocketReconnected = false;
        this.events.unsubscribe(Constants.EVENT_SOCKET_DISCONNECTED);
    }

    /* VIEW ACTIONS */

    public LoadMoreMessages(infiniteScroll: any): void {
        console.log("Begin load more messages operation");
        if (null != this.RoomID) {
            // Get current cursor and dispatch get history action
            this.store
                .select(state => state.messageState.cursor)
                .first()
                .subscribe((cursor: string) => {
                    if (cursor) {
                        this.store.dispatch(this.messageActions.GetMessageHistory(this.RoomID, cursor));
                        infiniteScroll.complete();
                    } else {
                        infiniteScroll.complete();
                        // Cursor doesnt exist, probably due to no more messages
                    }
                });
        } else {
            infiniteScroll.complete();
        }
    }

    public OpenFullImage(imageUrl: string): void {
        if (imageUrl) {
            this.photoViewer.show(imageUrl);
        } else {
            // Image url isnt passed in for some reason
        }
    }

    public OpenLink(txt: string): void {
        if (this.SocketReconnected) {
            window.location.href = txt;
        } else {
            this.controls.ShowAlert("Reconnecting...", "One moment while we connect your SilverBrick experience", "Thanks");
        }
    }

    public IsLink(txt: string): boolean {
        return txt.includes("https") || txt.includes("http");
    }

    public IsAuthor(author: string): boolean {
        return author === this.sessionData.userID;
    }

    private _reconnectReJoin(): void {
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (socketConnected: boolean) => {
            this.SocketReconnected = socketConnected;
            if (socketConnected) {
                // Clean up the chat state before joining a room
                this.store.dispatch(this.messageActions.LeaveMessageRoom());
                this.store.dispatch(this.messageActions.JoinMessageRoom(this.RoomID));
                this.SocketReconnected = true;
            } else {
                // Socket isnt connected, we wait...
            }
        });
    }
}

