// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Component, Optional, Input } from '@angular/core';

// Local modules
import { Constants } from '../../common/Constants';

@Component({
    selector: "loading-component",
    templateUrl: './loading.component.html',

})
export class LoadingComponent {
    @Input() ShowLoading: boolean;
    constructor() {

    }
}
