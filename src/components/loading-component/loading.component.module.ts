// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoadingComponent } from './loading.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
    LoadingComponent,
  ],
  imports: [
    IonicPageModule.forChild(LoadingComponent),
  ],
  exports: [
    LoadingComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  })
export class LoadingComponentModule {}
