// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrawerComponent } from './drawer';

@NgModule({
  declarations: [
    DrawerComponent,
  ],
  imports: [
    IonicPageModule.forChild(DrawerComponent),
  ],
  exports: [
    DrawerComponent
  ]
})
export class DrawerModule {}
