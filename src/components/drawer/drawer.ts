// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Nav, AlertController } from "ionic-angular";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { Component, Input } from "@angular/core";
import { Events } from "ionic-angular";

// Local modules
import { AppState } from "../../app/app.state";
import { IPage } from "../../shared/IParams";
import { Constants } from "../../common/Constants";

import { AuthActions } from "../../store/actions/AuthActions";
import { ISessionState, IAuthState } from "../../interfaces/IStoreState";
import { SessionData } from "../../providers/SessionData";

import { IUser } from "../../shared/SilverBrickTypes";
import { UserProfileMode, IUserProfileNavigationArgs } from "../../interfaces/IUserProfileNavigationArgs";

@Component({
    selector: "drawer",
    templateUrl: "drawer.html",
})
export class DrawerComponent {
    @Input() nav: Nav;

    public profile$: Observable<IAuthState>;
    public User: IUser = null;

    public SessionState$: Observable<ISessionState>;
    public DefaultProfile: string = Constants.DEFAULT_PROFILE_PHOTO_PATH;
    private _isTenant: boolean = false;
    private _hasNonUserDirectoryAccess: boolean = false;
    private _sessionStateSubscription: Subscription = null;
    private _profilePic: string = null;
    private _profileSubscription: Subscription = null;
    public Following$: Observable<number>;
    public Followers$: Observable<number>;

    constructor(public store: Store<AppState>, public authActions: AuthActions, public events: Events, public alertCtrl: AlertController, public sessionData: SessionData) {
        this.profile$ = this.store.select((state) => state.authState);
        this.SessionState$ = this.store.select((state) => state.sessionState);
    }

    get ProfilePic(): string {
        return null != this._profilePic ? this._profilePic : Constants.DEFAULT_PROFILE_PHOTO_PATH;
    }

    pages: IPage[] = [
        {
            title: "Profile",
            icon: "assets/img/Profile.png",
            pushOnStack: true,
            profile: true,
            args: this.User,
        },
    ];

    NavigateToCurrentUserProfile(): void {
        this.profile$.first().subscribe((state: IAuthState) => {
            if (null != state) {
                this.nav.push("ProfilePage", <IUserProfileNavigationArgs>{
                    UserID: state.profile.uid,
                    Mode: UserProfileMode.EDIT,
                });
            }
        });
    }

    GoToFeedback(): void {
        this.events.publish(Constants.EVENT_HIDE_DRAWER, true);
        this.nav.push("FeedbackPage");
    }

    GoToSettings(): void {
        this.events.publish(Constants.EVENT_HIDE_DRAWER, true);
        this.nav.push("SettingsPage");
    }

    IsActive(page: IPage): string {
        let childNav = this.nav.getActiveChildNavs();

        // Tabs are a special case because they have their own navigation
        if (childNav[0]) {
            if (childNav[0].getSelected() && null != page.tabComponent && 0 <= page.tabComponent.indexOf(childNav[0].getSelected().root)) {
                return Constants.NAV_PRIMARY_MARK;
            }
            return;
        }

        if (this.nav.getActive() && this.nav.getActive().component === page.component) {
            return Constants.NAV_PRIMARY_MARK;
        }

        return;
    }

    ngOnInit(): void {
        // Subscribe to signedInUser's info
        this._sessionStateSubscription = this.SessionState$.subscribe((state: ISessionState) => {
            if (null != state) {
                this._isTenant = state.user.role === "tenant";
            }
        });

        this.profile$.first().subscribe((state: IAuthState) => {
            if (null != state) {
                if (null != state.profile) {
                    this.User = state.profile;
                    this._profilePic = state.profile.photoURL;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.events.publish(Constants.EVENT_HIDE_DRAWER, false);
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        } else {
            // Nothing to do
        }

        if (null != this._profileSubscription) {
            this._profileSubscription.unsubscribe();
            this._profileSubscription = null;
        }
    }

    get ShouldShowCreateHeader(): boolean {
        return this._isTenant;
    }

    OpenPage(page: IPage): void {
        if (page.pushOnStack && !page.profile) {
            // not profile tab
            this.events.publish(Constants.EVENT_HIDE_TABS, false);
            // this.events.publish(Constants.EVENT_VIEWING_DASH_PROFILE, false);
            this.nav.push(page.component, page.args);
        } else if (page.pushOnStack && page.profile) {
            // is profile tab
            this.events.publish(Constants.EVENT_HIDE_TABS, false);
            this.events.publish(Constants.EVENT_VIEWING_DASH_PROFILE, false);
            this.NavigateToCurrentUserProfile();
        } else {
            // No action specified, so we just do the
            // page navigation
            this.openPageHelper(page);
        }
    }

    private LogOut(): void {
        let newAlert = this.alertCtrl.create({
            title: "Are you sure you want to Log Out?",
            message: "Please make sure you do.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: () => {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: "Log Out",
                    handler: () => {
                        // Conduct the logout action, (it auto navigates)
                        this.events.publish(Constants.EVENT_LOGOUT);
                        this.nav.setRoot("EntryPage");
                        this.store.dispatch(this.authActions.Logout());
                    },
                },
            ],
        });
        newAlert.present();
    }

    private openPageHelper(page: IPage): void {
        // the nav component was found using @ViewChild(Nav)
        // reset the nav to remove previous pages and only have this page
        // we wouldn't want the back button to show in this scenario
        if (page.index) {
            this.nav.setRoot(page.component, { tabIndex: page.index }).catch(() => {
                console.log("Didn't set nav root");
            });
        } else {
            this.nav.setRoot(page.component).catch((error) => {
                console.log("Didn't set nav root");
            });
        }
    }
}

