// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

// Local modules
import { Constants } from '../common/Constants';
import { ErrorMapper } from '../common/ErrorMapper';
import { IUser } from '../shared/SilverBrickTypes';

@Injectable()
export class StorageHelper {

    constructor(private _storage: Storage) {

    }

    async StoreAuth(token: string, profile: IUser): Promise<void> {
        try {
            await this._storage.ready();
            await localStorage.setItem(Constants.AUTH_TOKEN_STORAGE_NAME, token);
            await localStorage.setItem(Constants.AUTH_PROFILE_STORAGE_NAME, JSON.stringify(profile));
        }
        catch (error) {
            throw ErrorMapper.GetErrorFromStorageError(error);
        }
    }

    async ClearAuth(): Promise<void> {
        try {
            await this._storage.ready();
            await this._storage.remove(Constants.AUTH_TOKEN_STORAGE_NAME);
            await this._storage.remove(Constants.AUTH_PROFILE_STORAGE_NAME);
        }
        catch (error) {
            throw ErrorMapper.GetErrorFromStorageError(error);
        }
    }

    StoreStripeAuth(token: string): void {
        localStorage.setItem("stripe:id_token", token);
    }

    ClearStripeAuth(): void {
        localStorage.removeItem("stripe:id_token");
    }

    async StoreAccessTokenAuth(token: string): Promise<void> {
        try {
            await this._storage.ready();
            await localStorage.setItem(Constants.AUTH_TOKEN_STORAGE_NAME, token);
        }
        catch (error) {
            throw ErrorMapper.GetErrorFromStorageError(error);
        }
    }

    async GetAccessTokenAuth(): Promise<string> {
        try {
            await this._storage.ready();
            return <string>await localStorage.getItem(Constants.AUTH_TOKEN_STORAGE_NAME);
        }
        catch (error) {
            throw ErrorMapper.GetErrorFromStorageError(error);
        }
    }

    StoreAccessToken(token: string): void {
        localStorage.setItem(Constants.AUTH_TOKEN_STORAGE_NAME, token);
    }

    GetAccessToken(): string {
        return localStorage.getItem(Constants.AUTH_TOKEN_STORAGE_NAME) as string;
    }

    StoreStripeAccessToken(token: string): void {
        localStorage.setItem("stripe:id_token", token);
    }

    GetAccessStripeToken(): string {
        return localStorage.getItem("stripe:id_token") as string;
    }

    async GetValue<T>(key: string): Promise<any> {
        await this._storage.ready();
        return await localStorage.getItem(key);
    }

    async SetValue<T>(key: string, value: any): Promise<void> {
        await this._storage.ready();
        return await localStorage.setItem(key, value);
    }
}
