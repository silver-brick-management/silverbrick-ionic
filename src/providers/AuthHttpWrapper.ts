// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/fromPromise";

import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { AuthHttp, AuthConfigConsts, tokenNotExpired } from 'angular2-jwt';
import { Http, Response, RequestOptionsArgs, RequestOptions, Headers } from '@angular/http';

// Local modules
import { AppState } from '../app/app.state';
import { StorageHelper } from './storage-helper';
import { ErrorMapper } from '../common/ErrorMapper';
import { FirebaseAuth } from '../services/FirebaseAuth';
import { AuthActions } from '../store/actions/AuthActions';
import { Urls, JSON_CONTENT_TYPE } from '../common/Constants';
import { IServerResponseRefreshToken } from '../shared/ResponseTypes';

interface TokenRefreshSubject {
    IsRefreshing: boolean;
    RefreshResult: boolean;
}

@Injectable()
export class AuthHttpWrapper {
    private _isRefreshing: boolean;
    private _tokenIsBeingRefreshed: Subject<TokenRefreshSubject>;

    constructor(
        private _http: Http,
        private _authHttp: AuthHttp,
        private _store: Store<AppState>,
        private _authActions: AuthActions,
        private _firebaseAuth: FirebaseAuth,
        private _storageHelper: StorageHelper) {

        this._isRefreshing = false;
        this._tokenIsBeingRefreshed = new Subject<TokenRefreshSubject>();
        this._tokenIsBeingRefreshed.next(
            <TokenRefreshSubject>{
                IsRefreshing: false,
                RefreshResult: null
            });
    }

    GetInvite(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.authGet(url, options);
    }

    Get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authGet(url, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authGet(url, options);
            }
        });
    }

    Post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authPost(url, body, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authPost(url, body, options);
            }
        });
    }

    Put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authPut(url, body, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authPut(url, body, options);
            }
        });
    }

    Delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authDelete(url, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authDelete(url, options);
            }
        });
    }

    Patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authPatch(url, body, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authPatch(url, body, options);
            }
        });
    }

    Head(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authHead(url, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authHead(url, options);
            }
        });
    }

    Options(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.TokenRequiresRefresh()
        .switchMap((tokenRequiresRefresh) => {
            if (tokenRequiresRefresh) {
                return this.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    return Observable.fromPromise(this.RefreshTokenSuccessHandler(refreshTokenResponse))
                    .switchMap((isRefreshSuccessful) => {
                        if (isRefreshSuccessful) {
                            return this.authOptions(url, options);
                        } else {
                            // TODO: Implement sessiontimeout page
                            // this.router.navigate(['/sessiontimeout']);
                            return Observable.throw(refreshTokenResponse);
                        }
                    })
                    .catch((error) => {
                        this.RefreshTokenErrorHandler(error);
                        return Observable.throw(error);
                    });
                })
                .catch((error) => {
                    this.RefreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
            } else {
                return this.authOptions(url, options);
            }
        });
    }

    RefreshToken(): Observable<IServerResponseRefreshToken | boolean> {
        if (this._isRefreshing) {
            // If a refresh is happening, create an
            // observer as a queue for the request
            return new Observable((observer) => {
                this._tokenIsBeingRefreshed.subscribe((refreshSubject) => {
                    if (!refreshSubject.IsRefreshing) {
                        observer.next(refreshSubject.RefreshResult);
                        observer.complete();
                    } else {
                        // Refresh still happening,
                        // wait till its done
                    }
                });
            });
        } else {
            // Refresh has not been initiated,
            // start the refresh process
            return this.refreshToken();
        }
    }

    TokenRequiresRefresh(): Observable<boolean> {
        return Observable.fromPromise(this._storageHelper.GetAccessTokenAuth())
        .map((token: string) => {
            return !tokenNotExpired(null, token);
        });
    }

    async RefreshTokenSuccessHandler(refreshTokenResponse: IServerResponseRefreshToken | boolean): Promise<boolean> {
        let isSuccessful = false;

        if (typeof refreshTokenResponse === 'boolean') {
            // refresh token is coming from a queue, result
            // of the first request to perform the refresh
            // is being passed in directly. In this case,
            // we should just return it.
            isSuccessful = <boolean>refreshTokenResponse;
        } else {
            if (refreshTokenResponse.success && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                await this._storageHelper.StoreAccessToken(refreshTokenResponse.accessToken);
                this._store.dispatch(this._authActions.UpdateJwtToken(refreshTokenResponse.accessToken));
                isSuccessful = true;
            } else {
                await this._storageHelper.ClearAuth();
                // TODO: Implement ionic page navigation
                // this._router.navigate(['/login']);
            }
        }
        this.setIsRefreshing(false, isSuccessful);
        return isSuccessful;
    }

    async RefreshTokenErrorHandler(error: any): Promise<void> {
        await this._storageHelper.ClearAuth();
        this.setIsRefreshing(false, false);
        // TODO: Implement sessiontimeout page
        // this._router.navigate(['/sessiontimeout']);
    }

    private refreshToken(): Observable<IServerResponseRefreshToken> {
        this.setIsRefreshing(true, null);
        return Observable.fromPromise(this._firebaseAuth.GetTokenPromise())
        .switchMap((idToken: string) => {
            const url = Urls.API_REFRESH_V1;
            const body = { id_token: idToken };
            return Observable.fromPromise(this._storageHelper.GetAccessTokenAuth())
            .switchMap((token) => {
                const requestHeader = new Headers(JSON_CONTENT_TYPE);
                requestHeader.set(AuthConfigConsts.DEFAULT_HEADER_NAME, AuthConfigConsts.HEADER_PREFIX_BEARER + token);
                const options = new RequestOptions({ headers: requestHeader });

                return this._http.post(url, body, options)
                .map((response: Response) => response.json())
                .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));

            });
        });
    }

    private setIsRefreshing(isRefreshingStatus: boolean, refreshResult: boolean): void {
        this._isRefreshing = isRefreshingStatus;
        this._tokenIsBeingRefreshed.next(
            <TokenRefreshSubject>{
                RefreshResult: refreshResult,
                IsRefreshing: isRefreshingStatus
            });
    }

    private authGet(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.get(url, options);
    }

    private authPost(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.post(url, body, options);
    }

    private authPut(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.put(url, body, options);
    }

    private authDelete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.delete(url, options);
    }

    private authPatch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.patch(url, body, options);
    }

    private authHead(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.head(url, options);
    }

    private authOptions(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.options(url, options);
    }
}
