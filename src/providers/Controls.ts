// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable } from '@angular/core';
import {
    Loading,
    LoadingController,
    Alert,
    AlertController,
    Platform
} from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

// Local modules
import { Constants } from '../common/Constants';
import { IInAppToastData } from '../shared/SilverBrickTypes';

@Injectable()
export class Controls {

    public LoadingScreen: Loading;
    public loadingTimeoutHandler: number;
    public Alert: Alert;

    constructor(
        public loadingCtrl: LoadingController,
        public toast: Toast,
        public platform: Platform,
        public alertCtrl: AlertController) {
        this.LoadingScreen = null;
        this.Alert = null;
    }

    public ShowLoading(): void {
        // Dismiss any current loading control first
        this.DismissLoading();

        // Dont Show the loading UI for anything that takes less than
        // 400ms (current studies shows perceived performance seems to be
        // at that limit)
        this.loadingTimeoutHandler = window.setTimeout(() => {
            this._clearLoadingTimeout();
            // Create new loading controll
            this.LoadingScreen = this.loadingCtrl.create({
                content: Constants.STR_LOADING
            });
            this.LoadingScreen.present();
        }, 400);
    }

    public DismissLoading(): void {
        // Clear timeout if one exists
        this._clearLoadingTimeout();
        // Dismiss loading UI if one exists
        if (null != this.LoadingScreen) {
            this.LoadingScreen.dismiss();
            this.LoadingScreen = null;
        }
        else {
            // Nothing to do
        }
    }

    public SetContent(content: string): void {
        if (null != this.LoadingScreen) {
            this.LoadingScreen.setContent(content);
        }
        else {
            // Nothing to do
        }
    }

    public ShowToast(message: string, position: string): void {
        if (this.platform.is('cordova')) {
            this.toast.showWithOptions(
            {
                message: message,
                position: position,
                styling: {
                    backgroundColor: '#efefef',
                    textColor: '#4285f4',
                    opacity: 0.9,
                    horizontalPadding: 20
                }
            }).subscribe(
            (toastData: IInAppToastData) => {
                if (null != toastData) {
                }
                else {

                }
            }
            );
        }
    }

    public HideToast(): void {
        if (this.platform.is('cordova')) {
            this.toast.hide();
        }
    }
  /**
   * Show an alert controller with single button
   *
   * @param  {string} title title for alert
   * @param  {string} message message body inside of alert
   * @param  {string} button button text for alert
   */
   public ShowAlert(title: string, message: string, button: string) {
       this.Alert = this.alertCtrl.create({
           title: title,
           message: message,
           buttons: [
           {
               text: button,
               role: 'cancel',
               handler: () => {
                   this.Alert = null;
               }
           }
           ]
       });
       this.Alert.present();
   }

   private _clearLoadingTimeout(): void {
       if (this.loadingTimeoutHandler) {
           window.clearTimeout(this.loadingTimeoutHandler);
           this.loadingTimeoutHandler = null;
       }
       else {
           // Loading timeout already cleared
       }
   }
}
