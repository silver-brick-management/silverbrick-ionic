// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules
import { Geolocation, Geoposition, PositionError } from '@ionic-native/geolocation';
import { Injectable } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs/Rx';
import { IUserLocation } from '../shared/SilverBrickTypes';

@Injectable()
export class LocationProvider {
    private _locationPopupGuard: boolean = false;
    public coords: IUserLocation = null;
    public AllowShareLocation: boolean = false;
    public IsLoadingGeoLocation: boolean = false;
    constructor(
        private _geolocation: Geolocation,
        public alertCtrl: AlertController,
        public platform: Platform)
    {
    }

    public async GetGeoLocation(): Promise<IUserLocation> {
        this.IsLoadingGeoLocation = true;
        try {
            console.log("Requesting Location Authorization");

            // Once we reached this point, we should have the right permissions
            let resp: Geoposition = await this._geolocation.getCurrentPosition({
                timeout: 5000 /* 5 seconds */,
                enableHighAccuracy: true
            });
            if (null != resp.coords) {
                console.log('Geo-location set to: ' + JSON.stringify(resp));
                this.coords = {
                    latitude: resp.coords.latitude,
                    longtitude: resp.coords.longitude
                };
                return this.coords;
            }
            else {
                console.log('Geo-location, coordinates are null!');

            }
            this.IsLoadingGeoLocation = false;

        }
        catch (error) {
            let positionError: PositionError = error;
            console.error('Error getting location. Code - ' + positionError.code + ' Message - ' + positionError.message);

        }

    }

    private _promptForEnablingLocation(): void {
        if (this._locationPopupGuard) {
            console.log("A location pop-up is already in-effect, avoiding duplicate.");
            return;
        }
        else {
            // Continue
        }

        this._locationPopupGuard = true;

        const title: string = 'Share Your Location?';
        const newAlert = this.alertCtrl.create({
            title: title,
            message: "Sharing your location will help others find you better. Do you want to navigate to the app's settings to enable location access?",
            buttons: [
            {
                text: 'OK',
                role: 'ok',
                handler: () => {
                    console.log('OK clicked');
                    console.log('Navigating to Location settings');
                    this.platform.ready()
                    .then((readySource: string) => {
                        if ('cordova' === readySource) {
                            let platformSubscription: Subscription = this.platform.resume.subscribe(() => {
                                console.log("Detected resume");

                                console.log("Unsubscribing plaform subscription");
                                platformSubscription.unsubscribe();
                                platformSubscription = null;
                                console.log("Unsubscribe successful!");

                                // this._internalEnableLocationSetting();
                            });
                        }
                        else {

                        }
                    });
                    this._locationPopupGuard = false;
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                    console.log("Cancel selected");

                    this._locationPopupGuard = false;
                }
            }
            ]
        });
        newAlert.present();
    }

    private async _internalEnableLocationSetting(): Promise<void> {
    // if (await this._deviceDiagnostic.isLocationAuthorized()) {
        //     console.log("Detected a RESUME, and location is authorized! Toggling location again.");
        //     this.AllowShareLocation = true;
        // }
        // else {
            //     // Nothing to do
            //     console.log("Detected a RESUME, and location is NOT authorized. Doing nothing.");
            // }
    }
}
