import {Injectable} from '@angular/core';

@Injectable()
export class TabsService {
    constructor() {}

    public hide() {
        let tabs = <any>document.querySelectorAll('.tabbar');
        let scrollContent = <any>document.querySelectorAll('.scroll-content');
        if (tabs !== null) {
            Object.keys(tabs).map((key) => {
                tabs[key].style.transform = 'translateY(56px)';
            });

            setTimeout(() =>{
                Object.keys(scrollContent).map((key: any) => {
                    scrollContent[key].style.marginBottom = '0';
                });
            }, 50);
        }
    }

    public show() {
        let tabs = <any>document.querySelectorAll('.tabbar');
        if (tabs !== null) {
            Object.keys(tabs).map((key: any) => {
                tabs[key].style.transform = 'translateY(0px)';
            });
        }
    }
}