// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable } from "@angular/core";

// Local modules
import { Push, PushObject, PushOptions, RegistrationEventResponse, NotificationEventResponse } from "@ionic-native/push";
import { ConfigContext } from "../env/ConfigContext";
import { Subscription, Observable } from "rxjs/Rx";
import { IServerResponseBase, UserChatRoom, MessageParticipant } from "../shared/ResponseTypes";
import { Constants } from "../common/Constants";
import { Platform, Events, NavController, Modal, ModalController } from "ionic-angular";
import { Toast } from "@ionic-native/toast";
import { Vibration } from "@ionic-native/vibration";
// import { MediaObject } from "@ionic-native/media";
import { AppState } from "../app/app.state";
import { Store } from "@ngrx/store";
import { DeviceApi } from "../services/DeviceApi";
import { IDeviceInfo, ITask } from "../shared/SilverBrickTypes";
import { SessionData } from "./SessionData";
import { IMessageState, ISessionState } from "../interfaces/IStoreState";
import { MediaProvider } from "./media-provider";
import { IMessageExpandParams } from "../shared/IParams";
import { MessageActions } from "../store/actions/MessageActions";
import { MessagesPage } from "../pages/messages/messages";
import { MessageExpandPage } from "../pages/message-expand/message-expand";
import { IUserProfileNavigationArgs } from "../interfaces/IUserProfileNavigationArgs";
import { TasksDetailsPage } from "../pages/task-details/tasks-details";
import { TasksPage } from "../pages/tasks/tasks";
import { UserUtil } from "../common/UserUtil";

interface IAdditionalData {
    PostType: string;
    Timestamp: number;
    "force-start": string;
    icon: string;
    coldstart: boolean;
    foreground: boolean;
    RoomID?: string;
    RoomTitle?: string;
    PostID?: string;
    TaskID?: string;
}

interface IInAppToastData {
    event: "hide" | "touch";
    message: string;
    data: IAdditionalData;
    PushType: string;
}

interface IPlatformNotificationResponse extends NotificationEventResponse {
    sound: string;
    additionalData: IAdditionalData;
}

@Injectable()
export class PushService {
    private _isInitialized: boolean = false;
    private _notificationSubscription: Subscription = null;
    private _registrationSubscription: Subscription = null;
    private _errorSubscription: Subscription = null;
    private _toastSubscription: Subscription = null;
    private _lastPushTimestamp: number = null;
    private _navController: NavController = null;
    private _deviceInfo: IDeviceInfo = null;
    private _messageStateSubscription: Subscription = null;
    public messageState$: Observable<IMessageState>;
    public sessionState$: Observable<ISessionState>;

    constructor(
        private push: Push,
        private platform: Platform,
        private _toast: Toast,
        private _events: Events,
        private _vibrate: Vibration,
        public sessionData: SessionData,
        public messageActions: MessageActions,
        public modalCtrl: ModalController,
        public deviceApi: DeviceApi,
        public mediaProvider: MediaProvider,
        private store: Store<AppState>) {
        this.messageState$ = this.store.select((state) => state.messageState);
        this.sessionState$ = this.store.select((state) => state.sessionState);
    }

    private findChatView(): MessageExpandPage {
        if (null == this._navController) {
            return null;
        } else {
            // Nav controller is valid
        }

        for (let view of this._navController.getViews()) {
            if (view.component === MessageExpandPage) {
                return view.instance;
            } else {
                // Continue searching
            }
        }

        // Did not find a view
        return null;
    }

    private findTaskView(): TasksDetailsPage {
        if (null == this._navController) {
            return null;
        } else {
            // Nav controller is valid
        }

        for (let view of this._navController.getViews()) {
            if (view.component === TasksDetailsPage) {
                return view.instance;
            } else {
                // Continue searching
            }
        }

        // Did not find a view
        return null;
    }

    private _findTask(taskID: string): Promise<ITask> {
        return new Promise<ITask>((resolve, reject) => {
            if (!taskID) {
                resolve(null);
            } else {
                // warnableID is a non-empty string, continue
            }

            let _task: ITask = null;
            let isResolved: boolean = false;
            let subscription: Subscription = this.sessionState$
                .timeout(5000)
                .take(5)
                .subscribe(
                    (sessionState: ISessionState) => {
                        _task = UserUtil.FindTask(sessionState, taskID);
                        if (null == _task) {
                        } else {
                            if (null != subscription) {
                                subscription.unsubscribe();
                                subscription = null;
                            } else {
                            }

                            isResolved = true;

                            resolve(_task);
                        }
                    },
                    (error) => {
                        if (null != subscription) {
                            subscription.unsubscribe();
                            subscription = null;
                        } else {
                        }

                        if (!isResolved) {
                            isResolved = true;

                            resolve(null /* Returning null safely */);
                        } else {
                        }
                    },
                    (/* onComplete */) => {
                        if (null != subscription) {
                            subscription.unsubscribe();
                            subscription = null;
                        } else {
                        }

                        if (!resolve) {
                            isResolved = true;

                            resolve(null);
                        } else {
                        }
                    }
                );
        });
    }

    private _handleNotificationType(toastData: IAdditionalData): void {
        if (!toastData.PostType) {
        } else {
            // Toast data type is valid, continue.
        }

        console.log("_handleNotificationType", toastData);
        switch (toastData.PostType) {
            case Constants.TYPE_MESSAGE_PAYLOAD_V1: {
                if (null != this._navController) {
                    this._navController.popToRoot();
                } else {
                }
                const modal: Modal = this.modalCtrl.create("MessageExpandPage", <IMessageExpandParams>{
                    participants: [],
                    roomID: toastData.RoomID,
                    isNewChat: false,
                    title: "",
                });
                modal.present();
                break;
            }

            default: {
                break;
            }

            // case Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1:
            // case Constants.TYPE_SERVICE_NEW_PAYLOAD_V1:
            // case Constants.TYPE_SERVICE_PROGRESS_PAYLOAD_V1:
            // case Constants.TYPE_SERVICE_COMPLETE_PAYLOAD_V1:
            // {
            //     if (null != this._navController) {
            //         this._navController.popToRoot();
            //     }
            //     else {
            //     }

            //     const modal: Modal = this.modalCtrl.create("TasksDetailsPage",
            //          { task: task, isEditMode: false });
            //     modal.present();
            //     break;
            // }
        }
    }

    private async _handleNotification(notificationResponse: IPlatformNotificationResponse): Promise<void> {
        if (null == notificationResponse || null == notificationResponse.additionalData) {
            return;
        }

        if (null != this._lastPushTimestamp && notificationResponse.additionalData.Timestamp === this._lastPushTimestamp) {
            return;
        }

        this._lastPushTimestamp = notificationResponse.additionalData.Timestamp;

        let pushResponseData: IAdditionalData = notificationResponse.additionalData;
        if (null == pushResponseData) {
            return;
        } else {
            // We're okay, continue
        }
        console.log("Handle Notification", notificationResponse);
        // // let foundTask: ITask = await this._findTask(pushResponseData.TaskID);
        // if (null == foundTask) {
        //     return;
        // }
        // else {
        // }
        if (notificationResponse.additionalData.coldstart || !notificationResponse.additionalData.foreground) {
            this._handleNotificationType(pushResponseData);
        } else {
            const toastMessage: string = notificationResponse.title + ": " + notificationResponse.message;

            if (null != this._toastSubscription) {
                this._toastSubscription.unsubscribe();
                this._toastSubscription = null;
            } else {
                // Already null, continue
            }

            this._vibrate.vibrate(500);

            let _duration = 4000; /* Default to 4 seconds */
            let _backgroundColor = "#4285f4";
            let mediaObject: any = null;

            switch (pushResponseData.PostType) {
                case Constants.TYPE_MESSAGE_PAYLOAD_V1: {
                    let chatPage: MessageExpandPage = this.findChatView();
                    if (null != chatPage && null != chatPage.RoomID) {
                        if (chatPage.RoomID === pushResponseData.RoomID) {
                            if (this._navController.getActive().component === MessageExpandPage) {
                                console.log("The Chat Page view is on top, with a matching Warnable ID, don't show the toast! Warnable ID - " + pushResponseData.RoomID);
                                return;
                            } else {
                                console.log("The ChatPage is not in the foreground, but is around...");
                            }
                        } else {
                            // There isn't a match, continue to show toast
                        }
                    } else {
                        // There isn't a match, continue on to show toast
                    }
                    break;
                }

                // case Constants.TYPE_SERVICE_NEW_PAYLOAD_V1:
                // case Constants.TYPE_SERVICE_ASSIGN_PAYLOAD_V1:
                // case Constants.TYPE_SERVICE_NEW_PAYLOAD_V1:
                // case Constants.TYPE_SERVICE_COMPLETE_PAYLOAD_V1:
                // {
                //     let taskPage: TasksDetailsPage = this.findTaskView();
                //     if ((null != taskPage) &&
                //         (null != taskPage.task)) {
                //         if (taskPage.task.id === pushResponseData.TaskID) {
                //             if (this._navController.getActive().component === TasksDetailsPage) {
                //                 console.log("The Chat Page view is on top, with a matching Warnable ID, don't show the toast! Warnable ID - " + pushResponseData.RoomID);
                //                 return;
                //             }
                //             else {
                //                  console.log("The ChatPage is not in the foreground, but is around...");
                //             }
                //         }
                //         else {
                //             // There isn't a match, continue to show toast
                //         }
                //     }
                //     else {
                //         // There isn't a match, continue on to show toast
                //     }
                //     break;
                // }

                default: {
                    break;
                }
            }
            if (this.platform.is("cordova")) {
                const toastMessage: string = notificationResponse.title + ": " + notificationResponse.message;

                if (null != this._toastSubscription) {
                    this._toastSubscription.unsubscribe();
                    this._toastSubscription = null;
                } else {
                    // Already null, continue
                }

                this._vibrate.vibrate(500);

                let _duration = 4000; /* Default to 4 seconds */
                let _backgroundColor = "#4285f4";
                let mediaObject: any = null;
                let pushResponseData: IAdditionalData = notificationResponse.additionalData;
                console.log("additionalData", pushResponseData);
                this._toastSubscription = this._toast
                    .showWithOptions({
                        message: toastMessage,
                        duration: _duration,
                        position: "top",
                        data: pushResponseData,
                        styling: {
                            backgroundColor: _backgroundColor,
                            textColor: "#fff",
                        },
                    })
                    .subscribe((toastData: IInAppToastData) => {
                        if (null != toastData) {
                            switch (toastData.event) {
                                case "hide": {
                                    if (null != mediaObject) {
                                        mediaObject.stop();
                                    }
                                    break;
                                }

                                case "touch": {
                                    if (null != mediaObject) {
                                        mediaObject.stop();
                                    }
                                    this._toast.hide();
                                    this._handleNotificationType(toastData.data);
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                        } else {
                        }
                    });
            }
        }
    }

    private _registerDevice(registrationInfo: RegistrationEventResponse): void {
        let osType = Constants.OS_TYPE_UNKNOWN;
        if (this.platform.is("ios")) {
            osType = Constants.OS_TYPE_IOS;
        } else if (this.platform.is("android")) {
            osType = Constants.OS_TYPE_ANDROID;
        } else {
            osType = Constants.OS_TYPE_UNKNOWN;
        }

        console.log("_registerDevice! " + osType, registrationInfo, JSON.stringify(registrationInfo));

        let deviceInfo: IDeviceInfo = {
            fcmRegToken: registrationInfo.registrationId,
            osType: osType,
        };

        this._deviceInfo = deviceInfo;

        // Purposely not waiting for the observable to complete, best effort, and non blocking
        let subscription: Subscription = this.deviceApi
            .SetFcmRegistrationToken(deviceInfo)
            .first()
            .subscribe(
                (serverResponseBase: IServerResponseBase) => {},
                (error) => {
                    // Setting to info level logging lines, because we will always have falst positives
                    // from prompting the user for permissions
                },
                () => {
                    subscription.unsubscribe();
                }
            );
    }

    UnRegisterDeviceInfo(): Observable<void> {
        return new Observable<void>((observer) => {
            if (null != this._deviceInfo) {
                // Best-effort and non-blocking
                return this.deviceApi
                    .RemoveFcmRegistrationToken(this._deviceInfo)
                    .first()
                    .subscribe(
                        (response: IServerResponseBase) => {},
                        (error) => {},
                        () => {
                            // In completion handler, single observer to complete
                            observer.next();
                            observer.complete();
                        }
                    );
            } else {
                observer.next();
                observer.complete();
            }
        });
    }

    Initialize(navController: NavController = null, isOverride: boolean = false): void {
        if (!this._isInitialized || isOverride) {
            if (isOverride) {
                // Uninit before init
                this.UnInitialize();
            } else {
                // Nothing to do
            }

            if (null != navController) {
                this._navController = navController;
            } else {
                // Nothing to do
            }

            const options: PushOptions = {
                android: {
                    senderID: ConfigContext.FirebaseMessengerSenderID,
                    sound: true,
                    vibrate: true,
                },
                ios: {
                    sound: true,
                    alert: true,
                    badge: true,
                    fcmSandbox: ConfigContext.IsUsingProductionCert,
                },
                windows: {},
                browser: {
                    pushServiceURL: "http://push.api.phonegap.com/v1/push",
                },
            };

            const pushObject: PushObject = this.push.init(options);
            this._notificationSubscription = pushObject.on("notification").subscribe((notification: any) => {
                this._handleNotification(notification);
            });

            this._registrationSubscription = pushObject.on("registration").subscribe((registration: RegistrationEventResponse) => {
                if (null != registration) {
                    console.log("Registration Callback - ", registration);
                    this._registerDevice(registration);
                } else {
                    console.log("Registration is null");
                }
            });

            this._errorSubscription = pushObject.on("error").subscribe((error) => {});

            if (!isOverride) {
                this._isInitialized = true;
            } else {
                // Nothing to do
            }
        } else {
            // Already initialized
        }
    }

    async HasPermissions(): Promise<boolean> {
        let hasPermission: {
            isEnabled: boolean;
        } = await this.push.hasPermission();

        return null != hasPermission ? hasPermission.isEnabled : false;
    }

    UnInitialize(): void {
        if (null != this._toastSubscription) {
            this._toastSubscription.unsubscribe();
            this._toastSubscription = null;
        }

        if (null != this._notificationSubscription) {
            this._notificationSubscription.unsubscribe();
            this._notificationSubscription = null;
        }

        if (null != this._errorSubscription) {
            this._errorSubscription.unsubscribe();
            this._errorSubscription = null;
        }

        if (null != this._messageStateSubscription) {
            this._messageStateSubscription.unsubscribe();
            this._messageStateSubscription = null;
        }

        if (null != this._registrationSubscription) {
            this._registrationSubscription.unsubscribe();
            this._registrationSubscription = null;
        }

        this._navController = null;

        this._isInitialized = false;
    }
}