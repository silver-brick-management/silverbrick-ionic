// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";

import { ISessionState, SessionStateType } from "../interfaces/IStoreState";
import { SessionActions } from "../store/actions/SessionActions";
import { AuthActions } from "../store/actions/AuthActions";

import { AppState } from "../app/app.state";
import { Constants, Urls } from "../common/Constants";
import { IUser, IUserOrg, IUserBuilding, IUserOrgBuildingBase } from "../shared/SilverBrickTypes";
import { UserUtil } from "../common/UserUtil";
import { Controls } from './Controls';

// The purpose of this file is to have a shared subscription to the session state that will
// allow for easy access to important properties that a user will need throughout their time in the app.
// These properties are typically static in nature.

@Injectable()
export class SessionData {
    public SessionState$: Observable<ISessionState>;
    private _sessionStateSubscription: Subscription;
    private _user: IUser;
    private _username: string;
    private _uid: string;
    private _email: string;
    private _userPosition: string;
    private _isBuildingAdmin: boolean;
    private _isOrgAdmin: boolean;
    private _orgID: string;
    private _org: IUserOrg;
    private _buildingID: string;
    private _isFirstTime: boolean;
    private _role: string;
    private _isTenant: boolean;
    private _isClockedIn: boolean;
    // public SERVER_BASE: string = 'http://localhost:3000/';
    public SERVER_BASE: string = Urls.SERVER_BASE;
    public AUTH_BASE_V1: string = `${this.SERVER_BASE}auth/v1/dba/`;
    public API_BASE_V1: string = `${this.SERVER_BASE}dba/v1/`;

    public API_LOGIN_V1: string = `${this.AUTH_BASE_V1}login/`;
    public API_REFRESH_V1: string = `${this.AUTH_BASE_V1}refreshtoken/`;
    public API_PROFILE_V1: string = `${this.AUTH_BASE_V1}profile/`;
    public API_CHANGE_PASSWORD_V1 = `${this.API_BASE_V1}setNewPassword/`;
    public API_DEVICE_V1 = `${this.API_BASE_V1}device/`;

    public API_USERS_V1: string = `${this.API_BASE_V1}users/`;
    public API_LANDLORDS_V1: string = `${this.API_BASE_V1}landlords/`;
    public API_USER_IMPORT_V1 = `${this.API_BASE_V1}userimport/`;
    public API_BUILDING_USERS_V1 = `${this.API_BASE_V1}buildingusers/`;
    public API_BUILDING_TENANTS_V1 = `${this.API_BASE_V1}buildingtenants/`;
    public API_ALL_TENANTS_V1 = `${this.API_BASE_V1}alltenants/`;
    public API_BUILDING_INFO_V1 = `${this.API_BASE_V1}buildinginfo/`;
    public API_ORGS_V1 = `${this.API_BASE_V1}orgs/`;
    public API_CHAT_ROOMS_V1 = `${this.API_BASE_V1}messages/rooms/`;
    public API_CHAT_APP_ROOMS_V1 = `${this.API_BASE_V1}appMessages/rooms/`;
    public API_SERVICES_V1 = `${this.API_BASE_V1}services/`;
    public API_BOOKINGS_V1 = `${this.API_BASE_V1}bookings/`;
    public API_BLDGS_V1 = `${this.API_BASE_V1}buildings/`;
    public API_TENANT_TASKS_V1 = `${this.API_BASE_V1}tenantBookings/`;
    public API_TASKS_V1 = `${this.API_BASE_V1}staffBookings/`;
    public API_ALL_TASKS_V1 = `${this.API_BASE_V1}allBookings/`;
    public API_TASK_HISTORY_V1 = `${this.API_BASE_V1}taskHistory/`;
    public API_STAFF_HISTORY_V1 = `${this.API_BASE_V1}staffHistory/`;
    public API_ASSIGN_TASKS_V1 = `${this.API_BASE_V1}assignTasks/`;
    public API_PROGRESS_TASKS_V1 = `${this.API_BASE_V1}progressTasks/`;
    public API_COMPLETE_TASKS_V1 = `${this.API_BASE_V1}schedules/`;
    public API_CERTIFICATIONS_V1 = `${this.API_BASE_V1}certifications/`;
    public API_RECURRING_TASKS_V1 = `${this.API_BASE_V1}recurringTasks/`;
    public API_COMPLETE_RECURRING_V1 = `${this.API_BASE_V1}completeRecurring/`;
    public API_BUILDING_NOTES_V1 = `${this.API_BASE_V1}buildingNotes/`;  
    public DEFAULT_PROFILE_URL: string = "https://firebasestorage.googleapis.com/v0/b/silverbrick-live.appspot.com/o/Defaults%2FgkoXOHyv4kWmN67ttHUMjtiMAFI3.png?alt=media&token=7f75beaa-ac2c-4452-b391-667f4a179ca3";
    public SOCKET_MESSAGE_V1 = `${this.SERVER_BASE}messages`;
    public API_USER_LOCATION_V1 = `${Urls.API_BASE_V1}userLocations/`;
    public API_TIME_CARDS_V1 = `${this.API_BASE_V1}timeCard`;
    public API_LANDLORD_TENANTS_V1 = `${this.API_BASE_V1}landlordTenants/`;  
    public API_LANDLORD_TASKS_V1 = `${this.API_BASE_V1}landlordTasks/`;  
    public API_UPDATE_HISTORY_V1 = `${Urls.API_BASE_V1}updateHistory/`;

    get user(): IUser {
        return this._user;
    }

    set user(use: IUser) {
        this.user = use;
    }

    get username(): string {
        return this._username;
    }

    get userID(): string {
        return this._uid;
    }

    set userID(id: string) {
        this._uid = id;
    }

    get isOrgAdmin(): boolean {
        return this._isOrgAdmin;
    }

    set isOrgAdmin(isOrgAdmin: boolean) {
        this._isOrgAdmin = isOrgAdmin;
    }

    get isBuildingAdmin(): boolean {
        return this._isBuildingAdmin;
    }

    set isBuildingAdmin(isBuildingAdmin: boolean) {
        this._isBuildingAdmin = isBuildingAdmin;
    }

    get orgID(): string {
        return this._orgID;
    }

    set orgID(org: string) {
        this._orgID = org;
    }

    get buildingID(): string {
        return this._buildingID;
    }

    set buildingID(building: string) {
        this._buildingID = building;
    }

    get userPosition(): string {
        return this._userPosition;
    }

    set userPosition(position: string) {
        this._userPosition = position;
    }

    get org(): IUserOrg {
        return this._org;
    }

    set org(or: IUserOrg) {
        this._org = or;
    }

    get email(): string {
        return this._email;
    }

    get isFirstTime(): boolean {
        return this._isFirstTime;
    }

    set isFirstTime(time: boolean) {
        this._isFirstTime = time;
    }

    get role(): string {
        return this._role;
    }

    set role(newValue: string) {
        this._role = newValue;
    }

    get isTenant(): boolean {
        return this._isTenant;
    }

    set isTenant(newValue: boolean) {
        this._isTenant = newValue;
    }

    get isClockedIn(): boolean {
        return this._isClockedIn;
    }

    set isClockedIn(newValue: boolean) {
        this._isClockedIn = newValue;
    }

    constructor(
        public store: Store<AppState>,
        public controls: Controls,
        public sessionActions: SessionActions,
        public authActions: AuthActions) {
        this.SessionState$ = this.store.select((state) => state.sessionState);
    }

    public Initialize(): void {
        // console.log("Initialize Session");
        this._sessionStateSubscription = this.SessionState$.subscribe(
            (sessionState) => {
                try {
                    if (null != sessionState) {
                        // console.log("Initialize Session", sessionState);
                        switch (sessionState.type) {
                            case SessionStateType.SESSION_INIT_SUCCESS:
                            {
                                this._uid = sessionState.uid;
                                this._isFirstTime = sessionState.user.isFirstTime;
                                this._username = sessionState.username;
                                this._user = sessionState.user;
                                this._email = sessionState.email;
                                this._isBuildingAdmin = sessionState.isBldgAdmin;
                                this._isOrgAdmin = sessionState.isOrgAdmin;
                                this._userPosition = sessionState.user.role;
                                this._orgID = sessionState.orgID;
                                this._buildingID = sessionState.buildingID;
                                this._role = sessionState.userPosition;
                                this._org = sessionState.org;
                                this._isTenant = (sessionState.user.role === "Tenant");
                                if (null != sessionState.user.isClockedIn) {
                                    this._isClockedIn = sessionState.user.isClockedIn;
                                }
                                break;
                            }

                            case SessionStateType.COMPLETE_TASK_SUCCEEDED:
                            {
                                // this.controls.ShowAlert("Task Marked as Complete", 'You successfully completed the task and marked it complete', "Dismiss");
                                break;
                            }

                            default: {
                                break;
                            }
                        }
                    } else {
                        // No audiences available at the moment
                    }
                } catch (error) {
                    // fix later: 'RangeError: Maximum call stack size exceeded' here
                    console.log("Error - " + error);
                }
            },
            (error: any) => {
                console.log("Error - " + error);
            }
        );
    }

    public UnInitialize(): void {
        if (this._sessionStateSubscription != null) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }
    }
}

