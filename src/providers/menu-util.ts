// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { Injectable } from '@angular/core';
import { MenuController } from 'ionic-angular';
import { Constants } from '../common/Constants';
import { AppState } from '../app/app.state';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/map';

export enum EnableMenuType {
    ALL,
    LOGGED_IN_MENU,
    NOTIFICATION
}

@Injectable()
export class MenuUtil {

    constructor(
        public menuController: MenuController,
        public _store: Store<AppState>) {
    }

    ShouldEnableMenu(enableMenuType: EnableMenuType, loggedIn: boolean): void {
        if ((EnableMenuType.ALL === enableMenuType) || (EnableMenuType.LOGGED_IN_MENU === enableMenuType)) {
            this.menuController.enable(
                loggedIn,
                Constants.MENU_LOGGED_IN_MENU);
        }

        if ((EnableMenuType.ALL === enableMenuType) || (EnableMenuType.NOTIFICATION === enableMenuType)) {
            this.menuController.enable(
                loggedIn,
                Constants.MENU_NOTIFICATIONS);
        }
    }

    OpenNotificationMenu(): void {
        this.menuController.open('right');
    }

}