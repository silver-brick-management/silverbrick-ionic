// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { Injectable } from "@angular/core";
import { MenuController, Platform } from "ionic-angular";
import { Store } from "@ngrx/store";
import "rxjs/add/operator/map";

// Local imports
import { AppState } from "../app/app.state";

export enum MediaSoundType {
    TOAST,
}

@Injectable()
export class MediaProvider {
    private readonly _androidAssetPrefix: string = "/android_asset/www/";
    private readonly _toastMediaPath: string = "assets/audio/toast.mp3";
    private readonly _alertMediaPath: string = "assets/audio/nuke.mp3";

    private readonly _androidToastMediaPath: string = this._androidAssetPrefix + this._toastMediaPath;
    private readonly _androidAlertMediaPath: string = this._androidAssetPrefix + this._alertMediaPath;

    constructor(
        public menuController: MenuController,
        public _store: Store<AppState>,
        private _platform: Platform) {}

    PlaySound(soundType: MediaSoundType): any {
        let selectedMediaPath: string = null;

        switch (soundType) {
            case MediaSoundType.TOAST:
                selectedMediaPath = this._platform.isPlatformMatch("android") ? this._androidToastMediaPath : this._toastMediaPath;
                break;

            default:
                throw new Error("Unexpected media sound type!");
        }

        // let mediaObj: any = this._MediaPlugin.create(selectedMediaPath);

        // mediaObj.play();

        return;
    }

    public GetBlob(b64Data: string): Blob {
        let contentType = "";
        let sliceSize = 512;

        b64Data = b64Data.replace(/data\:image\/(jpeg|jpg|png)\;base64\,/gi, "");

        let byteCharacters = atob(b64Data);
        let byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            let byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        let blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
}