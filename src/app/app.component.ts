// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules
import * as firebase from "firebase";
import { FirebaseError, User } from "firebase";

import { Store } from "@ngrx/store";
import { Component, ViewChild } from "@angular/core";
import { tokenNotExpired } from "angular2-jwt";
import { Platform, Nav, ModalController, NavController, Events, AlertController, Alert, ModalOptions } from "ionic-angular";
// import { GooglePlus } from "@ionic-native/google-plus/";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Toast } from '@ionic-native/toast';
import { Observable, Subscription } from "rxjs/Rx";
import { Network } from "@ionic-native/network";
import { Vibration } from "@ionic-native/vibration";
import { FirebaseAuth } from "../services/FirebaseAuth";
import { TabsPage } from "../pages/tabs/tabs";
import { AngularFireAuth } from "angularfire2/auth";
import { Keyboard } from '@ionic-native/keyboard/ngx';

// State related
import { AppState } from "./app.state";
import { AuthActions } from "../store/actions/AuthActions";
import { UserActions } from "../store/actions/UserActions";
import { SessionActions } from "../store/actions/SessionActions";
import { OrgActions } from "../store/actions/OrgActions";

// Type related
import { IAuthState, AuthStateType } from "../interfaces/IStoreState";
import { Constants } from "../common/Constants";
import { ErrorMapper } from "../common/ErrorMapper";
import { IGetBldgHelper } from "../shared/IParams";

// Provider related
import { PushService } from "../providers/push-provider";
import { SessionData } from "../providers/SessionData";
import { MenuUtil, EnableMenuType } from "../providers/menu-util";
import { MessageActions } from "../store/actions/MessageActions";
import { Controls } from "../providers/Controls";

import { IUserLocation } from "../shared/silverbrickTypes";
// import { ProfilePage } from "../pages/profile/profile";
import { LocationProvider } from "../providers/location-provider";
import { SignUpModalArgs } from "../interfaces/IUserProfileNavigationArgs";
import { IInAppToastData } from '../shared/SilverBrickTypes';

@Component({
    templateUrl: "app.html"
})
export class SilverBrickApp {
    rootPage = "EntryPage";

    public alertScreen: Alert;
    public authState$: Observable<IAuthState>;
    public IsViewingDashboardProfile: boolean = true;
    private _allSubscription: Subscription = new Subscription();
    private _connectSubscription: Subscription;
    private _disconnectSubscription: Subscription;
    private _authStateSubscription: Subscription;
    private _isLoginLoadComplete: boolean;
    private _toastSubscription: Subscription = null;
    private _backgroundColor: string = '#333333';
    private _wasAlreadyLoggedIn: boolean;
    private _shouldSetupSocketListeners: boolean;
    private _isDrawerHidden: boolean;
    private _newUserSignUp: boolean = false;
    private _showWelcomeScreen: boolean = false;

    public OnConnect$: Observable<Network>;
    public OnDisconnect$: Observable<Network>;
    public Username: string = "";

    @ViewChild(Nav) nav: NavController;

    get IsDrawerHidden(): boolean {
        return this._isDrawerHidden;
    }

    get ShowWelcomeScreen(): boolean {
        return this._showWelcomeScreen;
    }

    constructor(
        public platform: Platform,
        public menuUtil: MenuUtil,
        public events: Events,
        public alertController: AlertController,
        private statusBar: StatusBar,
        public toast: Toast,
        public store: Store<AppState>,
        public authActions: AuthActions,
        public modalCtrl: ModalController,
        public sessionActions: SessionActions,
        public messageActions: MessageActions,
        public pushService: PushService,
        public sessionData: SessionData,
        public controls: Controls,
        private _angularFireAuth: AngularFireAuth,
        public splashScreen: SplashScreen,
        public userActions: UserActions,
        public firebaseAuth: FirebaseAuth,
        public orgActions: OrgActions,
        public keyboard: Keyboard,
        public vibrate: Vibration,
        // public google: GooglePlus,
        public locationProvider: LocationProvider,
        private _network: Network) {
        menuUtil.ShouldEnableMenu(EnableMenuType.ALL, false);

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            if (this.platform.is("cordova")) {
                this.statusBar.styleLightContent();
                splashScreen.hide();
                // let status bar overlay webview
                // statusBar.overlaysWebView(false);
                this.statusBar.backgroundColorByHexString("#eaeaea");
                this.statusBar.overlaysWebView(false);
                // this.keyboard.hideFormAccessoryBar(false);
                this.statusBar.isVisible = true;
                this.platform.pause.subscribe(() => {
                    // console.log("PAUSE DETECTED!");
                });

                this.platform.resume.subscribe(() => {
                    // console.log("RESUME DETECTED!");
                    this.store.dispatch(this.authActions.ForceReconnectFollowUp());
                });
            }
        });
        this.alertScreen = null;
        this.authState$ = this.store.select(state => state.authState);
        this.OnDisconnect$ = this._network.onDisconnect();
        this.OnConnect$ = this._network.onConnect();
    }

    ngOnInit(): void {
        this._disconnectSubscription = this.OnDisconnect$.subscribe(() => {
            // this._badConnection();
        });

        this._connectSubscription = this.OnConnect$.subscribe((network: Network) => {
            setTimeout(() => {
                switch (this._network.type) {
                    case "ethernet":
                    case "wifi":
                    case "4g":
                    case "cellular": {
                        // console.log("we got a wifi connection, woohoo!", this._network.type);
                        break;
                    }

                    case "none":
                    case "3g":
                    case "2g": {
                        // console.log("bad connection!", this._network.type);
                        // this._badConnection();
                        break;
                    }
                    default: {
                        // console.log("not sure connection!", this._network.type);
                        break;
                    }
                }
            }, 1000);
        });

        this.events.subscribe(Constants.EVENT_LOGOUT, () => {
            this._isLoginLoadComplete = false;
        });

        this.events.subscribe(Constants.EVENT_HIDE_DRAWER, (args: boolean) => {
            this._isDrawerHidden = args;
        });

        this.events.subscribe(Constants.EVENT_USER_CREATION, () => {
            this._isLoginLoadComplete = false;
            this._shouldSetupSocketListeners = true;
        });

        this.events.subscribe(Constants.EVENT_SIGN_IN, () => {});

        this._isLoginLoadComplete = false;
        this._wasAlreadyLoggedIn = false;
        this._shouldSetupSocketListeners = true;
        this._allSubscription.add(
            this.authState$.subscribe(
                (currentAuthState: IAuthState) => {
                    // console.log("Current", currentAuthState.type, AuthStateType[currentAuthState.type]);
                    switch (currentAuthState.type) {
                        // Idle states
                        case AuthStateType.UPDATE_MY_PROFILE_FAILED:
                        case AuthStateType.UPDATE_MY_PROFILE_SUCCEEDED:
                        case AuthStateType.UPDATE_JWT_TOKEN_SUCCEEDED:
                        case AuthStateType.UPDATE_MY_PROFILE: {
                            // Do nothing
                            break;
                        }

                        case AuthStateType.CHANGE_PASSWORD_SUCCEEDED: {
                            this.events.publish(Constants.EVENT_CHANGE_PASSWORD_SUCCESS);
                            break;
                        }

                        case AuthStateType.CHANGE_PASSWORD_FAILED: {
                            this.events.publish(Constants.EVENT_CHANGE_PASSWORD_FAIL);
                            break;
                        }

                        case AuthStateType.SOCKET_DISCONNECTED: {
                            // Only if you are logged in will these actions be called
                            if (this._isLoginLoadComplete) {
                                if (this.platform.is("cordova")) {
                                    this.controls.ShowToast("Reconnecting you..", "top");
                                }
                                this.events.publish(Constants.EVENT_SOCKET_DISCONNECTED, false);
                                this._shouldSetupSocketListeners = true;
                                this.store.dispatch(this.authActions.ForceReconnectFollowUp());
                            } else {
                            }
                            break;
                        }

                        

                        case AuthStateType.LOADING: {
                            this.nav.setRoot("EntryPage");
                            break;
                        }

                        case AuthStateType.LOGOUT_SUCCEEDED:
                        case AuthStateType.AUTH_REFRESH_INITIATED: {
                            this.firebaseAuth.UnInitialize();
                            this.pushService.UnInitialize();
                            this._shouldSetupSocketListeners = true;
                            // this.sessionData.UnInitialize();
                            this.sessionData.UnInitialize();
                            this._isLoginLoadComplete = false;
                            this._wasAlreadyLoggedIn = false;
                            this.nav.setRoot("EntryPage");

                            this.splashScreen.show();
                            // Splash screen will hide after force browser reload
                            window.location.reload();

                            // fallback to below
                            break;
                        }

                        case AuthStateType.NONE:
                        {
                            if (this.platform.is("cordova")) {
                                // console.log("NONE");

                                this.store.dispatch(this.authActions.Initialize());
                                this.nav.setRoot("EntryPage");
                            } else {
                                this.store.dispatch(this.authActions.Initialize());
                                this.nav.setRoot("EntryPage");
                            }
                            break;
                        }

                        case AuthStateType.LOGIN_FAILED: {
                            if (null != this.alertScreen) {
                                // NOTE: I am not sure if this is the desired path. Should the condition be flipped?
                                if (this._wasAlreadyLoggedIn) {
                                    this.firebaseAuth.UnInitialize();
                                    // this.store.dispatch(this.authActions.SocketUnableToConnect());
                                    this.alertScreen = null;
                                } else if (null != currentAuthState.error && !this._wasAlreadyLoggedIn) {
                                    this.events.publish(Constants.EVENT_LOGIN_FAILED);
                                    this.firebaseAuth.UnInitialize();
                                    this.alertScreen = this.alertController.create({
                                        title: "Login Failed",
                                        message: currentAuthState.error.Message,
                                        buttons: [
                                            {
                                                text: "Try Again",
                                                role: "cancel",
                                                handler: () => {
                                                    this.nav.setRoot("LoginPage");
                                                    this.alertScreen = null;
                                                }
                                            }
                                        ]
                                    });

                                    // this.alertScreen.present();
                                } else if (!this._isLoginLoadComplete) {
                                } else {
                                    // this.store.dispatch(this.authActions.SocketUnableToConnect());
                                    this.alertScreen = null;
                                }
                            } else {
                                // Show nothing.
                                if (!this._wasAlreadyLoggedIn) {
                                    this.nav.setRoot("LoginPage");
                                    if (null != currentAuthState.error) {
                                        this.alertScreen = this.alertController.create({
                                            title: "Login Failed",
                                            message: currentAuthState.error.Message,
                                            buttons: [
                                                {
                                                    text: "Try Again",
                                                    role: "cancel",
                                                    handler: () => {
                                                        this.nav.setRoot("LoginPage");
                                                        this.alertScreen = null;
                                                    }
                                                }
                                            ]
                                        });
                                        // this.alertScreen.present();
                                    }
                                }

                                // Refresh the app in case of a socket mishap. Unsure what the path above is checking with the
                                // alert screen. Check with adam on the desired behavior
                                if (this._wasAlreadyLoggedIn) {
                                    this.store.dispatch(this.authActions.ForceReconnect());
                                } else {
                                    // We only care to force a refresh in the case of login failure after the user
                                    // has successfully logged in. It is prob a socket reconnect issue.
                                }
                            }
                            break;
                        }

                        case AuthStateType.SOCKET_UNABLE_TO_CONNECT: {
                            // NOTE: I dont think this path will work as we have to uninitialize everything
                            // before intitializing again or we will be in a bizzare state
                            // Bayo to investigate what the original intent was
                            // Could potentially pass in the error to the login page
                            // as a param
                            this.firebaseAuth.UnInitialize();
                            this.store.dispatch(this.authActions.Initialize());
                            break;
                        }

                        case AuthStateType.SERVER_READY: {
                            // Initiate the socket connection
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && !currentAuthState.isAuthSocketConnected) {
                                this.store.dispatch(this.authActions.InitAuthSocket(currentAuthState.token));
                            } else {
                                this.store.dispatch(
                                    this.authActions.LoginFail({
                                        error: "Socket auth failed!"
                                    })
                                );
                            }
                            break;
                        }

                        case AuthStateType.AUTH_SOCKET_READY: {
                            // Initiate the socket connection
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && currentAuthState.isAuthSocketConnected) {
                                this.store.dispatch(this.authActions.InitMessageSocket());
                                this.store.dispatch(this.sessionActions.InitializeSession(currentAuthState.profile));
                            } else {
                                this.store.dispatch(
                                    this.authActions.LoginFail({
                                        error: "Auth Socket failed!"
                                    })
                                );
                            }
                            break;
                        }

                        case AuthStateType.MESSAGE_SOCKET_READY: {
                            // Initiate the socket connection
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && currentAuthState.isAuthSocketConnected && currentAuthState.isMessageSocketConnected) {
                                this.store.dispatch(this.authActions.LoginSuccess());

                            } else {
                                this.store.dispatch(
                                    this.authActions.LoginFail({
                                        error: "Track Socket failed!"
                                    })
                                );
                            }
                            break;
                        }

                        case AuthStateType.SERVER_AND_SOCKET_READY: {
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && currentAuthState.isAuthSocketConnected && currentAuthState.isMessageSocketConnected) {
                                // this._showWelcomeScreen = true;
                                // this.Username = currentAuthState.profile.firstName + " " + currentAuthState.profile.lastName;
                                // setTimeout(() => {
                                //     this._showWelcomeScreen = false;
                                // }, 4000);
                                this.sessionData.Initialize();
                                // Ensures we dont dupe the listners as they
                                // remain active during the lifetime of the app
                                // Init socket disconnect & reconnect listener
                                if (this._shouldSetupSocketListeners) {
                                    // Auth Actions
                                    this.store.dispatch(this.authActions.SocketDisconnectListener());
                                    this.store.dispatch(this.authActions.AuthSocketReconnectListener());
                                    this.store.dispatch(this.authActions.MessageSocketReconnectListener());
                                    this.store.dispatch(this.messageActions.GetMessageHistoryListener());
                                    this.store.dispatch(this.messageActions.AddMessageListener());
                                    this.store.dispatch(this.messageActions.NewChatRoomListener());
                                    this.store.dispatch(this.messageActions.GetChatRoomsListener());
                                    if (currentAuthState.profile.isFirstTime) {

                                    } else {
                                        if (!currentAuthState.profile.isAdmin) {

                                            if ((currentAuthState.profile.role === "Tenant")) {
                                                this.store.dispatch(this.sessionActions.GetTenantTasks(currentAuthState.profile.uid));
                                            } else if (currentAuthState.profile.role === "Landlord") {
                                                this.store.dispatch(this.sessionActions.GetLandlordTasks());
                                                this.store.dispatch(this.sessionActions.GetLandlordTasksCompleted());
                                                this.store.dispatch(this.userActions.GetAllTenantsLandlord());
                                            } else {
                                                let bldgHelper: any = {
                                                    orgID: this.sessionData.org.id,
                                                    buildingID: this.sessionData.org.buildings[0].id
                                                };
                                                // console.log("bldgHelper", bldgHelper);
                                                this.store.dispatch(this.orgActions.GetAllOrgsScope());
                                                // this.store.dispatch(this.sessionActions.GetUsersScope(null));
                                                this.store.dispatch(this.sessionActions.GetStaffTasks(currentAuthState.profile.uid));
                                                this.store.dispatch(this.sessionActions.GetStaffHistory(currentAuthState.profile.uid));
                                                setTimeout(() => {
                                                    this.store.dispatch(this.sessionActions.ScopeStaffTasks(currentAuthState.profile));
                                                }, 500);
                                                this.locationProvider.GetGeoLocation()
                                                .then((location: IUserLocation) => {
                                                    
                                                });
                                            }
                                        } else {
                                            this.store.dispatch(this.sessionActions.GetRecurringTasks());
                                            this.store.dispatch(this.sessionActions.GetUsersScope(null));
                                            this.store.dispatch(this.orgActions.GetAllOrgsScope());
                                            this.store.dispatch(this.userActions.GetAllTenants());
                                        }

                                    }
                                    /* Publish an event that tells the pages / components listening for the
                                       EVENT_SOCKET_DISCONNECTED event, that the socket is connected, parameter - true
                                    */
                                    this.events.publish(Constants.EVENT_SOCKET_DISCONNECTED, true);
                                    if (this.platform.is('cordova')) {
                                        this.toast.hide();
                                    }

                                    if (this._isLoginLoadComplete) {
                                    // Toast only works with cordova. Need to ensure it exist or it crashes the code
                                    if (this.platform.is('cordova')) {
                                        this._backgroundColor = Constants.TOAST_CONNECT_BG_COLOR;
                                        let _duration: number = 1000;
                                        this._toastSubscription = this.toast.showWithOptions(
                                        {
                                            message: 'Connected',
                                            position: "top",
                                            duration: _duration,
                                            styling: {
                                                backgroundColor: this._backgroundColor,
                                                textColor: '#444444',
                                                horizontalPadding: 20
                                            }
                                        }).subscribe(
                                            (toastData: IInAppToastData) => {
                                                if (null != toastData) {
                                                }    
                                                else {
                                                    
                                                }
                                            }
                                        );
                                        this._backgroundColor = '#333333';
                                    } else {
                                        // Not cordova. Skip showing toast
                                    }
                                }
                                    this._shouldSetupSocketListeners = false;
                                } else {
                                    // For socket reconnect and logout -> login
                                    // we do not need to reactivate the listeners
                                }
                                if (!this._isLoginLoadComplete) {
                                    this._isLoginLoadComplete = true;
                                    this._wasAlreadyLoggedIn = true;
                                    this.pushService.Initialize(this.nav);
                                    // User is logged in and the route is leading
                                    // to the entry page which is just a loading UI
                                    // This forces a navigation to the dashboard for
                                    // logged in users and seem to be going to the
                                    // entry component.
                                    // makes sure animation finishes before setting root

                                    if (currentAuthState.profile) {
                                        if (currentAuthState.profile.isFirstTime) {
                                            
                                        } else {
                                            this.nav.setRoot("TabsPage");
                                        }
                                    } else {
                                        this.nav.setRoot("TabsPage");
                                    }
                                } else {
                                    // For socket reconnect, we do not need to rerun
                                    // these actions. But we do for logout -> login
                                }
                            } else {
                                this.store.dispatch(
                                    this.authActions.LoginFail({
                                        error: "Server and Socket ready failed!"
                                    })
                                );
                            }
                            break;
                        }

                        default: 
                        {
                            break;
                            // throw new Error("Unexpected!");
                        }
                    }
                },
                error => {
                    this.nav.setRoot("LoginPage");
                }
            )
        );
    }

    menuClosed(): void {
        this.events.publish(Constants.EVENT_HIDE_DRAWER, false);
    }

    ngOnDestroy(): void {
        this.events.unsubscribe(Constants.EVENT_LOGOUT);

        this.events.unsubscribe(Constants.EVENT_HIDE_DRAWER);

        this.events.unsubscribe(Constants.EVENT_HIDE_TABS);

        this._isLoginLoadComplete = false;
        if (this._allSubscription) {
            this._allSubscription.unsubscribe();
        } else {
            // No subscription
        }
        this.sessionData.UnInitialize();
    }

    private _badConnection(): void {
        if (null != this.alertScreen) {
        } else {
            this.alertScreen = this.alertController.create({
                title: "Connection Issues",
                message: "It seems that your device is having connection issues. Please navigate to your data/wi-fi settings to check your status.",
                buttons: [
                    {
                        text: "OK",
                        role: "ok",
                        handler: () => {}
                    }
                ]
            });
            this.alertScreen.present();
        }
    }

    private findProfileView(): any {
        if (null == this.nav) {
            return null;
        } else {
            // Nav controller is valid
        }
        if (null != this.nav.getViews()) {
            for (let view of this.nav.getViews()) {
                if (view.component !== null) {
                    return view.instance;
                } else {
                    // Continue searching
                }
            }
        }

        // Did not find a view
        return null;
    }
}

