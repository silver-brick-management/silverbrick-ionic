// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Storage, StorageConfig } from '@ionic/storage';
import { NgModule } from '@angular/core';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Http, RequestOptions } from '@angular/http';

// Local modules
import { Constants, JSON_CONTENT_TYPE } from '../common/Constants';

let storageConfig: StorageConfig = {
    driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
};
let storage = new Storage(storageConfig);

export function authHttpServiceFactory(http: Http, options: RequestOptions): AuthHttp {
    return new AuthHttp(new AuthConfig({
            noJwtError: true,
            globalHeaders: [JSON_CONTENT_TYPE],
            tokenGetter: (() => localStorage.getItem(Constants.AUTH_TOKEN_STORAGE_NAME))
        }),
        http,
        options);
}

@NgModule({
    providers: [
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions]
        }
    ]
})
export class AppAuthHttpModule { }
