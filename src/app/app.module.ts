// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicStorageModule } from "@ionic/storage";
import { CalendarModule } from 'angular-calendar';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgCalendarModule  } from 'ionic2-calendar';

import { IonicApp, IonicModule } from "ionic-angular";
import { SilverBrickApp } from "./app.component";
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";
import { NgxLoadingModule } from 'ngx-loading';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { MainCalendarComponentModule } from '../components/calendar-components/main-calendar-component/main-calendar.component.module';

import { AppServicesModule } from "./app.services";
import { AppStateModule } from "./app.state";
import { AppAngularFireModule } from "./app.angularFire";
import { AppAuthHttpModule } from "./app.auth";
import { AppProvidersModule } from "./app.providers";
// import { DrawerModule } from '../components/drawer/drawer.module';

const ComponentArr: any[] = [ SilverBrickApp ];
@NgModule({
    declarations: ComponentArr,
    providers: [ Keyboard ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        AppStateModule,
        AppAuthHttpModule,
        AppServicesModule,
        AppProvidersModule,
        AppAngularFireModule,
        // DrawerModule,
        MainCalendarComponentModule,
        CalendarModule.forRoot(),
        IonicStorageModule.forRoot({
            name: 'offlinedb',
            driverOrder: ['sqlite', 'indexeddb', 'websql']
        }),
        NgCalendarModule,
        NgxLoadingModule.forRoot({}),
        IonicModule.forRoot(SilverBrickApp, {
            preloadModules: true,
            platforms: {
                ios: {
                    scrollPadding: false,
                    scrollAssist: false,
                    autoFocusAssist: false,
                },
            },
            tabsHideOnSubPages: false,
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: ComponentArr
})
export class AppModule {}

