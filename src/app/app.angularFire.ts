// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// Environmental
import { ConfigContext } from '../env/ConfigContext';


// Dev config
export const firebaseConfig = {
    apiKey: ConfigContext.FirebaseAPIKey,
    authDomain: ConfigContext.FirebaseAuthDomain,
    databaseURL: ConfigContext.FirebaseDatabaseURL,
    storageBucket: ConfigContext.FirebaseStorageBucket,
    messagingSenderId: ConfigContext.FirebaseMessengerSenderID
};

@NgModule({
    imports: [
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        AngularFireModule.initializeApp(firebaseConfig)
    ],
    exports: [
        AngularFireModule,
        AngularFireAuthModule,
        AngularFireAuthModule
    ]
})
export class AppAngularFireModule { }