// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Local modules
import { AuthEffects } from '../store/effects/AuthEffects';
import { SessionEffects } from '../store/effects/SessionEffects';
import { MessageEffects } from '../store/effects/MessageEffects';

import { AuthActions } from '../store/actions/AuthActions';
import { SessionActions } from '../store/actions/SessionActions';
import { MessageActions } from '../store/actions/MessageActions';

import { AuthReducers } from '../store/reducers/AuthReducers';
import { SessionReducers } from '../store/reducers/SessionReducers';
import { MessageReducers } from '../store/reducers/MessageReducers';
import { UserEffects } from '../store/effects/UserEffects';
import { UserActions } from '../store/actions/UserActions';
import { UserReducers } from '../store/reducers/UserReducers';
import { OrgEffects } from '../store/effects/OrgEffects';
import { OrgActions } from '../store/actions/OrgActions';
import { OrgReducers } from '../store/reducers/OrgReducers';

import { IAuthState, IUserState, ISessionState, IOrgState, IMessageState } from '../interfaces/IStoreState';


@NgModule({
    imports: [
        StoreModule.forRoot({
            authState: AuthReducers.Reduce,
            usersState: UserReducers.Reduce,
            orgState: OrgReducers.Reduce,
            sessionState: SessionReducers.Reduce,
            messageState: MessageReducers.Reduce
        }),
        EffectsModule.forRoot([AuthEffects, SessionEffects, MessageEffects, OrgEffects, UserEffects])
    ],
    exports: [
        StoreModule,
        EffectsModule
    ],
    providers: [
        AuthActions,
        SessionActions,
        MessageActions,
        OrgActions,
        UserActions
     ]
})
export class AppStateModule { }

export interface AppState {
    authState: IAuthState;
    sessionState: ISessionState;
    messageState: IMessageState;
    usersState: IUserState;
    orgState: IOrgState;
}
