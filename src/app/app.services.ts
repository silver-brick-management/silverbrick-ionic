// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// Services are network injectable (Hit the net)
// Mock services might be needed for functional tests
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';

import { FirebaseAuth } from '../services/FirebaseAuth';
import { AuthApi } from '../services/AuthApi';
import { StorageHelper } from "../providers/storage-helper";
import { FirebaseStorage } from '../services/FirebaseStorage';

import { DeviceApi } from '../services/DeviceApi';
import { AccountApi } from '../services/AccountApi';
import { MessageApi } from '../services/MessageApi';
import { UserApi } from '../services/UserApi';
import { OrgApi } from '../services/OrgApi';
import { AuthSocket } from '../services/AuthSocket';
import { MessageSocket } from '../services/MessageSocket';

@NgModule({
  providers: [
    FirebaseAuth,
    AuthApi,
    FirebaseStorage,
    DeviceApi,
    UserApi,
    OrgApi,
    AccountApi,
    MessageApi,
    AuthSocket,
    MessageSocket
  ]
})
export class AppServicesModule { }
