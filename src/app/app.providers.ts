// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// Providers are locally contained injectable (does not hit the net)
// Generally should be able to stand on their own in tests (no mock needed)
//
// ****************************************************************

// Node modules
import { IonicErrorHandler } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { NgModule, ErrorHandler } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Push } from '@ionic-native/push';
import { Geolocation } from '@ionic-native/geolocation';
import { AngularFireAuth } from 'angularfire2/auth';
import { Toast } from '@ionic-native/toast';
import { Vibration } from '@ionic-native/vibration';
import { Dialogs } from '@ionic-native/dialogs';
import { Network } from '@ionic-native/network';
import { KeychainTouchId } from '@ionic-native/keychain-touch-id';
// import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { TabsService } from '../providers/TabsService';
import { ImagePicker } from '@ionic-native/image-picker';

// Local modules
import { MenuUtil } from '../providers/menu-util';
import { StorageHelper } from "../providers/storage-helper";
import { AuthHttpWrapper } from '../providers/AuthHttpWrapper';
import { PushService } from '../providers/push-provider';

import { MediaProvider } from '../providers/media-provider';
import { SessionData } from '../providers/SessionData';
import { Controls } from '../providers/Controls';
import { LocationProvider } from '../providers/location-provider';

@NgModule({
  providers: [
    MenuUtil,
    StatusBar,
    SplashScreen,
    KeychainTouchId,
    StorageHelper,
    AuthHttpWrapper,
    GoogleMaps,
    ImagePicker,
    SessionData,
    // ScreenOrientation,
    TabsService,
    Geolocation,
    Network,
    LocationProvider,
    MediaProvider,
    Camera,
    PushService,
    PhotoViewer,
    Push,
    Toast,
    Controls,
    Vibration,
    Dialogs,
    AngularFireAuth,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppProvidersModule { }
