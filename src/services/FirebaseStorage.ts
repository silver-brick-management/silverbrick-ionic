// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { storage } from 'firebase';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class FirebaseStorage {

    constructor(
        private _camera: Camera,
        private _angularFireAuth: AngularFireAuth) {
    }

    // https://github.com/apache/cordova-plugin-camera#cameraoptions-errata-
    GetImageOptions(sourceType: number): CameraOptions {
        return {
            quality: 50,
            allowEdit: true,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            encodingType: this._camera.EncodingType.JPEG,
            destinationType: this._camera.DestinationType.DATA_URL
        };
    }

    Upload(path: string, data: string, format: string, contentType: string): storage.UploadTask {
        return this._angularFireAuth.app.storage().ref(path).putString(
            data,
            format,
            {
                contentType: contentType
            });
    }

    UploadFile(path: string, data: File): storage.UploadTask {
        return this._angularFireAuth.app.storage().ref(path).put(
            data,
            {
                contentType: 'image/png'
            });
    }
}
