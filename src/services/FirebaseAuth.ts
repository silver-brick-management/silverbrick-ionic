// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Observable, Subscription } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { FirebaseError, User } from "firebase";
import * as firebase from "firebase";
import { AngularFireAuth } from "angularfire2/auth";
import { UserFactoryType, UserTypeFactory } from "../factories/UserTypeFactory";
import { ISilverBrickUser } from "../shared/SilverBrickTypes";
// Local modules
import { ErrorMapper } from "../common/ErrorMapper";

@Injectable()
export class FirebaseAuth {
    private _authStateSubscription: Subscription;

    constructor(private _angularFireAuth: AngularFireAuth) {}

    Initialize(): Promise<User> {
        console.log("Calling FirebaseAuth:InitializePromise");
        return new Promise<User>((resolve, reject) => {
            this._authStateSubscription = this._angularFireAuth.authState.subscribe(
                firebaseUser => {
                    if (firebaseUser) {
                        resolve(firebaseUser);
                    } else {
                        this._authStateSubscription.unsubscribe();
                        reject();
                    }
                },
                (error: FirebaseError) => {
                    reject(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                }
            );
        });
    }

    GoogleSignIn(): Observable<User> {
        console.log("Calling GoogleSinIn:InitializePromise");
        return new Observable(observer => {
            let provider = new firebase.auth.GoogleAuthProvider();
            provider.addScope("profile email");
            firebase
                .auth()
                .signInWithPopup(provider)
                .then(result => {
                    console.log("User", result.user);
                    let token = result.credential.accessToken;

                    // The signed-in user info.
                    let user = result.user;
                    console.log("Success", user);
                    observer.next(user);
                    observer.complete();
                })
                .catch((error: FirebaseError) => {
                    observer.error(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    GoogleSignUp(): Observable<ISilverBrickUser> {
        console.log("Calling GoogleSignUp:InitializePromise");
        return new Observable(observer => {
            let provider = new firebase.auth.GoogleAuthProvider();
            provider.addScope("profile email");
            firebase
                .auth()
                .signInWithPopup(provider)
                .then(result => {
                    console.log("User", result.user);
                    let token = result.credential.accessToken;
                    // The signed-in user info.
                    let user = result.user;
                    console.log("Success", user);
                    let newUser: ISilverBrickUser = UserTypeFactory.CreateInstance(UserFactoryType.USER);
                    newUser.uid = user.uid;
                    newUser.email = user.email;
                    observer.next(newUser);
                    observer.complete();
                })
                .catch((error: FirebaseError) => {
                    observer.error(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    GetToken(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this._angularFireAuth.auth.currentUser.getToken(true)
                .then((idToken: string) => {
                    resolve(idToken);
                })
                .catch((error: FirebaseError) => {
                    reject(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    GetTokenPromise(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this._angularFireAuth.auth.currentUser
                .getIdToken(true)
                .then((idToken: string) => {
                    resolve(idToken);
                })
                .catch((error: FirebaseError) => {
                    reject(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    SendForgotPasswordEmail(email: string): Promise<void> {
        // Since firebase uses different 'promises', were wrapping
        // this around with our standard Promise
        return new Promise<any>((resolve, reject) => {
            firebase
                .auth()
                .sendPasswordResetEmail(email)
                .then((val: any) => {
                    resolve(val);
                })
                .catch((error: Error) => {
                    reject(error);
                });
        });
    }

    Login(username: string, password: string): Observable<User> {
        // Trim, since Android sometimes adds a space unintended by the user to the end of the user name.
        console.log("Login", username, password);
        return new Observable(observer => {
            this._angularFireAuth.auth
                .signInWithEmailAndPassword(username, password)
                .then((firebaseUser: User) => {
                    observer.next(firebaseUser);
                    observer.complete();
                })
                .catch((error: FirebaseError) => {
                    observer.error(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    async Logout(): Promise<void> {
        try {
            await this._angularFireAuth.auth.signOut();
            console.log("Successfully logged out!");
        } catch (error) {
            console.error("There was a problem logging out! Error - " + error);
            throw ErrorMapper.GetErrorFromFirebaseAuthError(<firebase.FirebaseError>error);
        }
    }

    UnInitialize(): void {
        console.log("UnInitialize", this._authStateSubscription);
        if (null != this._authStateSubscription) {
            this._authStateSubscription.unsubscribe();
            this._authStateSubscription = null;
        }
    }
}

