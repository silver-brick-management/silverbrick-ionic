// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import "rxjs/add/operator/switchMap";

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';

// Local modules
import { AuthHttpWrapper } from '../providers/AuthHttpWrapper';
import { ErrorMapper } from '../common/ErrorMapper';
import { FirebaseAuth } from '../services/FirebaseAuth';
import { IDeviceInfo } from '../shared/SilverBrickTypes';
import { StorageHelper } from '../providers/storage-helper';
import { Urls, JSON_HEADER } from '../common/Constants';
import { IServerResponseBase } from '../shared/ResponseTypes';
import { SessionData } from "../providers/SessionData";

@Injectable()
export class DeviceApi {

    constructor(
        private _http: Http,
        private _firebaseAuth: FirebaseAuth,
        private _sessionData: SessionData,
        private _storageHelper: StorageHelper,
        private _authHttp: AuthHttpWrapper) { }
    
    public SetFcmRegistrationToken(deviceInfo: IDeviceInfo): Observable<IServerResponseBase> {
        console.log("SetFcmRegistrationToken Params. Device Info - " + JSON.stringify(deviceInfo));

        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        
        return this._authHttp.Put(
            this._sessionData.API_DEVICE_V1,
            JSON.stringify(deviceInfo), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    public RemoveFcmRegistrationToken(deviceInfo: IDeviceInfo): Observable<IServerResponseBase> {
        console.log("RemoveFcmRegistrationToken Params. Device Info - " + JSON.stringify(deviceInfo));

        return this._authHttp.Patch(
            this._sessionData.API_DEVICE_V1,
            deviceInfo)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

}
