// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';

import { User } from 'firebase';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Response, RequestOptions, URLSearchParams } from '@angular/http';

// Local modules
import { ErrorMapper } from '../common/ErrorMapper';
import { IUser } from '../shared/SilverBrickTypes';
import { IPaginateQueryStrings, IGetBldgHelper } from '../shared/IParams';
import { AuthHttpWrapper } from '../providers/AuthHttpWrapper';
import { Constants, Urls, JSON_HEADER } from '../common/Constants';
import {
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseMessage,
    IServerResponseImportUsers,
    IServerResponseGetBuildingUsers,
    GetTimeCardHelper,
    AddTimeCardHelper,
    IServerResponseGetTimeCards,
    IServerResponseAddTimeCard,
    IServerResponseChangePassword
} from '../shared/ResponseTypes';
import { SessionData } from "../providers/SessionData";

@Injectable()
export class UserApi {
    private _options: RequestOptions = new RequestOptions(JSON_HEADER);

    constructor(
        private _sessionData: SessionData,
        private _authHttp: AuthHttpWrapper) { }

    GetUsers(paginateQueryStrings: IPaginateQueryStrings): Observable<IServerResponseGetUsers> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);

        if (paginateQueryStrings) {
            const queryStrings = new URLSearchParams();
            if (paginateQueryStrings.isForward) {
                queryStrings.set("is_forward", paginateQueryStrings.isForward.toString());
            }
            if (paginateQueryStrings.limit) {
                queryStrings.set("limit", paginateQueryStrings.limit);
            }
            if (paginateQueryStrings.prevCursor) {
                queryStrings.set("prev_cursor", paginateQueryStrings.prevCursor);
            }
            if (paginateQueryStrings.nextCursor) {
                queryStrings.set("next_cursor", paginateQueryStrings.nextCursor);
            }
            options.search = queryStrings;
        } else {
            // Pagination query strings not detected
            // Do nothing
        }

        return this._authHttp.Get(this._sessionData.API_USERS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetBuildingUsers(helper: IGetBldgHelper): Observable<IServerResponseGetBuildingUsers> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("org_id", helper.orgID);
        queryStrings.set("building_id", helper.buildingID);
        options.search = queryStrings;
        return this._authHttp.Get(this._sessionData.API_BUILDING_USERS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetBuildingTenants(helper: IGetBldgHelper): Observable<IServerResponseGetBuildingUsers> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("org_id", helper.orgID);
        queryStrings.set("building_id", helper.buildingID);
        options.search = queryStrings;
        return this._authHttp.Get(this._sessionData.API_BUILDING_TENANTS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetAllTenants(): Observable<IServerResponseGetBuildingUsers> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Patch(this._sessionData.API_BUILDING_TENANTS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetLandlordTenants(): Observable<IServerResponseGetBuildingUsers> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_LANDLORD_TENANTS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetAllLandlords(): Observable<IServerResponseGetBuildingUsers> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_LANDLORDS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetUsersFromFullUrl(url: string): Observable<IServerResponseGetUsers> {
        return this._authHttp.Get(url, this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetUser(uidOrEmail: string): Observable<IServerResponseAddOrUpdateOrGetUser> {
        return this._authHttp.Get(this._sessionData.API_USERS_V1 + uidOrEmail, this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AddUser(user: IUser): Observable<IServerResponseAddOrUpdateOrGetUser> {
        return this._authHttp.Post(this._sessionData.API_USERS_V1, JSON.stringify({ data: user }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetTimeCards(helper: GetTimeCardHelper): Observable<IServerResponseGetTimeCards> {
        return this._authHttp.Put(this._sessionData.API_TIME_CARDS_V1, JSON.stringify({ data: helper }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AddTimeCard(helper: AddTimeCardHelper): Observable<IServerResponseAddTimeCard> {
        return this._authHttp.Post(this._sessionData.API_TIME_CARDS_V1, JSON.stringify({ data: helper }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateTimeCard(helper: AddTimeCardHelper): Observable<IServerResponseAddTimeCard> {
        return this._authHttp.Patch(this._sessionData.API_TIME_CARDS_V1, JSON.stringify({ data: helper }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetTimeCard(): Observable<IServerResponseGetTimeCards> {
        return this._authHttp.Get(this._sessionData.API_TIME_CARDS_V1, this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    ImportUsers(users: IUser[]): Observable<IServerResponseImportUsers> {
        return this._authHttp.Post(this._sessionData.API_USER_IMPORT_V1, JSON.stringify({ data: users }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateUser(user: IUser): Observable<IServerResponseAddOrUpdateOrGetUser> {
        return this._authHttp.Put(this._sessionData.API_USERS_V1 + user.uid, JSON.stringify({ data: user }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateUserPassword(user: IUser): Observable<IServerResponseAddOrUpdateOrGetUser> {
        return this._authHttp.Put(this._sessionData.API_CHANGE_PASSWORD_V1 + user.uid, JSON.stringify({ data: user }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    ActivateUser(uid: string): Observable<IServerResponseMessage> {
        return this._authHttp.Patch(this._sessionData.API_USERS_V1 + uid, JSON.stringify({ data: Constants.PATCH_ACTIVATE_ACTION }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    DeactivateUser(uid: string): Observable<IServerResponseMessage> {
        return this._authHttp.Patch(this._sessionData.API_USERS_V1 + uid, JSON.stringify({ data: Constants.PATCH_DEACTIVATE_ACTION }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    DeleteUser(uid: string): Observable<IServerResponseMessage> {
        return this._authHttp.Delete(this._sessionData.API_USERS_V1 + uid, this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    ChangePassword(password: string): Observable<IServerResponseChangePassword> {
        return this._authHttp.Put(this._sessionData.API_CHANGE_PASSWORD_V1, JSON.stringify({ data: password }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }
}
