// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import "rxjs/add/operator/switchMap";

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { URLSearchParams, Response, RequestOptions, Http } from '@angular/http';

// Local modules
import { AuthHttpWrapper } from '../providers/AuthHttpWrapper';
import { ErrorMapper } from '../common/ErrorMapper';
import {
    IProfile,
    IProfileUser
} from '../shared/SilverBrickTypes';
import { StorageHelper } from '../providers/storage-helper';
import { Urls, JSON_HEADER } from '../common/Constants';
import {
    IServerResponseBase,
    IServerResponseMessage,
    IServerResponseAddMessage,
    IServerResponseGetChatRooms
} from '../shared/ResponseTypes';

@Injectable()
export class MessageApi {

    constructor(
        private _storageHelper: StorageHelper,
        private _http: Http) { }

    public GetMessageRooms(): Observable<IServerResponseGetChatRooms> {
        return this._http.get(
            Urls.API_CHAT_APP_ROOMS_V1, JSON_HEADER)
        .map((response: Response) => response.json())
        .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }
}
