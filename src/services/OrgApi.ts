// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';

import { User } from 'firebase';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Response, RequestOptions, URLSearchParams } from '@angular/http';

// Local modules
import { ErrorMapper } from '../common/ErrorMapper';
import { AuthHttpWrapper } from '../providers/AuthHttpWrapper';
import { Constants, Urls, JSON_HEADER } from '../common/Constants';
import {
    IServerResponseGetAllOrgs,
    IServerResponseMessage,
    IServerResponseCreateOrg,
    IServerResponseGetOrgInfo,
    IServerResponseCreateBldg,
    IServerResponseGetChatRooms,
    IServerResponseGetTasks,
    IServerResponseGetNotes,
    IServerResponseAddUpdateNote,
    IServerResponseDeleteNote,
    IServerResponseGetAllBuildings,
    IServerResponseGetBookings,
    IServerResponseAssignBooking
} from '../shared/ResponseTypes';
import { IOrgInfo, IBuilding, INote } from '../shared/SilverBrickTypes';
import { IAddBldgHelper, IGetBldgHelper } from '../shared/IParams';
import { SessionData } from "../providers/SessionData";

@Injectable()
export class OrgApi {
    private _options: RequestOptions = new RequestOptions(JSON_HEADER);

    constructor(
        private _sessionData: SessionData,
        private _authHttp: AuthHttpWrapper) { }

    GetOrgs(): Observable<IServerResponseGetAllOrgs> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_ORGS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetAllBuildings(): Observable<IServerResponseGetAllBuildings> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Put(this._sessionData.API_ORGS_V1, null, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetOrgInfo(orgID: string): Observable<IServerResponseGetOrgInfo> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_ORGS_V1 + orgID, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AddOrg(org: IOrgInfo): Observable<IServerResponseCreateOrg> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Post(this._sessionData.API_ORGS_V1, JSON.stringify({ data: org }),  options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateOrg(org: IOrgInfo): Observable<IServerResponseCreateOrg> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Put(this._sessionData.API_ORGS_V1 + org.id, JSON.stringify({ data: org }),  options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AddBldg(data: IAddBldgHelper): Observable<IServerResponseCreateBldg> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Post(this._sessionData.API_BLDGS_V1 + data.orgID, JSON.stringify({ data: data.building }),  options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateBldg(data: IAddBldgHelper): Observable<IServerResponseCreateBldg> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Put(this._sessionData.API_BLDGS_V1 + data.orgID, JSON.stringify({ data: data.building }),  options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetBldg(data: IGetBldgHelper): Observable<IServerResponseCreateBldg> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_BLDGS_V1 + data.orgID + '/' + data.buildingID, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AddNote(note: INote): Observable<IServerResponseAddUpdateNote> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Post(this._sessionData.API_BUILDING_NOTES_V1, JSON.stringify({ data: note }),  options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateNote(note: INote): Observable<IServerResponseAddUpdateNote> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Put(this._sessionData.API_BUILDING_NOTES_V1, JSON.stringify({ data: note }),  options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetNotes(helper: IGetBldgHelper): Observable<IServerResponseGetNotes> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("org_id", helper.orgID);
        queryStrings.set("building_id", helper.buildingID);
        options.search = queryStrings;
        return this._authHttp.Get(this._sessionData.API_BUILDING_NOTES_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetChatRooms(): Observable<IServerResponseGetChatRooms> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_CHAT_APP_ROOMS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetBookings(): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp.Get(this._sessionData.API_BOOKINGS_V1 + 'all/', options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetStaffBookings(helper: IGetBldgHelper): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("org_id", helper.orgID);
        queryStrings.set("building_id", helper.buildingID);
        options.search = queryStrings;
        return this._authHttp.Get(this._sessionData.API_TASKS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

}
