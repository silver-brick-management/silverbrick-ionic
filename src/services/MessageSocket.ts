// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import "rxjs/add/operator/map";
import "rxjs/add/operator/switchMap";

import * as io from "socket.io-client";

import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";

// Local modules
import { IError } from "../interfaces/IError";
import { ErrorMapper } from "../common/ErrorMapper";
import { Constants, Urls } from "../common/Constants";

import { IServerResponseMessage, IServerResponseGetMessageHistory, IServerResponseAddMessage, IServerResponseAddChat, AddMessageHelper, NewChatRoomHelper, AddChatHelper, IServerResponseNewRoom, UserChatRoom, IServerResponseGetChatRooms } from "../shared/ResponseTypes";
import { IMessage } from "../shared/SilverBrickTypes";
import { MessageActionTypes } from "../store/actions/MessageActions";

@Injectable()
export class MessageSocket {
    private _isInitialized: boolean;
    private _unsubscribeHelper: Subject<any>;
    private _messageSocket: SocketIOClient.Socket;
    private _cachedAuthResponse: IServerResponseMessage;

    constructor() {
        this._messageSocket = null;
        this._isInitialized = false;
        this._cachedAuthResponse = null;
        this._unsubscribeHelper = new Subject<any>();
    }

    Initialize(): Observable<IServerResponseMessage> {
        return new Observable((observer) => {
            if (!this._isInitialized) {
                // console.log("TrackSocket: Socket not initialized, attempting to connect: ", Urls.SOCKET_TRACK_V1);
                this._messageSocket = io.connect(Urls.SOCKET_MESSAGE_V1);
                this._messageSocket.on(Constants.SOCKET_CONNECT, () => {});

                this._messageSocket.on(Constants.SOCKET_AUTHENTICATED, (serverResponse: IServerResponseMessage) => {
                    this._isInitialized = true;
                    this._cachedAuthResponse = serverResponse;
                    // Turn off the init listeners. We will use a global one from now on.
                    this._messageSocket.removeAllListeners();
                    observer.next(serverResponse);
                    observer.complete();
                });

                this._messageSocket.on(Constants.SOCKET_UNAUTHORIZED, (error: IServerResponseMessage) => {
                    observer.error(error);
                });
            } else {
                // console.log("TrackSocket: Already initialized!");
                if ((null != this._messageSocket) && (this._messageSocket.disconnected)) {
                    // console.log("TrackSocket: Reconnecting...");
                    this._messageSocket.connect();
                } else {
                    // We are already connected
                    observer.next(this._cachedAuthResponse);
                    observer.complete();
                }
            }
        });
    }

    /* ACTIONS */

    JoinRoom(roomID: string = null): Observable<any> {
        // console.log("Joining message room", roomID);
        return new Observable<any>((observer) => {
            this._messageSocket.emit(Constants.SOCKET_JOIN, roomID);
            this._messageSocket.on(Constants.SOCKET_JOINED, (serverResponse: any) => {
                observer.next(serverResponse);
                observer.complete();
            });
        });
    }

    LeaveRoom(): Observable<any> {
        return new Observable((observer) => {
            this._messageSocket.emit(Constants.SOCKET_LEAVE);
            observer.next();
            observer.complete();
        });
    }

    GetChatRooms(): Observable<any> {
        return new Observable((observer) => {
            this._messageSocket.emit(MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS]);
            observer.next();
            observer.complete();
        });
    }

    GetMessageHistory(roomID: string, cursor: string = null): Observable<any> {
        console.log("GetMessageHistory", roomID, cursor);
        return new Observable((observer) => {
            this._messageSocket.emit(MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY], roomID, cursor);
            observer.next();
            observer.complete();
        });
    }

    NewChatRoom(helper: NewChatRoomHelper): Observable<any> {
        return new Observable((observer) => {
            this._messageSocket.emit(MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM], helper);
            observer.next();
            observer.complete();
        });
    }

    AddNewMessage(helper: AddChatHelper): Observable<any> {
        return new Observable((observer) => {
            this._messageSocket.emit(MessageActionTypes[MessageActionTypes.ADD_MESSAGE], helper);
            observer.next();
            observer.complete();
        });
    }

    Disconnect(): Observable<any> {
        console.log("[TRACE] TrackSocket.Disconnect: Disconnecting track socket");
        return new Observable((observer) => {
            this._messageSocket.disconnect();
            observer.next();
            observer.complete();
        });
    }

    UnInitialize(): Observable<any> {
        // console.log('[TRACE] TrackSocket.UnInitialize: Tearing down post socket');
        return new Observable((observer) => {
            this._unsubscribeHelper.next();
            this._unsubscribeHelper.complete();
            this._messageSocket.disconnect();
            this._messageSocket = null;
            this._isInitialized = false;
            observer.next();
            observer.complete();
        });
    }

    /* LISTENERS */

    // Used for reconnecting
    OnAuthenticated(): Observable<IServerResponseMessage> {
        return new Observable<IServerResponseMessage>((observer) => {
            this._messageSocket.on(Constants.SOCKET_AUTHENTICATED, (serverResponse: IServerResponseMessage) => {
                observer.next(serverResponse);
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    OnGetChatRoomsSuccess(): Observable<IServerResponseGetChatRooms> {
        return new Observable<IServerResponseGetChatRooms>((observer) => {
            this._messageSocket.on(MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS_SUCCESS], (serverResponse: IServerResponseGetChatRooms) => {
                observer.next(serverResponse);
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    OnGetMessageHistorySuccess(): Observable<IServerResponseGetMessageHistory> {
        return new Observable<IServerResponseGetMessageHistory>((observer) => {
            this._messageSocket.on(MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_SUCCESS], (serverResponse: IServerResponseGetMessageHistory) => {
                console.log("GET_MESSAGE_HISTORY_SUCCESS", serverResponse);
                observer.next(serverResponse);
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    OnNewChatRoomSuccess(): Observable<IServerResponseNewRoom> {
        return new Observable<IServerResponseNewRoom>((observer) => {
            this._messageSocket.on(MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_SUCCESS], (serverResponse: IServerResponseNewRoom) => {
                observer.next(serverResponse);
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    OnAddMessageSuccess(): Observable<IServerResponseAddChat> {
        return new Observable<IServerResponseAddChat>((observer) => {
            this._messageSocket.on(MessageActionTypes[MessageActionTypes.ADD_MESSAGE_SUCCESS], (serverResponse: IServerResponseAddChat) => {
                observer.next(serverResponse);
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    OnError(): Observable<IError> {
        return new Observable<IError>((observer) => {
            this._messageSocket.on(Constants.SOCKET_ERROR, (serverResponse: IServerResponseMessage) => {
                observer.next(ErrorMapper.GetErrorFromSocketError(serverResponse));
            });
        }).takeUntil(this._unsubscribeHelper);
    }
}

