// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import "rxjs/add/operator/switchMap";

import * as io from 'socket.io-client';

import { JwtHelper } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';

// Local modules
import { ErrorMapper } from '../common/ErrorMapper';
import { Constants, Urls } from '../common/Constants';
import { IServerResponseMessage } from '../shared/ResponseTypes';

@Injectable()
export class AuthSocket {

    jwtHelper: JwtHelper = new JwtHelper();
    private _isReconnectRequested: boolean;
    private _isInitialized: boolean;
    private _unsubscribeHelper: Subject<any>;
    private _authSocket: SocketIOClient.Socket;
    private _cachedAuthResponse: IServerResponseMessage;
    private _cachedToken: string;
    private _socketOptions: SocketIOClient.ConnectOpts = {
        reconnection: true,
        reconnectionAttempts: 300,
        reconnectionDelay: 12000
    };

    constructor() {
        this._authSocket = null;
        this._isInitialized = false;
        this._cachedAuthResponse = null;
        this._isReconnectRequested = false;
        this._unsubscribeHelper = new Subject<any>();
    }

    ReconnectFollowUp(): Observable<boolean> {
        return new Observable((observer) => {
            let isReconnectRequested: boolean = false;
            if (this._isReconnectRequested) {
                this._isReconnectRequested = false;
                isReconnectRequested = true;
            }
            else {
            }

            observer.next(isReconnectRequested);
            observer.complete();
        });
    }

    Initialize(token: string): Observable<IServerResponseMessage> {
        return new Observable((observer) => {
            if (!this._isInitialized) {
                this._authSocket = io.connect(Urls.SERVER_BASE, this._socketOptions);
                this._authSocket.on(Constants.SOCKET_CONNECT, () => {
                    this._authSocket.emit(Constants.SOCKET_AUTHENTICATE, `Bearer ${token}`);
                });

                this._authSocket.on(Constants.SOCKET_AUTHENTICATED, (serverResponse: IServerResponseMessage) => {
                    this._isInitialized = true;
                    this._cachedToken = token;
                    this._cachedAuthResponse = serverResponse;
                    // Turn off the init listeners. We will use a global one from now on.
                    this._authSocket.removeAllListeners();
                    observer.next(serverResponse);
                    observer.complete();
                });

                this._authSocket.on(Constants.SOCKET_UNAUTHORIZED, (serverResponse: IServerResponseMessage) => {
                    observer.error(ErrorMapper.GetErrorFromSocketError(serverResponse));
                });
            }
            else {
                if ((null != this._authSocket) && (this._authSocket.disconnected)) {
                    this._authSocket.connect();
                }
                else {
                    // We are already connected
                    observer.next(this._cachedAuthResponse);
                    observer.complete();
                }
            }
        });
    }

    UpdateToken(token: string): void {
        this._cachedToken = token;
    }

    Reconnect(): Observable<any> {
        return new Observable((observer) => {
            if (this._isInitialized) {
                if ((null != this._authSocket) && (this._authSocket.disconnected)) {
                    this._authSocket.connect();
                }
                else {
                    this._isReconnectRequested = true;
                    this._authSocket.disconnect();
                }
            }
            else {
            }

            observer.next();
            observer.complete();
        });
    }

    Disconnect(): Observable<any> {
        return new Observable((observer) => {
            this._isReconnectRequested = false;
            this._authSocket.disconnect();
            observer.next();
            observer.complete();
        });
    }

    UnInitialize(): Observable<any> {
        return new Observable((observer) => {
            this._unsubscribeHelper.next();
            this._unsubscribeHelper.complete();
            this._isReconnectRequested = false;
            this._authSocket.removeAllListeners();
            this._authSocket.disconnect();
            this._authSocket = null;
            this._isInitialized = false;
            observer.next();
            observer.complete();
        });
    }

    /* LISTENERS */

    // Used for reconnecting

    OnAuthenticated(): Observable<IServerResponseMessage> {
        return new Observable<IServerResponseMessage>((observer) => {
            this._authSocket.on(Constants.SOCKET_CONNECT, () => {
                this._authSocket.emit(Constants.SOCKET_AUTHENTICATE, `Bearer ${this._cachedToken}`);
            });

            this._authSocket.on(Constants.SOCKET_AUTHENTICATED, (serverResponse: IServerResponseMessage) => {
                observer.next(serverResponse);
            });

            this._authSocket.on(Constants.SOCKET_UNAUTHORIZED, (serverResponse: IServerResponseMessage) => {
                observer.error(ErrorMapper.GetErrorFromSocketError(serverResponse));
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    OnDisconnect(): Observable<any> {
        return new Observable<any>(observer => {
            this._authSocket.on(Constants.SOCKET_DISCONNECT, () => {
                observer.next();
            });
        }).takeUntil(this._unsubscribeHelper);
    }

    // Calculates the period at which a token should be refreshed
    // Creates an observable that fires at the calculated period
    // Formular today is to set the delay to half the expiration time
    OnIntervalTokenRefresh(token: string): Observable<any> {
        const jwtIat: number = this.jwtHelper.decodeToken(token).iat;
        const jwtExp: number = this.jwtHelper.decodeToken(token).exp;
        const iat: Date = new Date(0);
        const exp: Date = new Date(0);

        let delay: number = (exp.setUTCSeconds(jwtExp) - iat.setUTCSeconds(jwtIat)) / 2;
        delay = (delay < Constants.REFRESH_THRESHOLD_30_MIN) ? Constants.REFRESH_THRESHOLD_30_MIN : delay;


        return Observable.interval(delay).takeUntil(this._unsubscribeHelper);
    }
}
