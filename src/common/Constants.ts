// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { Headers } from "@angular/http";
import { ConfigContext } from "../env/ConfigContext";
import { RecurringEvent } from '../shared/SilverBrickTypes';
import {
    CalendarEventAction
} from "angular-calendar";

export class Constants {
    static readonly SILVERBRICK_TOUCH_ID_KEY = "silverbrick-touch-id";
    static readonly STR_FINISHING = "Finishing...";
    static readonly STR_SELECT_PHOTO = "Take or Choose a photo";
    static readonly STR_REPORT_POST = "Report Post";
    static readonly STR_TAKE_PHOTO = "Take Photo";
    static readonly STR_CHOOSE_PHOTO = "Choose a Photo";
    static readonly STR_CANCEL = "Cancel";
    static readonly STR_IMAGE_FSLASH = "image/";
    static readonly STR_PNG = "png";
    static readonly STR_BASE64 = "base64";
    static readonly STR_UPLOADING_IMAGE = "Uploading Image...";
    static readonly STR_LOADING = "Loading...";

    static readonly DIALOG_RESPONSE_ACTION_OK: string = "ok";
    static readonly DIALOG_RESPONSE_ACTION_CANCEL: string = "cancel";
    static readonly DIALOG_RESPONSE_ACTION_ADD: string = "Added";
    static readonly DIALOG_RESPONSE_ACTION_UPDATE: string = "Updated";
    static readonly DIALOG_RESPONSE_ACTION_ACTIVATE: string = "Activated";
    static readonly DIALOG_RESPONSE_ACTION_DEACTIVATE: string = "Deactivated";
    static readonly DIALOG_RESPONSE_ACTION_DELETE: string = "Deleted";
    static readonly DIALOG_RESPONSE_ACTION_SHIP: string = "Shipped";

    // Events
    static readonly EVENT_SOCKET_DISCONNECTED = "socket-disconnect";
    static readonly EVENT_LOGOUT = "logout:action";
    static readonly EVENT_LOGIN_FAILED = "login-fail:action";
    static readonly EVENT_CHANGE_PASSWORD_SUCCESS = "change-password:success";
    static readonly EVENT_CHANGE_PASSWORD_FAIL = "change-password:fail";
    static readonly EVENT_USER_PROFILE_NAVIGATION_REQUEST = "navigation-req:userprofile";
    static readonly EVENT_HIDE_PLAYER = "hide_player:action";
    static readonly EVENT_SHOW_PLAYER = "show_player:action";
    static readonly EVENT_HIDE_TABS = "hide_tabs:action";
    static readonly EVENT_HIDE_DRAWER = "hide_drawer:action";
    static readonly EVENT_UPDATE_CARDS = "payment-info:updatecards";
    static readonly EVENT_UPDATE_CARDS_FAILED = "payment-info:failupdate";
    static readonly EVENT_UPDATE_CART_TOTAL = "cart-total:update";
    static readonly EVENT_VIEWING_DASH_PROFILE = "view-dash:profile";
    static readonly EVENT_USER_CREATION = "user-creation:new";
    static readonly EVENT_SIGN_IN = "event:sign-in";
    static readonly EVENT_GO_TODAY = "event:go-today";

    // Errors
    static readonly ERROR_MESSAGE_IONIC_STORAGE_FAIL = "Ionic storage failed";
    static readonly ERROR_TYPE_STORAGE_UNAUTHORIZED = "storage/unauthorized";
    static readonly ERROR_TYPE_STORAGE_CANCELED = "storage/canceled";
    static readonly MENU_LOGGED_IN_MENU = "loggedInMenu";
    static readonly MENU_NOTIFICATIONS = "right";

    // Socket actions
    static readonly SOCKET_CONNECT = "connect";
    static readonly SOCKET_DISCONNECT = "disconnect";
    static readonly SOCKET_DISCONNECTING = "disconnecting";
    static readonly SOCKET_RECONNECT = "reconnect";
    static readonly SOCKET_RECONNECT_ATTEMPT = "reconnect_attempt";
    static readonly SOCKET_ERROR = "error";
    static readonly SOCKET_AUTHENTICATE = "authenticate";
    static readonly SOCKET_AUTHENTICATED = "authenticated";
    static readonly SOCKET_UNAUTHORIZED = "unauthorized";
    static readonly SOCKET_JOIN = "join";
    static readonly SOCKET_JOINED = "joined";
    static readonly SOCKET_LEAVE = "leave";
    static readonly STR_AUDIENCE_TENANT_ONLY: string = "tenant";
    static readonly STR_AUDIENCE_LANDLORD_ONLY: string = "landlord";
    static readonly STR_AUDIENCE_TENANT_AND_LANDLORD: string = "tenant_landlord";
    static readonly STR_AUDIENCE_TENANT_AND_STAFF: string = "tenant_staff";
    static readonly STR_AUDIENCE_STAFF_ONLY: string = "staff";

    static readonly DEFAULT_PROFILE_PHOTO_PATH = "https://firebasestorage.googleapis.com/v0/b/silver-brick.appspot.com/o/Defaults%2Fdefault-user.png?alt=media&token=864efc3d-7b31-4f68-8401-965e5c33f734";
    static readonly DEFAULT_CARD_PHOTO_PATH = "assets/icon/cards/";
    static readonly DEFAULT_FB_PROFILE_PATH = "DefaultProfile.png";
    static readonly PREMIUM_PLAN_ID = "plan_FRmVjmCAUVw4Ic";
    static readonly BASE_PLAN_ID = "plan_FRmUqX9sRf3fdX";
    static readonly TEST_PLAN_ID = "plan_Fbup3JUdc7lfx3";

    // OS Related
    static readonly OS_TYPE_ANDROID = "android";
    static readonly OS_TYPE_IOS = "ios";
    static readonly OS_TYPE_UNKNOWN = "unknown";

    static readonly TOAST_POSITION_TOP = "top";
    static readonly TOAST_POSITION_BOTTOM = "bottom";
    static readonly TOAST_POSITION_MIDDLE = "center";
    static readonly TOAST_CONNECT_BG_COLOR = '#27d679';

    // Messages
    static readonly MESSAGE_IMAGE_UPLOAD_ERROR =
        "There was an error uploading your image";
    static readonly MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED =
        "Image upload not authorized";
    static readonly MESSAGE_IMAGE_UPLOAD_CANCELED = "Image upload canceled";
    static readonly MESSAGE_IMAGE_UPLOAD_FAILURE = "Chat Image Upload failure";

    // Swipe Directions
    static readonly SWIPE_LEFT_TO_RIGHT: number = 4; // go back
    static readonly SWIPE_RIGHT_TO_LEFT: number = 2; // go forward
    static readonly SWIPE_DOWN_TO_UP: number = 8;
    static readonly SWIPE_UP_TO_DOWN: number = 16;

    // Push related
    static readonly TYPE_SilverBrick_V1 = "SilverBrickPayload_V1";
    static readonly TYPE_MESSAGE_PAYLOAD_V1 = "SilverBrickMessagePayload_V1";
    static readonly TYPE_SERVICE_ASSIGN_PAYLOAD_V1 = "SilverBrickServiceAssignPayload_V1";
    static readonly TYPE_SERVICE_COMPLETE_PAYLOAD_V1 = "SilverBrickServiceCompletePayload_V1";
    static readonly TYPE_SERVICE_NEW_PAYLOAD_V1 = "SilverBrickNewServicePayloadV1";
    static readonly TYPE_SERVICE_MESSAGE_PAYLOAD_V1 = "SilverBrickTaskMessagePayload_V1";
    static readonly TYPE_SERVICE_PROGRESS_PAYLOAD_V1 = "SilverBrickServiceProgressPayload_V1";
    static readonly GOOGLE_WEB_ID = "402133435532-ectdr0qanv62n6fm080vq5jhl01r1aup.apps.googleusercontent.com";

    static readonly AUTH_TOKEN_STORAGE_NAME = "silverbrick:ionic2:id_token";
    static readonly AUTH_PROFILE_STORAGE_NAME = "silverbrick:ionic2:profile";
    static readonly REFRESH_THRESHOLD_30_MIN: number = 30 * 60 * 1000; // in milliseconds (30 min * 60 seconds * 1000 milliseconds)
    static readonly DEFAULT_INFINITE_SCROLL_LENGTH: number = 20;
    static readonly NAV_PRIMARY_MARK = "primary";

    static readonly PATCH_ACTIVATE_ACTION: string = "activate";
    static readonly PATCH_DEACTIVATE_ACTION: string = "deactivate";
}

// API Urls
export class Urls {
    static readonly SERVER_BASE: string = ConfigContext.SilverBrickApiURL;
    static readonly AUTH_BASE_V1: string = `${Urls.SERVER_BASE}auth/v1/dba/`;
    static readonly API_BASE_V1: string = `${Urls.SERVER_BASE}dba/v1/`;

    static readonly API_LOGIN_V1: string = `${Urls.AUTH_BASE_V1}login/`;
    static readonly API_REFRESH_V1: string = `${Urls.AUTH_BASE_V1}refreshtoken/`;
    static readonly API_PROFILE_V1: string = `${Urls.AUTH_BASE_V1}profile/`;

    static readonly API_USERS_V1: string = `${Urls.API_BASE_V1}users/`;
    static readonly API_USER_IMPORT_V1 = `${Urls.API_BASE_V1}userimport/`;
    static readonly API_BUILDING_USERS_V1 = `${Urls.API_BASE_V1}buildingusers/`;
    static readonly API_BUILDING_TENANTS_V1 = `${Urls.API_BASE_V1}buildingtenants/`;
    static readonly API_ALL_TENANTS_V1 = `${Urls.API_BASE_V1}alltenants/`;
    static readonly API_BUILDING_INFO_V1 = `${Urls.API_BASE_V1}buildinginfo/`;
    static readonly API_ORGS_V1 = `${Urls.API_BASE_V1}orgs/`;
    static readonly API_CHAT_ROOMS_V1 = `${Urls.API_BASE_V1}messages/rooms/`;
    static readonly API_SERVICES_V1 = `${Urls.API_BASE_V1}services/`;
    static readonly API_BOOKINGS_V1 = `${Urls.API_BASE_V1}staffBookings/`;
    static readonly API_STAFF_BOOKINGS_V1 = `${Urls.API_BASE_V1}allBookings/`;

    static readonly API_CHAT_APP_ROOMS_V1 = `${Urls.API_BASE_V1}appMessages/rooms/`;
    static readonly API_BLDGS_V1 = `${Urls.API_BASE_V1}buildings/`;
    static readonly API_TASKS_V1 = `${Urls.API_BASE_V1}staffBookings/`;
    static readonly API_TASK_HISTORY_V1 = `${Urls.API_BASE_V1}taskHistory/`;
    static readonly API_ASSIGN_TASKS_V1 = `${Urls.API_BASE_V1}assignTasks/`;
    static readonly API_USER_LOCATION_V1 = `${Urls.API_BASE_V1}userLocations/`;
    static readonly API_UPDATE_HISTORY_V1 = `${Urls.API_BASE_V1}updateHistory/`;

    // NOTE: Socket channels cant end in a slash. Unsure why...
    static readonly SOCKET_DASHBOARD_V1 = `${Urls.SERVER_BASE}post`;
    static readonly SOCKET_TRACK_V1 = `${Urls.SERVER_BASE}track`;
    static readonly SOCKET_USER_V1 = `${Urls.SERVER_BASE}user`;
    static readonly SOCKET_MESSAGE_V1 = `${Urls.SERVER_BASE}messages`;
}
export const JSON_CONTENT_TYPE = { "Content-Type": "application/json" };
export const JSON_HEADER = { headers: new Headers(JSON_CONTENT_TYPE) };

export const STATE_LIST: string[] = [
    "AK",
    "AL",
    "AR",
    "AZ",
    "CA",
    "CO",
    "CT",
    "DC",
    "DE",
    "FL",
    "GA",
    "HI",
    "IA",
    "ID",
    "IL",
    "IN",
    "KS",
    "KY",
    "LA",
    "MA",
    "MD",
    "ME",
    "MI",
    "MN",
    "MO",
    "MS",
    "MT",
    "NC",
    "ND",
    "NE",
    "NH",
    "NJ",
    "NM",
    "NV",
    "NY",
    "OH",
    "OK",
    "OR",
    "PA",
    "RI",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VA",
    "VI",
    "VT",
    "WA",
    "WI",
    "WV",
    "WY"
];

export const CARD_LIST: string[] = [
    "Visa",
    "MasterCard",
    "American Express",
    "Discover"
];

export const EXP_MONTH: string[] = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12"
];

export const actions: CalendarEventAction[] = [
{
    label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
    onClick: ({ event, date }: { event: RecurringEvent, date: Date }): void => {
       // this.RecurringTask(event.task, date);
    }
}];

export const EXP_YEAR: string[] = [
    "2021",
    "2022",
    "2023",
    "2024",
    "2025",
    "2026",
    "2027",
    "2028",
    "2029",
    "2030"
];
export const DEFAULT_DAYS: string[] = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
];
export const SHIPPING_COST: number = 5;

export const map_style = [
    {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#444444"
            }
        ]
    },
    {
        featureType: "administrative.province",
        elementType: "geometry.fill",
        stylers: [
            {
                visibility: "on"
            },
            {
                color: "#ffffff"
            }
        ]
    },
    {
        featureType: "administrative.province",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#000000"
            },
            {
                visibility: "on"
            }
        ]
    },
    {
        featureType: "landscape",
        elementType: "all",
        stylers: [
            {
                color: "#f2f2f2"
            }
        ]
    },
    {
        featureType: "poi",
        elementType: "all",
        stylers: [
            {
                visibility: "off"
            }
        ]
    },
    {
        featureType: "road",
        elementType: "all",
        stylers: [
            {
                saturation: -100
            },
            {
                lightness: 45
            }
        ]
    },
    {
        featureType: "road.highway",
        elementType: "all",
        stylers: [
            {
                visibility: "simplified"
            }
        ]
    },
    {
        featureType: "road.arterial",
        elementType: "labels.icon",
        stylers: [
            {
                visibility: "off"
            }
        ]
    },
    {
        featureType: "transit",
        elementType: "all",
        stylers: [
            {
                visibility: "off"
            }
        ]
    },
    {
        featureType: "water",
        elementType: "all",
        stylers: [
            {
                color: "#aaaaaa"
            },
            {
                visibility: "on"
            }
        ]
    }
];

export const DEFAULT_SERVICES: any = {
    "5be2c6c2e37568e9a9557318": {
        active: true,
        icon: "tidy-up",
        id: "5be2c6c2e37568e9a9557318",
        name: "Tidy-up",
        notes: ""
    },
    "5be2cba5daa7faa8fb3a4258": {
        active: true,
        icon: "laundry",
        id: "5be2cba5daa7faa8fb3a4258",
        name: "Laundry & Dry Cleaning",
        notes: ""
    },
    "5be2cbb1daa7faa8fb3a4259": {
        active: true,
        icon: "deep",
        id: "5be2cbb1daa7faa8fb3a4259",
        name: "Deep Cleaning",
        notes: ""
    },
    "5be2cbbcdaa7faa8fb3a425a": {
        active: true,
        icon: "grocery",
        id: "5be2cbbcdaa7faa8fb3a425a",
        name: "Grocery Shopping",
        notes: ""
    }
};

export const PROPERTY_TYPES = [
    "Residential",
    "Single-Family",
    "Condo/Townhome",
    "Multi-Family",
    "Commercial",
    "Industrial",
    "Office",
    "Shopping Center",
    "Retail",
];


export const TASK_TYPES = [
    "General Inquiry",
    "Maintenance Request"
];

export const MAINTENANCE_REQUESTS = [
    "General",
    "Applicances",
    "Electrical",
    "HVAC",
    "Key & Lock",
    "Lighting",
    "Outside",
    "Plumbing"
];

export const TASK_STATUS = [
    "New",
    "In Progress",
    "Completed",
    "Deferred",
    "Assigned",
    "Closed"
];

export const URGENCY = [
    "Normal",
    "High",
    "Low"
];

export const colors: any = {
    red: {
        primary: "#ad2121",
        secondary: "#FAE3E3"
    },
    blue: {
        primary: "#1e90ff",
        secondary: "#D1E8FF"
    },
    yellow: {
        primary: "#e3bc08",
        secondary: "#FDF1BA"
    }
};

export const Map_Style: any = [
    {
        featureType: "all",
        elementType: "labels.text.fill",
        stylers: [
            {
                color: "#000000",
            },
        ],
    },
    {
        featureType: "all",
        elementType: "labels.text.stroke",
        stylers: [
            {
                visibility: "on",
            },
            {
                color: "#ffffff",
            },
            {
                weight: "4",
            },
        ],
    },
    {
        featureType: "all",
        elementType: "labels.icon",
        stylers: [
            {
                visibility: "on",
            },
            {
                saturation: "-100",
            },
        ],
    },
    {
        featureType: "administrative",
        elementType: "geometry.fill",
        stylers: [
            {
                color: "#ffffff",
            },
            {
                lightness: 20,
            },
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#000000",
            },
            {
                lightness: 17,
            },
            {
                weight: 1.2,
            },
        ],
    },
    {
        featureType: "administrative.locality",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "administrative.neighborhood",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "administrative.land_parcel",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
            {
                lightness: "80",
            },
        ],
    },
    {
        featureType: "landscape",
        elementType: "all",
        stylers: [
            {
                visibility: "simplified",
            },
            {
                color: "#797979",
            },
        ],
    },
    {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [
            {
                color: "#ffffff",
            },
            {
                lightness: 20,
            },
        ],
    },
    {
        featureType: "landscape.man_made",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "landscape.natural.landcover",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "landscape.natural.terrain",
        elementType: "all",
        stylers: [
            {
                visibility: "off",
            },
        ],
    },
    {
        featureType: "poi",
        elementType: "geometry",
        stylers: [
            {
                color: "#dfdfdf",
            },
            {
                lightness: 21,
            },
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [
            {
                color: "#fed41c",
            },
            {
                visibility: "on",
            },
            {
                weight: "3.00",
            },
        ],
    },
    {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#fed41c",
            },
            {
                gamma: "0.6",
            },
        ],
    },
    {
        featureType: "road.highway.controlled_access",
        elementType: "geometry",
        stylers: [
            {
                visibility: "on",
            },
            {
                color: "#fed41c",
            },
            {
                weight: "4.00",
            },
        ],
    },
    {
        featureType: "road.highway.controlled_access",
        elementType: "geometry.stroke",
        stylers: [
            {
                weight: "1",
            },
            {
                gamma: "0.6",
            },
        ],
    },
    {
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [
            {
                color: "#aeaeae",
            },
            {
                lightness: 18,
            },
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road.arterial",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#b6b6b6",
            },
        ],
    },
    {
        featureType: "road.local",
        elementType: "all",
        stylers: [
            {
                visibility: "on",
            },
            {
                color: "#656565",
            },
        ],
    },
    {
        featureType: "road.local",
        elementType: "geometry",
        stylers: [
            {
                color: "#c6c6c6",
            },
            {
                lightness: 16,
            },
        ],
    },
    {
        featureType: "road.local",
        elementType: "geometry.stroke",
        stylers: [
            {
                color: "#b1b1b1",
            },
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "road.local",
        elementType: "labels.text.stroke",
        stylers: [
            {
                visibility: "on",
            },
            {
                color: "#ffffff",
            },
        ],
    },
    {
        featureType: "transit",
        elementType: "geometry",
        stylers: [
            {
                color: "#bdbdbd",
            },
            {
                lightness: 19,
            },
            {
                visibility: "on",
            },
        ],
    },
    {
        featureType: "water",
        elementType: "geometry",
        stylers: [
            {
                color: "#c0d8e3",
            },
            {
                lightness: 17,
            },
            {
                visibility: "on",
            },
        ],
    },
];
