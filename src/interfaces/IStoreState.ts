// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules

// Local modules
import { IError } from "./IError";
// Local modules
import { ISilverBrickUser, ITimeCard, INote, IBuildingSimple, ITask, IUserOrgBuildingBase, IProfile, IUserOrg, IOrgBasic, IBuildingBasic, IOrgInfo, IBuilding, IUser, IMessage, ITaskFeed, IBooking, IService } from "../shared/SilverBrickTypes";
import { ICredentials } from "../shared/IParams";
import { UserChatRoom } from "../shared/ResponseTypes";

export interface IStateBase {
    error: IError;
    loading: boolean;
}

export enum AuthStateType {
    // Idle states
    NONE,
    LOADING,

    // Non-idle states
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    SERVER_READY,
    AUTH_SOCKET_READY,
    MESSAGE_SOCKET_READY,
    SERVER_AND_SOCKET_READY,
    LOGOUT_SUCCEEDED,
    LOGOUT_FAILED,
    CHECK_AUTH_NO_USER,
    CHECK_AUTH_SUCCESS,
    GETTING_USER,
    SESSION_INIT_SUCCESS,
    USER_READY,
    UPDATE_MY_PROFILE,
    UPDATE_MY_PROFILE_SUCCEEDED,
    UPDATE_MY_PROFILE_FAILED,
    UPDATE_JWT_TOKEN_SUCCEEDED,
    CREATE_USER_SUCCEEDED,
    CREATE_USER_FAILED,
    SOCKET_DISCONNECTED,
    SOCKET_UNABLE_TO_CONNECT,
    AUTH_REFRESH_INITIATED,
    CHANGE_PASSWORD_SUCCEEDED,
    CHANGE_PASSWORD_FAILED
}

export interface IAuthState extends IStateBase {
    token: string;
    profile: IUser;
    authRefreshInitiated: boolean;
    isAuthSocketConnected: boolean;
    isMessageSocketConnected: boolean;
    type: AuthStateType;
}

export interface IPartialAuthState extends IStateBase {
    token?: string;
    profile?: IUser;
    authRefreshInitiated?: boolean;
    isAuthSocketConnected?: boolean;
    isMessageSocketConnected?: boolean;
    type?: AuthStateType;
}

export enum UserStateType {
    NONE,
    LOADING,
    ADD_USER_SUCCEEDED,
    SUCCEEDED,
    GET_USERS_SUCCEEDED,
    GET_TENANTS_SUCCEEDED,
    GET_ALL_TENANTS_SUCCEEDED,
    UPDATE_USER_SUCCEEDED,
    GET_TIME_CARDS_SUCCEEDED,
    GET_TIME_CARDS_PREVIOUS_SUCCEEDED,
    ADD_TIME_CARD_SUCCEEDED,
    UPDATE_TIME_CARD_SUCCEEDED,
    GET_ALL_LANDLORDS_SUCCEEDED,
    SCOPE_USERS_SUCCEEDED
}

export interface IUserState extends IStateBase {
    limit: string;
    next: string;
    previous: string;
    prevCursor: string;
    nextCursor: string;
    users: Array<IUser>;
    buildingUsers: Array<IUser>;
    tenants: Array<IUser>;
    allUsers: Array<IUser>;
    allTenants: Array<IUser>;
    masterTenants: Array<IUser>;
    searchTenants: Array<IUser>;
    searchStaff: Array<IUser>;
    type: UserStateType;
    createdUser: any;
    credentials: ICredentials;
    timeCards: Array<ITimeCard>;
    timeCardsPrevious: Array<ITimeCard>;
}

export enum SessionStateType {
    NONE,
    SESSION_INIT_SUCCESS,
    SEARCH_USER_SUCCESS,
    GET_CUSTOMER_CARDS_SUCCESS,
    ADD_CARD_SUCCESS,
    ADD_CARD_FAILED,
    DELETE_CARD_SUCCESS,
    DELETE_CARD_FAILED,
    GET_BOOKINGS_SUCCESS,
    GET_SERVICES_SUCCESS,
    ASSIGN_BOOKING_SUCCESS,
    LOADING,
    GET_TASKS_SUCCEEDED,
    CREATE_TASKS_SUCCEEDED,
    UPDATE_TASKS_SUCCEEDED,
    ASSIGN_TASKS_SUCCEEDED,
    GET_TASKS_FAILED,
    CREATE_TASKS_FAILED,
    COMPLETE_TASK_SUCCEEDED,
    GET_RECURRING_TASKS_FAILED,
    SESSION_STATE_ERROR,
    GET_RECURRING_TASKS_SUCCEEDED,
    COMPLETE_TASK_FAILED,
    UPDATE_TASKS_FAILED,
    CHANGE_ACTIVE_DATE_SUCCEEDED,
    ASSIGN_TASKS_FAILED,
    SCOPE_USERS_SUCCEEDED,
    GET_USERS_SUCCEEDED
}

export interface ISessionState extends IStateBase {
    username: string;
    uid: string;
    email: string;
    orgID: string;
    buildingID: string;
    org: IUserOrg;
    userPosition: string;
    isOrgAdmin: boolean;
    isBldgAdmin: boolean;
    user: IUser;
    services: IService[];
    bookings: IBooking[];
    tenantBookings: IBooking[];
    completedTasks: ITask[];
    staffBookings: IBooking[];
    tasks: ITask[];
    allTasks: ITask[];
    // inProgressTasks: ITask[];
    // newTasks: ITask[];
    allHistory: ITaskFeed[];
    taskFeed: ITaskFeed[];
    type: SessionStateType;
    activeDate: Date;
    recurringTasks: ITask[];
    allRecurringTasks: ITask[];
    searchTasks: ITask[];
    selectedStaff: IUser;
    usersScope: Array<IUser>;
    allUsersScope: Array<IUser>;
    allEvents: any[];
    travelMode: google.maps.TravelMode;
    allEveryEvents: any[];
    startingLocation: string;
}

export interface IDashboardState extends IStateBase {
    PendingTasks: ITask[];
    CompletedTasks: ITask[];
}

export enum OrgStateType {
    NONE,
    LOADING,
    GET_ORG_SUCCEEDED,
    GET_ALL_ORGS_SUCCEEDED,
    GET_BUILDING_SUCCEEDED,
    UPDATE_ORG_SUCCEEDED,
    UPDATE_BLDG_SUCCEEDED,
    ADD_ORG_SUCCEEDED,
    ADD_BLDG_SUCCEEDED,
    ADD_NOTE_SUCCEEDED,
    UPDATE_NOTE_SUCCEEDED,
    GET_NOTES_SUCCEEDED,
    IMPORT_USERS_SUCCEEDED,
}

export interface IOrgState extends IStateBase {
    orgs: IOrgBasic[];
    selectedOrg: IOrgInfo;
    orgsInfo: IOrgInfo[];
    selectedOrgBldgs: IBuilding[];
    allBuildings: IBuildingSimple[];
    allFullBuildings: IBuilding[];
    selectedBldg: IBuilding;
    notes: INote[];
    type: OrgStateType;
    searchBuildings: IBuildingSimple[];
}
export enum MessageStateType {
    NONE,
    LOADING,
    JOIN_SUCCEEDED,
    GET_MESSAGES_SUCCESS,
    ADD_MESSAGE_SUCCESS,
    MESSAGE_STATE_FAILED,
    NEW_CHAT_ROOM_SUCCEEDED,
    GET_CHAT_ROOMS_SUCCEEDED,
    GET_CHAT_ROOMS_SUCCESS,
}

export interface IMessageState extends IStateBase {
    messages: IMessage[];
    type: MessageStateType;
    hasMoreMessages: boolean;
    cursor: string;
    chatRooms: UserChatRoom[];
    isInitialLoad: boolean;
}

