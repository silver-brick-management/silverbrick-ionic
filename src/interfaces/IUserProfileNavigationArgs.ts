// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { ISilverBrickUser, IUser, IBooking, IAccountInfo } from '../shared/SilverBrickTypes';

export enum UserProfileMode {
  VIEW_USER,
  VIEW_MINE,
  EDIT
}

export interface IUserProfileNavigationArgs {
  Mode?: UserProfileMode;
  UserID: string;
  User?: (ISilverBrickUser);
  ProfileID?: string;
  PhotoURL?: string;
}

export interface SignUpModalArgs {
    User: IUser;
    Booking?: IBooking;
    Shipping?: IAccountInfo;
    IsCleaning?: boolean;
}
