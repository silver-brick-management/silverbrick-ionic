// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { TasksDetailsPage } from './tasks-details';
import { LetterAvatarModule } from '../../components/letter-avatar/letter-avatar.module';
import { CalendarModule } from "ion2-calendar";

@NgModule({
    declarations: [
        TasksDetailsPage,
    ],
    imports: [
        LetterAvatarModule,
        CalendarModule,
        IonicPageModule.forChild(TasksDetailsPage),
    ],
    exports: [
        TasksDetailsPage
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TasksDetailsPageModule {}
