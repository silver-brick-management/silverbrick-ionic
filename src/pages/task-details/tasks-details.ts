// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild, Input, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { NavController, Alert, ActionSheetController, FabContainer, Modal, NavParams, AlertController, MenuController, Content, IonicPage, Events, Loading, LoadingController, Platform } from "ionic-angular";
import { ModalController } from "ionic-angular/components/modal/modal-controller";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";
import { storage, database } from "firebase";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Storage } from "@ionic/storage";
import { Vibration } from "@ionic-native/vibration";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { IonicSelectableComponent } from 'ionic-selectable';
import * as moment from "moment";
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';

// Local modules

// Local modules
import { AppState } from "../../app/app.state";
import { ISessionState, SessionStateType, IOrgState, IUserState } from "../../interfaces/IStoreState";
import { SessionActions } from "../../store/actions/SessionActions";
import { UserActions } from "../../store/actions/UserActions";
import { OrgActions } from "../../store/actions/OrgActions";
import { TabsService } from '../../providers/TabsService';

// Component Modules
import { OrgBldgUtil } from "../../shared/OrgBldgUtil";
import { StringUtil } from "../../shared/StringUtil";
import { FirebaseStorage } from "../../services/FirebaseStorage";

// Type Related Modules
import { IOrgSimple, IAssignee, IUserOrg, IOrgBasic, IUserBuilding, IUser, IBuildingBasic, IBuilding, IBuildingConfig, IBuildingInfo, ITask, IBuildingSimple, ITaskFeed, ISilverBrickUser } from "../../shared/SilverBrickTypes";
import { IFormSelctedValue, IGetBldgHelper } from "../../shared/IParams";
import { IError } from "../../interfaces/IError";
import { ITaskFeedJoin, AssignTaskHelper, AddMessageHelper } from "../../shared/ResponseTypes";
import { Constants, STATE_LIST, PROPERTY_TYPES, TASK_STATUS, TASK_TYPES, URGENCY, MAINTENANCE_REQUESTS } from "../../common/Constants";

import { SessionData } from "../../providers/SessionData";

// Utils and Helper modules
import { Controls } from "../../providers/Controls";
import { FormValidators } from "../../common/FormValidators";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-tasks-details",
    templateUrl: "tasks-details.html",
})
export class TasksDetailsPage {
    @ViewChild(Content) content: Content;
    private _isUploading: boolean;
    private _hasPendingUploadTask: boolean = false;
    private _uploadStatus: string;
    private _isUploadDone: boolean;
    private _uploadPreview: string;
    private _uploadPreviews: string[] = [];
    private _progressPercent: number;
    private _canCancelUpload: boolean;
    private _hideScrollDownButtom: boolean;
    private _imageDatas: string;
    private _imageDataArr: string[] = [];
    private _imageDataFiles: File[] = [];
    private _uploadTask: storage.UploadTask;
    private _participantIDs: string[] = [];
    private _messageStateSubscription: Subscription;
    private _sessionStateSubscription: Subscription;
    private _imageData: string;
    private _chatPhotoModified: boolean;
    private _photoModified: boolean;
    private _isSocketConnected: boolean = true;
    private _taskImageDataArr: string[] = [];
    private _taskUploadPreviews: string[] = [];
    private _taskUploadUrls: string[] = []; 

    private _orgStateSubscription: Subscription;
    private _orgs: IOrgBasic[] = [];
    private _screenSize: number = window.screen.width;
    private _task: ITask = null;
    private _userFormBuildings: FormArray;
    private _buildingIdNameMap: Map<string, string>;
    private _selectedOrgBuildings: Array<IBuildingSimple> = [];
    private _selectedOrgId: string;
    private _selectedBuildingId: string;
    private _hasSelectedAudience: boolean = false;
    private _uploadUrls: string[] = [];

    public IsEditMode: boolean = false;
    public IsReAssigning: boolean = false;
    public TaskToggle: string = "summary";
    public newTaskFeed: ITaskFeed = {
        audience: "",
        orgID: "",
        buildingID: "",
        clientUnit: "",
        type: "",
        timestamp: 0,
        taskID: "",
        message: "",
        category: "",
        assignees: [],
        subCategory: "",
        approved: true,
        photoGallery: []
    };
    public loading$: Observable<boolean>;
    public building: IBuildingSimple = null;
    public SharedWithTenant: boolean = false;
    public SharedWithLandlord: boolean = false;
    public SharedWithStaff: boolean = false;
    public minDate = new Date(Date.now());
    public taskForm = this._formBuilder.group({
        authorName: ["", Validators.required],
        authorID: ["", Validators.required],
        active: [true],
        isRecurring: [false],
        icon: [""],
        status: ["", Validators.required],
        deleted: [false],
        notes: [""],
        id: [""],
        type: [""],
        photoURL: [""],
        category: ["", Validators.required],
        subCategory: [""],
        buildingName: [""],
        buildingID: [""],
        scheduled: [false],
        scheduling: [false],
        orgID: [""],
        clientName: [""],
        clientID: [""],
        clientUnit: [""],
        subject: ["", Validators.required],
        description: ["", Validators.required],
        permissionToEnter: [false],
        havePets: [false],
        recurring: [true],
        startDate: [this.minDate],
        startTime: ['9:00', Validators.required],
        endTime: ['10:00', Validators.required],
        clockInTime: [0],
        clockOutTime: [0],
        availability: [''],
        durationAmount: [''],
        durationQuantity: [1],
        repeats: [''],
        repeatInterval: ['Weekly'],
        repeatQuantity: [1],
        monthlyConvention: ['Day of month']
    });;
    public settings: any = {
        bigBanner: true,
        defaultOpen: false,
        timePicker: true,
        closeOnSelect: true,
    };

    public IsReviewing: boolean = false;

    public orgID: string = "";
    public buildingID: string = "";
    public SelectedDate: any = '';
    public isNewTask: boolean = false;
    public stateList: string[] = STATE_LIST;
    public selectedCategory: string = "Choose a Category";
    public selectedUrgency: string = "Choose a Level";
    public selectedSubCategory: string = "Choose a Sub-Category";
    public selectedStatus: string = "Choose a Status";
    public selectedPropertyType: string = "";
    public userStateError: IError;
    public isOrgStateLoading: boolean;
    public orgs$: Observable<IOrgState>;
    public taskFeed$: Observable<ITaskFeed[]>;
    public tenants$: Observable<IUser[]>;
    public userLoading$: Observable<boolean>;

    public taskDetailTitle: string = "";
    public showExtras: boolean = true;
    public ScheduleListForms: FormArray;
    public SessionState$: Observable<ISessionState>;
    public users$: Observable<Array<IUser>>;
    public staffUsers$: Observable<Array<IUser>>;
    public searchStaff$: Observable<Array<IUser>>;
    public propertyTypes: string[] = PROPERTY_TYPES;
    public taskStatus: string[] = TASK_STATUS;
    public taskTypes: string[] = TASK_TYPES;
    public maintenanceReqs: string[] = MAINTENANCE_REQUESTS;
    public urgencyLevels: string[] = URGENCY;
    public searchString: string = "";
    public selectedTenant: IUser = null;
    public selectedStaff: IAssignee[] = [];
    public isSearching: boolean = false;
    public IsRecurring: boolean = false;
    public isSearchingStaff: boolean = false;
    public searchStaffStr: string = "";
    public newDate: Date = null;
    public activeDate$: Observable<Date>;
    public UnitNumber: string = '';
    public IsAddingUnit: boolean = false;

    public repeatDays: number[] = [];
    public monthlyDays: number[] = [];
    public monthlyWeekDays: number[] = [];
    public monthlyNWeekDays: number[][] = [[], [], [], []];

    public WeekDays: string[] = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'];
    public MonthDays: string[] = [];
    public RepeatOptions: string[] = ['As needed, we won\'t prompt you'];
    public DurationOptions: string[] = ['day(s)', 'week(s)', 'month(s)', 'year(s)', 'forever'];
    public FrequencyOptions: string[] = ['Daily', 'Weekly', 'Monthly', 'Yearly', 'Once'];
    public MonthlyConventions: string[] = ['Day of month', 'Day of week'];
    public ByNWeekDays: string[][] = [['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'], ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'], ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'], ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']];
    public monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    public SelectedRepeat: string = '';
    public SelectedRepeatQuantity: number = 1;
    public SelectedFrequency: string = 'Weekly';
    public SelectedMonthlyConvention: string = 'Day of month';
    public SelectedDuration: string = 'month(s)';

    public FullDate: Date = new Date(Date.now());
    public dateRange = {
        from: this.FullDate,
        to: this.FullDate.setMonth(new Date(Date.now()).getMonth() + 1)
    };

    public option: any = {
        pickMode: 'single',
        disableWeeks: [],
        showMonthPicker: true,
        from: new Date(2000, 1, 1)
    };
    public CurrentMonth: number = 0;
    public TodayDate: string = `${this.FullDate.getMonth()}/${this.FullDate.getDay()}/${this.FullDate.getFullYear()}`;
    public OneMonth: string = `${this.FullDate.getMonth() + 1}/${this.FullDate.getDay()}/${this.FullDate.getFullYear()}`;
    public CurrentMonthDayCount: number = 30;
    public DAYS_IN_MONTH = [
        31,
        28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31
    ];

    get IsAndroid(): boolean {
        // console.log("IsAndroid", (null != this._platform) && this._platform.is('android'));
        return this._platform.is('android');
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role !== 'Landlord') && (this.sessionData.user.role !== 'Tenant')));
    }

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    get IsNotStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role !== 'Staff') || ((this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff'))));
    }

    get IsLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Landlord'));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.isAdmin));
    }

    get ImageDataFiles(): File[] {
        return this._imageDataFiles;
    }

    set ImageDataFiles(newValue: File[]) {
        this._imageDataFiles = newValue;
    }

    get ChatPhotoModified(): boolean {
        return this._chatPhotoModified;
    }

    set ChatPhotoModified(newValue: boolean) {
        this._chatPhotoModified = newValue;
    }

    get ImageArray(): string[] {
        return this._imageDataArr;
    }

    set ImageArray(newValue: string[]) {
        this._imageDataArr = newValue;
    }

    get TaskImageArray(): string[] {
        return this._taskImageDataArr;
    }

    set TaskImageArray(newValue: string[]) {
        this._taskImageDataArr = newValue;
    }

    get PhotoModified(): boolean {
        return this._photoModified;
    }

    set PhotoModified(newValue: boolean) {
        this._photoModified = newValue;
    }

    get TaskUploadPreviews(): string[] {
        return this._taskUploadPreviews;
    }

    set TaskUploadPreviews(uploadPreview: string[]) {
        this._taskUploadPreviews = uploadPreview;
    }

    get IsUploading(): boolean {
        return this._isUploading;
    }

    set IsUploading(isUploading: boolean) {
        this._isUploading = isUploading;
    }

    get UploadStatus(): string {
        return this._uploadStatus;
    }
    
    set UploadStatus(uploadStatus: string) {
        this._uploadStatus = uploadStatus;
    }

    get IsSocketConnected(): boolean {
        return this._isSocketConnected;
    }

    set IsSocketConnected(socketConnected: boolean) {
        this._isSocketConnected = socketConnected;
    }

    get HasSelectedAudience(): boolean {
        return this._hasSelectedAudience;
    }

    set HasSelectedAudience(newValue: boolean) {
        this._hasSelectedAudience = newValue;
    }

    get IsUploadDone(): boolean {
        return this._isUploadDone;
    }

    set IsUploadDone(isUploadDone: boolean) {
        this._isUploadDone = isUploadDone;
    }

    get UploadPreview(): string {
        return this._uploadPreview;
    }

    set UploadPreview(uploadPreview: string) {
        this._uploadPreview = uploadPreview;
    }

    get UploadPreviews(): string[] {
        return this._uploadPreviews;
    }

    set UploadPreviews(uploadPreview: string[]) {
        this._uploadPreviews = uploadPreview;
    }

    get ProgressPercent(): number {
        return this._progressPercent;
    }

    set ProgressPercent(progressPercent: number) {
        this._progressPercent = progressPercent;
    }

    get CanCancelUpload(): boolean {
        return this._canCancelUpload;
    }

    set CanCancelUpload(canCancelUpload: boolean) {
        this._canCancelUpload = canCancelUpload;
    }

    get HideScrollDownButtom(): boolean {
        return this._hideScrollDownButtom;
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get task(): ITask {
        return this._task;
    }

    set task(newValue: ITask) {
        this._task = newValue;
    }

    get orgs(): IOrgBasic[] {
        return this._orgs;
    }

    set orgs(orgs: IOrgBasic[]) {
        this._orgs = orgs;
    }

    get selectedOrgId(): string {
        return this._selectedOrgId;
    }

    set selectedOrgId(id: string) {
        this._selectedOrgId = id;
    }

    get selectedBuildingId(): string {
        return this._selectedBuildingId;
    }

    set selectedBuildingId(id: string) {
        this._selectedBuildingId = id;
    }

    get userFormBuildings(): FormArray {
        return this._userFormBuildings;
    }

    set userFormBuildings(form: FormArray) {
        this._userFormBuildings = form;
    }

    get buildingIdNameMap(): Map<string, string> {
        return this._buildingIdNameMap;
    }

    set buildingIdNameMap(map: Map<string, string>) {
        this._buildingIdNameMap = map;
    }

    get selectedOrgBuildings(): Array<IBuildingSimple> {
        return this._selectedOrgBuildings;
    }

    set selectedOrgBuildings(buildings: Array<IBuildingSimple>) {
        this._selectedOrgBuildings = buildings;
    }

    get ShowTabs(): boolean {
        return (null != this.task)  && !this.IsEditMode && !this.isNewTask;
    }

    constructor(
        public navParams: NavParams,
        public tabService: TabsService,
        public navCtrl: NavController,
        private _controls: Controls,
        public events: Events,
        private _store: Store<AppState>,
        private _formBuilder: FormBuilder,
        private _sessionActions: SessionActions,
        private _userActions: UserActions,
        private _orgActions: OrgActions,
        public alertCtrl: AlertController,
        public camera: Camera,
        public cdr: ChangeDetectorRef,
        public photoViewer: PhotoViewer,
        public modalCtrl: ModalController,
        private _platform: Platform,
        private _keyboard: Keyboard,
        public vibration: Vibration,
        public imagePicker: ImagePicker,
        public actionSheetCtrl: ActionSheetController,
        public sessionData: SessionData,
        public firebaseStorage: FirebaseStorage) {
        this.orgs$ = this._store.select((state) => state.orgState);
        this.TaskToggle = "summary";
        this.userLoading$ = this._store.select((state) => state.usersState.loading);
        this.loading$ = this._store.select((state) => state.sessionState.loading);
        this.SessionState$ = this._store.select((state) => state.sessionState);
        this.users$ = this._store.select((state) => state.usersState.searchTenants);
        this.staffUsers$ = this._store.select((state) => state.usersState.users);
        this.searchStaff$ = this._store.select((state) => state.usersState.searchStaff);
        this.activeDate$ = this._store.select((state) => state.sessionState.activeDate);
        this.taskFeed$ = this._store.select((state) => state.sessionState.taskFeed);
        this._imageDatas = null;
        this.tenants$ = this._store.select((state) => state.usersState.tenants);
        this._imageData = null;
        this._photoModified = false;
        this._chatPhotoModified = false;
    }

    ionViewDidLoad(): void {
        let segmentTabs = document.getElementById("segmentTabs");
        let params: any = this.navParams.data;
        // console.log("params", params);
        if (null != params) {
            this._task = params.task;
            this.IsEditMode = params.isEditMode;
            this.isNewTask = (null == this._task);
            this.newDate = params.date;
            // console.log("newDate", this.newDate);
            this.IsRecurring = params.recurring;
            
            if (null != segmentTabs) {
                if ((this.isNewTask) || (this.IsEditMode)) {
                    segmentTabs.style.display = 'none';
                    segmentTabs.style.visibility = 'hidden';
                }
            }
            this.cdr.detectChanges();
            this.PopulateForm();
        }

        this._subscribeToSocketConnect();
        if (this._platform.is('cordova')) {
            // this._keyboard.hideFormAccessoryBar(false);
        }

        this._sessionStateSubscription = this.SessionState$
        .subscribe((state: ISessionState) => {
            if (null != state) {
                switch (state.type) {
                    case SessionStateType.ASSIGN_TASKS_SUCCEEDED:
                    {
                        this._store.dispatch(this._sessionActions.GetTasks());
                        this._controls.ShowAlert("Request Assigned", "You successfully assigned the request", "Dismiss");
                        this.closeAction();
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
        });
    }

    ionViewWillEnter(): void {
        
    }

    ionViewDidLeave(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }

        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }
        this.IsReviewing = false;
        this.events.unsubscribe(Constants.EVENT_SOCKET_DISCONNECTED);
    }

    ngOnInit(): void {
        const todayData: Date = new Date(Date.now());
        const newData: string = new Date(todayData.getFullYear(), todayData.getMonth(), todayData.getDate() + 5, todayData.getHours(), 0, 0, 0).toString();
        this.taskForm.patchValue({
            dueDate: newData,
        });
    }

    ngOnDestroy(): void {
        this.IsReviewing = false;
        this.events.unsubscribe(Constants.EVENT_SOCKET_DISCONNECTED);
    }

    public UnitNo(ev: string): void {
        if ((null != ev) && (ev.trim() !== '')) {
            this.UnitNumber = ev;
            this.taskForm.patchValue({
                clientID: this.UnitNumber,
                clientUnit: this.UnitNumber
            });
        }
    }

    public AddUnit(): void {
        if (this.IsAddingUnit) {
            this.taskForm.patchValue({
                clientID: this.UnitNumber,
                clientUnit: this.UnitNumber
            });
        }
        this.IsAddingUnit = !this.IsAddingUnit;
    }

    // Extension to the constructor that allows
    // the calling coponent to set the initial values for the
    // user form if possible.
    public PopulateForm(): void {
        

        if ((null != this.sessionData.user)) {
            if (this.sessionData.user.role === 'Staff') {
                this._store.dispatch(this._userActions.GetAllTenants());
                this._store.dispatch(this._userActions.GetUsers(null));
            }
        }
        if (this.isNewTask && this.IsAdmin) {
            this._store.dispatch(this._orgActions.GetAllOrgs());
        }
        if (this.IsLandlord || !this.IsNotTenant) {
            this.SharedWithStaff = true;
        }
        // if (this.isNewTask && this.IsLandlord) {
        //     if (this.sessionData.user.orgs.buildings.length === 1) {
        //         this.building = {
        //             id: this.sessionData.user.orgs.buildings[0].id,
        //             name: this.sessionData.user.orgs.buildings[0].name,
        //             orgID: this.sessionData.user.orgs.id,
        //         };

        //     }
        // }
        let currentDate: Date = new Date(Date.now());
        let currentHour: number = currentDate.getHours();
        let newHour: number = currentHour + 1;
        if (this.IsRecurring) {
            let currentWeek: number = this._weekCount(moment(currentDate));
            for(let i = 0; i < currentWeek; i++){
                this.option.disableWeeks.push(i);
            };
            this.CurrentMonthDayCount = this.DAYS_IN_MONTH[currentDate.getMonth()];
            let day: string = this._getDay(currentDate.getDay());
            this.RepeatOptions.push(`Weekly on ${ day }`);
            this.RepeatOptions.push(`Every 2 weeks on ${ day }`);
            this.RepeatOptions.push(`Monthly on the ${currentDate.getDate() }${ this._nth(currentDate.getDate()) } day of the month`);
            this.RepeatOptions.push(`Custom`);
            this.SelectedRepeat = this.RepeatOptions[4];
            this.taskForm.patchValue({
                repeats: this.RepeatOptions[4],
                durationAmount: this.DurationOptions[3],
                durationQuantity: 1,
                category: 'Recurring Task'
            });
            // console.log("this.task", this.task);
            let timeViews = document.getElementsByClassName('time-view');
            // console.log("timeViews", timeViews);
            if ((null != timeViews) && (null != timeViews[0])) {
                timeViews[0].className = 'time-view time-view-visible';
            }
        }
        if (!this.isNewTask) {
            let helper: ITaskFeedJoin = {
                orgID: this.task.orgID,
                buildingID: this.task.buildingID,
                taskID: this.task.id,
            };
            this.taskDetailTitle = `Request: ${ this.task.subject }`
            this.orgID = this.task.orgID;
            this.buildingID = this.task.buildingID;
            this.newTaskFeed.orgID = this.task.orgID;
            this.newTaskFeed.buildingID = this.task.buildingID;
            this.newTaskFeed.taskID = this.task.id;
            this.newTaskFeed.assignees = this.task.assignees;
            this.newTaskFeed.authorName = `${this.sessionData.user.firstName} ${this.sessionData.user.lastName}`;
            this.newTaskFeed.clientUnit = this.task.clientUnit;
            this.newTaskFeed.category = this.task.category;
            this.newTaskFeed.subCategory = this.task.subCategory;
            this.newTaskFeed.status = this.task.status;
            this.newTaskFeed.role = this.sessionData.user.role;
            this.newTaskFeed.photoGallery = [];
            this.SelectedDuration = this.task.durationAmount;
            this.SelectedFrequency = this.task.repeatInterval;
            this.SelectedRepeat = this.task.repeats;
            this.SelectedDate = new Date(this.task.startDate);
            this.SelectedRepeat = this.task.repeats;
            if (this.SelectedFrequency === this.FrequencyOptions[2]) {
                this.SelectedMonthlyConvention = this.task.monthlyConvention;
                if (this.SelectedMonthlyConvention === 'Day of month') {
                    this.monthlyDays = this.task.monthlyDays;
                } else {
                    if (null != this.task.monthlyNWeekDays) {
                        let keys = Object.keys(this.task.monthlyNWeekDays);
                        for (let key of keys) {
                            if (null != this.task.monthlyNWeekDays[Number(key)]) {
                                // let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `
                                for (let day of this.task.monthlyNWeekDays[Number(key)]) {
                                    if (null != day) {
                                        this.monthlyNWeekDays[Number(key)].push(Number(day));
                                        // let dayStr: string = `${weekStr} ${this.GetDay(Number(key))}, `;
                                        // repeat += dayStr;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (this.SelectedFrequency === this.FrequencyOptions[1]) {
                this.monthlyWeekDays = this.task.monthlyWeekDays;
                // this.taskForm.patchValue({
                //     monthlyConvention
                // })
            }
            if ((!this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff')) {
                this.newTaskFeed.approved = false;
            } else {
                this.newTaskFeed.approved = true;
            }
            this.newTaskFeed.type = "update";
            if ((null != this.task.assignees) && (this.task.assignees.length > 0)) {
                this.selectedStaff = this.task.assignees;
            }
            this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            this._orgStateSubscription = this.orgs$.subscribe((orgState: IOrgState) => {
                if (null != orgState.orgs && orgState.orgs.length > 0) {
                    this.orgs = orgState.orgs;
                    this.selectedCategory = this.task.category;
                    if (this.task.category === "Maintenance Request") {
                        this.selectedSubCategory = this.task.subCategory;
                    }
                    if (null != this.task.clientID) {
                        let match: any = {
                            uid: this.task.clientID,
                            firstName: this.task.clientName.split(" ", 2)[0],
                            lastName: this.task.clientName.split(" ", 2)[1],
                            clientUnit: this.task.clientUnit ? this.task.clientUnit : "",
                            orgs: {
                                id: this.task.orgID,
                                buildings: [
                                    {
                                        name: this.task.buildingName,
                                        id: this.task.buildingID,
                                    },
                                ],
                            },
                        };
                        this.selectedTenant = match;
                    }
                    this.taskForm.patchValue({
                        authorName: this.task.authorName ? this.task.authorName : "",
                        authorID: this.task.authorID ? this.task.authorID : "",
                        clientID: this.task.clientID ? this.task.clientID : "",
                        clientName: this.task.clientName ? this.task.clientName : "",
                        clientUnit: this.task.clientUnit ? this.task.clientUnit : "",
                        active: this.task.active ? this.task.active : "",
                        icon: this.task.icon ? this.task.icon : "",
                        status: this.task.status ? this.task.status : "",
                        deleted: this.task.deleted ? this.task.deleted : "",
                        notes: this.task.notes ? this.task.notes : "",
                        date: this.task.date ? this.task.date : "",
                        id: this.task.id ? this.task.id : "",
                        type: this.task.type ? this.task.type : "",
                        availability: this.task.availability ? this.task.availability : "",
                        category: this.task.category ? this.task.category : "",
                        subCategory: this.task.subCategory ? this.task.subCategory : "",
                        orgID: this.task.orgID ? this.task.orgID : "",
                        buildingID: this.task.buildingID ? this.task.buildingID : "",
                        buildingName: this.task.buildingName ? this.task.buildingName : "",
                        subject: this.task.subject ? this.task.subject : "",
                        description: this.task.description ? this.task.description : "",
                        photos: this.task.photoURL ? this.task.photoURL : "",
                        files: this.task.files ? this.task.files : "",
                        lastUpdated: this.task.lastUpdated ? this.task.lastUpdated : "",
                        buildingAddress: this.task.buildingAddress ? this.task.buildingAddress : "",
                        permissionToEnter: this.task.permissionToEnter ? this.task.permissionToEnter : false,
                        havePets: this.task.havePets ? this.task.havePets : false,
                        startDate: this.task.startDate ? new Date(this.task.startDate) : currentDate,
                        startTime: this.task.startTime ? this.task.startTime : `9:00`,
                        recurring: this.task.recurring ? this.task.recurring : true,
                        durationAmount:  this.task.durationAmount ? this.task.durationAmount : currentDate,
                        durationQuantity:  this.task.durationQuantity ? this.task.durationQuantity : currentDate,
                        endTime:  this.task.endTime ? this.task.endTime : `10:00`,
                        clockInTime:  this.task.clockInTime ? this.task.clockInTime : 0,
                        clockOutTime:  this.task.clockOutTime ? this.task.clockOutTime : 0,
                        repeats: this.task.repeats ? this.task.repeats : '',
                        repeatDays: this.task.repeatDays ? this.task.repeatDays : [''],
                        repeatInterval: this.task.repeatInterval ? this.task.repeatInterval : 'Weekly',
                        repeatQuantity: this.task.repeatQuantity ? this.task.repeatQuantity : 1
                    });
                }
            });
        } else {
            this.taskDetailTitle = "New Request";
            if (this.sessionData.user.role === "Tenant") {
                this.SelectTenant(this.sessionData.user);
            }
            let category: string = 'Maintenance Request';
            if (this.IsRecurring) {
                category = 'Recurring Task';
                this.selectedCategory = 'Recurring Task';
            } else {
                this.selectedCategory = 'Maintenance Request';
            }
            this.taskForm.patchValue({
                authorName: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
                authorID: this.sessionData.user.uid,
                orgID: this.sessionData.org.id,
                category: category,
                urgency: "Normal",
                status: "New",
                durationAmount: this.DurationOptions[4],
                repeats: this.RepeatOptions[2],
                durationQuantity: 1,
                deleted: false,
                active: true,
                startDate: currentDate,
                startTime: `9:00`,
                endTime: `10:00`
            });
            if ((this.sessionData.user.role === 'Landlord') && (this.sessionData.user.orgs.buildings.length === 1)) {
                this.building = {
                    orgID: this.sessionData.user.orgs.id,
                    id: this.sessionData.user.orgs.buildings[0].id,
                    name: this.sessionData.user.orgs.buildings[0].name
                };

                this.taskForm.patchValue({
                    orgID: this.sessionData.user.orgs.id,
                    buildingID: this.sessionData.user.orgs.buildings[0].id,
                    buildingName: this.sessionData.user.orgs.buildings[0].name,
                    clientID: this.sessionData.user.orgs.buildings[0].id
                });
            };
            if (!this.IsRecurring) {
                this.taskForm.patchValue({
                    recurring: false,
                    repeatInterval: 'Once',
                    durationAmount: this.DurationOptions[0],
                    durationQuantity: 0,
                });
            } else {
                this.taskForm.patchValue({
                    recurring: true
                });
            }
        }
    }

    public AssignTask(): void {
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            let staffStr: string = '';
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `<br\>${staff.name}`
            });
            let newAlert = this.alertCtrl.create({
                title: `Update staff assignments?`,
                message: "Please confirm that you want to update staff assignments on this request to:<br\><br\>" + `${ staffStr }`,
                inputs: [
                    {
                        type: 'text',
                        name: 'message',
                        placeholder: 'Enter a message for the assignee(s)'
                    }
                ],
                buttons: [
                    {
                        text: "Cancel",
                        role: "cancel",
                        handler: () => {
                            // console.log("Cancel clicked");
                        },
                    },
                    {
                        text: "Assign Request",
                        handler: (data: any) => {
                            this.task.status = "Assigned";
                            this.task.assignees = this.selectedStaff;
                            let helper: AssignTaskHelper = {
                                userID: '',
                                authorID: this.sessionData.user.uid,
                                task: this.task
                            };

                            if ((null != data) && (null != data.message) && (data.message !== '')) {
                                // console.log("data", data.message);
                                helper.message = data.message;
                            }
                            this._store.dispatch(this._sessionActions.AssignTask(helper));
                            setTimeout(() => {
                                if (null != this.sessionData.user) {
                                    this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                    this._store.dispatch(this._sessionActions.GetUsersScope(null));
                                    this._store.dispatch(this._orgActions.GetAllOrgsScope());
                                    this._store.dispatch(this._userActions.GetAllTenants());
                                    this.IsReAssigning = false;
                                    this.closeAction();
                                    this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                                }
                            }, 500);
                        }
                    }
                ]
            });
            newAlert.present();
        } else {
            this._controls.ShowAlert("No One Selected", "Please choose at least one staff member to assign.", "Dismiss");
        }
    }

    public AssignRecurringTask(): void {
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            let staffStr: string = '';
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `<br\>${staff.name}`
            });
            if (null != this.newDate) {
                let newAlert = this.alertCtrl.create({
                    title: `Re-Assign this task`,
                    message: `Re-assign this task just once or all future events?<br\><br\>${staffStr} `,
                    inputs: [
                        {
                            type: 'text',
                            name: 'message',
                            placeholder: 'Enter a message for the assignee(s)'
                        }
                    ],
                    buttons: [
                        {
                            text: "Cancel",
                            role: 'cancel',
                            handler: () => {

                            }
                        },
                        {
                            text: 'All',
                            handler: (data: any) => {
                                this.task.assignees = this.selectedStaff;
                                let helper: AssignTaskHelper = {
                                    userID: '',
                                    authorID: this.sessionData.user.uid,
                                    task: this.task,
                                };
                                if ((null != data) && (null != data.message) && (data.message !== '')) {
                                    // console.log("data", data.message);
                                    helper.message = data.message;
                                }
                                this._store.dispatch(this._sessionActions.AssignTask(helper));
                 
                                setTimeout(() => {
                                    this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                    this.IsReAssigning = false;
                                    this.closeAction();
                                    this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                                }, 700);
                            }
                        },
                        {
                            text: 'Once',
                            handler: ()=> {
                                let oldEvent: ITask = Object.assign({}, this.task);
                                let oldTime: Date = new Date(this.task.startDate);
                                let skipDate = `${this.newDate.getMonth()}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                                // console.log("eventTimesChanged", skipDate);
                                if ((null != oldEvent.skipDates)) {
                                    oldEvent.skipDates.push(skipDate);
                                } else {
                                    oldEvent.skipDates = [ skipDate ];
                                }
                                let newNonSkip = `${this.newDate.getMonth()}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                                if (null != oldEvent.skipDates) {
                                    if (oldEvent.skipDates.includes(newNonSkip)) {
                                        oldEvent.skipDates = oldEvent.skipDates.filter((dte: string) => {
                                            return (dte !== newNonSkip);
                                        });
                                    }
                                }
                                                // console.log("old task", oldEvent, oldEvent.startDate);
                                this._store.dispatch(this._sessionActions.UpdateTask(oldEvent));

                                let newEvent: ITask = Object.assign({}, this.task);
                                Object.assign({}, newEvent, {
                                    startDate: this.newDate.getTime(),
                                    durationAmount: 'day(s)',
                                    durationQuantity: 0,
                                    repeatInterval: "Once",
                                    subject: `Re-Assign: ${newEvent.subject}`,
                                    description: `Reassigned task once for date: ${this.newDate.toDateString()} ${newEvent.description}`,
                                    status: "Assigned",
                                    assignees: this.selectedStaff,
                                    recurring: true
                                });
                                newEvent.startDate = this.newDate.getTime();
                                newEvent.subject = `Re-Assign: ${newEvent.subject}`;
                                newEvent.description = `Reassigned task once for date: ${this.newDate.toDateString()} ${newEvent.description}`;
                                newEvent.durationAmount = 'day(s)';
                                newEvent.durationQuantity = 0;
                                newEvent.repeatInterval = "Once";
                                newEvent.status = "Assigned";
                                newEvent.assignees = this.selectedStaff;
                                newEvent.recurring = true;
                                newEvent.skipDates = [];
                                newEvent.completedDates = [];
                                let endDate: Date = new Date(newEvent.startDate);
                                endDate.setDate(endDate.getDate() + 1);
                                newEvent.endDate = endDate.getTime();
                                // console.log("new task", newEvent);
                                this._store.dispatch(this._sessionActions.CreateTask(newEvent));
                                setTimeout(() => {
                                    if (null != this.sessionData.user) {
                                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                        this.IsReAssigning = false;
                                        this.closeAction();
                                        this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                                    }
                                }, 700);
                            }
                        }
                    ]
                });
                
                newAlert.present();
            } else {
                let currentDate: Date = new Date(Date.now());
                let newAlert = this.alertCtrl.create({
                    title: `Re-Assign this task`,
                    message: `Re-assign this task just once or all future events?<br\><br\>${staffStr} `,
                    inputs: [
                        {
                            type: 'text',
                            name: 'message',
                            placeholder: 'Enter a message for the assignee(s)'
                        },
                         {
                            type: 'date',
                            min: `${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDate()}`,
                            name: 'date',
                            placeholder: 'Re-assignment start date',
                            value: `${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDate()}`
                        }
                    ],
                    buttons: [
                        {
                            text: "Cancel",
                            role: 'cancel',
                            handler: () => {

                            }
                        },
                        {
                            text: 'All',
                            handler: (data: any) => {
                                this.task.assignees = this.selectedStaff;
                                let helper: AssignTaskHelper = {
                                    userID: '',
                                    authorID: this.sessionData.user.uid,
                                    task: this.task,
                                };
                                if ((null != data) && (null != data.message) && (data.message !== '')) {
                                    // console.log("data", data.message);
                                    helper.message = data.message;
                                }
                                this._store.dispatch(this._sessionActions.AssignTask(helper));
                 
                                setTimeout(() => {
                                    this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                    this.IsReAssigning = false;
                                    this.closeAction();
                                    this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                                }, 700);
                            }
                        },
                        {
                            text: 'Once',
                            handler: (data: any)=> {
                                let oldEvent: ITask = Object.assign({}, this.task);
                                // console.log("data", data, data.date);
                                let dateArray: string[] = data.date.split("-", 3);
                                let newDate: Date = new Date(Number(dateArray[0]), Number(dateArray[1]), Number(dateArray[2]));
                                let skipDate = `${dateArray[1]}_${dateArray[2]}_${dateArray[0]}`;
                                // console.log("eventTimesChanged", skipDate);
                                if ((null != oldEvent.skipDates)) {
                                    oldEvent.skipDates.push(skipDate);
                                } else {
                                    oldEvent.skipDates = [ skipDate ];
                                }
                                let newEvent: ITask = Object.assign({}, this.task);
                                let newerDate: Date = new Date(Number(dateArray[0]), Number(dateArray[1]) - 1, Number(dateArray[2]));
                                let newNonSkip = `${this.newDate.getMonth()}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                                if (null != oldEvent.skipDates) {
                                    if (oldEvent.skipDates.includes(newNonSkip)) {
                                        oldEvent.skipDates = oldEvent.skipDates.filter((dte: string) => {
                                            return (dte !== newNonSkip);
                                        });
                                    }
                                }
                                // console.log("old task", oldEvent, oldEvent.startDate);
                                this._store.dispatch(this._sessionActions.UpdateTask(oldEvent));

                                
                                Object.assign({}, newEvent, {
                                    startDate: newerDate.getTime(),
                                    durationAmount: 'day(s)',
                                    durationQuantity: 0,
                                    subject: `Re-Assign: ${newEvent.subject}`,
                                    description: `Reassigned task once for date: ${newerDate.toDateString()} ${newEvent.description}`,
                                    repeatInterval: "Once",
                                    status: "Assigned",
                                    assignees: this.selectedStaff,
                                    recurring: true
                                });
                                newEvent.startDate = newerDate.getTime();
                                newEvent.durationAmount = 'day(s)';
                                newEvent.durationQuantity = 0;
                                newEvent.subject = `Re-Assign: ${newEvent.subject}`;
                                newEvent.description = `Reassigned task once for date: ${newerDate.toDateString()} ${newEvent.description}`;
                                newEvent.repeatInterval = "Once";
                                newEvent.status = "Assigned";
                                newEvent.assignees = this.selectedStaff;
                                newEvent.recurring = true;
                                newEvent.completedDates = [];
                                let endDate: Date = new Date(newEvent.startDate);
                                endDate.setDate(endDate.getDate() + 1);
                                newEvent.endDate = endDate.getTime();
                                // console.log("new task", newEvent);
                                this._store.dispatch(this._sessionActions.CreateTask(newEvent));
                                setTimeout(() => {
                                    if (null != this.sessionData.user) {
                                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                        this.IsReAssigning = false;
                                        this.closeAction();
                                        this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                                    }
                                }, 700);
                            }
                        }
                    ]
                });
                
                newAlert.present();
            }
        } else {
            this._controls.ShowAlert("No One Selected", "Please choose at least one staff member to assign.", "Dismiss");
        }
    }

    public ChangeTaskDate(): void {
        if (null != this.newDate) {
            let skippedDate: string = `${this.newDate.getMonth() }_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
            // console.log("eventTimesChanged", skippedDate);
            let newAlert = this.alertCtrl.create({
                title: `Change task date?`,
                message: `Change this task's date for just this occurance?`,
                inputs: [
                    {
                        type: 'date',
                        name: 'date',
                        value: `${this.newDate.getFullYear()}-${this.newDate.getMonth() + 1}-${this.newDate.getDate()}`
                    }
                ],
                buttons: [
                    {
                        text: "Cancel",
                        role: 'cancel',
                        handler: () => {

                        }
                    },
                    {
                        text: 'Confirm',
                        handler: (data: any)=> {
                            let parts: string[];
                            if ((null != data) && (null != data.date)) {
                                // console.log("data", data.date);
                                parts = data.date.split("-", 3);
                                let newDate: Date = new Date(Number(parts[0]), Number(parts[1]) - 1, Number(parts[2]));
                                let oldEvent: ITask = Object.assign({}, this.task);
                                let oldTime: Date = new Date(this.task.startDate);
                                let skipDate = skippedDate;
                                // console.log("eventTimesChanged", skipDate);
                                if ((null != oldEvent.skipDates)) {
                                    oldEvent.skipDates.push(skipDate);
                                } else {
                                    oldEvent.skipDates = [ skipDate ];
                                }
                                let newNonSkip = `${newDate.getMonth()}_${newDate.getDate()}_${newDate.getFullYear()}`;
                                if (null != oldEvent.skipDates) {
                                    if (oldEvent.skipDates.includes(newNonSkip)) {
                                        oldEvent.skipDates = oldEvent.skipDates.filter((dte: string) => {
                                            return (dte !== newNonSkip);
                                        });
                                    }
                                }
                                // console.log("old task", oldEvent, oldEvent.startDate);
                                this._store.dispatch(this._sessionActions.UpdateTask(oldEvent));

                                let newEvent: ITask = Object.assign({}, this.task);
                                Object.assign({}, newEvent, {
                                    startDate: newDate.getTime(),
                                    durationAmount: 'day(s)',
                                    durationQuantity: 0,
                                    repeatInterval: "Once",
                                    subject: `Date Change: ${newEvent.subject}`,
                                    description: `Date Change task once for task: ${newEvent.description}`,
                                    status: "Assigned",
                                    assignees: this.task.assignees,
                                    recurring: true
                                });
                                newEvent.startDate = newDate.getTime();
                                newEvent.subject = `Date Change: ${newEvent.subject}`;
                                newEvent.description =  `Date Change task once for task: ${newEvent.description}`;
                                newEvent.durationAmount = 'day(s)';
                                newEvent.durationQuantity = 0;
                                newEvent.repeatInterval = "Once";
                                newEvent.status = "Assigned";
                                newEvent.assignees = this.task.assignees;
                                newEvent.recurring = true;
                                newEvent.skipDates = [];
                                newEvent.completedDates = [];
                                let endDate: Date = new Date(newEvent.startDate);
                                endDate.setDate(endDate.getDate() + 1);
                                newEvent.endDate = endDate.getTime();
                                // console.log("new task", newEvent);
                                this._store.dispatch(this._sessionActions.CreateTask(newEvent));
                                setTimeout(() => {
                                    if (null != this.sessionData.user) {
                                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                        this.IsReAssigning = false;
                                        this.closeAction();
                                        this._controls.ShowAlert("Successfully Change Date", "Successfully change the date the of this task", "Thanks");
                                    }
                                }, 700);
                            }
                            
                            
                        }
                    }
                ]
            });
            
            newAlert.present();
        }
    }

    public OpenImage(imageUrl: string): void {
        if (imageUrl) {
            this.photoViewer.show(imageUrl, null);
        } else {
            // Image url isnt passed in for some reason
        }
    }

    GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public SwitchEditMode(): void {
        if (this.IsEditMode && this.taskForm.dirty) {
        }
        this.IsEditMode = !this.IsEditMode;
    }

    public SelectTenant(user: IUser): void {
        if (user !== this.selectedTenant) {
            this.selectedTenant = user;
            this.taskForm.patchValue({
                clientID: user.uid,
                clientName: user.firstName + " " + user.lastName,
                clientUnit: user.unit ? user.unit : "",
                orgID: user.orgs.id,
                buildingID: user.orgs.buildings[0].id,
                buildingName: user.orgs.buildings[0].name,
            });
            this.isSearching = false;
            this.searchString = "";
        } else {
            this._controls.ShowAlert("Already Selected", "This tenant is currently selected. Please choose a different one.", "Dismiss");
        }
    }

    public GetName(user: IUser): string {
        return `${ user.firstName} ${ user.lastName }`;
    }

    public SearchTenants(event: string): void {
        if (null != event) {
            let newVal: string = event;
            // console.log("newVal", newVal);
            this._store.dispatch(this._userActions.SearchAllTenants(newVal));
            if (newVal.trim() === "") {
                this.isSearching = false;
            } else {
                this.isSearching = true;
            }
        }
    }

    public AddStaffMember(): void {
        this._store.dispatch(this._userActions.GetUsers(null));
        let modal = this.modalCtrl.create("AudienceChooserPage", { searchStaff: true, searchTenants: false, selected: this.selectedStaff, scopingTasks: false });
        modal.present();
        modal.onDidDismiss((data: any) => {
            if (null != data) {
                let user: IUser = <IUser>data;
                // console.log("Chosen person", user);
                let assignee: IAssignee = {
                    name: `${ user.firstName } ${user.lastName}`,
                    uid: user.uid
                };
                if (null != this.selectedStaff) {
                    this.selectedStaff.push(assignee);
                } else {
                    this.selectedStaff = [];
                    this.selectedStaff.push(assignee);
                }
            }
        });
    }

    public RemoveStaff(staff: IAssignee): void {
        let newAlert = this.alertCtrl.create({
            title: `Remove staff?`,
            message: "Please confirm that you want to remove: " + `${ staff.name}`,
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: () => {
                        // console.log("Cancel clicked");
                    },
                },
                {
                    text: "Remove",
                    handler: () => {
                        newAlert.present();
                        let newStaff = this.selectedStaff.filter((assignee: IAssignee) => {
                            return (assignee.uid !== staff.uid);
                        });
                        // console.log("newStaff", newStaff);
                        this.selectedStaff = newStaff;
                    },
                },
            ],
        });
        newAlert.present();
    }

    public AddTenant(): void {
        if (this.sessionData.user.isAdmin) {
            this._store.dispatch(this._userActions.GetAllTenants());
        } else {
            this._store.dispatch(this._userActions.GetAllTenantsLandlord());
        }
        let modal = this.modalCtrl.create("AudienceChooserPage", { searchStaff: false, searchTenants: true, tenant: this.selectedTenant, scopingTasks: false });
        modal.present();
        modal.onDidDismiss((data: any) => {
            // console.log("Chosen person", data);
            if (null != data) {
                let user: IUser = data;
                this.selectedTenant = user;
                this.taskForm.patchValue({
                    clientID: user.uid,
                    clientName: user.firstName + " " + user.lastName,
                    clientUnit: user.unit ? user.unit : "",
                    orgID: user.orgs.id,
                    buildingID: user.orgs.buildings[0].id,
                    buildingName: user.orgs.buildings[0].name,
                });
                this.isSearching = false;
            }
        });
    }

    public AddBuilding(): void {
        
        let options: any = {
            searchStaff: false,
            searchTenants: false,
            searchBuildings: true,
            landlordSearch: false,
            scopingTasks: false
        }
        if (this.sessionData.user.role === "Landlord") {
            options.landlordSearch = true;
            options.searchBuildings = false;
        } else {
            this._store.dispatch(this._orgActions.GetAllOrgsScope());
        }
        let modal = this.modalCtrl.create("AudienceChooserPage", options);
        modal.present();
        modal.onDidDismiss((data: any) => {
            // console.log("Chosen person", data);
            if (null != data) {
                let building: IBuildingSimple = <IBuildingSimple>data;
                this.building = building;
                this.taskForm.patchValue({
                    clientID: building.id,
                    clientName: building.name,
                    clientUnit: "",
                    orgID: building.orgID,
                    buildingID: building.id,
                    buildingName: building.name,
                });
                this.isSearching = false;
            }
        });
    }

    public GetAudience(id: string): string {
        let audience: string = Constants.STR_AUDIENCE_LANDLORD_ONLY;
        switch (id) {
            case Constants.STR_AUDIENCE_TENANT_AND_LANDLORD:
            {
                return "the tenant and landlord(s)";
            }

            case Constants.STR_AUDIENCE_TENANT_AND_STAFF:
            {
                return "the tenant and the Silver Brick team";
            }

            case Constants.STR_AUDIENCE_STAFF_ONLY:
            {
                return "the Silver Brick team";
            }

            case Constants.STR_AUDIENCE_TENANT_ONLY:
            {
                return "the tenant";
            }

            case Constants.STR_AUDIENCE_LANDLORD_ONLY:
            {
                return "the landlord(s)";
            }

            default:
            {
                return "the tenant";
            }
        }
    }

    public onChange($event: string): void {
        // console.log($event);
        let dateArray: string[] = $event.split("/", 3);
        let date: Date = new Date(Number(dateArray[2]), Number(dateArray[0]) - 1, Number(dateArray[1]));
        this.SelectedDate = date;
    }

    public SearchStaff(event: string): void {
        if (null != event) {
            let newVal: string = event;
            // console.log("newVal", newVal);
            this._store.dispatch(this._userActions.SearchAllStaff(newVal));
            if (newVal.trim() === "") {
                this.isSearchingStaff = false;
            } else {
                this.isSearchingStaff = true;
            }
        }
    }

    public onSubCategoryChange(type: string): void {
        this.selectedSubCategory = type;
    }

    public onUrgencyChange(type: string): void {
        this.selectedUrgency = type;
    }

    public onCategoryChange(state: string): void {
        this.selectedCategory = state;
    }

    public onStatusChange(state: string): void {
        this.selectedStatus = state;
    }

    public onDateSelect(event: any): void {
        // console.log("onDateSelect", event);
        this.SelectedDate = event;
    }

    public GetIconName(activity: ITaskFeed): string {
        let iconName: string = "chat";
        switch (activity.type) {
            case "update": {
                iconName = "chatboxes";
                break;
            }

            case "insert_photo": {
                iconName = "camera";
                break;
            }

            case "new": {
                iconName = "add";
                break;
            }

            case "assign": {
                iconName = "clipboard";
                break;
            }

            case "complete": {
                iconName = "checkmark";
                break;
            }

            default: {
                break;
            }
        }
        return iconName;
    }

    public onRepeatChange(repeat: any): void {
        // console.log("onRepeatChange", repeat);
        this.SelectedRepeat = repeat;
    }

    public onDurationChange(duration: string): void {
        this.SelectedDuration = duration;
    }

    public ChangeRepeatQuantity(quantity: number): void {
        this.SelectedRepeatQuantity = quantity;
    }

    public onFrequencyChange(frequency: string): void {
        this.SelectedFrequency = frequency;
        let match: number = this.FrequencyOptions.findIndex((fre: string) => {
            return (fre === frequency);
        });

        let match2: number = this.DurationOptions.findIndex((dur: string) => {
            return (dur === this.SelectedDuration);
        });

        if (match > match2) {
            this.SelectedDuration = this.DurationOptions[match];
        }
    }

    public GetPhoto(url: ITask): string {
        return (null != url) && (null != url.photoGallery) && (url.photoGallery.length > 0) ? url.photoGallery[0] : './assets/icon/icon.jpg';
    }

    public ReAssignStaff(): void {
        this.IsReAssigning = !this.IsReAssigning;
    }

    public onOrgChange(value: IFormSelctedValue): void {
        // Since I am not using the change event of the
        // select due to needing a complext value, this
        // is an optimization to only perform work if
        // the org is actually changed.
        if (value.id !== this.selectedOrgId) {
            this.selectedOrgId = value.id;
            // Extract the building list from the selected org
            const newBuildings: IBuildingSimple[] = OrgBldgUtil.Transform(this.orgs[value.index].buildings);
            this.selectedOrgBuildings = [...newBuildings];
            this._createBuildingIdNameMap();
            // Clear the building form array to avoid
            // cross org contamination
            this._resetUserFormBuildingArray();
        } else {
            // Selected org has not changed
            // Do nothing
        }
    }

    public async saveAction(): Promise<void> {
        if (this.taskForm.valid && this.taskForm.dirty) {
            const newTask: ITask = this.taskForm.value;
            newTask.status = "New";
            this.buildingID = newTask.buildingID;
            this.orgID = newTask.orgID;
            // console.log("New Task", newTask);
            if (null != newTask.clientID && newTask.clientID !== "") {
                // if ((null != newTask.assignID) && (newTask.assignName !== '')) {
                if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
                    newTask.assignees = this.selectedStaff;
                }
                if (this.IsAdmin && (null != newTask.startDate) && (null != newTask.startTime)) {
                    newTask.scheduled = true;
                    newTask.scheduling = false;
                    if (null != this.SelectedDate) {
                        // console.log("newDate", this.SelectedDate);
                        newTask.startDate = this.SelectedDate.getTime();
                    }
                }
                
                newTask.durationAmount = this.DurationOptions[3];
                newTask.durationQuantity = 1;
                newTask.repeatInterval = 'Once';

                if ((null != newTask.assignees) && (newTask.assignees.length > 0)) {
                    newTask.status = "Assigned";
                }
                newTask.recurring = false;
                let self = this;
                if (this._photoModified) {
                    this._uploadMultipleFilesTask(newTask)
                    .then(() => {
                        setTimeout(function() {
                            
                            // self._taskUploadPreviews = [];
                            // self._taskImageDataArr = [];
                        }, (this._taskImageDataArr.length * 1000));
                    });
                } else {
                    let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
                    if (!this.isNewTask) {
                        action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                    } else {
                        newTask.date = new Date(Date.now()).getTime();
                        // Create a IUser object and add it to the dialogResult
                    }
                    let dialogResponse: any = {
                        task: newTask,
                        action: action,
                    };
                    this._handleTaskDetailAction(dialogResponse);
                }
            } else {
                this._controls.ShowAlert("Missing Tenant", "Please make sure you have chosen a tenant for this task", "Dismiss");
            }
        } else {
            this.tabService.show();
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public async saveRecurringTask(): Promise<void> {
        // console.log("saveRecurringTask", this.taskForm.valid, this.taskForm.errors, this.taskForm.value);
        const newTask: ITask = this.taskForm.value;
        this.buildingID = newTask.buildingID;
        this.orgID = newTask.orgID;
        newTask.recurring = true;
        newTask.scheduled = true;
        newTask.scheduling = false;
        if (null != this.SelectedDuration) {
            newTask.durationAmount = this.SelectedDuration;
        }
            newTask.repeatInterval = this.SelectedFrequency;
        if (null != this.SelectedRepeat) {
            newTask.repeats = this.SelectedRepeat;
        }
        if (null != this.SelectedDate) {
            newTask.startDate = this.SelectedDate.getTime();
        }
        if (null != this.SelectedRepeatQuantity) {
            newTask.repeatQuantity = this.SelectedRepeatQuantity;
        }
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            newTask.assignees = this.selectedStaff;
        }
        if (null != this.SelectedDuration) {
            newTask.durationAmount = this.SelectedDuration;
        }
            newTask.repeatInterval = this.SelectedFrequency;
        if (null != this.SelectedRepeat) {
            newTask.repeats = this.SelectedRepeat;
        }
        if (null != this.SelectedDate) {
            newTask.startDate = this.SelectedDate.getTime();
        }
        if (null != this.SelectedRepeatQuantity) {
            newTask.repeatQuantity = this.SelectedRepeatQuantity;
        }
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            newTask.assignees = this.selectedStaff;
        }
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            newTask.assignees = this.selectedStaff;
        }
        // console.log("assignees", this.selectedStaff, newTask.assignees);
        if (null != this.SelectedDate) {
            // console.log("newDate", this.SelectedDate);
            newTask.startDate = this.SelectedDate.getTime();
        }
        if ((null == newTask.clientID) || (newTask.clientID === '')) {
            this._controls.ShowAlert("Missing Building", "Please choose a building", "Dismiss");
            return;
        } else if ((null == newTask.assignees) || (newTask.assignees.length === 0)) {
            this._controls.ShowAlert("Missing Staff", "Please assign this request to a staff member", "Dismiss");
            return;
        } else if ((null == newTask.startTime) || (newTask.startTime === "") || (null == newTask.endTime) || (newTask.endTime === "")) {
            this._controls.ShowAlert("Missing start/end time", "Please choose a start/end time", "Dismiss");
            return;
        } else if ((null == newTask.durationAmount) || (newTask.durationAmount === "")) {
            this._controls.ShowAlert("Missing duration", "Please choose a duration", "Dismiss");
            return;
        } else if ((null == newTask.subject) || (newTask.subject === "")) {
            this._controls.ShowAlert("Missing subject", "Please enter a subject", "Dismiss");
            return;
        } else if ((null == newTask.description) || (newTask.description === "")) {
            this._controls.ShowAlert("Missing description", "Please enter a description", "Dismiss");
            return;
        } else if ((null == newTask.category) || (newTask.category === "")) {
            this._controls.ShowAlert("Missing Category", "Please choose a category", "Dismiss");
            return;
        } else {
            if (newTask.repeats === "Custom") {
                switch (this.SelectedFrequency) {
                    case this.FrequencyOptions[0]:
                    {
                        
                        break;
                    }

                    case this.FrequencyOptions[1]:
                    {
                        newTask.monthlyWeekDays = this.monthlyWeekDays;
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    case this.FrequencyOptions[2]:
                    {
                        newTask.monthlyConvention = this.SelectedMonthlyConvention;
                        if (newTask.monthlyConvention === 'Day of month') {
                            newTask.monthlyDays = this.monthlyDays;
                            if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                                this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                                return;
                            }
                        } else {
                            newTask.monthlyNWeekDays = this.monthlyNWeekDays;
                            if ((null == newTask.monthlyNWeekDays) || (null != newTask.monthlyNWeekDays) && (newTask.monthlyNWeekDays.length === 0)) {
                                this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                                return;
                            }
                        }
                        // // console.log("monthly", newTask.monthlyNWeekDays, newTask.monthlyConvention);
                        break;
                    }

                    case this.FrequencyOptions[3]:
                    {
                        newTask.monthlyDays = [new Date(Date.now()).getDate()];
                        if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
            } else {
                let currentDate: Date = new Date(Date.now());
                let day: string = this._getDay(currentDate.getDay());
                switch (newTask.repeats) {
                    case this.RepeatOptions[1]:
                    {
                        newTask.monthlyWeekDays = [currentDate.getDay()];
                        newTask.repeatInterval = "Weekly";
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        newTask.repeatQuantity = 1;
                        break;
                    }

                    case this.RepeatOptions[2]:
                    {
                        newTask.monthlyWeekDays = [currentDate.getDay()];
                        newTask.repeatInterval = "Weekly";
                        newTask.repeatQuantity = 2;
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    case this.RepeatOptions[3]:
                    {
                        newTask.monthlyDays = [currentDate.getDate() - 1];
                        newTask.repeatInterval = "Monthly";
                        newTask.repeatQuantity = 1;
                        if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }
                    
                    
                    default:
                    {
                        break;
                    }
                }
            }
            // console.log("New Task", newTask);
            let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
            if (!this.isNewTask) {
                newTask.status = this.task.status;
                action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
            } else {
                newTask.status = "New";
                newTask.date = new Date(Date.now()).getTime();
                // Create a IUser object and add it to the dialogResult
            }
            let self = this;
            if (this._photoModified) {
                // newTask.photoURL = await this._uploadPhoto();
                this._uploadMultipleFilesTask(newTask)
                .then(() => {
                    setTimeout(function() {
                        
                    }, (this._taskImageDataArr.length * 1000));
                });
            } else {
                if (!this.isNewTask) {
                    action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                } else {
                    newTask.date = new Date(Date.now()).getTime();
                    // Create a IUser object and add it to the dialogResult
                }
                let dialogResponse: any = {
                    task: newTask,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse);
            }
        }
    }

    public DisableControls(args: boolean): void {
        this.IsSocketConnected = args;
    }

    public closeAction(): void {

        if (this.taskForm.dirty) {
            this._dismissDialog();
            this.tabService.show();
        } else {
            this._dismissDialog();
            this.tabService.show();
        }
    }

    public SendChatImage(): void {
        // Prep action sheet
        this.actionSheetCtrl
            .create({
                title: Constants.STR_SELECT_PHOTO,
                buttons: [
                    {
                        text: Constants.STR_TAKE_PHOTO,
                        handler: () => {
                            this._takeChatPhoto();
                        },
                    },
                    {
                        text: Constants.STR_CHOOSE_PHOTO,
                        handler: () => {
                            this._chooseChatPhoto();
                        },
                    },
                    {
                        text: Constants.STR_CANCEL,
                        role: "cancel",
                        handler: () => {
                            // console.log("[TRACE] WarnableDetailsPage.SendImage Destructive clicked");
                        },
                    },
                ],
            })
            .present();
    }

    public SendTaskImage(): void {
        // Prep action sheet
        this.actionSheetCtrl
            .create({
                title: Constants.STR_SELECT_PHOTO,
                buttons: [
                    {
                        text: Constants.STR_TAKE_PHOTO,
                        handler: () => {
                            this._takeTaskPhoto();
                        },
                    },
                    {
                        text: Constants.STR_CHOOSE_PHOTO,
                        handler: () => {
                            this._chooseTaskPhoto();
                        },
                    },
                    {
                        text: Constants.STR_CANCEL,
                        role: "cancel",
                        handler: () => {
                            // console.log("[TRACE] WarnableDetailsPage.SendImage Destructive clicked");
                        },
                    },
                ],
            })
            .present();
    }

    public ApproveHistory(history: ITaskFeed): void {
        let audience: string = this.GetAudience(history.audience);
        let alert: Alert = this.alertCtrl.create({
            title: 'Approve Note?',
            message: `Please confirm you want to approve this note. It will be send out to the audience of ${audience}`,
            buttons: [
                {
                    text: 'Approve',
                    handler:()=> {
                        let helper: AddMessageHelper = {
                            message: history,
                            taskID: history.taskID
                        };
                        this._store.dispatch(this._sessionActions.ApproveHistory(helper));
                        setTimeout(() => {
                            this._store.dispatch(this._sessionActions.GetTaskFeed({ orgID: history.orgID, buildingID: history.buildingID, taskID: history.taskID }));
                        }, 1000);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        alert.present();            
    }

    public UpdateHistory(history: ITaskFeed, photo: string): void {
        let audience: string = this.GetAudience(history.audience);
        
        let alert: Alert = this.alertCtrl.create({
            title: 'Delete Photo?',
            message: `Please confirm you want to delete this photo. This action cannot be undone`,
            buttons: [
                {
                    text: 'Delete',
                    handler:()=> {
                        history.photoGallery = history.photoGallery.filter((pht: string) => {
                            return (pht !== photo);
                        });
                        this.cdr.detectChanges();
                        console.log("UpdateHistory", history);
                        let helper: AddMessageHelper = {
                            message: history,
                            taskID: history.taskID
                        };
                        this._store.dispatch(this._sessionActions.UpdateHistory(helper));
                        setTimeout(() => {
                            this._store.dispatch(this._sessionActions.GetTaskFeed({ orgID: history.orgID, buildingID: history.buildingID, taskID: history.taskID }));
                        }, 1000);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        alert.present();            
    }

    public async SendText(): Promise<void> {
        if (this.IsSocketConnected) {
            // if ((null != this.newTaskFeed.message) && (this.newTaskFeed.message !== '')) {
                // Create a partial message object for the object clone
                this.newTaskFeed.timestamp = new Date(Date.now()).getTime();
                // Create a new message object from the default message object with an override of the text message
                if (((this.sessionData.user.role === 'Landlord') && (!this.task.recurring)) || ((this.sessionData.user.role === 'Staff') && (this.sessionData.user.isAdmin))) {
                    this.IsReviewing = !this.IsReviewing;
                } else {
                    this.newTaskFeed.role = this.sessionData.user.role;
                    this.newTaskFeed.category = this.task.category;
                    this.newTaskFeed.subCategory = this.task.subCategory;
                    this.newTaskFeed.type = 'update';
                    if ((this.sessionData.user.role === 'Staff') && (this.sessionData.user.isAdmin)) {
                        this.newTaskFeed.message = 'Update';
                    }

                    if (this.IsStaff) {
                        if (this.task.recurring) {
                            this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                        } else {
                            this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                        }
                    }

                    if (this.sessionData.user.role === 'Landlord') {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                    // if (this._chatPhotoModified) {
                    //     let self = this;
                    //     this.newTaskFeed.category = this.task.category;
                    //     this.newTaskFeed.subCategory = this.task.subCategory;
                    //     this.newTaskFeed.type = "insert_photo";
                    //     // this.newTaskFeed.photoURL = await this._uploadChatPhoto(this._imageData);
                    //     this._uploadMultipleFiles()
                    //     .then(() => {
                    //         setTimeout(function() {
                    //             // console.log("newTaskFeed", self.newTaskFeed);
                    //             const helper: AddMessageHelper = {
                    //                 message: self.newTaskFeed,
                    //                 taskID: self.newTaskFeed.taskID,
                    //             };
                    //             self._store.dispatch(self._sessionActions.AddTaskMessage(helper));
                    //             self.newTaskFeed.message = '';
                    //             self.IsReviewing = false;
                    //             self._finishImageSend(true);
                    //             self._uploadPreviews = [];
                    //             self._imageDataArr = [];
                    //         }, (this._imageDataArr.length * 1000));
                    //     });
                           
                    // } else {
                        const helper: AddMessageHelper = {
                            message: this.newTaskFeed,
                            taskID: this.newTaskFeed.taskID,
                        };
                        this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                        this.newTaskFeed.message = '';
                        this.IsReviewing = false;
                        this._finishImageSend(true);
                    // }
                }
            // } else {
            //     this._controls.ShowAlert('Empty message', 'Please enter a message for this update', 'Dismiss');
            // }
        } else {
            this._controls.ShowAlert('Reconnecting...', 'One moment while we connect your Platform experience', 'Thanks');
        }
    }

    public UnConfirm(): void {
        this.IsReviewing = false;
    }

    public SelectedAudience(): void {
        this.HasSelectedAudience = true;
    }

    public FormatTime(time: string): string {
        let newTime: string = time.split(":", 2)[0];
        let numberTime: number = Number(newTime);
        if (Number(newTime) > 12) {
            newTime = `${(numberTime - 12)}:${ time.split(":", 2)[1]} PM`;
            return newTime;
        } else if (Number(newTime) === 12) {
            newTime = `${(numberTime)}:${ time.split(":", 2)[1]} PM`;
            return newTime;
        } else {
            newTime = `${(numberTime)}:${ time.split(":", 2)[1]} AM`;
            return newTime;
        }
    }

    public async Confirm(): Promise<void> {
        if (this.IsStaff) {
            this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
        } else if (this.sessionData.user.isAdmin) {
            if (this.task.recurring) {
                if (this.SharedWithLandlord) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                } else {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                }
            } else {
                if (this.SharedWithTenant && this.SharedWithLandlord) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                } else if (this.SharedWithTenant && !this.SharedWithLandlord) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_ONLY;
                } else if (!this.SharedWithTenant && this.SharedWithLandlord) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                } else {
                    // this._controls.ShowAlert("Message Audience", "Please choose at least one party to share this message with (tenant or landlord)", "Dismiss");
                    // return;
                }
            }
        } else if (this.sessionData.user.role === "Landlord") {
            if (this.task.recurring) {
                this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
            } else {
                if (this.SharedWithTenant && this.SharedWithStaff) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                } else if (this.SharedWithTenant && !this.SharedWithStaff) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                } else if (!this.SharedWithTenant && this.SharedWithStaff) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                } else {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                }
            }
        } else {
            // this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
        }

        if ((this.sessionData.user.role === 'Staff') && (!this.HasSelectedAudience)) {
            this._controls.ShowAlert("Please select an audience", "You have not selected an audience to send this update to", "Dismiss");
            return;
        }

        if ((!this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff')) {
            this.newTaskFeed.approved = false;
        } else {
            this.newTaskFeed.approved = true;
        }
        // console.log("Audience", this.newTaskFeed.audience);
        this.newTaskFeed.role = this.sessionData.user.role;
        this.newTaskFeed.category = this.task.category;
        this.newTaskFeed.subCategory = this.task.subCategory;
        this.newTaskFeed.type = 'update';
        // if ((null != this._imageDataArr) && (this._imageDataArr.length > 0)) {
        //     let self = this;
        //     this.newTaskFeed.category = this.task.category;
        //     this.newTaskFeed.subCategory = this.task.subCategory;
        //     this.newTaskFeed.type = "insert_photo";
        //     // this.newTaskFeed.photoURL = await this._uploadChatPhoto(this._imageData);
        //     this._uploadMultipleFiles()
        //     .then(() => {
        //         setTimeout(function() {
        //             const helper: AddMessageHelper = {
        //                 message: self.newTaskFeed,
        //                 taskID: self.newTaskFeed.taskID,
        //             };
        //             self._store.dispatch(self._sessionActions.AddTaskMessage(helper));
        //             self.newTaskFeed.message = '';
        //             self.IsReviewing = false;
        //             self._finishImageSend(true);
        //             self._uploadPreviews = [];
        //             self._imageDataArr = [];
        //         }, (this._imageDataArr.length * 1500));
        //     });
        // } else {
            const helper: AddMessageHelper = {
                message: this.newTaskFeed,
                taskID: this.newTaskFeed.taskID,
            };
            this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
            this.newTaskFeed.message = '';
            this.IsReviewing = false;
        // }
    }

    public GetNewDate(): string {
        let text: string = '';
        let taskDate: Date = new Date(this.task.startDate);
        switch (this.task.durationAmount) {
            case this.DurationOptions[0]:
            {
                let endDate: Date = new Date(this.task.startDate);
                endDate.setDate(endDate.getDate() + Number(this.task.durationQuantity));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Daily", endDate);
                return text;
            }

            case this.DurationOptions[1]:
            {
                let endDate: Date = new Date(this.task.startDate);
                endDate.setDate(endDate.getDate() + (Number(this.task.durationQuantity) * 7));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Weekly", endDate);
                return text;
            }

            case this.DurationOptions[2]:
            {
                let date: Date = new Date(this.task.startDate);
                let currentDate = date.getDate();
                // Set to day 1 to avoid forward
                date.setDate(1);
                // Increase month by 1
                date.setMonth(date.getMonth() + Number(this.task.durationQuantity));
                // console.log("Monthly", date);
                // Get max # of days in this new month
                let daysInMonth = new Date(taskDate.getFullYear(), date.getMonth(), 0).getDate();
                // Set the date to the minimum of current date of days in month
                date.setDate(Math.min(currentDate, daysInMonth));
                // date.setMonth(date.getMonth() + task.durationQuantity);
                text = `until ${ this._getMonthDayYear(date) }`;
                // // console.log("Monthly", date);
                return text;
            }

            case this.DurationOptions[3]:
            {
                let endDate: Date = new Date(this.task.startDate);
                endDate.setFullYear(endDate.getFullYear() + Number(this.task.durationQuantity));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Yearly", endDate);
                return text;
            }

            case this.DurationOptions[4]:
            {
                text = ` forever`;
                // // console.log("Yearly", endDate);
                return text;
            }
            
            default:
            {
                break;
            }
        }
    }

    public GetYearRepeat(): string {
        let repeat: string = '';
        let date: Date = new Date(this.task.startDate);
        repeat = `the ${ this.Nth(date.getDate()) } day of ${ this.monthNames[date.getMonth()] }`;
        return repeat;
    }

    public GetHowOftenRepeat(): string {
        let repeatInterval: string = this.SelectedFrequency;
        let weekly: string  = (repeatInterval === 'Weekly') ? 'week(s)' : 'month(s)';
        let repeatQuantity: number = this.taskForm.get("repeatQuantity").value;
        if (repeatQuantity > 1) {
            return `every ${ repeatQuantity } ${ weekly }`;
        } else {
            return `${ repeatInterval.toLowerCase() }`;
        }
    }

    public ChooseDay(day: number): void {
        if (!this.monthlyWeekDays.includes(Number(day))) {
            this.monthlyWeekDays.push(day);
        } else {
            this.monthlyWeekDays = this.monthlyWeekDays.filter((days: number) => {
                return (day !== days);
            });
        }
    }

    public ChooseConvention(choice: string): void {
        this.SelectedMonthlyConvention = choice;
        this.taskForm.patchValue({
            monthlyConvention: choice
        });
    }

    public IncludesDay(day: number): boolean {
        return this.monthlyWeekDays.includes(day);
    }

    public GetDay(day: number): string {
        return this._getDay(day);
    }

    public GetWeek(): string {
        let repeat: string = ``;
        if (null == this.task) {
            if (null != this.monthlyNWeekDays) {
                let keys = Object.keys(this.monthlyNWeekDays);
                for (let key of keys) {
                    if (null != this.monthlyNWeekDays[Number(key)]) {
                        let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `;
                        for (let day of this.monthlyNWeekDays[Number(key)]) {
                            if (null != day) {
                                let dayStr: string = `${weekStr} ${this.GetDay(Number(day))}, `;
                                repeat += dayStr;
                            }
                        }
                    }
                }
            }              
        } else {
            if (null != this.task.monthlyNWeekDays) {
                let keys = Object.keys(this.task.monthlyNWeekDays);
                
                for (let key of keys) {
                    if (null != this.task.monthlyNWeekDays[Number(key)]) {
                        let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `;
                        for (let day of this.task.monthlyNWeekDays[Number(key)]) {
                            if (null != day) {
                                let dayStr: string = `${weekStr} ${this.GetDay(Number(day))}, `;
                                repeat += dayStr;
                            }
                        }
                    }
                }
            }
        }
        return repeat;
    }

    public Nth(day: number): string {
        return this._nth(day);
    }

    public ChooseMonthDay(day: number): void {
        if (!this.monthlyDays.includes(Number(day))) {
            this.monthlyDays.push(day);
        } else {
            this.monthlyDays = this.monthlyDays.filter((days: number) => {
                return (day !== days);
            });
        }
    }

    public ChooseNWeekMonthDay(day: number, week: number): void {
        if ((null != this.monthlyNWeekDays[week]) && (!this.monthlyNWeekDays[week].includes(Number(day)))) {
            this.monthlyNWeekDays[week].push(day);
        } else {
            this.monthlyNWeekDays[week] = this.monthlyNWeekDays[week].filter((days: number) => {
                return (day !== days);
            });
        }
        // // console.log("this.task", this.taskForm.valid, this.taskForm.errors);
    }

    public IncludesNWeekMonthDay(day: number, week: number): boolean {
        return (null != this.monthlyNWeekDays[week]) ? this.monthlyNWeekDays[week].includes(day) : false;
    }

    public IncludesMonthDay(day: number): boolean {
        return this.monthlyDays.includes(day);
    }


    private _dismissDialog(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.navCtrl.pop();
    }

    // Initially tried this.userFormBuildings.reset();
    // but that nulls out the values without resetting the length
    // This allows clearing of the array while keeping the binding in tact
    private _resetUserFormBuildingArray(): void {
        while (this.userFormBuildings.length > 0) {
            this.userFormBuildings.removeAt(this.userFormBuildings.length - 1);
        }
    }

    // Creates a map of the building id to building name
    // of the selected orgs. This allows us to show the
    // building name in the selected buildings list.
    private _createBuildingIdNameMap(): void {
        this.buildingIdNameMap = new Map<any, any>();
        for (const building of this.selectedOrgBuildings) {
            this.buildingIdNameMap.set(building.id, building.name);
        }
    }

    public async handleFileInput(files: FileList): Promise<void> {
        // console.log("File", files);
        let images: File[] = Array.from(files);
        this._imageDataFiles = Array.from(files);
        this._chatPhotoModified = true;
        this._isUploading = true;
        await this._uploadMultipleFilesAndroid();
        //this._uploadPreview = URL.createObjectURL(this._imageData);
        // this._setPhoto(this._imageData);
    }

    /* Helper Methods */
    public readURL(event: any): void {
        let self = this;
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: any) => {
                let _uploadPreview = <string>e.target.result;
                // // // console.log("_uploadPreview", _uploadPreview);
                self._uploadPreviews.push(<string>e.target.result);
                self._chatPhotoModified = true;
                self.cdr.detectChanges();

            };
            reader.readAsDataURL(file);
        }
    }

    public async handleFileInputTask(files: FileList): Promise<void> {
        // console.log("File", files);
        let images: File[] = Array.from(files);
        this._imageDataFiles = Array.from(files);
        this._photoModified = true;
        //this._uploadPreview = URL.createObjectURL(this._imageData);
    }

    /* Helper Methods */
    public readURLTask(event: any): void {
        let self = this;
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: any) => {
                let _uploadPreview = <string>e.target.result;
                // // // console.log("_uploadPreview", _uploadPreview);
                self._taskUploadPreviews.push(<string>e.target.result);
                self._photoModified = true;
                self.cdr.detectChanges();

            };
            reader.readAsDataURL(file);
        }
    }


    /* Helper Methods */

    public CancelImageUpload(): void {
        if (this._uploadTask) {
            this._uploadTask.cancel();
        } else {
            // Upload task not set yet
        }
        this._finishImageSend(false);
    }

    public RemoveImage(): void {
        // Remove image data
        if (this._imageData) {
            this._imageData = null;
            this._photoModified = true;
        }
        // Hide the remove button & show add button
    }

    /* Helper Methods */

    private _takeChatPhoto(): void {
        this.newTaskFeed.photoGallery = [];
        this._imageDataArr = [];
        try {
            if (this._platform.is("android")) {
                let imageOptions: CameraOptions = {
                    quality: 75,
                    allowEdit: false,
                    saveToPhotoAlbum: true,
                    encodingType: this.camera.EncodingType.PNG,
                    destinationType: this.camera.DestinationType.DATA_URL,
                    targetHeight: 1000,
                    targetWidth: 1000,
                };
                // console.log("android detected, setting camera", imageOptions);
                // Launch the camera
                this.camera.getPicture(imageOptions).then(
                    (imageData: string) => {
                        // Begin the upload process
                        // this._imageDataArr.push(imageData);
                        this._setPhoto([imageData]);
                        this._hasPendingUploadTask = true;
                    },
                    (err) => {}
                );
            } else {
                let imageOptions: CameraOptions = {
                    quality: 75,
                    targetHeight: 1000,
                    targetWidth: 1000,
                    correctOrientation: true,
                    saveToPhotoAlbum: true,
                    allowEdit: true,
                    encodingType: this.camera.EncodingType.PNG,
                    destinationType: this.camera.DestinationType.DATA_URL,
                };
                // console.log("not android detected, setting camera", imageOptions);
                // Launch the camera
                this.camera.getPicture(imageOptions).then(
                    (imageData: string) => {
                        // Begin the upload process
                        this._setPhoto([imageData]);
                    },
                    (err) => {}
                );
            }
        } catch (e) {
            console.error(e);
        }
    }

    getTimeStamp(): string {
      const date = new Date();

      return `${date.getDate()}. ${date.getMonth() +1}. ${date.getFullYear()}, ${date.getHours()}:${date.getMinutes()}`;
    }

    private _chooseChatPhoto(): void {
        this.newTaskFeed.photoGallery = [];
        this._imageDataArr = [];
        try {
            if (this._platform.is("android")) {
                let imageOptions: ImagePickerOptions = {
                    outputType: 1,
                    //here Quality of images, defaults to 100  
                    quality: 80,  
                    //here Width of an Image  
                    width: 1000,  
                    //here Height of an Image  
                    height: 1000,  
                    //here Maximum image count for selection, defaults to 15.  
                    maximumImagesCount: 15
                    //while setting a number 15 we can load 15 images in one selection. 
                }
                this.imagePicker.hasReadPermission()
                .then((hasPerm: boolean) => {
                    if (hasPerm) {
                        this.imagePicker.getPictures(imageOptions).then(
                            (results: any) => {
                                // Begin the upload process
                                // this._imageData = imageData;
                                // this._hasPendingUploadTask = true;
                                // for (let index = 0; index < results.length; index++) {  
                                    //here iam converting image data to base64 data and push a data to array value.  
                                    // this._imageDataArr.push(results[index]);
                                    this._hasPendingUploadTask = true;
                                    this._setPhoto(results);
                                // }  
                                // console.log("Image Lists", this._imageDataArr);  
                            },
                            (err) => {}
                        );
                    } else  {
                        this.imagePicker.requestReadPermission()
                        .then(() => {
                            // Launch the camera
                            this.imagePicker.getPictures(imageOptions).then(
                                (results: any) => {
                                    // Begin the upload process
                                    // this._imageData = imageData;
                                    // this._hasPendingUploadTask = true;
                                    // for (let index = 0; index < results.length; index++) {  
                                        //here iam converting image data to base64 data and push a data to array value.  
                                        // this._imageDataArr.push(results[index]);
                                        this._hasPendingUploadTask = true;
                                        this._setPhoto(results);
                                    // }  
                                    // console.log("Image Lists", this._imageDataArr);  
                                },
                                (err) => {}
                            );
                        });
                    }
                });
               
            } else {
                let imageOptions: ImagePickerOptions = {
                    outputType: 1,
                    //here Quality of images, defaults to 100  
                    quality: 80,  
                    //here Width of an Image  
                    width: 1000,  
                    //here Height of an Image  
                    height: 1000,  
                    //here Maximum image count for selection, defaults to 15.  
                    maximumImagesCount: 15
                    //while setting a number 15 we can load 15 images in one selection. 
                }
                // Launch the camera
                this.imagePicker.getPictures(imageOptions).then(
                    (results: any) => {
                        // Begin the upload process
                        // this._imageData = imageData;
                        // this._hasPendingUploadTask = true;
                        // for (let index = 0; index < results.length; index++) {  
                            //here iam converting image data to base64 data and push a data to array value.  
                        this._setPhoto(results);
                        // } 
                        // console.log("Image Lists", this._imageDataArr);  
                    },
                    (err) => {}
                );
            }
        } catch (e) {
            console.error(e);
        }
    }

    private _setPhoto(imageData: string[]): void {
        if ((null != imageData) && (imageData.length > 0)) {
            this._imageDataArr = imageData;
            // Update view with picture
            
            for (let img of imageData) {
                this.UploadPreviews.push(<string>img);
            }
        
            this._chatPhotoModified = true;
            this._uploadMultipleFiles();
        }
    }

    private _takeTaskPhoto(): void {
        try {
            if (this._platform.is("android")) {
                let imageOptions: CameraOptions = {
                    quality: 75,
                    allowEdit: false,
                    saveToPhotoAlbum: false,
                    encodingType: this.camera.EncodingType.PNG,
                    destinationType: this.camera.DestinationType.DATA_URL,
                    targetHeight: 1000,
                    targetWidth: 1000,
                };
                // console.log("android detected, setting camera", imageOptions);
                // Launch the camera
                this.camera.getPicture(imageOptions).then(
                    (imageData: string) => {
                        // Begin the upload process
                        // this._imageData = imageData;
                        this._setTaskPhoto(imageData);
                        this._hasPendingUploadTask = true;
                    },
                    (err) => {}
                );
            } else {
                let imageOptions: CameraOptions = {
                    quality: 75,
                    targetHeight: 1000,
                    targetWidth: 1000,
                    correctOrientation: true,
                    allowEdit: true,
                    encodingType: this.camera.EncodingType.PNG,
                    destinationType: this.camera.DestinationType.DATA_URL,
                };
                // console.log("not android detected, setting camera", imageOptions);
                // Launch the camera
                this.camera.getPicture(imageOptions).then(
                    (imageData: string) => {
                        // Begin the upload process
                        this._setTaskPhoto(imageData);
                    },
                    (err) => {}
                );
            }
        } catch (e) {
            console.error(e);
        }
    }

    private _chooseTaskPhoto(): void {
        try {
            if (this._platform.is("android")) {
                let imageOptions: ImagePickerOptions = {
                    outputType: 1,
                    //here Quality of images, defaults to 100  
                    quality: 80,  
                    //here Width of an Image  
                    width: 1000,  
                    //here Height of an Image  
                    height: 1000,  
                    //here Maximum image count for selection, defaults to 15.  
                    maximumImagesCount: 15
                    //while setting a number 15 we can load 15 images in one selection. 
                }
                this.imagePicker.hasReadPermission()
                .then((hasPerm: boolean) => {
                    if (hasPerm) {
                        this.imagePicker.getPictures(imageOptions).then(
                            (results: any) => {
                                // Begin the upload process
                                // this._imageData = imageData;
                                // this._hasPendingUploadTask = true;
                                for (let index = 0; index < results.length; index++) {  
                                    //here iam converting image data to base64 data and push a data to array value.  
                                    // this._imageDataArr.push(results[index]);
                                    this._hasPendingUploadTask = true;
                                    this._setPhoto(results[index]);
                                }  
                                // console.log("Image Lists", this._imageDataArr);  
                            },
                            (err) => {}
                        );
                    } else  {
                        this.imagePicker.requestReadPermission()
                        .then(() => {
                            // Launch the camera
                            this.imagePicker.getPictures(imageOptions).then(
                                (results: any) => {
                                    // Begin the upload process
                                    // this._imageData = imageData;
                                    // this._hasPendingUploadTask = true;
                                    for (let index = 0; index < results.length; index++) {  
                                        //here iam converting image data to base64 data and push a data to array value.  
                                        // this._imageDataArr.push(results[index]);
                                        this._hasPendingUploadTask = true;
                                        this._setTaskPhoto(results[index]);
                                    }  
                                    // console.log("Image Lists", this._imageDataArr);  
                                },
                                (err) => {}
                            );
                        });
                    }
                });

            } else {
                let imageOptions: ImagePickerOptions = {
                    outputType: 1,
                    //here Quality of images, defaults to 100  
                    quality: 80,  
                    //here Width of an Image  
                    width: 1000,  
                    //here Height of an Image  
                    height: 1000,  
                    //here Maximum image count for selection, defaults to 15.  
                    maximumImagesCount: 15
                    //while setting a number 15 we can load 15 images in one selection. 
                }
                this.imagePicker.getPictures(imageOptions).then(
                    (results: any) => {
                        // Begin the upload process
                        // this._imageData = imageData;
                        // this._hasPendingUploadTask = true;
                        for (let index = 0; index < results.length; index++) {  
                            //here iam converting image data to base64 data and push a data to array value.  
                            // this._imageDataArr.push(results[index]);
                            this._hasPendingUploadTask = true;
                            this._setTaskPhoto(results[index]);
                        }  
                        // console.log("Image Lists", this._imageDataArr);  
                    },
                    (err) => {}
                );
            }
        } catch (e) {
            console.error(e);
        }
    }
    

    private _setTaskPhoto(imageData: string): void {
        // Set image data for later upload
        this._taskImageDataArr.push(imageData);
        // Update view with picture
        this._photoModified = true;
        this._taskUploadPreviews.push(<string>imageData);
    }

    private async _uploadChatPhoto(imageData: string): Promise<string> {
        // console.log("_uploadChatPhoto");
        return new Promise<string>((resolve, reject) => {
            if (!imageData) {
                // Return empty string
                // Set in updatedProfile object to clear image
                resolve("");
            } else {
                // Set the upload preview
                // Show the upload UI
                this.IsUploading = true;
                // Generate filename and remove dashes
                // Generate full file path
                const fileName: string = null != this.task ? this.task.id + StringUtil.GenerateUUID() + Date.now() : StringUtil.GenerateUUID() + Date.now();
                let taskID: string = null != this.task ? this.task.id : StringUtil.GenerateUUID() + Date.now();
                const fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
                // Start image upload process and get upload task
                const contentType: string = "image.png";
                const format: string = "base64";
                this._uploadTask = this.firebaseStorage.Upload(fullPath, imageData, format, contentType);
                // Enable the cancel button now that the upload task is available
                this.CanCancelUpload = true;
                // Listen for state changes, error, completion of the upload
                this._uploadTask.on(
                    storage.TaskEvent.STATE_CHANGED,
                    (snapshot: any) => {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        // this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    },
                    (error) => {
                        let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                        switch (error.name) {
                            case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                                // User doesn't have permission to access the object
                                message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                                break;
                            }

                            case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                                // User canceled the upload
                                message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                                break;
                            }

                            default: {
                                break;
                            }
                        }
                        this._controls.ShowAlert(error.name, message, "Ok");
                        reject(message);
                    },
                    () => {
                        this.UploadPreview = this._uploadTask.snapshot.downloadURL;
                        // Update UI with finishing and remove cancel button
                        this.IsUploadDone = true;
                        this.CanCancelUpload = false;
                        this._photoModified = false;
                        this._isUploading = false;
                        // Create a partial message object for the message clone
                        // // Create a partial message object for the message clone
                        resolve(this._uploadTask.snapshot.downloadURL);
                        // Update the photoURL path
                    }
                );
            }
        });
    }

    private async _uploadPhoto(): Promise<string> {
        // console.log("_uploadPhoto", this._uploadPreview);
        return new Promise<string>((resolve, reject) => {
            if (!this._imageData) {
                // Return empty string
                // Set in updatedProfile object to clear image
                resolve("");
            } else {
                // Set the upload preview
                // Show the upload UI
                this.IsUploading = true;
                // Generate filename and remove dashes
                // Generate full file path
                const fileName: string = (null != this.task) ? this.task.id + StringUtil.GenerateUUID() + Date.now() : StringUtil.GenerateUUID() + Date.now();
                let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
                const fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
                // Start image upload process and get upload task
                const contentType: string = "image.png";
                const format: string = "base64";
                this._uploadTask = this.firebaseStorage.Upload(fullPath, this._imageData, format, contentType);
                // Enable the cancel button now that the upload task is available
                this.CanCancelUpload = true;
                // Listen for state changes, error, completion of the upload
                this._uploadTask.on(
                    storage.TaskEvent.STATE_CHANGED,
                    (snapshot: any) => {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        // this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    },
                    (error) => {
                        let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                        switch (error.name) {
                            case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                                // User doesn't have permission to access the object
                                message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                                break;
                            }

                            case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                                // User canceled the upload
                                message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                                break;
                            }

                            default: {
                                break;
                            }
                        }
                        this._controls.ShowAlert(error.name, message, "Ok");
                        reject(message);
                    },
                    () => {
                        this.UploadPreview = this._uploadTask.snapshot.downloadURL;
                        // Update UI with finishing and remove cancel button
                        this.IsUploadDone = true;
                        this.CanCancelUpload = false;
                        // Create a partial message object for the message clone
                        this.newTaskFeed.photoURL = fullPath;
                        // Create a new message object from the default message object with an override of the text message
                        // const helper: AddMessageHelper = {
                        //     message: this.newTaskFeed,
                        //     taskID: this.newTaskFeed.taskID,
                        // };
                        // this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                        // // Create a partial message object for the message clone
                        // this._finishImageSend(true);
                        resolve(this._uploadTask.snapshot.downloadURL);
                        // Update the photoURL path
                    }
                );
            }
        });
    }

    public async _uploadMultipleFiles(): Promise<void> {
        const promises: any[] = [];
        let self = this;
        let count: number = (this._imageDataArr.length - 1);
        this._imageDataArr.forEach(file => {
            this.IsUploading = true;
            let fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
            // console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            // console.log("file", file);
            let uploadTask: storage.UploadTask = this.firebaseStorage.Upload(fullPath, file, format, contentType);
            // // console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, (snapshot: any) => {
               let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            (error: any) => {
                let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                switch (error.name) {
                    case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED:
                    {
                        // User doesn't have permission to access the object
                        message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                        break;
                    }

                    case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                        // User canceled the upload
                        message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
                this._controls.ShowAlert(error.name, message, "Ok");
            },
            () => {
                this._uploadUrls.push(uploadTask.snapshot.downloadURL);
                this.newTaskFeed.photoGallery.push(uploadTask.snapshot.downloadURL);
                
                // Update UI with finishing and remove cancel button
                // console.log("uploadUrls", this.newTaskFeed.photoGallery);
                // Create a partial message object for the message clone
                // // Create a partial message object for the message clone
                // Update the photoURL path
            });
        });
        Promise.all(promises)
            .then(() => {
                this.newTaskFeed.role = this.sessionData.user.role;
                this.newTaskFeed.category = this.task.category;
                this.newTaskFeed.subCategory = this.task.subCategory;
                if (this.IsStaff) {
                    if (this.task.recurring) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                    }
                }
                if (this.sessionData.user.role === 'Landlord') {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                }
                this.newTaskFeed.type = "insert_photo";
                const helper: AddMessageHelper = {
                    message: self.newTaskFeed,
                    taskID: self.newTaskFeed.taskID,
                };
                self._store.dispatch(self._sessionActions.AddTaskMessage(helper));
                self.newTaskFeed.message = '';
                self.IsReviewing = false;
                self._finishImageSend(true);
                self._uploadPreviews = [];
                self._imageDataArr = [];
                this.newTaskFeed.photoGallery = [];
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                // console.log("Upload done");
            })
            .catch(err =>  console.log(err));
    }

    public async _uploadMultipleFilesTask(task: ITask): Promise<void> {
        const promises: any[] = [];
        let count: number = (this._taskImageDataArr.length - 1);
        this._taskImageDataArr.forEach(file => {
            this.IsUploading = true;
            let fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
            // console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            // console.log("file", file);
            let uploadTask: storage.UploadTask = this.firebaseStorage.Upload(fullPath, file, format, contentType);
            // // console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, (snapshot: any) => {
               let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            (error: any) => {
                let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                switch (error.name) {
                    case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED:
                    {
                        // User doesn't have permission to access the object
                        message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                        break;
                    }

                    case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                        // User canceled the upload
                        message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
                this._controls.ShowAlert(error.name, message, "Ok");
            },
            () => {
                this._taskUploadUrls.push(uploadTask.snapshot.downloadURL);
                if (null == task.photoGallery) {
                    task.photoGallery = [];
                }
                task.photoGallery.push(uploadTask.snapshot.downloadURL);
                // this.newTaskFeed.photoGallery.push(uploadTask.snapshot.downloadURL);
                // Update UI with finishing and remove cancel button
                // console.log("uploadUrls", this._taskUploadUrls);
                // Create a partial message object for the message clone
                // // Create a partial message object for the message clone
                // Update the photoURL path
            });
        });
        Promise.all(promises)
            .then(() => {
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                // console.log("Upload done");
                let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
                if (!this.isNewTask) {
                    action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                } else {
                    task.date = new Date(Date.now()).getTime();
                    // Create a IUser object and add it to the dialogResult
                }
                let dialogResponse: any = {
                    task: task,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse);
                this._finishImageSend(true);
            })
            .catch(err => console.log(err));
    }

    public async _uploadMultipleFilesAndroid(): Promise<void> {
        const promises: any[] = [];
        this._imageDataFiles.forEach((file) => {
            this.IsUploading = true;
            let fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
            // // console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            let uploadTask: storage.UploadTask = this.firebaseStorage.UploadFile(fullPath, file);
            // // console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(
                storage.TaskEvent.STATE_CHANGED,
                (snapshot: any) => {
                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                },
                (error: any) => {
                    let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                    switch (error.name) {
                        case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                            // User doesn't have permission to access the object
                            message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                            break;
                        }

                        case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                            // User canceled the upload
                            message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                    this._controls.ShowAlert(error.name, message, "Ok");
                },
                () => {
                    this._uploadUrls.push(uploadTask.snapshot.downloadURL);
                    this.newTaskFeed.photoGallery.push(uploadTask.snapshot.downloadURL);
                    
                    // Update UI with finishing and remove cancel button
                    // console.log("uploadUrls", this.newTaskFeed.photoGallery);
                    // Create a partial message object for the message clone
                    // // Create a partial message object for the message clone
                    // Update the photoURL path
                }
            );
        });
        let self = this;
        Promise.all(promises)
            .then(() => {
                this.newTaskFeed.role = this.sessionData.user.role;
                this.newTaskFeed.category = this.task.category;
                this.newTaskFeed.subCategory = this.task.subCategory;
                if (this.IsStaff) {
                    if (this.task.recurring) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                    }
                }
                if (this.sessionData.user.role === 'Landlord') {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                }
                this.newTaskFeed.type = "insert_photo";
                const helper: AddMessageHelper = {
                    message: self.newTaskFeed,
                    taskID: self.newTaskFeed.taskID,
                };
                self._store.dispatch(self._sessionActions.AddTaskMessage(helper));
                self.newTaskFeed.message = '';
                self.IsReviewing = false;
                self._finishImageSend(true);
                self._uploadPreviews = [];
                self._imageDataArr = [];
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                // console.log("Upload done");
                // // console.log("Upload done");
            })
            .catch((err: any) => {

                // // console.log("Error _uploadMultipleFiles", err);
            });
    }

    public async _uploadMultipleFilesAndroidTask(task: ITask): Promise<void> {
        const promises: any[] = [];
        let self = this;
        this._imageDataFiles.forEach((file) => {
            this.IsUploading = true;
            let fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
            // // console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            let uploadTask: storage.UploadTask = this.firebaseStorage.UploadFile(fullPath, file);
            // // console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(
                storage.TaskEvent.STATE_CHANGED,
                (snapshot: any) => {
                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                },
                (error: any) => {
                    let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                    switch (error.name) {
                        case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                            // User doesn't have permission to access the object
                            message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                            break;
                        }

                        case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                            // User canceled the upload
                            message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                    this._controls.ShowAlert(error.name, message, "Ok");
                },
                () => {
                    this._taskUploadUrls.push(uploadTask.snapshot.downloadURL);
                    if (null == task.photoGallery) {
                        task.photoGallery = [];
                    }
                    task.photoGallery.push(uploadTask.snapshot.downloadURL);
                    // this.newTaskFeed.photoGallery.push(uploadTask.snapshot.downloadURL);
                    // Update UI with finishing and remove cancel button
                    // console.log("uploadUrls", this._taskUploadUrls);
                    // Create a partial message object for the message clone
                    // // Create a partial message object for the message clone
                    // Update the photoURL path
                }
            );
        });
        Promise.all(promises)
            .then(() => {
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                // console.log("Upload done");
                let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
                if (!this.isNewTask) {
                    action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                } else {
                    task.date = new Date(Date.now()).getTime();
                    // Create a IUser object and add it to the dialogResult
                }
                let dialogResponse: any = {
                    task: task,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse);
                this._finishImageSend(true);
                let inputImageMultiple = <HTMLInputElement>document.getElementById("inputImageMultiple");
                if (null != inputImageMultiple) {
                    inputImageMultiple.value = "";
                }

                // // console.log("Upload done");
            })
            .catch((err: any) => {

                // // console.log("Error _uploadMultipleFiles", err);
            });
    }


    private _finishImageSend(isDone: boolean): void {
        // Update UI with done
        this.UploadStatus = isDone ? "Done!" : "Canceled!";
        // keep the done UI for 1 second
        setTimeout(() => {
            // Hides all upload UI elements
            // Re-enables the image upload button
            this.IsUploading = false;
            // Reset the image done flag and status
            this.IsUploadDone = false;
            this.UploadStatus = Constants.STR_FINISHING;
            // Resets the progress bar
            this.ProgressPercent = 0;
            // Reset upload preview
            this.UploadPreview = null;
            // Clear upload task
            this._imageDataFiles = [];
            this.newTaskFeed.photoGallery = [];

            this._uploadTask = null;
            this._photoModified = false;
            this._chatPhotoModified = false;
            this._uploadPreviews = [];
            this._imageDataArr = [];
            this._taskUploadPreviews = [];
            this._taskImageDataArr = [];
            this._taskUploadUrls = [];
        }, 1000);
    }

    private _subscribeToSocketConnect(): void {
        this.events.subscribe(
            Constants.EVENT_SOCKET_DISCONNECTED,
            (args: boolean) => {
                this.DisableControls(args);
                if (args) {
                    if (this.TaskToggle === 'timeline') {
                        let helper: ITaskFeedJoin = {
                            orgID: this.task.orgID,
                            buildingID: this.task.buildingID,
                            taskID: this.task.id,
                        };
                        this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
                    }
                }
            }
        );
    }

    private _getDay(day: number): string {
        switch (day) {
            case 0:
            {
                return 'Sunday';
            }

            case 1:
            {
                return 'Monday';
            }

            case 2:
            {
                return 'Tuesday';
            }

            case 3:
            {
                return 'Wednesday';
            }

            case 4:
            {
                return 'Thursday';
            }

            case 5:
            {
                return 'Friday';
            }

            case 6:
            {
                return 'Saturday';
            }
            
            default:
            {
                break;
            }
        }
    }

    private _getMonthDayYear(curDate: Date): string {
        let newDate: string = "";
        newDate = `${this.monthNames[curDate.getMonth() + 1]} ${curDate.getDate()}${ this._nth(curDate.getDate()) }, ${curDate.getFullYear()}`;
        return newDate;
    }

    private _nth(d: number): string {
      if (d > 3 && d < 21) {
          return 'th';
      } else {
          switch (d % 10) {
            case 1:
            {
                return "st";
            }

            case 2:
            {
                return "nd";
            }

            case 3:
            {
                return "rd";
            }

            default:
            {
                return "th";
            }
          }
      }
    }

    private _weekCount(m: moment.Moment) {
        return m.week() - moment(m).startOf("month").week();
    }

    private _handleTaskDetailAction(response: any) {
        this.SessionState$.skip(2)
            .take(1)
            .subscribe((currentSessionState) => {
                if (!currentSessionState.error) {
                    // this.navCtrl.pop();
                } else {
                    // There was an error, stay on the page
                }
            });
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                this._store.dispatch(this._sessionActions.CreateTask(response.task));
                setTimeout(() => {
                    if (null != this.sessionData.user) {
                        if (!this.sessionData.user.isAdmin) {
                            if (this.sessionData.user.role === "Tenant") {
                                this._store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                this.navCtrl.pop();
                                this.tabService.show();
                            } else if (this.sessionData.user.role === "Landlord") {
                                
                                this._store.dispatch(this._userActions.GetAllTenantsLandlord());
                                this.navCtrl.pop();
                                this.tabService.show();
                            } else {

                                let bldgHelper: IGetBldgHelper = {
                                    orgID: this.sessionData.org.id,
                                    buildingID: this.sessionData.org.buildings[0].id,
                                };
                                this._store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                this._store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                this.navCtrl.pop();
                                this.tabService.show();
                            }
                        } else {
                            this._store.dispatch(this._sessionActions.GetUsersScope(null));
                            this._store.dispatch(this._orgActions.GetAllOrgsScope());
                            this._store.dispatch(this._userActions.GetAllTenants());
                            this.navCtrl.pop();
                            this.tabService.show();
                        }
                   }
               }, 500);
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                this._store.dispatch(this._sessionActions.UpdateTask(response.task));
                setTimeout(() => {
                    if (null != this.sessionData.user) {
                        if (!this.sessionData.user.isAdmin) {
                            if (this.sessionData.user.role === "Tenant") {
                                this._store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                this.navCtrl.pop();
                                this.tabService.show();
                            } else if (this.sessionData.user.role === "Landlord") {
                                this._store.dispatch(this._userActions.GetAllTenantsLandlord());
                                this.navCtrl.pop();
                                this.tabService.show();
                            } else {

                                let bldgHelper: IGetBldgHelper = {
                                    orgID: this.sessionData.org.id,
                                    buildingID: this.sessionData.org.buildings[0].id,
                                };
                                this._store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                this._store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                this.navCtrl.pop();
                                this.tabService.show();
                            }
                        } else {
                            this._store.dispatch(this._sessionActions.GetUsersScope(null));
                            this._store.dispatch(this._orgActions.GetAllOrgsScope());
                            this._store.dispatch(this._userActions.GetAllTenants());
                            this.navCtrl.pop();
                            this.tabService.show();
                        }
                    }
                }, 500);
                break;
            }

            default:
            {
                break;
            }
        }
    }
}

