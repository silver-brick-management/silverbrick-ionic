// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules
import { FirebaseError, User } from "firebase";
import * as firebase from "firebase";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";

import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { IonicPage, NavController, NavParams, ModalController, AlertController, Alert, Platform } from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { NgForm } from "@angular/forms";
import { KeychainTouchId } from "@ionic-native/keychain-touch-id";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { Controls } from "../../providers/Controls";
import { ErrorType } from "../../interfaces/IError";

// Local Modules
import { ICredentials } from "../../shared/IParams";
import { IAuthState, AuthStateType } from "../../interfaces/IStoreState";
import { AppState } from "../../app/app.state";
import { AuthActions } from "../../store/actions/AuthActions";
import { PushService } from "../../providers/push-provider";
import { Constants } from "../../common/Constants";
import { ErrorMapper } from "../../common/ErrorMapper";
import { UserFactoryType, UserTypeFactory } from "../../factories/UserTypeFactory";
import { ISilverBrickUser } from "../../shared/SilverBrickTypes";
import { FirebaseAuth } from "../../services/FirebaseAuth";

declare const gapi: any;
@IonicPage({
    priority: "high",
})
@Component({
    selector: "page-login",
    templateUrl: "login.html",
})
export class LoginPage {
    private _allSubscription: Subscription;
    private touchIdAvailable: boolean = false;
    private hasKey: boolean = false;
    private _authStateSubscription: Subscription;
    public authState$: Observable<IAuthState>;
    public alert: Alert;
    public login: ICredentials = {
        email: "",
        password: "",
    };

    public submitted = false;
    public isLoggingIn: boolean = false;

    get IsTouchIdAvailable(): boolean {
        return this.touchIdAvailable && this.hasKey;
    }

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private _angularFireAuth: AngularFireAuth,
        private _firebaseAuth: FirebaseAuth,
        private _keyboard: Keyboard,
        public authActions: AuthActions,
        private _angularFireDatabase: AngularFireDatabase,
        private _alertController: AlertController,
        public store: Store<AppState>,
        public controls: Controls,
        private _platform: Platform,
        public pushService: PushService,
        public mdlCtrl: ModalController,
        private keychainTouchId: KeychainTouchId) {
        this.authState$ = this.store.select((state) => state.authState);
        this._allSubscription = null;
    }

    ionViewDidLoad(): void {
        console.log("ionViewDidLoad LoginPage");
        this._allSubscription = this.authState$.first().subscribe((authState: IAuthState) => {
            if (authState.type === AuthStateType.LOGIN_FAILED) {
                if (null != authState.error) {
                    // if (null == this.alert) {
                    console.log("Error", authState.error);
                    let title: string = "Login failed!";
                    let message: string = authState.error.Message;
                    if (null != authState.error && null != authState.error.Type && authState.error.Type === ErrorType.AUTH_USER_NOT_FOUND) {
                        title = "Login Error";
                        message = "Please try again";
                    }
                    let alert: Alert = this._alertController.create({
                        title: title,
                        message: message,
                        buttons: [
                            {
                                text: "OK",
                                handler: () => {},
                            },
                        ],
                    });
                    alert.present();
                }
                // }
            } else {
            }
        });

        this._platform.ready().then((readySource: string) => {
            this._isTouchAvailable();
            try {
                if (this.keychainTouchId.has(Constants.SILVERBRICK_TOUCH_ID_KEY)) {
                    this.hasKey = true;
                } else {
                    this.hasKey = false;
                }
            } catch (error) {
                console.error("Error", error);
            }
            if ("cordova" === readySource) {
                // Best-effort
                this.pushService.Initialize(null, true);
            } else {
                // Nothing to do
            }
        });
        if (this._platform.is("cordova")) {
            // this._keyboard.hideFormAccessoryBar(false);
        }
    }

    ngOnDestroy(): void {
        this.alert = null;
        if (this._allSubscription != null) {
            this._allSubscription.unsubscribe();
            this._allSubscription = null;
        }
        if (this._authStateSubscription != null) {
            this._authStateSubscription.unsubscribe();
            this._authStateSubscription = null;
        }
    }

    public SignIn(form: NgForm): void {
        this.submitted = true;
        // console.log("form", form.value, this.login);
        if (form.valid) {
            if (this._platform.is("cordova")) {
                this.keychainTouchId
                    .save(Constants.SILVERBRICK_TOUCH_ID_KEY, this.login.email + "_" + this.login.password)
                    .then(() => {
                        this.store.dispatch(this.authActions.LoginFirebase(this.login));
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            } else {
                this.store.dispatch(this.authActions.LoginFirebase(this.login));
            }
        } else {
            // Login form isnt valid yet
            // Do nothing
        }
    }

    public forgotSignIn(): void {
        const forgotModal = this.mdlCtrl.create("EmailRecoveryPage");
        forgotModal.present();
    }

    public SignUpEmail(): void {
        this.navCtrl.push("AccountInfoPage");
    }

    public SwitchLoggingIn(): void {
        this.isLoggingIn = !this.isLoggingIn;
    }

    public onSignIn(googleUser: any): void {
        let profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Do not send to your backend! Use an ID token instead.
        console.log("Name: " + profile.getName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail()); // This is null if the 'email' scope is not present.
    }

    public SignUp() {
        console.log("starting sign up process");
        const signUpModal = this.mdlCtrl.create("ProfileSelectionPage");
        signUpModal.present();
    }

    public onForgotPassword(): void {
        let alert: Alert = this._alertController.create({
            title: "Reset Your Password?",
            message: "Please enter your e-mail address to reset your password.",
            inputs: [
                {
                    name: "email",
                    placeholder: "E-mail Address",
                    type: "email",
                },
            ],
            buttons: [
                {
                    text: "Send",
                    handler: (data) => {
                        if (data === "" || null == data) {
                        }
                        this._sendForgotPasswordEmail(data.email);
                    },
                },
                {
                    text: "Cancel",
                    handler: (data) => {},
                },
            ],
        });

        alert.present();
    }

    public startTouchID(): void {
        console.log("startTouchID");
        let alert = this._alertController.create({
            title: "No password stored!",
            message: "You have to have signed in with an email and password once before using this!",
            buttons: [
                {
                    text: "Dismiss",
                    role: "cancel",
                },
            ],
        });

        if (this.touchIdAvailable) {
            this.keychainTouchId
                .verify(Constants.SILVERBRICK_TOUCH_ID_KEY, "Verifying fingerprint....")
                .then((password: string) => {
                    let components: string[] = password.split("_", 2);
                    // We hit the LOADING state, navigate to entry page, to handle rest of the flow
                    console.log("Navigating to entry page");
                    this.navCtrl.setRoot("EntryPage");
                    this.login.email = components[0];
                    this.login.password = components[1];
                    this.store.dispatch(this.authActions.LoginFirebase(this.login));
                })
                .catch((error) => {
                    alert.present();
                    console.error("startTouchID ", error);
                });
        } else {
            alert.present();
            console.log("Touch ID Not available");
        }
    }

    private async _sendForgotPasswordEmail(email: string): Promise<void> {
        try {
            await this._firebaseAuth.SendForgotPasswordEmail(email);
            let alert: Alert = this._alertController.create({
                title: "Success!",
                message: "Please check your e-mail inbox for a password reset e-mail.",
                buttons: [
                    {
                        text: "Ok",
                        handler: (data) => {},
                    },
                    {
                        text: "Cancel",
                        handler: (data) => {
                            alert.dismiss();
                        },
                    },
                ],
            });
            alert.present();
        } catch (error) {
            const _error: Error = error;
            const _message: string = _error.message;

            let alert: Alert = this._alertController.create({
                title: "Reset Your Password?",
                message: "Please make sure a valid e-mail address is entered, and that internet access is available.",
                buttons: [
                    {
                        text: "Ok",
                        handler: (data) => {},
                    },
                ],
            });
            alert.present();
        }
    }

    private _isTouchAvailable(): void {
        if (this._platform.is("cordova")) {
            this.keychainTouchId
                .isAvailable()
                .then((res: any) => {
                    console.log("isAvailable", res);
                    this.touchIdAvailable = true;
                })
                .catch((error: any) => {
                    console.error("is not Available", error);
                    this.touchIdAvailable = false;
                });
        } else {
            this.touchIdAvailable = false;
        }
    }
}
