// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild, Input } from "@angular/core";
import { NavController, Events, AlertController, MenuController, Platform, NavParams, Modal, Content, IonicPage, ViewController } from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { PhotoViewer } from "@ionic-native/photo-viewer";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IOrgBasic, IAssignee, IUser, IUserOrg, IOrgInfo, IBuildingSimple, ITask } from "../../shared/SilverBrickTypes";
import { IOrgState, IUserState, UserStateType, ISessionState, SessionStateType, IAuthState, OrgStateType } from "../../interfaces/IStoreState";
import { OrgActions } from "../../store/actions/OrgActions";
import { IFormSelctedValue, IGetBldgHelper } from "../../shared/IParams";
import { SessionData } from "../../providers/SessionData";
import { Controls } from "../../providers/Controls";
import { UserActions } from "../../store/actions/UserActions";
import { TabsService } from '../../providers/TabsService';
import { AssignTaskHelper } from "../../shared/ResponseTypes";

import { SessionActions } from "../../store/actions/SessionActions";

import { OrgBldgUtil } from "../../shared/OrgBldgUtil";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-audience-chooser",
    templateUrl: "audience-chooser.html",
})
export class AudienceChooserPage {
    @ViewChild(Content) content: Content;
    private _screenSize: number = window.screen.width;
    private _sessionStateSubscription: Subscription;
    private _userStateSubscription: Subscription;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgSearch: string = "";
    public searchRef: string = "";
    public hasBuildings: number = 0;
    public authState$: Observable<IAuthState>;
    public UsersState$: Observable<IUserState>;
    public SessionState$: Observable<ISessionState>;
    public tenants$: Observable<IUser[]>;
    public usersScope$: Observable<IUser[]>;
    public orgID: string = "";
    public buildingID: string = "";
    public staffUsers$: Observable<Array<IUser>>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public TaskToggle: string = 'new';
    public RequestsTitle: string = 'Picker';
    public SearchingStaff: boolean = false;
    public SearchingTenant: boolean = false;
    public SelectedStaff: IAssignee[] = [];
    public ScopingTasks: boolean = false;
    public IsLoading: boolean = false;
    public SelectedTenant: IUser = null;
    public SearchingBuildings: boolean = false;
    public LandlordSearch: boolean = false;

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Staff'));
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Tenant'));
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    public orgHasBuildings(org: IOrgBasic): boolean {
        if (org.buildings.length > 0 && null != org.buildings) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    constructor(
        public store: Store<AppState>,
        public photoViewer: PhotoViewer,
        public viewCtrl: ViewController,
        public sessionData: SessionData,
        public tabService: TabsService,
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        private _platform: Platform,
        private _keyboard: Keyboard,
        public controls: Controls,
        private _sessionActions: SessionActions,
        private _orgActions: OrgActions,
        private _userActions: UserActions,
        public orgActions: OrgActions) {
        this.staffUsers$ = this.store.select((state) => state.usersState.searchStaff);
        this.tenants$ = this.store.select((state) => state.usersState.searchTenants);
        this.usersScope$ = this.store.select((state) => state.sessionState.usersScope);
        this.UsersState$ = this.store.select((state) => state.usersState);
        this.SessionState$ = this.store.select((state) => state.sessionState);
        this.error$ = this.store.select((state) => state.orgState.error);
        this.loading$ = this.store.select((state) => state.sessionState.loading);
        this.TaskToggle = 'new';
        this.AllBuildings$ = this.store.select((state) => state.orgState.searchBuildings);
    }

    ionViewWillEnter(): void {
        this.IsLoading = true;
        if (this._platform.is("cordova")) {
            // this._keyboard.hideFormAccessoryBar(false);
        }
        let params: any = this.navParams.data;
        console.log("params", params);
        if (null != params) {
            this.SearchingStaff = params.searchStaff;
            this.SearchingTenant = params.searchTenants;
            this.ScopingTasks = params.scopingTasks;
            this.SearchingBuildings = params.searchBuildings;
            this.LandlordSearch = params.landlordSearch;
            if (this.SearchingStaff) {
                this.SelectedStaff = params.selected;
                this.RequestsTitle = 'Select Staff Member';
            }
            if (this.SearchingTenant) {
                this.SelectedTenant = params.tenant;
                this.RequestsTitle = 'Select A Tenant';
            }

            if (this.ScopingTasks) {
                this.RequestsTitle = 'Staff Schedule';
            }

            if (this.SearchingBuildings || this.LandlordSearch) {
                this.RequestsTitle = 'Select a Building';
            }
        }
        if (this.SearchingStaff) {
            this._userStateSubscription = this.UsersState$
            .subscribe((state: IUserState) => {
                if (null != state) {
                    switch(state.type) {
                        case UserStateType.GET_USERS_SUCCEEDED:
                        case UserStateType.SCOPE_USERS_SUCCEEDED:
                        {
                            this.IsLoading = false;
                            break;
                        }

                        default:
                        {
                            setTimeout(() => {
                                this.IsLoading = false;
                            }, 1000);
                            break;
                        }
                    }
                }
            });
        }

        if (!this.SearchingStaff) {
            this._sessionStateSubscription = this.SessionState$
            .subscribe((state: ISessionState) => {
                if (null != state) {
                    switch(state.type) {
                        case SessionStateType.GET_USERS_SUCCEEDED:
                        case SessionStateType.SCOPE_USERS_SUCCEEDED:
                        {
                            this.IsLoading = false;
                            break;
                        }

                        default:
                        {
                            setTimeout(() => {
                                this.IsLoading = false;
                            }, 1000);
                            break;
                        }
                    }
                }
            });
        }
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {

    }

    public CloseAction(user: IUser): void {
        this._cleanUp();
        this.viewCtrl.dismiss(user);
    }

    public Dismiss(): void {
        this._cleanUp();
        this.viewCtrl.dismiss();
    }

    public scopeStaff(user: IUser): void {
        let assignee: IAssignee = {
            name: `${user.firstName} ${user.lastName}`,
            uid: user.uid
        };
        if (null != user) {
            this.store.dispatch(this._sessionActions.ScopeStaffTasks(user));
            this._cleanUp();
            this.viewCtrl.dismiss();
        } else {
            this.controls.ShowAlert('Already Selected', 'This staff member is already selected', 'Dismiss');
        }
    }

    public chooseStaff(user: IUser): void {
        let assignee: IAssignee = {
            name: `${user.firstName} ${user.lastName}`,
            uid: user.uid
        };
        let match: IAssignee = this.SelectedStaff.find((assign: IAssignee) => {
            return (assign.uid === user.uid);
        });
        if (((null != this.SelectedStaff) && (null == match)) || ((null != this.SelectedStaff) && (this.SelectedStaff.length === 0))) {
            this.CloseAction(user);
        } else {
            this.controls.ShowAlert('Already Selected', 'This staff member is already selected', 'Dismiss');
        }
    }

    public chooseBuilding(user: IBuildingSimple): void {
        this._cleanUp();
        this.viewCtrl.dismiss(user);
    }

    public chooseTenant(user: IUser): void {
        if ((null != this.SelectedTenant) && (this.SelectedTenant.uid !== user.uid) || (null == this.SelectedTenant)) {
            this.CloseAction(user);
        } else {
            this.controls.ShowAlert('Already Selected', 'This tenant is already selected', 'Dismiss');
        }
    }

    public GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public SearchUsers(event: string): void {
        if (null != event) {
            let newVal: string = event;
            console.log("newVal", newVal);
            if (this.SearchingTenant) {
                this.store.dispatch(this._userActions.SearchAllTenants(newVal));
            } else if (this.SearchingStaff) {
                this.store.dispatch(this._userActions.SearchAllStaff(newVal));
            } else if (this.ScopingTasks) {
                this.store.dispatch(this._sessionActions.ScopeStaff(newVal));
            } else if (this.SearchingBuildings) {
                this.store.dispatch(this._orgActions.SearchBuildings(newVal));
            }
        }
    }

    private _cleanUp(): void {
        this.SearchingStaff = false;
        this.SearchingTenant = false;
        this.SelectedStaff = [];
        this.SelectedTenant = null;
        this.store.dispatch(this._sessionActions.CleanUpSearch());
        this.store.dispatch(this._userActions.CleanUpSearch());
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }

        if (null != this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
        this.store.dispatch(this._orgActions.CleanUpSearch());
    }
}

