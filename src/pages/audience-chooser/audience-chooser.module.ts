// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudienceChooserPage } from './audience-chooser';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'

@NgModule({
  declarations: [
    AudienceChooserPage,
  ],
  imports: [
    LoadingComponentModule,
    IonicPageModule.forChild(AudienceChooserPage),
  ],
  exports: [
  	AudienceChooserPage
  ]
})
export class AudienceChooserPageModule {}
