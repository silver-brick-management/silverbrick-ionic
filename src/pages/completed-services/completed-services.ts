// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import * as moment from "moment";
import { Component, ViewChild, Input, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { NavController, Events, Alert, FabContainer, AlertController, MenuController, Modal, Content, IonicPage, ModalController } from "ionic-angular";
import { Observable, Subject, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { CalendarMode, Step } from "ionic2-calendar/calendar";
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay, CalendarWeekViewEvent } from "angular-calendar";
import { CalendarModal, CalendarModalOptions, DayConfig, CalendarResult, CalendarComponentOptions, CalendarComponent } from "ion2-calendar";

import { getMonth, startOfMonth, startOfWeek, startOfDay, endOfWeek, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from "date-fns";
import RRule from "rrule";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants, colors } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IOrgBasic, BeforeCalendarLoadMonth, BeforeCalendarLoadWeek, RecurringEvent, IUser, IOrgInfo, IBuildingSimple, ITask } from "../../shared/SilverBrickTypes";
import { IOrgState, IUserState, ISessionState, IAuthState, OrgStateType, SessionStateType } from "../../interfaces/IStoreState";
import { OrgActions } from "../../store/actions/OrgActions";
import { IFormSelctedValue, IGetBldgHelper } from "../../shared/IParams";
import { SessionData } from "../../providers/SessionData";
import { TabsService } from "../../providers/TabsService";
import { AssignTaskHelper } from "../../shared/ResponseTypes";
import { SessionActions } from "../../store/actions/SessionActions";
import { UserActions } from "../../store/actions/UserActions";
import { UserUtil } from '../../common/UserUtil';
import { OrgBldgUtil } from "../../shared/OrgBldgUtil";
import { Operator } from "rxjs/Operator";
export declare type WeekdayStr = "SU" | "MO" | "TU" | "WE" | "TH" | "FR" | "SA";

declare module "rxjs/Subject" {
    interface Subject<T> {
        lift<R>(operator: Operator<T, R>): Observable<R>;
    }
}

@IonicPage({ priority: "high" })
@Component({
    selector: "page-completed-services",
    templateUrl: "completed-services.html",
})
export class CompletedServicesPage {
    @ViewChild("inputCalendar") input: HTMLElement;
    @ViewChild(Content) content: Content;
    private _orgStateSubscription: Subscription;
    private _authSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    private _isSocketConnected: boolean = true;
    private _sessionStateSubscription: Subscription;

    public tasks$: Observable<Array<ITask>>;
    // public progressTasks$: Observable<Array<ITask>>;
    // public newTasks$: Observable<Array<ITask>>;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public hasBuildings: number = 0;
    public authState$: Observable<IAuthState>;
    public orgID: string = "";
    public buildingID: string = "";
    public staffUsers$: Observable<Array<IUser>>;
    public userState$: Observable<IUserState>;
    public RequestsTitle: string = "Your Requests";
    public SessionState$: Observable<ISessionState>;
    public selectedStaff$: Observable<IUser>;
    public eventSource: any;
    public viewTitle: any;
    public TasksScope: string = "All";


    public allEvents: any[] = [];
    public isNewTask: boolean = false;
    public refresh: Subject<any> = new Subject();

    public orgHasBuildings(org: IOrgBasic): boolean {
        if (org.buildings.length > 0 && null != org.buildings) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    get IsStaff(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Staff";
    }

    get IsNotTenant(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role !== "Landlord" && this.sessionData.user.role !== "Tenant";
    }

    get IsLandLord(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Landlord";
    }

    get IsAdmin(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.isAdmin;
    }

    get IsTenant(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Tenant";
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsSocketConnected(): boolean {
        return this._isSocketConnected;
    }

    set IsSocketConnected(socketConnected: boolean) {
        this._isSocketConnected = socketConnected;
    }

    constructor(
        public store: Store<AppState>,
        public photoViewer: PhotoViewer,
        public modalCtrl: ModalController,
        public sessionData: SessionData,
        public events: Events,
        public tabService: TabsService,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private _sessionActions: SessionActions,
        private _orgActions: OrgActions,
        private _userActions: UserActions,
        private cdr: ChangeDetectorRef,
        public orgActions: OrgActions) {
        this.staffUsers$ = this.store.select((state) => state.usersState.users);
        this.error$ = this.store.select((state) => state.orgState.error);
        this.tasks$ = this.store.select((state) => state.sessionState.completedTasks);
        this.loading$ = this.store.select((state) => state.sessionState.loading);
        this.orgState$ = this.store.select((state) => state.orgState);
        this.authState$ = this.store.select((state) => state.authState);
        this.userState$ = this.store.select((state) => state.usersState);
        this.selectedStaff$ = this.store.select((state) => state.sessionState.selectedStaff);
        this.SessionState$ = this.store.select((state) => state.sessionState);
    }

    ionViewDidLoad(): void {
        
        // this.store.dispatch(this.orgActions.GetAllOrgsScope());
        this._authSubscription = this.authState$.subscribe((state: IAuthState) => {
            if (null != state) {
                if (null != state.profile) {
                    if (this.IsNotTenant) {
                        this.RequestsTitle = "Requests";
                    }
                    
                }
            }
        });
        
        this._subscribeToSocketConnect();
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }

        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }
    }

    // public ShowMarkComplete(task: ITask): boolean {
    //     return (task.status === "Assigned");
    // }

    public GetStaffName(user: IUser): string {
        if (null != user) {
            return `${ user.firstName } ${ user.lastName }`;
        } else {
            return '';
        }
    }

    public ScopeStaffTasks(): void {
        
        let modal = this.modalCtrl.create("AudienceChooserPage", { searchStaff: false, searchTenants: false, selected: null, scopingTasks: true });
        modal.present();
    }

    public OpenFullImage(task: ITask): void {
        if (null != task.photoURL && task.photoURL !== "") {
            this.photoViewer.show(task.photoURL);
        }
    }

    public expandTask(task: ITask = null, fab?: FabContainer): void {
        if (null != fab) {
            fab.close();
        } else {
            // don't close fab
        }
        const modal: Modal = this.modalCtrl.create("TasksDetailsPage", { task: task, isEditMode: false, recurring: false });
        modal.present();
        this.tabService.hide();
        // this.navCtrl.push("TasksDetailsPage", {task: task, isEditMode: false });
    }

    public newRecurringTask(fab?: FabContainer): void {
        if (null != fab) {
            fab.close();
        } else {
            // don't close fab
        }
        const modal: Modal = this.modalCtrl.create("TasksDetailsPage", { task: null, isEditMode: false, recurring: true });
        modal.present();
        this.tabService.hide();
        // this.navCtrl.push("TasksDetailsPage", {task: task, isEditMode: false });
    }

    public searchByName(): void {
        if (this.orgSearch.trim()) {
        } else {
            // No string detected
            // Do nothing
        }
    }

    public DisableControls(args: boolean): void {
        this.IsSocketConnected = args;
    }

    // public ShowProgress(task: ITask): boolean {
    //     return (task.status === "Assigned");
    // }

    public ChangeScope(scope: string): void {
        // console.log("scope", scope);
        this.store.dispatch(this._sessionActions.ScopeBuildingTasks(this.TasksScope));
    }

    public StartProgress(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Begin Request?",
            message: "Please confirm you want to begin this request.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.status = "In Progress";
                        this.store.dispatch(this._sessionActions.StartTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    // this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetTasks());
                                this.store.dispatch(this._userActions.GetUsers(null));
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public MarkComplete(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Complete Request?",
            message: "Please confirm you want to mark this request as completed.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.active = false;
                        task.status = "Completed";
                        let fullDate: Date = new Date(Date.now());
                        let date: string = `${fullDate.getMonth() + 1}_${fullDate.getDate()}_${fullDate.getFullYear()}`;
                        if (null != task.completedDates) {
                            task.completedDates.push(date);
                        } else {
                            task.completedDates = [];
                            task.completedDates.push(date);
                        }
                        this.store.dispatch(this._sessionActions.CompleteTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    // this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetTasks());
                                this.store.dispatch(this._userActions.GetUsers(null));
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public ReOpen(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Re-Open Request?",
            message: "Please confirm you want to re-open this request.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.active = true;
                        task.status = "In Progress";
                        this.store.dispatch(this._sessionActions.ReopenTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    // this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetTasks());
                                this.store.dispatch(this._userActions.GetUsers(null));
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public IsCompleted(task: ITask): boolean {
        let fullDate: Date = new Date(Date.now());
        let date: string = `${fullDate.getMonth() + 1}_${fullDate.getDate()}_${fullDate.getFullYear()}`;
        // console.log("IsCompleted", date, task, (null != task.completedDates) && (task.completedDates.length > 0) && (task.completedDates.includes(date)));
        if ((null != task.completedDates) && (task.completedDates.length > 0) && (task.completedDates.includes(date))) {
            return true;
        } else {
            return false;
        }
    }

    private _subscribeToSocketConnect(): void {
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (args: boolean) => {
            this.DisableControls(args);
            if (args) {
                // if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                //     if (this.sessionData.user.role === "Tenant") {
                //         this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                //     } else {
                //         this.orgID = this.sessionData.org.id;
                //         this.buildingID = this.sessionData.org.buildings[0].id;
                //         let bldgHelper: IGetBldgHelper = {
                //             orgID: this.sessionData.org.id,
                //             buildingID: this.sessionData.org.buildings[0].id,
                //         };
                //         // console.log("bldgHelper", bldgHelper);
                //         this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                //         this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                //     }
                // } else {
                //     this.store.dispatch(this._sessionActions.GetTasks());
                //     this.store.dispatch(this._userActions.GetUsers(null));
                // }
            }
        });
    }

    private _getDay(day: number): string {
        switch (day) {
            case 0: {
                return "Sunday";
            }

            case 1: {
                return "Monday";
            }

            case 2: {
                return "Tuesday";
            }

            case 3: {
                return "Wednesday";
            }

            case 4: {
                return "Thursday";
            }

            case 5: {
                return "Friday";
            }

            case 6: {
                return "Saturday";
            }

            default: {
                break;
            }
        }
    }

    private _nth(d: number): string {
        if (d > 3 && d < 21) {
            return "th";
        } else {
            switch (d % 10) {
                case 1: {
                    return "st";
                }

                case 2: {
                    return "nd";
                }

                case 3: {
                    return "rd";
                }

                default: {
                    return "th";
                }
            }
        }
    }

    private _weekCount(m: moment.Moment) {
        return m.week() - moment(m).startOf("month").week();
    }

    private _nthDayOfMonth(monthMoment: moment.Moment, day: number, weekNumber: number): string {
        let m = monthMoment.clone().startOf("month").day(day);
       if (m.month() !== monthMoment.month()) m.add(7, "d");
        return m.add(7 * (weekNumber - 1), "d").format("YYYY-MM-DD");
    }
}
