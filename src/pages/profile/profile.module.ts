// // ****************************************************************
// //
// // This file is subject to the terms and conditions defined in file
// // 'LICENSE.text', which can be found at the root of this project.
// //
// // ****************************************************************

// import { NgModule } from '@angular/core';
// import { IonicPageModule } from 'ionic-angular';
// import { ProfilePage } from './profile';
// import { AdaptableTextLabelComponentModule } from '../../components/adaptable-text-label/adaptable-text-label.module';

// @NgModule({
//   declarations: [
//     ProfilePage,
//   ],
//   imports: [
//     AdaptableTextLabelComponentModule,
//     IonicPageModule.forChild(ProfilePage),
//   ],
//   exports: [
//     ProfilePage
//   ]
// })
// export class ProfilePageModule { }
