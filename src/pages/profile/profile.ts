// // ****************************************************************
// //
// // This file is subject to the terms and conditions defined in file
// // 'LICENSE.text', which can be found at the root of this project.
// //
// // ****************************************************************
// // Node Modules
// import { storage } from "firebase";
// import { Store } from "@ngrx/store";
// import { Subscription, Observable } from "rxjs";
// import { Component } from "@angular/core";
// import { NgForm } from '@angular/forms';
// import { Camera, CameraOptions } from "@ionic-native/camera";
// import {
//     IonicPage,
//     NavParams,
//     NavController,
//     ViewController,
//     AlertController,
//     ActionSheetController,
//     ModalController,
//     Events,
//     Platform,
//     Alert
// } from "ionic-angular";
// import { Keyboard } from "@ionic-native/keyboard";
// import { ICard, cards } from "stripe";
// import { PopoverController, PopoverCmp } from 'ionic-angular';

// // Local Modules
// import { SessionData } from "../../providers/SessionData";
// import {
//     ISilverBrickUser,
//     IProfile,
//     IUser,
//     IAccountInfo,
//     IProfileUser
// } from "../../shared/SilverBrickTypes";
// import { AppState } from "../../app/app.state";
// import { Constants, CARD_LIST, STATE_LIST, EXP_MONTH, EXP_YEAR } from "../../common/Constants";
// import {
//     IAuthState,
//     AuthStateType,
//     ISessionState,
//     SessionStateType
// } from "../../interfaces/IStoreState";
// import { AuthActions } from "../../store/actions/AuthActions";
// import {
//     UserProfileMode,
//     IUserProfileNavigationArgs
// } from "../../interfaces/IUserProfileNavigationArgs";
// import { FirebaseStorage } from "../../services/FirebaseStorage";
// import { SessionActions } from "../../store/actions/SessionActions";
// import { TransactionalObject } from "../../common/TransactionalObject";
// import { Controls } from "../../providers/Controls";
// import { AccountInfoFactory } from "../../factories/AccountInfoFactory";
// import { ArrayUtil } from "../../shared/ArrayUtil";
// import { NumUtil } from "../../shared/NumUtil";
// import { UserUtil } from "../../common/UserUtil";
// import { IMessageExpandParams } from "../../shared/IParams";
// import { MessageActions } from "../../store/actions/MessageActions";
// @IonicPage({
//     priority: "off"
// })
// @Component({
//     selector: "page-profile",
//     templateUrl: "profile.html"
// })
// export class ProfilePage {
//     private _shippingInfo: IAccountInfo = new AccountInfoFactory();
//     private _user: IUser;
//     private _userObjectTransaction: TransactionalObject<IUser>;
//     private _paymentInfo: ICard[] = [];
//     private _userID: string;
//     private _canEdit: boolean;
//     private _imageData: string;
//     private _profilePic: string;
//     private _isEditMode: boolean;
//     private _photoModified: boolean;
//     private _hasProfilePhoto: boolean;
//     private _updatedSettings: boolean = false;
//     private _bio: string;
//     private _authSubscription: Subscription;
//     private _userProfileMode: UserProfileMode;
//     private _sessionStateSubscription: Subscription;
//     private _accountInfo: IAccountInfo = new AccountInfoFactory();
//     private _isSocketConnected: boolean = true;

//     public CardInfo: cards.ISourceCreationOptionsExtended;
//     public ProfileToggle: string = "account";
//     public Loading$: Observable<boolean>;
//     public PostLoad$: Observable<boolean>;
//     public SessionState$: Observable<ISessionState>;
//     public IsDeleteCardMode: boolean = false;
//     public IsAddingCard: boolean = false;
//     // public LovedPosts$: Observable<(IPost | IMediaTrack | IMediaPicture)[]>;
//     public profile$: Observable<IUser>;
//     public viewFix: string = null; // used for extra margin when viewing someone else's profile
//     public IsFromDashboard: boolean = true;
//     public isBlocked: boolean = false;
//     public AuthState$: Observable<IAuthState>;
//     public NewCard: any = {
//         exp_month: "01",
//         exp_year: `${ new Date(Date.now()).getFullYear() }`,
//         number: "",
//         cvc: ""
//     };
//     public States: string[] = STATE_LIST;
//     public ExpMonth: string[] = EXP_MONTH;
//     public ExpYear: string[] = EXP_YEAR;
//     public CreditCardTypes: string[] = CARD_LIST;
//     public expYrOptions = {
//         title: 'Expiry Year',
//         subTitle: 'Select an expiring year below',
//         mode: 'md'
//     };

//     public expMonthOptions = {
//         title: 'Expiry Month',
//         subTitle: 'Select an expiring month below',
//         mode: 'md'
//     };
//     public stateOptions = {
//         title: 'Select State',
//         subTitle: 'Select a state below',
//         mode: 'md'
//     };

//     get ShippingInfo(): IAccountInfo {
//         return this._shippingInfo;
//     }

//     set ShippingInfo(info: IAccountInfo) {
//         this._shippingInfo = info;
//     }
//     get ViewingMine(): boolean {
//         return this._userProfileMode !== UserProfileMode.VIEW_USER;
//     }
//     get User(): IUser {
//         return this._user;
//     }

//     get CanEdit(): boolean {
//         return this._canEdit;
//     }
//     set CanEdit(canEdit: boolean) {
//         this._canEdit = canEdit;
//     }
//     get ProfilePic(): string {
//         return null != this._user && this._profilePic != null
//             ? this._profilePic
//             : Constants.DEFAULT_PROFILE_PHOTO_PATH;
//     }
//     set ProfilePic(profilePic: string) {
//         this._profilePic = profilePic;
//     }

//     get IsEditMode(): boolean {
//         return this._isEditMode;
//     }
//     get HasProfilePhoto(): boolean {
//         return this._hasProfilePhoto;
//     }
//     set HasProfilePhoto(hasProfilePhoto: boolean) {
//         this._hasProfilePhoto = hasProfilePhoto;
//     }
//     get IsSocketConnected(): boolean {
//         return this._isSocketConnected;
//     }
//     set IsSocketConnected(connected: boolean) {
//         this._isSocketConnected = connected;
//     }
//     get BlockedButtonURL(): string {
//         return "assets/icon/UnblockButton.png";
//     }

//     get PaymentInfo(): ICard[] {
//         return this._paymentInfo;
//     }

//     get Username(): string {
//         return (null != this._user) ? this._user.firstName + " " + this._user.lastName : "Profile";
//     }

//     get IsValidAddress(): boolean {
//         return UserUtil.ValidAddress({
//             line1: this.ShippingInfo.shipping.address.line1,
//             line2: this.ShippingInfo.shipping.address.line2,
//             city: this.ShippingInfo.shipping.address.city,
//             state: this.ShippingInfo.shipping.address.state,
//             zip: this.ShippingInfo.shipping.address.postal_code
//         });
//     }

//     constructor(
//         private _keyboard: Keyboard,
//         private _platform: Platform,
//         public camera: Camera,
//         public navCtrl: NavController,
//         public navParams: NavParams,
//         public store: Store<AppState>,
//         public viewCtrl: ViewController,
//         public authActions: AuthActions,
//         public alertCtrl: AlertController,
//         public firebaseStorage: FirebaseStorage,
//         public mdlCtrl: ModalController,
//         public actionSheetCtrl: ActionSheetController,
//         public sessionData: SessionData,
//         public controls: Controls,
//         public sessionActions: SessionActions,
//         public messageActions: MessageActions,
//         public events: Events
//     ) {
//         // Init view variables
//         this.HasProfilePhoto = false;
//         this.ProfileToggle = "account";
//         this.ProfilePic = Constants.DEFAULT_PROFILE_PHOTO_PATH;
//         // Init private variables
//         this._canEdit = false;
//         this._isEditMode = false;
//         this._photoModified = false;
//         this._imageData = null;
//         this._updatedSettings = false;
//         // Init User Info
//         this.SessionState$ = this.store.select(state => state.sessionState);
//         this.AuthState$ = this.store.select(state => state.authState);
//         this.profile$ = this.store.select(state => state.authState.profile);
//     }

//     ionViewWillEnter(): void {
//         const args: IUserProfileNavigationArgs = this.navParams.data;
//         this._userProfileMode = args.Mode;
//         this._user = this.sessionData.user;
//         this._userID = this.sessionData.userID;
//         this._profilePic = this.sessionData.user.fullPhotoURL ? this.sessionData.user.fullPhotoURL : Constants.DEFAULT_PROFILE_PHOTO_PATH;
//         if (args.UserID === this.sessionData.userID) {
//             // if on user's profile
//             this._userProfileMode = UserProfileMode.EDIT;
//         }
//         if (this._platform.is("cordova")) {
//             this._keyboard.hideFormAccessoryBar(false);
//         } else {
//             // keyboard is a cordova plugin & this is
//             // not a cordova platform.
//         }
//         this._shippingInfo.shipping.address = {
//             line1: this.sessionData.user.address ? this.sessionData.user.address.line1 : "",
//             line2: this.sessionData.user.address ? this.sessionData.user.address.line2 : "",
//             city: this.sessionData.user.address ? this.sessionData.user.address.city : "",
//             state: this.sessionData.user.address ? this.sessionData.user.address.state : "",
//             postal_code: this.sessionData.user.address ? this.sessionData.user.address.zip : "",
//             country: "US"
//         };
//         this._sessionStateSubscription = this.SessionState$.subscribe(
//             (state: ISessionState) => {
//                 if (null != state) {
//                     switch (state.type) {
//                         case SessionStateType.GET_CUSTOMER_CARDS_SUCCESS: {
//                             console.log(
//                                 "[TRACE] shopping-cart.ts: GET_CUSTOMER_CARDS_SUCCESS"
//                             );
//                             this.controls.DismissLoading();
//                             break;
//                         }

//                         case SessionStateType.ADD_CARD_SUCCESS: {
//                             console.log(
//                                 "[TRACE] shopping-cart.ts: ADD_CARD_SUCCESS"
//                             );
//                             this.controls.ShowAlert(
//                                 "Payment Info Update Success!",
//                                 "Successfully update your payment info",
//                                 "Thanks"
//                             );
//                             this.controls.DismissLoading();
//                             break;
//                         }

//                         case SessionStateType.DELETE_CARD_SUCCESS: {
//                             console.log(
//                                 "[TRACE] shopping-cart.ts: ADD_CARD_SUCCESS"
//                             );
//                             this.controls.ShowAlert(
//                                 "Payment Info Update Success!",
//                                 "Successfully update your payment info",
//                                 "Thanks"
//                             );
//                             this.controls.DismissLoading();
//                             break;
//                         }

//                         default: {
//                             setTimeout(() => {
//                                 this.controls.DismissLoading();
//                             }, 2000);
//                             break;
//                         }
//                     }
//                 }
//             }
//         );

//         this._authSubscription = this.AuthState$.subscribe(
//             (state: IAuthState) => {
//                 if (null != state) {
//                     switch (state.type) {
//                         case AuthStateType.UPDATE_MY_PROFILE_SUCCEEDED: {
//                             if (this._updatedSettings) {
//                                 this.sessionData.user.address = {
//                                     line1: state.profile.address.line1,
//                                     line2: state.profile.address.line2,
//                                     city: state.profile.address.city,
//                                     state: state.profile.address.state,
//                                     zip: state.profile.address.zip
//                                 };
//                                 this._shippingInfo.shipping.address = {
//                                     line1: state.profile.address.line1,
//                                     line2: state.profile.address.line2,
//                                     city: state.profile.address.city,
//                                     state: state.profile.address.state,
//                                     postal_code: state.profile.address.zip,
//                                     country: "US"
//                                 };
//                                 this._updatedSettings = false;
//                                 setTimeout(() => {
//                                     this.controls.DismissLoading();
//                                     this.alertCtrl
//                                         .create({
//                                             title:
//                                                 "Profile update successfully!",
//                                             buttons: ["Thanks"]
//                                         })
//                                         .present();
//                                 }, 2000);
//                             }
//                             break;
//                         }

//                         default: {
//                             setTimeout(() => {
//                                 this.controls.DismissLoading();
//                             }, 2000);
//                             break;
//                         }
//                     }
//                 }
//             }
//         );
//         this._userObjectTransaction = TransactionalObject.CreateInstance(<IUser>this.sessionData.user);
//         this._user = this._userObjectTransaction.Get();
//         this._user = this.sessionData.user;
//         // this._paymentInfo = this.sessionData.cards;
//         this._userID = this.sessionData.userID;
//         if (this._platform.is("cordova")) {
//             this._keyboard.hideFormAccessoryBar(false);
//         } else {
//             // keyboard is a cordova plugin & this is
//             // not a cordova platform.
//         }
//         this.events.subscribe(Constants.EVENT_UPDATE_CARDS, () => {
//             this.controls.DismissLoading();
//             // this.controls.ShowAlert(
//             //     "Payment Info Update Success!",
//             //     "Successfully update your payment info",
//             //     "Thanks"
//             // );
//             // this._paymentInfo = this.sessionData.cards;
//         });
//         this.events.subscribe(
//             Constants.EVENT_UPDATE_CARDS_FAILED,
//             (args: any) => {
//                 this.controls.DismissLoading();
//                 this.controls.ShowAlert(
//                     "Payment Info Update Error!",
//                     args.message,
//                     "Thanks"
//                 );
//             }
//         );
//         // this.store.dispatch(this.sessionActions.GetCustomerCards());
//     }

//     ionViewWillLeave(): void {
//         if (null != this._authSubscription) {
//             this._authSubscription.unsubscribe();
//             this._authSubscription = null;
//         }
//         if (null != this._sessionStateSubscription) {
//             this._sessionStateSubscription.unsubscribe();
//             this._sessionStateSubscription = null;
//         }
//         this._isEditMode = false;
//         this.IsAddingCard = false;
//         this.IsDeleteCardMode = false;
//         this.events.unsubscribe(Constants.EVENT_VIEWING_DASH_PROFILE);
//         // this._userObjectTransaction.Reset();
//         this.events.unsubscribe(Constants.EVENT_SOCKET_DISCONNECTED);
//     }

//     ngOnDestroy(): void {}

//     public AddImage(): void {
//         // Prep action sheet
//         this.actionSheetCtrl
//             .create({
//                 title: Constants.STR_SELECT_PHOTO,
//                 buttons: [
//                     {
//                         text: Constants.STR_TAKE_PHOTO,
//                         handler: () => {
//                             this._takePhoto();
//                         }
//                     },
//                     {
//                         text: Constants.STR_CHOOSE_PHOTO,
//                         handler: () => {
//                             this._choosePhoto();
//                         }
//                     },
//                     {
//                         text: Constants.STR_CANCEL,
//                         role: "cancel",
//                         handler: () => {
//                             console.log("AddImage. Cancel clicked");
//                         }
//                     }
//                 ]
//             })
//             .present();
//     }

//     public RemoveImage(): void {
//         // Remove image data
//         if (this._imageData) {
//             this._imageData = null;
//         }
//         // Hide the remove button & show add button
//         this._photoModified = true;
//         this.HasProfilePhoto = false;
//         this.ProfilePic = Constants.DEFAULT_PROFILE_PHOTO_PATH;
//     }

//     public CancelEdits(): void {
//         if (null != this._userObjectTransaction) {
//             this._userObjectTransaction.Reset();
//             this._user = this._userObjectTransaction.Get();
//         }
//         this.IsDeleteCardMode = false;
//         this.IsAddingCard = false;
//         this._isEditMode = false;
//     }

//     public GoBack(): void {
//         this.navCtrl.pop();
//     }

//     public Logout(): void {
//         let newAlert = this.alertCtrl.create({
//             title: "Are you sure you want to Log Out?",
//             message: "Please make sure you do.",
//             buttons: [
//                 {
//                     text: "Cancel",
//                     role: "cancel",
//                     handler: () => {
//                         console.log("Cancel clicked");
//                     }
//                 },
//                 {
//                     text: "Log Out",
//                     handler: () => {
//                         // Conduct the logout action, (it auto navigates)
//                         this.events.publish(Constants.EVENT_LOGOUT);
//                         this.navCtrl.setRoot("EntryPage");
//                         this.store.dispatch(this.authActions.Logout());
//                     }
//                 }
//             ]
//         });
//         newAlert.present();
//     }

//     public ShowEditMode(): void {
//         this.viewCtrl.showBackButton(false);
//         this._isEditMode = true;
//     }

//     public GetIconName(card: string): string {
//         let cardName: string = "visa";
//         switch (card) {
//             case CARD_LIST[0]: {
//                 card = "visa";
//                 break;
//             }

//             case CARD_LIST[1]: {
//                 card = "mastercard";
//                 break;
//             }

//             case CARD_LIST[2]: {
//                 card = "amex";
//                 break;
//             }

//             case CARD_LIST[3]: {
//                 card = "discover";
//                 break;
//             }

//             default: {
//                 break;
//             }
//         }
//         return Constants.DEFAULT_CARD_PHOTO_PATH + card + ".png";
//     }

//   public SwitchDeleteMode(): void {
//     this.IsAddingCard = false;
//     this._isEditMode = false;
//     this.IsDeleteCardMode = !this.IsDeleteCardMode;
//   }

//   public SwitchAddCardMode(): void {
//     this.IsDeleteCardMode = false;
//     this._isEditMode = false;
//     this.IsAddingCard = !this.IsAddingCard;
//   }

//   public DeleteCard(card: ICard): void {
//     console.log("Delete Card");
//     let alert = this.alertCtrl.create({
//       title: 'Delete this ' + card.brand + ' card?',
//       message: 'Make sure you really want to delete it.',
//       buttons: [
//         {
//           role: 'cancel',
//           text: 'Cancel'
//         },
//         {
//           text: 'Delete It',
//           handler: () => {
//             this.controls.ShowLoading();
//             this.store.dispatch(this.sessionActions.DeleteCard(card.id));
//             this.IsDeleteCardMode = false;
//           }
//         }
//       ]

//     });
//     alert.present();
//   }

//   public SaveCard(card: NgForm): void {
//     if ((!card.invalid) || (this.NewCard.cvc === "") || (this.NewCard.number === "")) {
//       this.CardInfo = {
//         object: "card",
//         exp_month: Number(this.NewCard.exp_month),
//         exp_year: Number(this.NewCard.exp_year),
//         number: this.NewCard.number,
//         cvc: this.NewCard.cvc
//       };
//       console.log("Save Card", this.NewCard);
//       this.store.dispatch(this.sessionActions.AddCard(this.CardInfo));
//       this.IsAddingCard = false;
//       this.controls.ShowLoading();
//     } else {
//       this.controls.ShowAlert("Missing Info!", "Please enter all required information", "Will Do");
//     }
//   }

//     public async SaveProfile(): Promise<void> {
//         if (this.IsSocketConnected) {
//             //console.log("Saving", this._accountInfo, this._user);
//             try {
//                 this.controls.ShowLoading();
//                 this._userObjectTransaction.Commit();
//                 this._updatedSettings = true;
//                 console.log("Save", this.ShippingInfo, this.User.address);
//                 this.User.address = {
//                     line1: this.ShippingInfo.shipping.address.line1,
//                     line2: this.ShippingInfo.shipping.address.line2,
//                     city: this.ShippingInfo.shipping.address.city,
//                     state: this.ShippingInfo.shipping.address.state,
//                     zip: this.ShippingInfo.shipping.address.postal_code
//                 };
//                 const updatedProfile: IProfile = {
//                     uid: this._user.uid,
//                     photoURL: this._user.photoURL,
//                     firstName: this._user.firstName,
//                     lastName: this._user.lastName,
//                     address: this._user.address
//                 };
//                 if (this._photoModified) {
//                     // Upload photo and update photo url
//                     // or clear photo url
//                     updatedProfile.photoURL = await this._uploadPhoto();
//                     this._photoModified = false;
//                     this._isEditMode = false;
//                     this.IsAddingCard = false;
//                     this.IsDeleteCardMode = false;
//                 } else {
//                     // No photo added. Continue...
//                 }
//                 this.store.dispatch(this.authActions.UpdateMyProfile(updatedProfile));
//             } catch (error) {
//                 console.error("Error", error);
//                 this.controls.DismissLoading();
//                 this.alertCtrl
//                     .create({
//                         title: "Profile Save Upload failure",
//                         subTitle: error.message,
//                         buttons: ["Ok"]
//                     })
//                     .present();
//             }
//             this._isEditMode = false;
//             // setTimeout(() => {
//             //     this.controls.DismissLoading();
//             //     this.alertCtrl
//             //         .create({
//             //             title: "Profile update successfully!",
//             //             buttons: ["Thanks"]
//             //         })
//             //         .present();
//             // }, 2000);
//         } else {
//             this.controls.ShowAlert(
//                 "Reconnecting...",
//                 "One moment while we connect your SilverBrick experience",
//                 "Thanks"
//             );
//         }
//     }
//     /*
//      * Navigation Methods
//      */

//     private _updateUserBinding(): void {
//         if (this._user) {
//             if (this._user.photoURL) {
//                 this.HasProfilePhoto = true;
//                 this.ProfilePic = this._user.photoURL;
//             } else {
//                 this.ProfilePic = Constants.DEFAULT_PROFILE_PHOTO_PATH;
//             }
//         } else {
//             // User not found and set
//             // Clear view variables
//         }
//     }
//     private _takePhoto(): void {
//         // Set cordova camera option as camera
//         const imageOptions: CameraOptions = this.firebaseStorage.GetImageOptions(
//             this.camera.PictureSourceType.CAMERA
//         );
//         // Launch the camera
//         this.camera.getPicture(imageOptions).then(
//             (imageData: string) => {
//                 // Update the UI with the pic and prep for upload
//                 this._setPhoto(imageData);
//             },
//             err => {
//                 console.log(
//                     "[TRACE] UserProfilePage.takePhoto. Get picture error: ",
//                     err
//                 );
//             }
//         );
//     }
//     private _choosePhoto(): void {
//         // Set cordova camera option as lib
//         const imageOptions: CameraOptions = this.firebaseStorage.GetImageOptions(
//             this.camera.PictureSourceType.PHOTOLIBRARY
//         );
//         // Launch the camera
//         this.camera.getPicture(imageOptions).then(
//             (imageData: string) => {
//                 // Update the UI with the pic and prep for upload
//                 this._setPhoto(imageData);
//             },
//             err => {
//                 console.log(
//                     "[TRACE] UserProfilePage.choosePhoto. Get picture error: ",
//                     err
//                 );
//             }
//         );
//     }

//     private _setPhoto(imageData: string): void {
//         // Set image data for later upload
//         this._imageData = imageData;
//         // Update view with picture
//         this._photoModified = true;
//         this.HasProfilePhoto = true;
//         this.ProfilePic = "data:image/png;base64," + imageData;
//         this.SaveProfile();
//     }
    
//     private _uploadPhoto(): Promise<string> {
//         return new Promise<string>((resolve, reject) => {
//             if (!this._imageData) {
//                 // Return empty string
//                 // Set in updatedProfile object to clear image
//                 resolve("");
//             } else {
//                 // Generate full file path
//                 const fullPath: string = `/users/${this._userID}/${this._userID}.${Constants.STR_PNG}`;
//                 // Start image upload process and get upload task
//                 const contentType: string =
//                     Constants.STR_IMAGE_FSLASH + Constants.STR_PNG;
//                 const format: string = Constants.STR_BASE64;
//                 const uploadTask: storage.UploadTask = this.firebaseStorage.Upload(
//                     fullPath,
//                     this._imageData,
//                     format,
//                     contentType
//                 );
//                 // Prep loading UI
//                 this.controls.SetContent(Constants.STR_UPLOADING_IMAGE);
//                 // Listen for state changes, error, completion of the upload
//                 uploadTask.on(
//                     storage.TaskEvent.STATE_CHANGED,
//                     (snapshot: storage.UploadTaskSnapshot) => {
//                         // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
//                         const progressString =
//                             Constants.STR_UPLOADING_IMAGE +
//                             "<br>" +
//                             Math.round(
//                                 (snapshot.bytesTransferred /
//                                     snapshot.totalBytes) *
//                                     100
//                             );
//                         this.controls.SetContent(progressString);
//                     },
//                     error => {
//                         // Failed image upload
//                         // Handle image upload error
//                         let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
//                         switch (error.name) {
//                             case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED:
//                                 // User doesn't have permission to access the object
//                                 message =
//                                     Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
//                                 break;
//                             case Constants.ERROR_TYPE_STORAGE_CANCELED:
//                                 // User canceled the upload
//                                 message =
//                                     Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
//                                 break;
//                             default:
//                                 break;
//                         }
//                         this.alertCtrl
//                             .create({
//                                 title: Constants.MESSAGE_IMAGE_UPLOAD_FAILURE,
//                                 subTitle: message,
//                                 buttons: ["Ok"]
//                             })
//                             .present();
//                         reject(message);
//                     },
//                     () => {
//                         // Successful image upload
//                         this.ProfilePic = uploadTask.snapshot.downloadURL;
//                         // Update the photoURL path
//                         resolve(fullPath);
//                     }
//                 );
//             }
//         });
//     }
// }

