// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeCardPage } from './time-card';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    TimeCardPage,
  ],
  imports: [
    LoadingComponentModule,
    CalendarModule,
    IonicPageModule.forChild(TimeCardPage),
  ],
  exports: [
  	TimeCardPage
  ]
})
export class TimeCardPageModule {}
