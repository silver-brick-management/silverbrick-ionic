// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild, Input } from "@angular/core";
import { NavController, Events, AlertController, MenuController, Modal, Alert, Content, IonicPage, ModalController } from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { CalendarModal, CalendarModalOptions, DayConfig, CalendarResult, CalendarComponentOptions, CalendarComponent } from "ion2-calendar";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IOrgBasic, ITimeCard, IUser, IOrgInfo, IBuildingSimple, ITask } from "../../shared/SilverBrickTypes";
import { IOrgState, UserStateType, IUserState, ISessionState, IAuthState, OrgStateType } from "../../interfaces/IStoreState";
import { OrgActions } from "../../store/actions/OrgActions";
import { IFormSelctedValue, IGetBldgHelper } from "../../shared/IParams";
import { SessionData } from "../../providers/SessionData";
import { UserActions } from "../../store/actions/UserActions";
import { TabsService } from '../../providers/TabsService';
import { AssignTaskHelper, AddTimeCardHelper, GetTimeCardHelper } from "../../shared/ResponseTypes";
import { SessionActions } from "../../store/actions/SessionActions";
import { LocationProvider } from '../../providers/location-provider';
import { Controls } from "../../providers/Controls";

import { OrgBldgUtil } from "../../shared/OrgBldgUtil";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-time-card",
    templateUrl: "time-card.html",
})
export class TimeCardPage {
    @ViewChild(Content) content: Content;
    @ViewChild('dateCalendar') calendar: CalendarComponent;
    private _orgStateSubscription: Subscription;
    private _authSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    private tick: number;
    private _timeSubscription: Subscription;
    private _isSocketConnected: boolean = true;

    public timeCards$: Observable<Array<ITimeCard>>;
    public timeCardsPrevious$: Observable<Array<ITimeCard>>;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public hasBuildings: number = 0;
    public authState$: Observable<IAuthState>;
    public orgID: string = "";
    public buildingID: string = "";
    public staffUsers$: Observable<Array<IUser>>;
    public userState$: Observable<IUserState>;
    public sessionState$: Observable<ISessionState>;
    public TimeToggle: string = 'today';
    public timeLeft: number = 60;
    public interval: any;
    public SelectedDate: string = '';
    public alert: Alert = null;

    public FullDate: Date = new Date(Date.now());
    public dateRange = {
        from: this.FullDate,
        to: this.FullDate.setMonth(new Date(Date.now()).getMonth() + 1)
    };
    public options: CalendarModalOptions = {
        pickMode: "single",
        title: "Date",
        defaultDateRange: this.dateRange,
        canBackwardsSelected: true,
        showYearPicker: true
    };

    public option: CalendarComponentOptions = {
        pickMode: 'single',
        showMonthPicker: true,
        from: new Date(2000, 1, 1)
    };
    public CurrentMonth: number = 0;
    public TodayDate: string = `${this.FullDate.getMonth()}/${this.FullDate.getDay()}/${this.FullDate.getFullYear()}`;
    public OneMonth: string = `${this.FullDate.getMonth() + 1}/${this.FullDate.getDay()}/${this.FullDate.getFullYear()}`;
    public timeCard: ITimeCard = null;

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Tenant'));
    }

    get IsClockedIn(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (null != this.sessionData.isClockedIn) && (this.sessionData.isClockedIn));
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsSocketConnected(): boolean {
        return this._isSocketConnected;
    }

    set IsSocketConnected(socketConnected: boolean) {
        this._isSocketConnected = socketConnected;
    }

    public orgHasBuildings(org: IOrgBasic): boolean {
        if (org.buildings.length > 0 && null != org.buildings) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    constructor(
        public store: Store<AppState>,
        public photoViewer: PhotoViewer,
        public modalCtrl: ModalController,
        public sessionData: SessionData,
        public events: Events,
        public tabService: TabsService,
        public controls: Controls,
        public locationProvider: LocationProvider,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private _sessionActions: SessionActions,
        private _orgActions: OrgActions,
        private _userActions: UserActions,
        public orgActions: OrgActions) {
        this.staffUsers$ = this.store.select((state) => state.usersState.users);
        this.error$ = this.store.select((state) => state.orgState.error);
        this.timeCards$ = this.store.select((state) => state.usersState.timeCards);
        this.timeCardsPrevious$ = this.store.select((state) => state.usersState.timeCardsPrevious);
        this.loading$ = this.store.select((state) => state.usersState.loading);
        this.orgState$ = this.store.select((state) => state.orgState);
        this.authState$ = this.store.select(state => state.authState);
        this.userState$ = this.store.select(state => state.usersState);
        this.sessionState$ = this.store.select(state => state.sessionState);
        this.TimeToggle = 'today';
    }

    ionViewWillEnter(): void {
        this.TimeToggle = 'today';
        let date: Date = new Date(Date.now());
        let helper: GetTimeCardHelper = {
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
            date: date.getTime(),
            uid: this.sessionData.userID
        };
        this._subscribeToSocketConnect();
        this.store.dispatch(this._userActions.GetTimeCards(helper));
        this._timeSubscription = this.userState$
        .subscribe((state: IUserState) => {
            if (null != state) {
                switch (state.type) {
                    case UserStateType.GET_TIME_CARDS_SUCCEEDED:
                    {
                        if (null != state.timeCards) {
                            this.timeCard = state.timeCards[state.timeCards.length - 1];
                            // console.log("GET_TIME_CARDS_SUCCEEDED", this.timeCard);
                        }
                        break;
                    }

                    case UserStateType.ADD_TIME_CARD_SUCCEEDED:
                    {
                        if (null != state.timeCards) {
                            this.timeCard = state.timeCards[state.timeCards.length - 1];
                            // console.log("ADD_TIME_CARD_SUCCEEDED", this.timeCard);
                            
                            // this.controls.ShowAlert("Clocked In", "You successfully clocked in at " + date.toDateString(), "Dismiss");
                        }
                        break;
                    }

                    case UserStateType.UPDATE_TIME_CARD_SUCCEEDED:
                    {
                        if (null != state.timeCards) {
                            this.timeCard = state.timeCards[state.timeCards.length - 1];
                            // this.controls.ShowAlert("Clocked Out", "You successfully clocked out at " + date.toDateString(), "Dismiss");
                        }
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
        })
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }

    }

    public onChange($event: string): void {
        console.log($event);
        let dateArray: string[] = $event.split("/", 3);
        let date: Date = new Date(Number(dateArray[2]), Number(dateArray[0]) - 1, Number(dateArray[1]));
        let helper: GetTimeCardHelper = {
            day: Number(dateArray[1]),
            month: Number(dateArray[0]),
            year: Number(dateArray[2]),
            uid: this.sessionData.userID,
            date: date.getTime()
        };
        this.SelectedDate = `${ $event }`;
        this.store.dispatch(this._userActions.GetTimeCardsPrevious(helper));
    }

    public monthChange($event: any): void {
        this.CurrentMonth = this.CurrentMonth + 1;
        console.log(this.CurrentMonth);
        if (this.CurrentMonth >= 2) {
            console.log('monthChange',$event, this.CurrentMonth);
            this.calendar.switchView();
            this.CurrentMonth = 0;
            console.log('monthChange',$event, this.CurrentMonth);
        } else {
            
        }
    }

    startTimer(): void {
        // this.interval = setInterval(() => {
        //   if(this.timeLeft > 0) {
        //     this.timeLeft--;
        //   } else {
        //     this.timeLeft = 60;
        //   }
        // },1000)
        let timer = TimerObservable.create(2000, 1000);
        this._timeSubscription = timer.subscribe(t => {
            this.tick = t;
        });
    }

    public stopTimer(): void {
        this._timeSubscription.unsubscribe();
    }

    pauseTimer(): void {
        this._timeSubscription.unsubscribe();
    }

    public async ClockIn(): Promise<void> {
        let location = await this.locationProvider.GetGeoLocation();
        let date: Date = new Date(Date.now());
        let timeCard: ITimeCard = {
            startTime: new Date(Date.now()).getTime(),
            location: {
                lat: location.latitude,
                lng: location.longtitude
            },
            authorID: this.sessionData.userID,
            authorName: this.sessionData.username,
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        };
        let helper: AddTimeCardHelper = {
            timeCard: timeCard,
            date: date.getTime()
        };
        console.log("helper", helper);
        this.store.dispatch(this._userActions.AddTimeCard(helper));
        this.sessionData.isClockedIn = true;
        setTimeout(() => {
            let date: Date = new Date(timeCard.startTime);
            // if (null != this.alert) {
            this.alert = this.alertCtrl.create({
                title: "Clocked In",
                message: "You successfully clocked in at " + date.toDateString(),
                buttons: [
                    {
                        text: "Dismiss",
                        role: 'cancel',
                        handler: ()=> {
                            // this.alert.dismiss();
                            // this.alert = null;
                        }
                    }
                ]
            });
            this.alert.present();
            // }
        }, 750);
    }

    public async ClockOut(): Promise<void> {
        let location = await this.locationProvider.GetGeoLocation();
        let date: Date = new Date(Date.now());
        let timeCard: ITimeCard = {
            startTime: this.timeCard.startTime,
            endTime: date.getTime(),
            location: {
                lat: location.latitude,
                lng: location.longtitude
            },
            id: this.timeCard.id,
            authorID: this.sessionData.userID,
            authorName: this.sessionData.username,
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        };
        let helper: AddTimeCardHelper = {
            timeCard: timeCard,
            date: date.getTime()
        };
        this.store.dispatch(this._userActions.UpdateTimeCard(helper));
        this.sessionData.isClockedIn = false;
        setTimeout(() => {
            // console.log("UPDATE_TIME_CARD_SUCCEEDED", this.timeCard);
            let date: Date = new Date(timeCard.endTime);
            this.alert = this.alertCtrl.create({
                title: "Clocked Out",
                message: "You successfully clocked out at " + date.toDateString(),
                buttons: [
                    {
                        text: "Dismiss",
                        role: 'cancel',
                        handler: ()=> {
                        }
                    }
                ]
            });
            this.alert.present();
        }, 750);
    }

    public StartProgress(): void {

    }

    public MarkComplete(task: ITask): void {

    }

    // public ShowMarkComplete(task: ITask): boolean {
    //     return (task.status === "Assigned");
    // }

    public OpenFullImage(task: ITask): void {
        if ((null != task.photoURL) && (task.photoURL !== '')) {
            this.photoViewer.show(task.photoURL);
        }
    }

    public expandTask(task: ITask = null): void {
        const modal: Modal = this.modalCtrl.create("TasksDetailsPage", {task: task, isEditMode: false });
        modal.present();
        this.tabService.hide();
        // this.navCtrl.push("TasksDetailsPage", {task: task, isEditMode: false });
    }

    public searchByName(): void {
        if (this.orgSearch.trim()) {
        } else {
            // No string detected
            // Do nothing
        }
    }

    public DisableControls(args: boolean): void {
        this.IsSocketConnected = args;
    }

    private _subscribeToSocketConnect(): void {
        this.events.subscribe(
            Constants.EVENT_SOCKET_DISCONNECTED,
            (args: boolean) => {
                this.DisableControls(args);
                if (args) {
                    if (this.TimeToggle === 'today') {
                        let date: Date = new Date(Date.now());
                        let helper: GetTimeCardHelper = {
                            day: date.getDate(),
                            month: date.getMonth() + 1,
                            year: date.getFullYear(),
                            date: date.getTime(),
                            uid: this.sessionData.userID
                        };
                        this.store.dispatch(this._userActions.GetTimeCards(helper));
                    } else {
                        let date: string[] = this.SelectedDate.split("/", 3);
                        let helper: GetTimeCardHelper = {
                            day: Number(date[1]),
                            month: Number(date[0]),
                            year: Number(date[2]),
                            uid: this.sessionData.userID,
                            date: 0
                        };
                        this.store.dispatch(this._userActions.GetTimeCardsPrevious(helper));
                    }
                }
            }
        );
    }
}

