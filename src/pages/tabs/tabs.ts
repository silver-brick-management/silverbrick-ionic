// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild } from "@angular/core";
import { IonicPage, Events, Tabs } from "ionic-angular";

import { Constants } from "../../common/Constants";
import { SessionData } from '../../providers/SessionData';

@IonicPage()
@Component({
    templateUrl: "tabs.html",
    selector: "page-tabs",
})
export class TabsPage {
    @ViewChild('mainTabs') tabs: Tabs;
    tab1Root = "CalendarPage";
    tab2Root = "TasksPage";
    tab3Root = "MessagesPage";
    tab4Root = "SettingsPage";
    tab5Root = "TimeCardPage";
    tab6Root = "CompletedServicesPage";

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role !== 'Landlord') && (this.sessionData.user.role !== 'Tenant')));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.isAdmin));
    }

    get IsTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Tenant'));
    }

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    get IsLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Landlord'));
    }

    constructor(public events: Events, public sessionData: SessionData) {}

    ionViewWillEnter(): void {
        this.events.publish(Constants.EVENT_HIDE_TABS, true);
    }

    ionViewDidLoad(): void {
        this.events.publish(Constants.EVENT_HIDE_TABS, true);
    }

    ionViewWillLeave(): void {
        this.events.publish(Constants.EVENT_HIDE_TABS, false);
        this.events.publish(Constants.EVENT_HIDE_DRAWER, false);
    }

    ngOnInit(): void {}

    public tabChanged($ev: any): void {
       $ev.setRoot($ev.root);
    }
}