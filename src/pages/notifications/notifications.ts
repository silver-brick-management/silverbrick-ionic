// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules
import { Component } from "@angular/core";
import { NavController, IonicPage, Events } from "ionic-angular";
import { Observable } from "rxjs/Rx";
import { Store } from "@ngrx/store";

// Local
import { Constants } from "../../common/Constants";
import { SessionData } from "../../providers/SessionData";
import { ISilverBrickUser, INotificationData, NotificationType, IUser } from "../../shared/SilverBrickTypes";
import { AppState } from "../../app/app.state";
import { SessionActions } from "../../store/actions/SessionActions";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-notifications",
    templateUrl: "notifications.html",
})
export class NotificationsPage {
    private _profileURL: string;
    public profile$: Observable<IUser>;
    public DefaultProfile: string = Constants.DEFAULT_PROFILE_PHOTO_PATH;
    public NotificationsToggle: string = null;
    public notifications$: Observable<INotificationData[]>;
    public loading$: Observable<boolean>;

    constructor(public navCtrl: NavController, public sessionData: SessionData, public sessionActions: SessionActions, public store: Store<AppState>, public events: Events) {
        this.NotificationsToggle = "all";
        this._profileURL = null != this.sessionData.user && this.sessionData.user.photoURL ? this.sessionData.user.photoURL : Constants.DEFAULT_PROFILE_PHOTO_PATH;
        this.profile$ = this.store.select((state) => state.authState.profile);
        this.loading$ = this.store.select((state) => state.sessionState.loading);
        // this.notifications$ = this.store.select(state => state.sessionState.notifications);
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad NotificationsPage");
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (args: boolean) => {
            if (args) {
                // this.store.dispatch(this.sessionActions.GetNotifications());
            }
        });
    }
    ionViewWillEnter(): void {
        // this.store.dispatch(this.sessionActions.GetNotifications());
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (args: boolean) => {
            if (args) {
                // this.store.dispatch(this.sessionActions.GetNotifications());
            }
        });
    }

    ionViewDidLeave(): void {
        this.events.unsubscribe(Constants.EVENT_SOCKET_DISCONNECTED);
    }

    public OpenLink(txt: string): void {
        window.location.href = txt;
    }

    public GetIcon(type: NotificationType): string {
        switch (type) {
            default: {
                return "assets/icon/NewMessage.png";
            }
        }
    }
}
