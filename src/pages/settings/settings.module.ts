// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsPage } from './settings';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'
import { LetterAvatarModule } from '../../components/letter-avatar/letter-avatar.module';

@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    LetterAvatarModule,
    LoadingComponentModule,
    IonicPageModule.forChild(SettingsPage),
  ],
  exports: [
  	SettingsPage
  ]
})
export class SettingsPageModule {}
