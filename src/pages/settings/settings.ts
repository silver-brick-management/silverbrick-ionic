// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild, Input } from "@angular/core";
import { NavController, Events, AlertController, MenuController, Modal, Content, IonicPage, ModalController } from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { PhotoViewer } from "@ionic-native/photo-viewer";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IOrgBasic, IUserOrg, IUser, IOrgInfo, IBuildingSimple, ITask } from "../../shared/SilverBrickTypes";
import { IOrgState, IUserState, ISessionState, IAuthState, OrgStateType } from "../../interfaces/IStoreState";
import { OrgActions } from "../../store/actions/OrgActions";
import { IFormSelctedValue, IGetBldgHelper } from "../../shared/IParams";
import { SessionData } from "../../providers/SessionData";
import { UserActions } from "../../store/actions/UserActions";
import { TabsService } from '../../providers/TabsService';
import { AssignTaskHelper } from "../../shared/ResponseTypes";
import { SessionActions } from "../../store/actions/SessionActions";
import { AuthActions } from "../../store/actions/AuthActions";

import { OrgBldgUtil } from "../../shared/OrgBldgUtil";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-settings",
    templateUrl: "settings.html",
})
export class SettingsPage {
    @ViewChild(Content) content: Content;
    private _orgStateSubscription: Subscription;
    private _authSubscription: Subscription;
    private _screenSize: number = window.screen.width;

    public tasks$: Observable<Array<ITask>>;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public hasBuildings: number = 0;
    public authState$: Observable<IAuthState>;
    public orgID: string = "";
    public buildingID: string = "";
    public user$: Observable<IUser>;
    public userState$: Observable<IUserState>;
    public sessionState$: Observable<ISessionState>;
    public TaskToggle: string = 'mine';

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsLandLord(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Landlord";
    }

    get IsTenant(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Tenant";
    }

    public orgHasBuildings(org: IOrgBasic): boolean {
        if (org.buildings.length > 0 && null != org.buildings) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    constructor(
        public store: Store<AppState>,
        public events: Events,
        public photoViewer: PhotoViewer,
        public modalCtrl: ModalController,
        public authActions: AuthActions, 
        public sessionData: SessionData,
        public tabService: TabsService,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private _sessionActions: SessionActions,
        private _orgActions: OrgActions,
        private _userActions: UserActions,
        public orgActions: OrgActions) {
        this.user$ = this.store.select((state) => state.authState.profile);
        this.error$ = this.store.select((state) => state.orgState.error);
        this.tasks$ = this.store.select((state) => state.sessionState.tasks);
        this.loading$ = this.store.select((state) => state.authState.loading);
        this.orgState$ = this.store.select((state) => state.orgState);
        this.authState$ = this.store.select(state => state.authState);
        this.userState$ = this.store.select(state => state.usersState);
        this.sessionState$ = this.store.select(state => state.sessionState);
        this.TaskToggle = 'mine';
    }

    ionViewWillEnter(): void {
        
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
    }

    public Logout(): void {
        let newAlert = this.alertCtrl.create({
            title: "Are you sure you want to Log Out?",
            message: "Please make sure you do.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: () => {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: "Log Out",
                    handler: () => {
                        // Conduct the logout action, (it auto navigates)
                        this.events.publish(Constants.EVENT_LOGOUT);
                        this.navCtrl.setRoot("EntryPage");
                        this.store.dispatch(this.authActions.Logout());
                    },
                },
            ],
        });
        newAlert.present();
    }
    
    public OpenFullImage(task: ITask): void {
        if ((null != task.photoURL) && (task.photoURL !== '')) {
            this.photoViewer.show(task.photoURL);
        }
    }

    GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public GetName(user: IUser): string {
        return `${ user.firstName} ${ user.lastName }`;
    }

    public searchByName(): void {
        if (this.orgSearch.trim()) {
        } else {
            // No string detected
            // Do nothing
        }
    }
}

