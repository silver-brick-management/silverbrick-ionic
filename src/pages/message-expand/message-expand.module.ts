import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageExpandPage } from './message-expand';
import { MessageControlModule } from '../../components/message-control/message-control.module';
import { LetterAvatarModule } from '../../components/letter-avatar/letter-avatar.module';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

@NgModule({
    declarations: [
        MessageExpandPage,
    ],
    imports: [
        MessageControlModule,
        LetterAvatarModule,
        LoadingComponentModule,
        IonicPageModule.forChild(MessageExpandPage),
    ],
    exports: [
        MessageExpandPage
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MessageExpandPageModule {}
