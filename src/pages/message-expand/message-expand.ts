// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules
import { Component, ViewChild, Input, OnChanges, OnInit, OnDestroy } from "@angular/core";
import { Events, NavController, Content, NavParams, ScrollEvent, ViewController, AlertController, ActionSheetController, IonicPage } from "ionic-angular";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { Observable, Subscription } from "rxjs/Rx";
import { storage } from "firebase";
import { Store } from "@ngrx/store";
import { database } from "firebase";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Platform } from "ionic-angular/platform/platform";

// Local
import { Constants } from "../../common/Constants";
import { SessionData } from "../../providers/SessionData";
import { MessageActions } from "../../store/actions/MessageActions";
import { SessionActions } from "../../store/actions/SessionActions";
import { ISilverBrickUser, IMessage, IUser, IUserOrg} from "../../shared/SilverBrickTypes";
import { AppState } from "../../app/app.state";
import { AddMessageHelper, AddChatHelper, NewChatRoomHelper, MessageParticipant, UserChatRoom } from "../../shared/ResponseTypes";
import { FirebaseStorage } from "../../services/FirebaseStorage";
import { IMessageExpandParams } from "../../shared/IParams";
import { Controls } from "../../providers/Controls";
import { IUserProfileNavigationArgs, UserProfileMode } from "../../interfaces/IUserProfileNavigationArgs";
import { StringUtil } from "../../shared/StringUtil";
import { IMessageState, MessageStateType } from "../../interfaces/IStoreState";
import { MediaProvider } from "../../providers/media-provider";
import { UserActions } from "../../store/actions/UserActions";

export interface IPartialChatMessage {
    text: string;
    type?: string;
}

export interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}

@IonicPage({ priority: "off" })
@Component({
    selector: "page-message-expand",
    templateUrl: "message-expand.html"
})
export class MessageExpandPage implements OnInit, OnDestroy {
    @ViewChild(Content) content: Content;
    @ViewChild("input") myInput: any;
    discoverInput: string; // searchbar input

    private _profileURL: string;
    private _chatTopic: string = "Select a topic";
    private _isLeaving: boolean = false;
    private _textMessage: string;
    private _isUploading: boolean;
    private _uploadStatus: string;
    private _isUploadDone: boolean;
    private _uploadPreview: string;
    private _progressPercent: number;
    private _canCancelUpload: boolean;
    private _hideScrollDownButtom: boolean;
    private _uploadTask: storage.UploadTask;
    private _defaultMessage: IMessage;
    private _isSocketConnected: boolean = true;
    private _selectedUser: IUser = null;
    private _messageParticipants: MessageParticipant[] = [];
    private _isNewChat: boolean = false;
    private _participantIDs: string[] = [];
    private _messageStateSubscription: Subscription = null;

    public IsLoading$: Observable<boolean>;
    public SearchUsers$: Observable<(IUser)[]>;
    public profile$: Observable<IUser>;
    public DefaultProfile: string = Constants.DEFAULT_PROFILE_PHOTO_PATH;
    public RoomID: string = "";
    public searchString: string = "";
    public Topics: string[] = ['Account Related', 'Service Issue', 'Payments & Billing', 'Other'];
    public SearchToggle: string;
    public IsSearching: boolean = false;
    public messageState$: Observable<IMessageState>;
    public fromProfile: boolean = false;
    private _gotRooms: boolean = false; // check if we got rooms yet after opening message expand
    public RoomTitle: string = "New Chat";
    public topicOptions = {
        title: "Chat Topics",
        subTitle: "Select a chat topic below",
        mode: "md"
    };
    public IsLoading: boolean = false;

    get IsNewChat(): boolean {
        return this._isNewChat;
    }

    set IsNewChat(news: boolean) {
        this._isNewChat = news;
    }

    get SelectedUser(): IUser {
        return this._selectedUser;
    }

    set SelectedUser(users: IUser) {
        this._selectedUser = users;
    }

    get MessageParticipants(): MessageParticipant[] {
        return this._messageParticipants;
    }

    set MessageParticipants(users: MessageParticipant[]) {
        this._messageParticipants = users;
    }

    get IsUploading(): boolean {
        return this._isUploading;
    }

    set IsUploading(isUploading: boolean) {
        this._isUploading = isUploading;
    }

    get UploadStatus(): string {
        return this._uploadStatus;
    }

    set UploadStatus(uploadStatus: string) {
        this._uploadStatus = uploadStatus;
    }

    get IsUploadDone(): boolean {
        return this._isUploadDone;
    }

    set IsUploadDone(isUploadDone: boolean) {
        this._isUploadDone = isUploadDone;
    }

    get UploadPreview(): string {
        return this._uploadPreview;
    }

    set UploadPreview(uploadPreview: string) {
        this._uploadPreview = uploadPreview;
    }

    get ProgressPercent(): number {
        return this._progressPercent;
    }

    set ProgressPercent(progressPercent: number) {
        this._progressPercent = progressPercent;
    }

    get ChatTopic(): string {
        return this._chatTopic;
    }

    set ChatTopic(topic: string) {
        this._chatTopic = topic;
    }

    get TextMessage(): string {
        return this._textMessage;
    }

    set TextMessage(message: string) {
        this._textMessage = message;
    }

    get CanCancelUpload(): boolean {
        return this._canCancelUpload;
    }

    set CanCancelUpload(canCancelUpload: boolean) {
        this._canCancelUpload = canCancelUpload;
    }

    get HideScrollDownButtom(): boolean {
        return this._hideScrollDownButtom;
    }

    set HideScrollDownButtom(hideScrollDownButtom: boolean) {
        this._hideScrollDownButtom = hideScrollDownButtom;
    }

    get IsSocketConnected(): boolean {
        return this._isSocketConnected;
    }

    set IsSocketConnected(connected: boolean) {
        this._isSocketConnected = connected;
    }

    get ProfileUrl(): string {
        return null != this._profileURL ? this._profileURL : Constants.DEFAULT_PROFILE_PHOTO_PATH;
    }

    get UserID(): string {
        return null != this.sessionData.user ? this.sessionData.user.uid : "";
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role !== 'Landlord') && (this.sessionData.user.role !== 'Tenant')));
    }

    constructor(
        private _keyboard: Keyboard,
        private _userActions: UserActions,
        private _platform: Platform,
        public navCtrl: NavController,
        public sessionData: SessionData,
        public navParams: NavParams,
        public camera: Camera,
        public viewCtrl: ViewController, public firebaseStorage: FirebaseStorage, public events: Events, public messageActions: MessageActions, public sessionActions: SessionActions, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController, public mediaProvider: MediaProvider, public controls: Controls, public store: Store<AppState>) {
        this._profileURL = null != this.sessionData.user && this.sessionData.user.photoURL ? this.sessionData.user.photoURL : Constants.DEFAULT_PROFILE_PHOTO_PATH;
        this.profile$ = this.store.select(state => state.authState.profile);
        // Init view variables
        this.TextMessage = "";
        this.IsUploading = false;
        this.IsUploadDone = false;
        this.UploadPreview = null;
        this.ProgressPercent = 0;
        this.CanCancelUpload = false;
        this.IsSearching = false;
        this.UploadStatus = Constants.STR_FINISHING;
        this.IsLoading$ = this.store.select(state => state.messageState.loading);
        this.messageState$ = this.store.select(state => state.messageState);
        this.SearchToggle = "people";
        this.SearchUsers$ = this.store.select(state => state.usersState.searchStaff);
    }

    ionViewWillEnter(): void {
        console.log("Params", this.navParams.data);
        if (this._platform.is("cordova")) {
            // this._keyboard.hideFormAccessoryBar(false);
        } else {
            // keyboard is a cordova plugin & this is
            // not a cordova platform.
        }
        const params: IMessageExpandParams = this.navParams.data;
        console.log("Params", params);
        this.IsNewChat = params.isNewChat;

        if ((null != this.sessionData.user) && (this.sessionData.user.role !== 'Tenant') && this.IsNewChat) {
            this.store.dispatch(this._userActions.GetAllTenants());
            this.store.dispatch(this._userActions.GetUsers(null));
        }
        this._subscribeToSocketConnect();
        this.IsSearching = false;
        this.MessageParticipants = [];
        this.RoomID = null;
        this.RoomID = ((null != params) && (null != params.roomID)) ? params.roomID : null;
        this._defaultMessage = {
            date: new Date(Date.now()).getTime(),
            authorID: this.sessionData.userID,
            authour: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
            text: null,
            photoURL: `/users/${this.sessionData.userID}/${this.sessionData.userID}.${Constants.STR_PNG}`,
            type: "text",
            role: this.sessionData.user.role
        };
        // this.controls.ShowLoading();
        this.IsLoading = true;
        this._messageStateSubscription = this.messageState$.subscribe((state: IMessageState) => {
            if (null != state) {
                switch (state.type) {
                    case MessageStateType.GET_CHAT_ROOMS_SUCCESS:
                    {
                        let chatRooms: UserChatRoom[] = state.chatRooms;
                        if (params.fromProfile && !this._gotRooms) {
                            console.log("CHECKED FROM PROFILE!");
                            this.fromProfile = true;
                            let solo: boolean = false;
                            for (let room of chatRooms) {
                                if (room.users.length === 2 && room.users.filter(user => user.uid == params.participants[0].uid).length > 0) {
                                    // we have a solo room together
                                    console.log("WE HAVE A SOLO ROOM TOGETHER!");
                                    console.log("PARTICIPANTS: ", params.participants);
                                    this.IsNewChat = false;
                                    this.RoomID = room.roomID;
                                    solo = true;
                                    break;
                                }
                            }
                            if (!solo) {
                                console.log("NO SOLO ROOMS BETWEEN US");
                                this.IsNewChat = true;
                            }
                        }
                        this._gotRooms = true;
                        break;
                    }

                    case MessageStateType.ADD_MESSAGE_SUCCESS:
                    {
                        setTimeout(() => {
                            if (!this._isLeaving) {
                                // this.content.scrollToBottom();
                                this.IsLoading = false;
                            }
                        }, 500);
                        break;
                    }

                    case MessageStateType.GET_MESSAGES_SUCCESS:
                    {
                        this.content.scrollToBottom();
                        this.IsLoading = false;
                        break;
                    }

                    default: 
                    {
                        setTimeout(() => {
                            this.IsLoading = false;
                        }, 500);
                        break;
                    }
                }
            }
        });
        if (!this.IsNewChat) {
            this.RoomTitle = ` Chat`;
        }
        setTimeout(() => {
            // this.content.scrollToBottom();

            if (!this.IsNewChat) {
                // this.store.dispatch(this.messageActions.LeaveMessageRoom());
                // this.store.dispatch(this.messageActions.JoinMessageRoom(this.RoomID));
                if (null != params.participants) {
                    this.MessageParticipants = params.participants;
                    console.log("PARTICIPANTS: ", this.MessageParticipants);
                    this.MessageParticipants.forEach((participant: MessageParticipant) => {
                        participant.uid === this.sessionData.userID ? console.log("we matchy") : this._participantIDs.push(participant.uid);
                    });
                }
                // this.store.dispatch(this.messageActions.GetMessageHistory(this.RoomID));
                console.log("PARTICIPANT ID: ", this._participantIDs);
                console.log("PARTICIPANTS: ", this.MessageParticipants);
            }
        }, 1250);
    }

    ionViewWillLeave(): void {
        this.events.unsubscribe(Constants.EVENT_SOCKET_DISCONNECTED);
        this.IsSearching = false;
        this.MessageParticipants = [];
        this.RoomID = null;
        this.TextMessage = "";
        this.store.dispatch(this.messageActions.LeaveMessageRoom());
        if (null != this._messageStateSubscription) {
            this._messageStateSubscription.unsubscribe();
            this._messageStateSubscription = null;
        }
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {
        
    }

    public DisableChatControls(args: boolean): void {
        this.IsSocketConnected = args;
    }

    public closeMessage(): void {
        if (this.IsSocketConnected) {
            this._isLeaving = true;
            this.store.dispatch(this.messageActions.GetChatRooms());
            this.navCtrl.pop();
        } else {
            this.controls.ShowAlert("One Moment", "We are reconnecting you...", "Thanks");
        }
    }

    public SelectUser(user: IUser): void {
        if (this._participantIDs.length >= 3) {
            this.controls.ShowAlert("Too many people!", "You can add up to 3SelectedUser members to a message", "Okay");
            return;
        }
        this.SelectedUser = user;
        let participant: MessageParticipant = {
            uid: user.uid,
            photoURL: user.photoURL,
            authour: user.firstName + " " + user.lastName,
            role: user.role
        };
        this.searchString = '';
        this.MessageParticipants.push(participant);
        this._participantIDs.push(user.uid);
        // this.store.dispatch(this.sessionActions.ClearSearch());
        this.discoverInput = "";
        if (this.myInput) this.myInput.setFocus(); // keep focus on keyboard after adding user
        this.IsSearching = false;;
    }

    public RemoveUser(user: MessageParticipant): void {
        this._participantIDs = this._participantIDs.filter((users: string) => {
            return users != user.uid;
        });

        this.MessageParticipants = this.MessageParticipants.filter((users: MessageParticipant) => {
            return users.uid != user.uid;
        });
    }

    public CheckParticipants(uid: string): boolean {
        return this._participantIDs.includes(uid);
    }

    public StartChat(): void {
        if (this.IsSocketConnected) {
            if (this.TextMessage) {
                // if (this.ChatTopic !== 'Select a topic') {
                    // Create a partial message object for the object clone
                    const partialChatMessage: IPartialChatMessage = {
                        text: this.TextMessage
                    };
                    let messageParticipants: MessageParticipant[] = [];
                    if (null != this.MessageParticipants) {
                        this.MessageParticipants.forEach((participant: MessageParticipant) => {
                            let newParticipant: MessageParticipant = {
                                uid: participant.uid,
                                photoURL: `/users/${participant.uid}/${participant.uid}.${Constants.STR_PNG}`,
                                authour: participant.authour,
                                role: participant.role
                            };
                            messageParticipants.push(newParticipant);
                        });
                    }
                    // add myself to the chat
                    const myself: MessageParticipant = {
                        uid: this.sessionData.userID,
                        photoURL: `/users/${this.sessionData.userID}/${this.sessionData.userID}.${Constants.STR_PNG}`,
                        authour: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
                        role: this.sessionData.user.role
                    };
                    messageParticipants.push(myself);
                    this._participantIDs.push(myself.uid);

                    // Create a new message object from the default message object with an override of the text message
                    let message: IMessage = Object.assign({}, this._defaultMessage, partialChatMessage);
                    message.date = new Date(Date.now()).getTime();
                    message.authour = this.sessionData.user.firstName + " " + this.sessionData.user.lastName;
                    const helper: NewChatRoomHelper = {
                        message: message,
                        roomID: this.RoomID,
                        participants: messageParticipants,
                        authorID: this.sessionData.userID
                    };

                    console.log("HELPER: ", helper);
                    this.navCtrl.pop();
                    this.store.dispatch(this.messageActions.NewChatRoom(helper));
                    
                    this.controls.ShowAlert(`Room started`, `New chat room started`, "Thanks");
                    // Clear the text message field
                    this.TextMessage = "";
                    setTimeout(() => {
                        this.store.dispatch(this.messageActions.GetChatRooms());
                    }, 1000);
                // } else {
                //     this.controls.ShowAlert("Select a topic", "Please select a topic", "Thanks");
                // }
            } else {
                this.controls.ShowAlert("Enter a message", "Please enter a message", "Thanks");
                // Text message field is empty
            }
        } else {
            this.controls.ShowAlert("Reconnecting...", "One moment while we connect your SilverBrick experience", "Thanks");
        }
    }

    public SendText(): void {
        if (this.IsSocketConnected) {
            if (this.TextMessage) {
                // Create a partial message object for the object clone
                const partialChatMessage: IPartialChatMessage = {
                    text: this.TextMessage,
                    type: "text"
                };
                // Create a new message object from the default message object with an override of the text message
                let message: IMessage = Object.assign({}, this._defaultMessage, partialChatMessage);
                message.date = new Date(Date.now()).getTime();
                message.authour = this.sessionData.user.firstName + " " + this.sessionData.user.lastName;
                const helper: AddChatHelper = {
                    message: message,
                    roomID: this.RoomID,
                    participants: this._participantIDs,
                    authorID: this.sessionData.userID
                };
                this.store.dispatch(this.messageActions.AddMessage(helper));
                // Clear the text message field
                this.TextMessage = "";
            } else {
                // Text message field is empty
            }
        } else {
            this.controls.ShowAlert("Reconnecting...", "One moment while we connect your SilverBrick experience", "Thanks");
        }
    }

    public SendImage(): void {
        if (this.IsSocketConnected) {
            // Prep action sheet
            this.actionSheetCtrl
                .create({
                    title: Constants.STR_SELECT_PHOTO,
                    buttons: [
                        {
                            text: Constants.STR_TAKE_PHOTO,
                            handler: () => {
                                this._takePhoto();
                            }
                        },
                        {
                            text: Constants.STR_CHOOSE_PHOTO,
                            handler: () => {
                                this._choosePhoto();
                            }
                        },
                        {
                            text: Constants.STR_CANCEL,
                            role: "cancel",
                            handler: () => {
                                console.log("[TRACE] MessageExpandPage.SendImage Destructive clicked");
                            }
                        }
                    ]
                })
                .present();
        }
    }

    public GetName(user: IUser): string {
        return `${ user.firstName} ${ user.lastName }`;
    }

    public CancelImageUpload(): void {
        if (this._uploadTask) {
            this._uploadTask.cancel();
        } else {
            // Upload task not set yet
        }
        // Image upload clean up
        // False signifies its a cancel request and the message should
        // reflect "Cancelled" vs "Done"
        this._finishImageSend(false);
    }

    GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public OnScroll(event: ScrollEvent): void {
        if (!this._isLeaving) {
            if (null != event) {
                const currentTop: number = event.scrollTop + event.contentTop + event.contentBottom;
                // console.log("OnScroll", currentTop);
                // console.log('[TRACE] ChatPage.OnScroll event:', event);
                // console.log('[TRACE] ChatPage.OnScroll currentTop:', currentTop);
                // console.log('[TRACE] ChatPage.OnScroll contentTop:', event.contentTop);
                // console.log('[TRACE] ChatPage.OnScroll contentBottom:', event.contentBottom);
                // console.log('[TRACE] ChatPage.OnScroll scrollHeight:', event.scrollHeight);
                // console.log('[TRACE] ChatPage.OnScroll scrollElement.scrollHeight:', event.scrollElement.scrollHeight);
                // console.log('[TRACE] ChatPage.OnScroll scrollTop:', event.scrollTop);
                // console.log('[TRACE] ChatPage.OnScroll contentHeight:', event.contentHeight);

                this.HideScrollDownButtom = currentTop >= event.scrollHeight || event.scrollElement.scrollHeight <= event.contentHeight + 1;
            }
        }
    }

    public ScrollToBottom(): void {
        this.content.scrollToBottom();
    }

    public SearchUsers() {
        // fix this later: set timeout function to keep from excessive server calls
        console.log("input: ", this.searchString);
        console.log("Search toggle: ", this.SearchToggle);
        
        this.IsSearching = true;
        this.store.dispatch(this._userActions.SearchAllStaff(this.searchString));
    }

    /* HELPERS */

    private _subscribeToSocketConnect(): void {
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (args: boolean) => {
            this.DisableChatControls(args);
            
        });
    }

    private _takePhoto(): void {
        // Set cordova camera option as camera
        const imageOptions: CameraOptions = this.firebaseStorage.GetImageOptions(this.camera.PictureSourceType.CAMERA);
        // Launch the camera
        this.camera.getPicture(imageOptions).then(
            (imageData: string) => {
                // Begin the upload process
                this._uploadPhoto(imageData);
            },
            err => {}
        );
    }

    private _choosePhoto(): void {
        // Set cordova camera option as lib
        const imageOptions: CameraOptions = this.firebaseStorage.GetImageOptions(this.camera.PictureSourceType.PHOTOLIBRARY);
        // Launch the camera
        this.camera.getPicture(imageOptions).then(
            (imageData: string) => {
                // Begin the upload process
                this._uploadPhoto(imageData);
            },
            err => {}
        );
    }

    private _uploadPhoto(imageData: string): void {
        // Set the upload preview
        let urlCreator = window.URL;
        let dataBlob = this.mediaProvider.GetBlob("data:image/png;base64," + imageData);
        let imageUrl = urlCreator.createObjectURL(dataBlob);
        this.UploadPreview = <string>imageData;
        // Show the upload UI
        this.IsUploading = true;
        // Generate filename and remove dashes
        const fileName: string = this.RoomID + StringUtil.GenerateUUID() + Date.now();
        // Generate full file path
        const fullPath: string = `/messages/${this.RoomID}/${fileName}.${Constants.STR_PNG}`;
        // Start image upload process and get upload task
        const contentType: string = Constants.STR_IMAGE_FSLASH + Constants.STR_PNG;
        const format: string = Constants.STR_BASE64;
        this._uploadTask = this.firebaseStorage.Upload(fullPath, imageData, format, contentType);
        // Enable the cancel button now that the upload task is available
        this.CanCancelUpload = true;
        // Listen for state changes, error, completion of the upload
        this._uploadTask.on(
            storage.TaskEvent.STATE_CHANGED,
            (snapshot: storage.UploadTaskSnapshot) => {
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                setTimeout(() => {
                    this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                });
            },
            error => {
                // Handle image upload error
                let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                switch (error.name) {
                    case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED:
                        // User doesn't have permission to access the object
                        message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                        break;

                    case Constants.ERROR_TYPE_STORAGE_CANCELED:
                        // User canceled the upload
                        message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                        break;

                    default:
                        break;
                }
                this.alertCtrl
                    .create({
                        title: Constants.MESSAGE_IMAGE_UPLOAD_FAILURE,
                        subTitle: message,
                        buttons: ["Ok"]
                    })
                    .present();
                // Image upload clearn up (still needed in the failure case)
                // True signifies its a done request and the message should
                // reflect "Done" vs "Cancelled"
                this._finishImageSend(true);
            },
            () => {
                // Update UI with finishing and remove cancel button
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                // Create a partial message object for the message clone
                const partialChatMessage: IPartialChatMessage = {
                    text: this._uploadTask.snapshot.downloadURL,
                    type: "photo"
                };
                // Create a new message object from the default message object with an override of the text message
                const message: IMessage = Object.assign({}, this._defaultMessage, partialChatMessage);
                message.authour = this.sessionData.user.firstName + " " + this.sessionData.user.lastName;
                const helper: AddChatHelper = {
                    message: message,
                    roomID: this.RoomID,
                    participants: this._participantIDs,
                    authorID: this.sessionData.userID
                };
                this.store.dispatch(this.messageActions.AddMessage(helper));
                // GET THIS IN MSG LISTNER.... maybe
                // OR have a add chat vs add image that can have states for differntiating things
                this._finishImageSend(true);
            }
        );
    }

    private _finishImageSend(isDone: boolean): void {
        // Remove cancel button if its in view
        this.CanCancelUpload = false;
        // Update UI with done
        this.UploadStatus = isDone ? "Done!" : "Canceled!";
        // keep the done UI for 1 second
        setTimeout(() => {
            // Hides all upload UI elements
            // Re-enables the image upload button
            this.IsUploading = false;
            // Reset the image done flag and status
            this.IsUploadDone = false;
            this.UploadStatus = Constants.STR_FINISHING;
            // Resets the progress bar
            this.ProgressPercent = 0;
            // Reset upload preview
            this.UploadPreview = null;
            // Clear upload task
            this._uploadTask = null;
        }, 1000);
    }
}

