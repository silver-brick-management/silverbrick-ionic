// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    LoadingComponentModule,
    IonicPageModule.forChild(HomePage),
  ],
  exports: [
  	HomePage
  ]
})
export class HomePageModule {}
