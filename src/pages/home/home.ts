// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild, Input } from "@angular/core";
import {
    NavController,
    Events,
    MenuController,
    Content,
    Modal,
    Alert,
    AlertController,
    ModalController,
    IonicPage
} from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";

// Local modules
import { AppState } from '../../app/app.state';
import { Constants, PROPERTY_TYPES } from '../../common/Constants';
import { IError } from '../../interfaces/IError';
import { IOrgBasic, IUser, IOrgInfo, IBuilding, IBuildingSimple, IBuildingBasic } from '../../shared/SilverBrickTypes';
import { IOrgState, OrgStateType } from '../../interfaces/IStoreState';
import { OrgActions } from '../../store/actions/OrgActions';
import { SessionActions } from '../../store/actions/SessionActions';
import { IFormSelctedValue, IGetBldgHelper } from '../../shared/IParams';
import { TabsService } from '../../providers/TabsService';
import { LocationProvider } from '../../providers/location-provider';

import { OrgBldgUtil } from '../../shared/OrgBldgUtil';

@IonicPage({ priority: "high" })
@Component({
    selector: "page-home",
    templateUrl: "home.html"
})
export class HomePage {
    @ViewChild(Content) content: Content;
    public allBuildings$: Observable<IBuilding[]>;
    public Orgs: Array<IOrgBasic> = [];
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public hasBuildings: number = 0;
    private _orgStateSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    private _isSocketConnected: boolean = true;
    public Landlords$: Observable<IUser[]>;


    get IsSocketConnected(): boolean {
        return this._isSocketConnected;
    }

    set IsSocketConnected(socketConnected: boolean) {
        this._isSocketConnected = socketConnected;
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    public orgHasBuildings(org: IOrgBasic): boolean {
        if ((org.buildings.length > 0) && (null != org.buildings)) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    constructor(
        public tabService: TabsService,
        public navCtrl: NavController,
        public sessionActions: SessionActions,
        public modalCtrl: ModalController,
        public alertCtrl: AlertController,
        public store: Store<AppState>,
        public orgActions: OrgActions) {
        this.error$ = this.store.select(state => state.orgState.error);
        this.allBuildings$ =  this.store.select(state => state.orgState.allFullBuildings);
        this.loading$ = this.store.select(state => state.orgState.loading);
        this.orgState$ = this.store.select(state => state.orgState);
    }

    ngOnInit() {
        this.store.dispatch(this.orgActions.GetAllBuildings());
        this._orgStateSubscription = this.orgState$
        .subscribe((orgState: IOrgState) => {
            if ((null != orgState.orgs) && (orgState.orgs.length > 0)) {
                this.Orgs = orgState.orgs;
                switch (orgState.type) {
                    case OrgStateType.NONE:
                    case OrgStateType.LOADING:
                    {
                        break;
                    }
                    
                    case OrgStateType.UPDATE_BLDG_SUCCEEDED:
                    case OrgStateType.ADD_BLDG_SUCCEEDED:
                    {
                        this.store.dispatch(this.orgActions.GetAllBuildings());
                        // for (let org of orgState.orgs) {
                        //     if ((null != org.buildings) && (org.buildings)) {
                        //         org.buildings = OrgBldgUtil.Transform(org.buildings);
                        //     } else {

                        //     }
                        // }
                        break;
                    }

                    case OrgStateType.GET_ALL_ORGS_SUCCEEDED:
                    {
                        // for (let org of orgState.orgs) {
                        //     if ((null != org.buildings) && (org.buildings)) {
                        //         org.buildings = OrgBldgUtil.Transform(org.buildings);
                        //     } else {

                        //     }
                        // }
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
            } else {
                
            }
        });
    }

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
    }

    public DisableControls(args: boolean): void {
        this.IsSocketConnected = args;
    }

    public expandBldg(building: IBuildingSimple, orgID: string): void {
        const buildingHelper: IGetBldgHelper = {
            orgID: orgID,
            buildingID: building.id
        };
        this.store.dispatch(this.orgActions.GetBldg(buildingHelper));
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: orgID
        };
        this.store.dispatch(this.sessionActions.GetBuildingTasks(helper));
        const modal: Modal = this.modalCtrl.create("BuildingDetailsPage");
        modal.present();
        // this.navCtrl.push("BuildingDetailsPage");
        this.tabService.hide();
    }

    public EmailProfile(email: string): void {
        window.location.href = "mailto:" + email;
    }

    public CallProfile(number: string, firstName: string, lastName: string): void {
        let newAlert = this.alertCtrl.create({
            title: 'Select how you want to contact ' + firstName + " " + lastName,
            message: 'Via Call or Text',
            buttons: [
                {
                    text: 'Call',
                    handler: () => {
                        window.location.href = "tel:" + number;
                    }
                },
                {
                    text: 'Text',
                    handler: () => {
                        window.location.href = "sms:" + number;

                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        newAlert.present();
    }

    public GetIcon(type: string): string {
        switch (type) {
            case "Multi-Family":
            {
                return "assets/icon/buildings.png";
            }

            case "Single-Family":
            {
                return "assets/icon/house.png";
            }
            
            default:
            {
                return "assets/icon/building.png";
            }
        }
    }

    public searchByName(): void {
        if (this.orgSearch.trim()) {
        } else {
            // No string detected
            // Do nothing
        }
    }
}
