// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarPage } from './calendar';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'
import { CalendarHeaderComponentModule } from '../../components/calendar-components/calendar-header.component.module';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { MainCalendarComponentModule } from '../../components/calendar-components/main-calendar-component/main-calendar.component.module';
import { NgCalendarModule  } from 'ionic2-calendar';
import { IonicConferenceCalendarHeaderModule } from '../../components/calendar-header/IonicConferenceCalendarHeader.module';

@NgModule({
    declarations: [
        CalendarPage,
    ],
    imports: [
        NgCalendarModule,
        IonicConferenceCalendarHeaderModule,
        MainCalendarComponentModule,
        CalendarHeaderComponentModule,
        LoadingComponentModule,
        IonicPageModule.forChild(CalendarPage),
    ],
    exports: [
        CalendarPage
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class CalendarPageModule {}
