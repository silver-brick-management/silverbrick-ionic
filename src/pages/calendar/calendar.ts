// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import * as moment from "moment";
import { Component, ViewChild, Input, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { NavController, Platform, Events, Alert, AlertController, MenuController, FabContainer, Modal, Content, IonicPage, ModalController } from "ionic-angular";
import { Observable, Subject, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";
import { Keyboard } from "@ionic-native/keyboard/ngx";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay, CalendarWeekViewEvent } from "angular-calendar";

import { getMonth, startOfMonth, startOfWeek, startOfDay, endOfWeek, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from "date-fns";
import RRule from "rrule";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants, colors } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IOrgBasic, BeforeCalendarLoadMonth, BeforeCalendarLoadWeek, RecurringEvent, IUser, IOrgInfo, IBuildingSimple, ITask } from "../../shared/SilverBrickTypes";
import { IOrgState, IUserState, ISessionState, IAuthState, OrgStateType, SessionStateType } from "../../interfaces/IStoreState";
import { OrgActions } from "../../store/actions/OrgActions";
import { IFormSelctedValue, IGetBldgHelper } from "../../shared/IParams";
import { SessionData } from "../../providers/SessionData";
import { Controls } from "../../providers/Controls";
import { TabsService } from "../../providers/TabsService";
import { LocationProvider } from "../../providers/location-provider";

import { AssignTaskHelper } from "../../shared/ResponseTypes";
import { SessionActions } from "../../store/actions/SessionActions";
import { UserActions } from "../../store/actions/UserActions";
import { UserUtil } from "../../common/UserUtil";
import { OrgBldgUtil } from "../../shared/OrgBldgUtil";
import { Operator } from "rxjs/Operator";

export declare type WeekdayStr = "SU" | "MO" | "TU" | "WE" | "TH" | "FR" | "SA";
declare var google: any;

declare module "rxjs/Subject" {
    interface Subject<T> {
        lift<R>(operator: Operator<T, R>): Observable<R>;
    }
}

@IonicPage({ priority: "high" })
@Component({
    selector: "page-calendar",
    templateUrl: "calendar.html",
})
export class CalendarPage {
    private _input: HTMLElement = null;
    @ViewChild("inputCalendar") input: HTMLElement;
    @ViewChild(Content) content: Content;
    private _orgStateSubscription: Subscription;
    private _authSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    private _isSocketConnected: boolean = true;
    private _sessionStateSubscription: Subscription;

    public tasks$: Observable<Array<ITask>>;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public hasBuildings: number = 0;
    public authState$: Observable<IAuthState>;
    public orgID: string = "";
    public buildingID: string = "";
    public staffUsers$: Observable<Array<IUser>>;
    public userState$: Observable<IUserState>;
    public TaskToggle: string = "calendar";
    public RequestsTitle: string = "Your Requests";
    public TasksScope: string = "All";
    public activeDate: Date = new Date(Date.now());
    public activeDate$: Observable<Date>;
    public eventDates: Date[] = [];
    public TravelMode$: Observable<google.maps.TravelMode>;
    public SessionState$: Observable<ISessionState>;
    public eventSource: any;
    public viewTitle: any;
    public DateNames: string[] = [];
    public startingAddress: string = '68 4th Place, Brooklyn, NY';
    public ShowingRoute: boolean = false;
    public TravelMode: string = "Bike";
    public IsLoading: boolean = false;
    public IsListView: boolean = true;

    public isToday: boolean;

    public viewDate: Date = new Date(Date.now());

    public allEvents$: Observable<any[]>;
    public activeDayIsOpen: boolean = false;
    public isNewTask: boolean = false;
    public dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    public recurringEvents: any[] = [];
    // public actions: CalendarEventAction[] = [
    //     {
    //         label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
    //         onClick: ({ event, date }: { event: RecurringEvent; date: Date }): void => {
    //             // this.RecurringTask(event.task, date);
    //         },
    //     },
    //     {
    //         label: '<i style="color: #fff !important;" class="fa fa-fw fa-times"></i>',
    //         onClick: ({ event }: { event: RecurringEvent }): void => {
    //             this.allEvents = this.allEvents.filter((iEvent) => iEvent !== event);
    //             //  this.handleEvent("Deleted", event);
    //         },
    //     },
    // ];
    public view: string = "day";
    public WeekDays: WeekdayStr[] = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"];
    public DurationOptions: string[] = ["day(s)", "week(s)", "month(s)", "year(s)"];
    public FrequencyOptions: string[] = ["Daily", "Weekly", "Monthly", "Yearly"];
    public FullDate: Date = new Date(Date.now());
    public dateRange = {
        from: this.FullDate,
        to: this.FullDate.setMonth(new Date(Date.now()).getMonth() + 1),
    };

    public CurrentMonth: number = 0;
    public TodayDate: string = `${this.FullDate.getMonth()}/${this.FullDate.getDay()}/${this.FullDate.getFullYear()}`;
    public OneMonth: string = `${this.FullDate.getMonth() + 1}/${this.FullDate.getDay()}/${this.FullDate.getFullYear()}`;

    public refresh: Subject<any> = new Subject();
    public selectedStaff$: Observable<IUser>;

    public orgHasBuildings(org: IOrgBasic): boolean {
        if (org.buildings.length > 0 && null != org.buildings) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    get IsStaff(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Staff";
    }

    get IsNotTenant(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role !== "Landlord" && this.sessionData.user.role !== "Tenant";
    }

    get IsLandLord(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Landlord";
    }

    get IsAdmin(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.isAdmin;
    }

    get IsTenant(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.role === "Tenant";
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsSocketConnected(): boolean {
        return this._isSocketConnected;
    }

    set IsSocketConnected(socketConnected: boolean) {
        this._isSocketConnected = socketConnected;
    }

    constructor(
        public store: Store<AppState>,
        public photoViewer: PhotoViewer,
        public modalCtrl: ModalController,
        public sessionData: SessionData,
        public events: Events,
        public controls: Controls,
        public tabService: TabsService,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        private _sessionActions: SessionActions,
        private _orgActions: OrgActions,
        private _userActions: UserActions,
        public locationProvider: LocationProvider,
        public platform: Platform,
        private cdr: ChangeDetectorRef,
        public orgActions: OrgActions) {
        this.staffUsers$ = this.store.select((state) => state.usersState.users);
        this.error$ = this.store.select((state) => state.orgState.error);
        this.allEvents$ = this.store.select((state) => state.sessionState.allEvents);
        this.tasks$ = this.store.select((state) => state.sessionState.tasks);
        this.loading$ = this.store.select((state) => state.sessionState.loading);
        this.activeDate$ = this.store.select((state) => state.sessionState.activeDate);
        this.orgState$ = this.store.select((state) => state.orgState);
        this.authState$ = this.store.select((state) => state.authState);
        this.userState$ = this.store.select((state) => state.usersState);
        this.SessionState$ = this.store.select((state) => state.sessionState);
        this.selectedStaff$ = this.store.select((state) => state.sessionState.selectedStaff);
        this.TravelMode$ = this.store.select((state) => state.sessionState.travelMode);
        this.TaskToggle = "calendar";
    }

    ionViewDidLoad(): void {
        this.activeDate = new Date(Date.now());
        this.activeDate.setMonth(this.activeDate.getMonth());
        this.activeDate.setHours(0);
        this.store.dispatch(this._sessionActions.ChangeActiveDate(this.activeDate));
        let today: Date = this.activeDate;
        today.setHours(today.getHours());
        let oneMonthBeforeNow: Date = new Date(today);
        oneMonthBeforeNow.setMonth(oneMonthBeforeNow.getMonth() - 4);
        let oneMonthAfterNow: Date = new Date(today);
        let monthNo: number = oneMonthAfterNow.getMonth();
        if (monthNo > 6) {
            oneMonthAfterNow.setFullYear(oneMonthAfterNow.getFullYear() + 1);
            oneMonthAfterNow.setMonth(6);
        } else {
            oneMonthAfterNow.setMonth(oneMonthAfterNow.getMonth() + 12);
        }
        let self = this;
        if (this.platform.is('cordova')) {
            this.locationProvider.GetGeoLocation()
            .then((resp: any) => {
                let google_map_position = new google.maps.LatLng( resp.latitude, resp.longtitude );
                let google_maps_geocoder = new google.maps.Geocoder();
                google_maps_geocoder.geocode(
                    { 'latLng': google_map_position },
                    function( results: any, status: any ) {
                        console.log( results );
                        if ( status == google.maps.GeocoderStatus.OK && results[0] ) {
                            self.startingAddress = results[0].formatted_address;
                            console.log("startingAddress", self.startingAddress);
                        }
                    }
                );
                self.store.dispatch(self._sessionActions.ChangeLocation(`${resp.coords.latitude}, ${resp.coords.longitude}`));
            });
        }
        this.getDatesBetweenDates(oneMonthBeforeNow, oneMonthAfterNow);
        this.TaskToggle = "calendar";
        let date: Date = new Date(Date.now());
        date.setHours(today.getHours() - 4);
        this._authSubscription = this.authState$.subscribe((state: IAuthState) => {
            if (null != state) {
            }
        });
        this._sessionStateSubscription = this.SessionState$.subscribe(
            async (state: ISessionState) => {
                if (null != state) {
                    // console.log("SessionStateType", SessionStateType[state.type], state.recurringTasks);
                    switch (state.type) {
                        case SessionStateType.GET_RECURRING_TASKS_FAILED:
                        {
                            this.IsLoading = false;
                            break;
                        }

                        case SessionStateType.COMPLETE_TASK_SUCCEEDED:
                        case SessionStateType.GET_USERS_SUCCEEDED:
                        {
                            this.cdr.detectChanges();
                            break;
                        }

                        case SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED:
                        case SessionStateType.CREATE_TASKS_SUCCEEDED:
                        case SessionStateType.GET_RECURRING_TASKS_SUCCEEDED:
                        {
                            this.cdr.detectChanges();
                            this.activeDate = state.activeDate;

                            setTimeout(() => {
                                this.IsLoading = false;
                                // this.updateCalendarEvents();
                            }, 2000);
                            break;
                        }

                        default:
                        {
                            break;
                        }
                    }
                }
            },
            (error: any) => {
                this.IsLoading = false;
                // console.log("Error - " + error);
            }
        );
        this._subscribeToSocketConnect();
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }

        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }
        this.IsLoading = false;
    }

    public GetStaffName(user: IUser): string {
        if (null != user) {
            return `${user.firstName} ${user.lastName}`;
        } else {
            return "";
        }
    }

    public GoToToday(): void {
        let newDate: Date = new Date(Date.now());
        this.store.dispatch(this._sessionActions.ChangeActiveDate(newDate));
    }

    public ScopeStaffTasks(): void {
        // this.store.dispatch(this._sessionActions.GetUsersScope(null));
        let modal = this.modalCtrl.create("AudienceChooserPage", { searchStaff: false, searchTenants: false, selected: null, scopingTasks: true });
        modal.present();
    }

    public ScrollUp(): void {
        this.content.scrollToTop();
    }

    public onDateChanged(event: any): void {
        console.log("onDateChanged", event);
        let parts: string[] = event.split("-", 3);
        let newDate: Date = new Date(Number(parts[0]), Number(parts[1]) - 1, Number(parts[2]));
        Object.assign({}, this.activeDate, newDate);
        this.store.dispatch(this._sessionActions.ChangeActiveDate(newDate));
        // console.log("event", event, newDate);
        this.cdr.detectChanges();
        // this.store.dispatch(this._sessionActions.RefreshRecurringTasks());
    }

    public AutoComplete(ele: HTMLInputElement): void {
        this._input = ele;
        // console.log("input", ele);
        const options = {
            fields: ["formatted_address", "address_component", "geometry", "name"],
            strictBounds: false,
            types: ["address"],
        };
        const autocomplete = new google.maps.places.Autocomplete(this._input, options);

        const infowindow = new google.maps.InfoWindow();
        const infowindowContent = document.getElementById("infowindow-content") as HTMLElement;

        infowindow.setContent(infowindowContent);

        autocomplete.addListener("place_changed", () => {
            infowindow.close();
            const place = autocomplete.getPlace();
            let street_number = place.address_components.find((data: any) => {
                return data.types.includes("street_number");
            });
            let route = place.address_components.find((data: any) => {
                return data.types.includes("route");
            });
            // console.log("route", route, street_number);
            let addressParts: string[] = place.formatted_address.split(",");
            let zipParts: string[] = addressParts[addressParts.length - 2].split(" ");
            let city: string = addressParts[addressParts.length - 3];
            let state: string = zipParts[1];
            let zip: string = zipParts[2];
            this.startingAddress = `${street_number.short_name} ${route.short_name}, ${city}, ${state} ${zip}`;
            this.store.dispatch(this._sessionActions.ChangeLocation(this.startingAddress));
            this.ShowingRoute = false;
            // console.log("addressParts", zipParts, addressParts[addressParts.length - 2], city, state);
            // this.bldgForm.patchValue({
            //     info: {
            //         name: `${street_number.short_name} ${route.short_name}`,
            //         address: {
            //             line1: `${street_number.short_name} ${route.short_name}`,
            //             city: city,
            //             state: state,
            //             zip: zip,
            //         },
            //     },
            // });
            // this.HasSelectedAnAddress = true;
        });
    }


    public OpenFullImage(task: ITask): void {
        if (null != task.photoURL && task.photoURL !== "") {
            this.photoViewer.show(task.photoURL);
        }
    }

    public SwitchListView(): void {
        this.IsListView = !this.IsListView;
    }

    public expandTask(task: ITask = null, fab?: FabContainer): void {
        if (null != fab) {
            fab.close();
        } else {
            // don't close fab
        }
        const modal: Modal = this.modalCtrl.create("TasksDetailsPage", { task: task, isEditMode: false, date: this.activeDate, recurring: false });
        modal.present();
        this.tabService.hide();
        // this.navCtrl.push("TasksDetailsPage", {task: task, isEditMode: false });
    }

    public newRecurringTask(fab?: FabContainer): void {
        if (null != fab) {
            fab.close();
        } else {
            // don't close fab
        }
        const modal: Modal = this.modalCtrl.create("TasksDetailsPage", { task: null, isEditMode: false, date: this.activeDate, recurring: true });
        modal.present();
        this.tabService.hide();
    }

    public searchByName(): void {
        if (this.orgSearch.trim()) {
        } else {
            // No string detected
            // Do nothing
        }
    }

    public DisableControls(args: boolean): void {
        this.IsSocketConnected = args;
    }

    private getDatesBetweenDates(startDate: Date, endDate: Date): Date[] {
        let dates: Date[] = [];
        //to avoid modifying the original date
        const theDate = startDate;
        while (theDate < endDate) {
            dates = [...dates, new Date(theDate)];
            theDate.setDate(theDate.getDate() + 1);
        }
        dates = [...dates, endDate];
        this.eventDates = UserUtil.SortDates(dates);
        // // console.log("eventDates", this.eventDates);
        return UserUtil.SortDates(dates);
    }

    public GetMode(travelMode: google.maps.TravelMode): string {
        let mode: string = "Bike";
        switch (travelMode) {
            case google.maps.TravelMode.WALKING:
            {
                mode = 'Walking';
                break;
            }

            case google.maps.TravelMode.DRIVING:
            {
                mode = 'Car';
                break;
            }

            case google.maps.TravelMode.BICYCLING:
            {
                mode = 'Bike';
                break;
            }

            case null:
            default:
            {
                break;
            }
        }
        return mode;
    }

    public ChangeMode(travelMode: string): void {
        
        this.TravelMode = travelMode;
        console.log("TravelMode", this.TravelMode);
        let mode: google.maps.TravelMode = google.maps.TravelMode.WALKING;
        switch (this.TravelMode) {
            case 'Walking':
            {
                mode = google.maps.TravelMode.WALKING;
                console.log("Walking", mode);
                break;
            }

            case 'Car':
            {
                mode = google.maps.TravelMode.DRIVING;
                console.log("Ca",mode);
                break;
            }

            case 'Bike':
            {
                mode = google.maps.TravelMode.BICYCLING;
                console.log("Bike", mode);
                break;
            }

            case "":
            case null:
            {
                mode = google.maps.TravelMode.BICYCLING;
                break;
            }

            default:
            {
                break;
            }
        }
        this.store.dispatch(this._sessionActions.SwitchTravelMode(mode));
    }

    public ChangeLocation(): void {
        if ((null != this.startingAddress) && (this.startingAddress.trim() !== "")) {
            this.store.dispatch(this._sessionActions.ChangeLocation(this.startingAddress));
            this.ShowingRoute = false;
        } else {
            this.controls.ShowAlert("Missing Address", "Please enter a valid address", "Try Again");
        }
    }

    public FormatTime(time: string): string {
        let newTime: string = time.split(":", 2)[0];
        let numberTime: number = Number(newTime);
        if (Number(newTime) > 12) {
            newTime = `${numberTime - 12}:${time.split(":", 2)[1]} PM`;
            return newTime;
        } else if (Number(newTime) === 12) {
            newTime = `${numberTime}:${time.split(":", 2)[1]} PM`;
            return newTime;
        } else {
            newTime = `${numberTime}:${time.split(":", 2)[1]} AM`;
            return newTime;
        }
    }

    public ChangeScope(scope: string): void {
        // console.log("scope", scope);
        this.store.dispatch(this._sessionActions.ScopeBuildingTasks(this.TasksScope));
    }

    public StartProgress(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Begin Request?",
            message: "Please confirm you want to begin this request.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.status = "In Progress";
                        this.store.dispatch(this._sessionActions.StartTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetRecurringTasks());
                                // setTimeout(() => {
                                    // this.store.dispatch(this._userActions.GetUsersScope(null));
                                // }, 500);

                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public MarkComplete(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Complete Request?",
            message: "Please confirm you want to mark this request as completed.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.active = false;
                        task.status = "Completed";
                        let date: string = `${this.activeDate.getMonth() + 1}_${this.activeDate.getDate()}_${this.activeDate.getFullYear()}`;
                        if (null != task.completedDates) {
                            task.completedDates.push(date);
                        } else {
                            task.completedDates = [];
                            task.completedDates.push(date);
                        }
                        this.store.dispatch(this._sessionActions.CompleteTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetRecurringTasks());
                                // setTimeout(() => {
                                    // this.store.dispatch(this._userActions.GetUsersScope(null));
                                // }, 500);
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public MarkRecurringComplete(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Complete Job?",
            message: "Please confirm you want to mark this job as completed.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.status = "Assigned";
                        let date: string = `${this.activeDate.getMonth() + 1}_${this.activeDate.getDate()}_${this.activeDate.getFullYear()}`;
                        if (null != task.completedDates) {
                            task.completedDates.push(date);
                        } else {
                            task.completedDates = [];
                            task.completedDates.push(date);
                        }
                        this.store.dispatch(this._sessionActions.CompleteRecurringTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetRecurringTasks());
                                // setTimeout(() => {
                                    // this.store.dispatch(this._userActions.GetUsersScope(null));
                                // }, 500);
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public ReOpenRecurring(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Un-Mark Complete?",
            message: "Please confirm you want to un-mark this task as complete.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        let date: string = `${this.activeDate.getMonth() + 1}_${this.activeDate.getDate()}_${this.activeDate.getFullYear()}`;
                        if (null != task.completedDates) {
                            task.completedDates = task.completedDates.filter((oldDate: string) => {
                                return (oldDate !== date);
                            });
                        }
                        task.status = "In Progress";
                        this.store.dispatch(this._sessionActions.ReopenRecurringTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetRecurringTasks());
                                // setTimeout(() => {
                                    // this.store.dispatch(this._userActions.GetUsersScope(null));
                                // }, 500);
                                // this.store.dispatch(this._userActions.GetUsers(null));
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public ReOpen(task: ITask): void {
        let alert: Alert = this.alertCtrl.create({
            title: "Un-Mark Complete?",
            message: "Please confirm you want to un-mark this task as complete.",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                },
                {
                    text: "Confirm",
                    handler: () => {
                        task.active = true;
                        task.status = "In Progress";
                        this.store.dispatch(this._sessionActions.ReopenTask(task));
                        setTimeout(() => {
                            if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                                if (this.sessionData.user.role === "Tenant" || this.sessionData.user.role === "Landlord") {
                                    this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                                } else {
                                    this.orgID = this.sessionData.org.id;
                                    this.buildingID = this.sessionData.org.buildings[0].id;
                                    let bldgHelper: IGetBldgHelper = {
                                        orgID: this.sessionData.org.id,
                                        buildingID: this.sessionData.org.buildings[0].id,
                                    };
                                    // console.log("bldgHelper", bldgHelper);
                                    this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                                    this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                                }
                            } else {
                                this.store.dispatch(this._sessionActions.GetRecurringTasks());
                                // setTimeout(() => {
                                    // this.store.dispatch(this._userActions.GetUsersScope(null));
                                // }, 500);
                            }
                        }, 700);
                    },
                },
            ],
        });
        alert.present();
    }

    public IsCompleted(task: ITask): boolean {
        let date: string = `${this.activeDate.getMonth() + 1}_${this.activeDate.getDate()}_${this.activeDate.getFullYear()}`;
        if (null != task.completedDates && task.completedDates.length > 0 && task.completedDates.includes(date)) {
            return true;
        } else {
            return false;
        }
    }

    private _subscribeToSocketConnect(): void {
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (args: boolean) => {
            this.DisableControls(args);
            if (args) {
                // if (null != this.sessionData.user && !this.sessionData.user.isAdmin) {
                //     if (this.sessionData.user.role === "Tenant") {
                //         this.store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                //     } else {
                //         this.orgID = this.sessionData.org.id;
                //         this.buildingID = this.sessionData.org.buildings[0].id;
                //         let bldgHelper: IGetBldgHelper = {
                //             orgID: this.sessionData.org.id,
                //             buildingID: this.sessionData.org.buildings[0].id,
                //         };
                //         // console.log("bldgHelper", bldgHelper);
                //         this.store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                //         this.store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                //     }
                // } else {
                //     this.store.dispatch(this._sessionActions.GetTasks());
                //     this.store.dispatch(this._userActions.GetUsers(null));
                // }
            }
        });
    }

    private _getDay(day: number): string {
        switch (day) {
            case 0: {
                return "Sunday";
            }

            case 1: {
                return "Monday";
            }

            case 2: {
                return "Tuesday";
            }

            case 3: {
                return "Wednesday";
            }

            case 4: {
                return "Thursday";
            }

            case 5: {
                return "Friday";
            }

            case 6: {
                return "Saturday";
            }

            default: {
                break;
            }
        }
    }

    private _nth(d: number): string {
        if (d > 3 && d < 21) {
            return "th";
        } else {
            switch (d % 10) {
                case 1: {
                    return "st";
                }

                case 2: {
                    return "nd";
                }

                case 3: {
                    return "rd";
                }

                default: {
                    return "th";
                }
            }
        }
    }

    private _sortBy(users: RecurringEvent[]): RecurringEvent[] {
        let newUsers: RecurringEvent[] = users.sort((user1, user2) => {
            if (user1.start < user2.start) {
                return -1;
            }

            if (user1.start > user2.start) {
                return 1;
            }

            return 0;
        });
        // console.log("_sortBy", newUsers);
        return newUsers;
    }

    private _weekCount(m: moment.Moment) {
        return m.week() - moment(m).startOf("month").week();
    }

    private _nthDayOfMonth(monthMoment: moment.Moment, day: number, weekNumber: number): string {
        let m = monthMoment.clone().startOf("month").day(day);
        if (m.month() !== monthMoment.month()) m.add(7, "d");
        return m.add(7 * (weekNumber - 1), "d").format("YYYY-MM-DD");
    }
}





/*

    OLD CODE

*/

// this.recurringEvents = [];
// this.allEvents = [];
// let allTasks: ITask[] = [];
// Object.assign(allTasks, state.recurringTasks);
// console.log("allTasks", allTasks);
// this.IsLoading = true;
// let taskAction = allTasks.forEach((task: ITask) => {
//     // for (let task of allTasks) {
//     let matchDate = `${state.activeDate.getMonth()}_${state.activeDate.getDate()}_${state.activeDate.getFullYear()}`;
//     if ((null != task.skipDates && !task.skipDates.includes(matchDate)) || null == task.skipDates) {
//         let startHours: number = Number(task.startTime.split(":", 2)[0]);
//         let endHours: number = Number(task.endTime.split(":", 2)[0]);
//         let startMinutes: number = Number(task.startTime.split(":", 2)[1]);
//         let endMinutes: number = Number(task.endTime.split(":", 2)[1]);
//         let currentDate: Date = new Date(Date.now());
//         let taskDate: Date = new Date(task.startDate);
//         let taskEndDate: Date = new Date(task.endDate);
//         let shouldAddEvent: boolean = false;
//         let newEvent: any = {
//             start: new Date(taskDate.getFullYear(), taskDate.getMonth(), taskDate.getDate(), startHours, startMinutes),
//             end: new Date(taskEndDate.getFullYear(), taskEndDate.getMonth(), taskEndDate.getDate(), endHours, endMinutes),
//             title: `${task.subject}`,
//             actions: this.actions,
//             color: colors.blue,
//             resizable: {
//                 beforeStart: true,
//                 afterEnd: true,
//             },
//             meta: `${task.subject}`,
//             displayTitle: `${task.subject}`,
//             startTime: new Date(date.getFullYear(), date.getMonth(), date.getDate(), startHours, startMinutes),
//             endTime: new Date(date.getFullYear(), date.getMonth(), date.getDate(), endHours, endMinutes),
//         };
//         Object.assign(newEvent, {
//             task: task,
//         });
//         if (null != newEvent.end && null != state.activeDate && newEvent.end.getTime() > state.activeDate.getTime()) {
//             switch (task.repeatInterval) {
//                 case "Daily": {
//                     newEvent.rrule = {
//                         freq: RRule.DAILY,
//                         interval: task.repeatQuantity,
//                     };
//                     this.allEvents.push(newEvent);
//                     break;
//                 }

//                 case "Weekly": {
//                     let newWeeklyMonthlyDays = [];
//                     let weeklyMonthDays: string[] = [];
//                     let taskIDs: string[] = [];
//                     if (task.monthlyWeekDays.includes(state.activeDate.getDay())) {
//                         shouldAddEvent = true;
//                         if (!taskIDs.includes(newEvent.task.id)) {
//                             this.allEvents.push(newEvent);
//                             // this.allEvents = this._sortBy(this.allEvents);
//                             this.cdr.detectChanges();
//                             taskIDs.push(newEvent.task.id);
//                             return;
//                         }
//                     }
//                     for (let day of task.monthlyWeekDays) {
//                         weeklyMonthDays.push(this._getDay(day));
//                         this.cdr.detectChanges();
//                     }

//                     break;
//                 }

//                 case "Monthly": {
//                     if (task.monthlyConvention === "Day of month") {
//                         let newMonthlyDays = [];
//                         let weeklyMonthDays: string[] = [];
//                         for (let day of task.monthlyDays) {
//                             newMonthlyDays.push(day);
//                             weeklyMonthDays.push(`${day + 1}${this._nth(day + 1)}`);
//                             if (state.activeDate.getDate() === day + 1) {
//                                 shouldAddEvent = true;
//                                 this.allEvents.push(newEvent);
//                                 // this.allEvents = this._sortBy(this.allEvents);
//                                 this.cdr.detectChanges();
//                             } else {
//                                 shouldAddEvent = false;
//                             }
//                         }
//                         newEvent.title = newEvent.title + ` (Monthly on ${weeklyMonthDays.join(", ")})`;
//                         newEvent.rrule = {
//                             freq: RRule.MONTHLY,
//                             interval: task.repeatQuantity,
//                             bymonthday: newMonthlyDays,
//                         };
//                     } else {
//                         let year = state.activeDate.getFullYear();
//                         let month = state.activeDate.getMonth();
//                         let day = new Date(year + "-" + month + "-01").getDay();
//                         let weekCount = 1;
//                         let currentWeek: number = this._weekCount(moment(state.activeDate));
//                         let dayCount = 1;
//                         let taskIDs: string[] = [];
//                         newEvent.rrule = {
//                             freq: RRule.MONTHLY,
//                             interval: task.repeatQuantity,
//                             byweekday: [],
//                         };
//                         let firstDay = new Date(state.activeDate.getFullYear(), state.activeDate.getMonth(), 1);
//                         let firstDayNumber = firstDay.getDay();
//                         let addCount: number = 0;
//                         taskIDs = [];
//                         // // console.log("firstDayNumber", firstDayNumber, firstDay);
//                         let finalCurrentWeek: number = state.activeDate.getDay() >= firstDayNumber ? currentWeek : currentWeek - 1;
//                         // // console.log("finalCurrentWeek", finalCurrentWeek, finalCurrentWeek);
//                         if (null != task.monthlyNWeekDays[finalCurrentWeek]) {
//                             let keys = task.monthlyNWeekDays[finalCurrentWeek];
//                             for (let day of keys) {
//                                 let result = this._nthDayOfMonth(moment(), day, currentWeek);
//                                 let matchDate: Date = new Date(result);

//                                 if (keys.includes(state.activeDate.getDay())) {
//                                     shouldAddEvent = true;
//                                     addCount = 1;
//                                     if (!taskIDs.includes(newEvent.task.id)) {
//                                         this.allEvents.push(newEvent);
//                                         // this.allEvents = this._sortBy(this.allEvents);
//                                         this.cdr.detectChanges();
//                                         taskIDs.push(newEvent.task.id);
//                                         return;
//                                     }
//                                 }
//                                 dayCount += 1;
//                                 // // console.log("shouldAddEvent", shouldAddEvent, this.activeDate.getDay(), day, currentWeek, weekCount);
//                             }
//                         }
//                         weekCount += 1;
//                         dayCount = 1;
//                         // }
//                         if (addCount > 0) {
//                             addCount = 0;
//                         }
//                     }
//                     break;
//                 }

//                 case "Yearly": {
//                     let newMonthlyDays = [];
//                     for (let day of task.monthlyDays) {
//                         newMonthlyDays.push(day + 1);
//                     }
//                     newEvent.rrule = {
//                         freq: RRule.YEARLY,
//                         interval: task.repeatQuantity,
//                         bymonthday: newMonthlyDays,
//                         bymonth: new Date(task.startDate).getMonth(),
//                     };
//                     let startMonth: number = new Date(task.startDate).getMonth();
//                     let startDay: number = new Date(task.startDate).getDate();
//                     if (state.activeDate.getDate() === startDay && state.activeDate.getMonth() === startMonth) {
//                         shouldAddEvent = true;
//                         this.allEvents.push(newEvent);
                        
//                         this.cdr.detectChanges();
//                     } else {
//                         shouldAddEvent = false;
//                     }
//                     break;
//                 }

//                 default: {
//                     break;
//                 }
//             }
//         }
//         if (task.repeatInterval === "Once") {
//             let newDate: Date = new Date(state.activeDate.getFullYear(), state.activeDate.getMonth(), state.activeDate.getDate());
//             let secondDate: Date = new Date(newEvent.start.getFullYear(), newEvent.start.getMonth(), newEvent.start.getDate());
//             // console.log("newDate", newDate, secondDate);
//             if (newDate.getTime() === secondDate.getTime()) {
//                 this.allEvents.push(newEvent);
//                 // this.allEvents = this._sortBy(this.allEvents);
//                 this.cdr.detectChanges();
//             }
//         }
//     }
// });
// await taskAction;
// this.allEvents = this._sortBy(this.allEvents);