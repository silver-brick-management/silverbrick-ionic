// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { Component, ViewChild, Input } from "@angular/core";
import {
    NavController,
    Events,
    MenuController,
    Alert,
    AlertController,
    Modal,
    ModalController,
    Content,
    NavParams,
    IonicPage
} from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";

// Local modules
import { AppState } from '../../app/app.state';
import { Constants, PROPERTY_TYPES } from '../../common/Constants';
import { IError } from '../../interfaces/IError';
import { IOrgBasic, INote, IUser, IOrgInfo, IBuilding, IBuildingSimple, IBuildingBasic, ITask } from '../../shared/SilverBrickTypes';
import { IOrgState, OrgStateType } from '../../interfaces/IStoreState';
import { OrgActions } from '../../store/actions/OrgActions';
import { IFormSelctedValue, IGetBldgHelper } from '../../shared/IParams';
import { TabsService } from '../../providers/TabsService';
import { SessionActions } from "../../store/actions/SessionActions";
import { SessionData } from "../../providers/SessionData";
import { Controls } from "../../providers/Controls";

import { OrgBldgUtil } from '../../shared/OrgBldgUtil';

@IonicPage({ priority: "high" })
@Component({
    selector: "page-building-details",
    templateUrl: "building-details.html"
})
export class BuildingDetailsPage {
    @ViewChild(Content) content: Content;
    public allBuildings$: Observable<IBuilding[]>;
    public notes$: Observable<INote[]>;
    public Orgs: Array<IOrgBasic> = [];
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public hasBuildings: number = 0;
    public BuildingTitle: string = "Building";
    public building$: Observable<IBuilding>;
    private _orgStateSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    public tasks$: Observable<Array<ITask>>;
    public BuildingToggle: string = "details";
    public building: IBuilding = null;
    public Landlords$: Observable<IUser[]>;

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Tenant'));
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    public orgHasBuildings(org: IOrgBasic): boolean {
        if ((org.buildings.length > 0) && (null != org.buildings)) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    constructor(
        public store: Store<AppState>,
        public sessionData: SessionData,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public sessionActions: SessionActions,
        public navCtrl: NavController,
        public tabService: TabsService,
        public controls: Controls,
        public orgActions: OrgActions) {
        this.tasks$ = this.store.select((state) => state.sessionState.tasks);
        this.error$ = this.store.select(state => state.orgState.error);
        this.allBuildings$ =  this.store.select(state => state.orgState.allFullBuildings);
        this.notes$ =  this.store.select(state => state.orgState.notes);
        this.building$ = this.store.select(state => state.orgState.selectedBldg);
        this.loading$ = this.store.select(state => state.orgState.loading);
        this.orgState$ = this.store.select(state => state.orgState);
    }

    ionViewWillEnter(): void {
        this._orgStateSubscription = this.orgState$
        .subscribe((state: IOrgState) => {
            if (null != state) {
                if (null != state.selectedBldg) {
                    this.building = state.selectedBldg;
                    this.BuildingTitle = state.selectedBldg.info.name;
                }
            }
        });
    }

    ngOnInit(): void {

    }

    ionViewDidLeave(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
    }

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
    }

    public GetIcon(type: string): string {
        switch (type) {
            case "Multi-Family":
            {
                return "assets/icon/buildings.png";
            }

            case "Single-Family":
            {
                return "assets/icon/house.png";
            }
            
            default:
            {
                return "assets/icon/building.png";
            }
        }
    }

    public AddNote(): void {
        let alert: Alert = this.alertCtrl.create({
            title: 'Add a Note',
            inputs: [
            {
                name: 'title',
                placeholder: 'Title'
            },
            {
                name: 'message',
                placeholder: 'Message',
                type: 'textarea'
            }
            ],
            buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: (data: any) => {
                    console.log('Cancel clicked');
                }
            },
            {
                text: 'Save',
                handler: (data: any) => {
                    if ((null != data) &&
                        (null != data.message) && (data.message !== "")
                         && (null != data.title) && (data.title !== "")) {
                        let newNote: INote = {
                            deleted: false,
                            message: data.message,
                            authorName: this.sessionData.username,
                            authorID: this.sessionData.userID,
                            buildingID: this.building.id,
                            orgID: this.building.orgID,
                            title: data.title,
                            updated: new Date(Date.now()).getTime(),
                            date: new Date(Date.now()).getTime()
                        };
                        this.store.dispatch(this.orgActions.AddNote(newNote));
                        setTimeout(() => {
                            this.controls.ShowAlert("New Note Added", "Your new note was successfully added", "Dismiss");
                        }, 500);
                        } else {
                            this.controls.ShowAlert('Missing Information', 'Please make sure all fields are filled out', 'Dismiss');
                        }
                }
            }
            ]
        });
        alert.present();
    }

    public UpdateNote(note: INote): void {
        let alert: Alert = this.alertCtrl.create({
            title: note.title,
            inputs: [
            {
                name: 'title',
                placeholder: 'Title',
                value: note.title
            },
            {
                name: 'message',
                placeholder: 'Message',
                value: note.message,
                type: 'textarea'
            }
            ],
            buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: (data: any) => {
                    console.log('Cancel clicked');
                }
            },
            {
                text: 'Save',
                handler: (data: any) => {
                    if ((null != data) &&
                        (null != data.message) && (data.message !== "")
                         && (null != data.title) && (data.title !== "")) {
                        let newNote: INote = {
                            deleted: false,
                            message: data.message,
                            authorName: this.sessionData.username,
                            authorID: this.sessionData.userID,
                            buildingID: this.building.id,
                            orgID: this.building.orgID,
                            id: note.id,
                            title: data.title,
                            updated: new Date(Date.now()).getTime(),
                            date: new Date(Date.now()).getTime()
                        };
                        this.store.dispatch(this.orgActions.UpdateNote(newNote));
                        setTimeout(() => {
                            this.controls.ShowAlert("Note Updated", "Your new note was successfully updated", "Dismiss");
                        }, 500);
                    } else {
                        this.controls.ShowAlert('Missing Information', 'Please make sure all fields are filled out', 'Dismiss');
                    }
                    
                }
            }
            ]
        });
        alert.present();
    }

    public expandTask(task: ITask = null): void {
        const modal: Modal = this.modalCtrl.create("TasksDetailsPage", {task: task, isEditMode: false });
        modal.present();
        this.tabService.hide();
        // this.navCtrl.push("TasksDetailsPage", {task: task, isEditMode: false });
    }

    public closeAction(): void {
        this._dismissDialog();
        this.tabService.show();
    }

    private _dismissDialog(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.navCtrl.pop();
    }
}
