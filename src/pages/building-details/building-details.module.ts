// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuildingDetailsPage } from './building-details';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module';

@NgModule({
  declarations: [
    BuildingDetailsPage,
  ],
  imports: [
    LoadingComponentModule,
    IonicPageModule.forChild(BuildingDetailsPage),
  ],
  exports: [
  	BuildingDetailsPage
  ]
})
export class BuildingDetailsPageModule {}
