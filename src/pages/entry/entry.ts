// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { NavController, Events } from "ionic-angular";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IAuthState, AuthStateType } from "../../interfaces/IStoreState";
import { MenuUtil, EnableMenuType } from "../../providers/menu-util";
import { AuthActions } from "../../store/actions/AuthActions";
import { IonicPage } from "../../../node_modules/ionic-angular/navigation/ionic-page";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-entry",
    templateUrl: "entry.html"
})
export class EntryPage implements OnInit, OnDestroy {
    // private variables
    private _authStateSubscription: Subscription;
    private _didLoginFail: boolean;

    // public variables
    public authState$: Observable<IAuthState>;
    public LoadingText: string = "Loading...";

    get didLoginFail(): boolean {
        return this._didLoginFail;
    }

    set didLoginFail(complete: boolean) {
        this._didLoginFail = complete;
    }

    constructor(
        public menuUtil: MenuUtil,
        public store: Store<AppState>,
        public events: Events,
        public authActions: AuthActions,
        public navCtrl: NavController) {
        /* Disable the menu, since this is the entry screen */
        this.menuUtil.ShouldEnableMenu(EnableMenuType.ALL, false);
        this.authState$ = this.store.select(state => state.authState);
    }

    ngOnInit(): void {
        this.LoadingText = "Loading...";
        /*
            Initialize Auth Logic
        */
        this.events.subscribe(Constants.EVENT_LOGIN_FAILED, () => {
            this.didLoginFail = true;
        });
        this._authStateSubscription = this.authState$.subscribe(
            (currentAuthState: IAuthState) => {
                if (currentAuthState.authRefreshInitiated) {
                    this.LoadingText = "RELOADING...";
                } else {
                    switch (currentAuthState.type) {
                        // Idle states
                        case AuthStateType.LOGOUT_SUCCEEDED:
                        {
                            this.LoadingText = "LOG OUT SUCCESSFUL!";
                            break;
                        }

                        case AuthStateType.LOGIN_FAILED:
                        {
                            if (this.didLoginFail) {
                                this.LoadingText = "LOGIN FAILED!";
                            } else {
                                this.LoadingText = "Loading...";
                            }
                            // this.navCtrl.setRoot("LoginPage");
                            break;
                        }

                        case AuthStateType.SOCKET_DISCONNECTED: {
                            this.LoadingText = "LOGGING OUT...";
                            break;
                        }

                        case AuthStateType.SERVER_READY:
                        case AuthStateType.AUTH_SOCKET_READY: {
                            this.LoadingText = "CONNECTING...";
                            break;
                        }

                        case AuthStateType.SERVER_AND_SOCKET_READY: {
                            this.LoadingText = "PLATFORM ALMOST READY...";
                            break;
                        }

                        case AuthStateType.NONE:
                        case AuthStateType.LOADING:
                        default:
                        {
                            this.LoadingText = "Loading...";
                            break;
                        }
                    }
                }
            },
            error => {
                console.log("Entry.ts: Auth subscription failed! Go log in");
            }
        );
    }

    ionViewWillEnter(): void {}

    ngOnDestroy(): void {
        // Unsubscribe from auth state
        // this.events.unsubscribe(Constants.EVENT_LOGIN_FAILED);
        this._authStateSubscription.unsubscribe();
    }
}

