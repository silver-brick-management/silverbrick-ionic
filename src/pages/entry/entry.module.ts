// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

//Node Modules
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EntryPage } from './entry';

@NgModule({
    declarations: [
        EntryPage,
    ],
    imports: [
        IonicPageModule.forChild(EntryPage),
    ],
    exports: [
        EntryPage
    ]
})
export class EntryPageModule {}
