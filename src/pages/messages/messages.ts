// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

// Node Modules
import { Component } from "@angular/core";
import { NavController, ModalController, Modal, Events } from "ionic-angular";
import { Observable, Subscription } from "rxjs/Rx";
import { Store } from "@ngrx/store";

// Local
import { Constants } from "../../common/Constants";
import { SessionData } from "../../providers/SessionData";
import { IUser } from "../../shared/SilverBrickTypes";
import { AppState } from "../../app/app.state";
import { IMessageExpandParams } from "../../shared/IParams";
import { UserChatRoom } from "../../shared/ResponseTypes";
import { MessageActions } from "../../store/actions/MessageActions";
import { Controls } from "../../providers/Controls";
import { IonicPage } from "../../../node_modules/ionic-angular/navigation/ionic-page";
import { IMessageState, MessageStateType } from "../../interfaces/IStoreState";

@IonicPage({ priority: "high" })
@Component({
    selector: "page-messages",
    templateUrl: "messages.html"
})
export class MessagesPage {
    private _profileURL: string;
    public profile$: Observable<IUser>;
    public DefaultProfile: string = Constants.DEFAULT_PROFILE_PHOTO_PATH;
    public ChatRooms$: Observable<UserChatRoom[]>;
    public loading$: Observable<boolean>;
    public loading: boolean = false;
    public IsSocketConnected: boolean = true;
    private _messageStateSubscription: Subscription = null;
    public messageState$: Observable<IMessageState>;

    constructor(
        public navCtrl: NavController,
        public sessionData: SessionData,
        public controls: Controls,
        public events: Events,
        public messageActions: MessageActions,
        public modalCtrl: ModalController,
        public store: Store<AppState>) {
        this._profileURL = null != this.sessionData.user && this.sessionData.user.photoURL ? this.sessionData.user.photoURL : Constants.DEFAULT_PROFILE_PHOTO_PATH;
        this.profile$ = this.store.select(state => state.authState.profile);
        this.ChatRooms$ = this.store.select(state => state.messageState.chatRooms);
        this.loading$ = this.store.select(state => state.messageState.loading);
        this.messageState$ = this.store.select(state => state.messageState);
    }

    get ProfileUrl(): string {
        return null != this._profileURL ? this._profileURL : Constants.DEFAULT_PROFILE_PHOTO_PATH;
    }

    ionViewDidLoad(): void {
        this.store.dispatch(this.messageActions.GetChatRooms());
    }

    ionViewWillEnter(): void {
        this.loading = true;
        this.store.dispatch(this.messageActions.GetChatRooms());
        this.events.subscribe(Constants.EVENT_SOCKET_DISCONNECTED, (args: boolean) => {
            this.IsSocketConnected = args;
            if (args) {
                this.store.dispatch(this.messageActions.GetChatRooms());
            }
        });

        this._messageStateSubscription = this.messageState$.subscribe((state: IMessageState) => {
        if (null != state) {
            switch (state.type) {
                case MessageStateType.GET_CHAT_ROOMS_SUCCESS: {
                    this.loading = false;
                    break;
                }

                default: 
                {
                    this.loading = false;
                    break;
                }
            }
        }
    });
    }

    ionViewDidLeave(): void {
        this.loading = false;
        if (null != this._messageStateSubscription) {
            this._messageStateSubscription.unsubscribe();
            this._messageStateSubscription = null;
        }
    }


    public GetName(user: IUser): string {
        return `${ user.firstName} ${ user.lastName }`;
    }

    public NewMessage(): void {
        if (this.IsSocketConnected) {
            // this.navCtrl.push('MessageExpandPage', <IMessageExpandParams>{ roomID: null, isNewChat: true });
            const modal: Modal = this.modalCtrl.create("MessageExpandPage", <IMessageExpandParams>{ roomID: null, isNewChat: true });
            modal.present();
        } else {
            this.controls.ShowAlert("One Moment", "We are reconnecting you...", "Thanks");
        }
    }

    public OpenChat(room: UserChatRoom): void {
        if (this.IsSocketConnected) {
            // this.navCtrl.push('MessageExpandPage', <IMessageExpandParams>{ participants: room.users, roomID: room.roomID, isNewChat: false });
            const modal: Modal = this.modalCtrl.create("MessageExpandPage", <IMessageExpandParams>{ participants: room.users, roomID: room.roomID, isNewChat: false });
            modal.present();
        } else {
            this.controls.ShowAlert("One Moment", "We are reconnecting you...", "Thanks");
        }
    }

    public IsLink(txt: string): boolean {
        return txt.includes("https") || txt.includes("http");
    }
}

