import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesPage } from './messages';
import { LetterAvatarModule } from '../../components/letter-avatar/letter-avatar.module';
import { LoadingComponentModule } from '../../components/loading-component/loading.component.module'

@NgModule({
	declarations: [
		MessagesPage,
	],
	imports: [
        LoadingComponentModule,
		LetterAvatarModule,
		IonicPageModule.forChild(MessagesPage),
	],
	exports: [
		MessagesPage
	]
})
export class MessagesPageModule {}
