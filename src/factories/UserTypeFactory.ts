import { NewUser } from './Users/NewUser';

export enum UserFactoryType {
	USER,
	DEFAULT_USER
}

export class UserTypeFactory {
	static CreateInstance(type: UserFactoryType): any {
		switch (type) {
		
			case UserFactoryType.USER:
			{
				let newUser = new NewUser();
				return newUser;
			}

			default:
			{
				break;
			}
		}
	}
}
