import { Constants } from "../../common/Constants";
import { ISilverBrickUser, IAddress, IClient } from "../../shared/SilverBrickTypes";

export class NewUser implements ISilverBrickUser {
    firstName: string;
    lastName: string;
    isAdmin: boolean;
    email: string;
    phone: string;
    fcmRegToken: string;
    osType: string;
    photoURL: string;
    role: string;
    clients: IClient[];
    stripeID: string;
    address: IAddress;

    constructor() {
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.role = "Tenant";
        this.isAdmin = false;
        this.phone = "";
        this.fcmRegToken = "";
        this.photoURL = Constants.DEFAULT_FB_PROFILE_PATH;
        this.clients = [];
        this.osType = "ios";
        this.stripeID = "";
        this.address = {
            line1: "",
            line2: "",
            city: "",
            state: "",
            zip: ""
        };
    }
}

