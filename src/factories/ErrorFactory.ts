// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { ErrorType, IError } from '../interfaces/IError';
import { BasicError } from './Errors/BasicError';

export class ErrorFactory {
    static CreateInstance(errorType: ErrorType, data: any, message: string): IError {
        return new BasicError(errorType, data, message);
    }
}