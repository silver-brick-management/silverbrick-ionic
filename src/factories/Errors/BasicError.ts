// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { ErrorType, IError } from "../../interfaces/IError";

export class BasicError implements IError {
    constructor(public Type: ErrorType, public Data: any, public Message: string) {
    }
}