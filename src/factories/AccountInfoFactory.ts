import { IAccountInfo } from '../shared/SilverBrickTypes';

export class AccountInfoFactory implements IAccountInfo {
	object: any;
	created: number;
	currency: string;
	default_source: any;
	delinquent: boolean;
	livemode: boolean;
	shipping: any;
	subscriptions: any;
	id: string;
	name: string;
	metadata: string;

	constructor() {
		this.name = "";
		this.shipping = {name: "Recipient Name", address: { line1: "", line2: "", city: "",state: "NY", postal_code: "" } };
		this.object = "customer";
		this.id = "";
		this.metadata = "";
		this.currency = "USD";
		this.delinquent = false;
		this.livemode = true;
	}
}