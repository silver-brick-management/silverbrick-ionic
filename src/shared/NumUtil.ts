// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

export class NumUtil {
  /**
   * Checks if the given price is valid
   * @param  {string} price
   * @returns boolean
   */
  public static isValidPrice(price: string): boolean {
    const regex = /^[1-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/gm;
    return regex.test(price);
  }

  /**
   * Checks if a number is an integer or not (whole number)
   *
   * @param  {number} n number to check
   * @returns boolean (true if number is an integer)
   */
  public static isInt(n: number): boolean {
    return Number(n) === n && n % 1 === 0;
  }

  /**
   * Format price (e.g. $17 -> $17.00)
   *
   * @param  {number} price
   * @returns number
   */
  public static formatPrice(price: string): string {
    return parseFloat(price).toFixed(2);
  }
}
