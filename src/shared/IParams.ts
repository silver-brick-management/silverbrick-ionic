// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { MessageParticipant } from './ResponseTypes';
import { ISilverBrickUser, IUser, IOrgInfo, IBuilding, ITask } from './SilverBrickTypes';

export interface ICredentials {
  email: string;
  password: string;
}

export interface IChangePasswordParam {
  uid: string;
  credentials: ICredentials;
}

export interface IServerCredentials {
  email: string;
  id_token: string;
}

export interface IUserActivationParam {
  uid: string;
  activationStatus: boolean;
}

export interface IPaginateQueryStrings {
  limit?: string;
  isForward?: boolean;
  prevCursor?: string;
  nextCursor?: string;
}

export interface IPage {
  icon: string;
  title: string;
  component?: any;
  index?: number;
  logsOut?: boolean;
  tabComponent?: any[];
  pushOnStack?: boolean;
  profile?: boolean;
  args?: any;
}

export interface IAddBldgHelper {
    building: IBuilding;
    orgID: string;
}

export interface IGetBldgHelper {
    buildingID: string;
    orgID: string;
}

export interface IFormSelctedValue {
  id: string;
  index: number;
}

export interface IMessageExpandParams {
  isNewChat: boolean;
  roomID?: string;
  participants?: MessageParticipant[];
  fromProfile?: boolean;
  title?: string;
}
