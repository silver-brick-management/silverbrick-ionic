// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

export class StringUtil {

    public static ToNameCase(input: string): string {
        if (input) {
            let inputRes = "";

            // Handles the white space case
            let inputSegment: string[] = input.split(" ");

            // Fence-post
            let initPass = true;
            for (let i in inputSegment) {
                let inputTemp = "";
                let iterStr = inputSegment[i];

                iterStr = StringUtil.toNameCaseHelper(iterStr);

                if (initPass) {
                    initPass = false;
                }
                else {
                    inputRes += " ";
                }

                inputRes += iterStr;
            }

            return inputRes;
        }
        else {
            // null, undefined, or empty, just return back the original string
            return input;
        }
    }

    // This util is only for whole string segments, and
    // not for full name strings that contain white space.
    private static toNameCaseHelper(input: string): string {
        if (input) {
            let inputRes = "";

            // Handles the Mary-Anne case
            let inputSegment: string[] = input.split("-");

            // Fence-post
            let initPass = true;
            for (let i in inputSegment) {
                let inputTemp = "";
                let iterStr = inputSegment[i];

                if (0 === iterStr.length) {
                    // Nothing to do
                }
                else if (1 === iterStr.length) {
                    // A, B, C, etc.
                    inputTemp = iterStr.charAt(0).toUpperCase();
                }
                else {
                    inputTemp = iterStr.charAt(0).toUpperCase();
                    inputTemp += iterStr.slice(1).toLowerCase();
                }

                if (initPass) {
                    initPass = false;
                }
                else {
                    inputRes += "-";
                }

                inputRes += inputTemp;
            }

            return inputRes;
        }
        else {
            // null, undefined, or empty, just return back the original string
            return input;
        }
    }

    public static IsStringNullOrEmpty(input: string): boolean {
        return (!input) || (!(input.trim()));
    }

    // Function to check a string against a REGEX for email validity
    public static IsValidEmail(email: string): boolean {
        let re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,9}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    public static IsValidURL(url: string): boolean {
        let re = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/gm;
        return re.test(url);
    }

    public static RemoveQuotations(input: string): string {
        let output: string = input.replace(/'/g, "");
        output = output.replace(/"/g, "");
        return output;
    }

    // Helper function to validate string length
    public static IsOutofBounds(word: string, min: number, max: number): boolean {
        // If the string is outside the passed in bounds...
        return !(word.length > max || word.length < min);
    }

    public static GenerateUUID(): string {
        let d = Date.now();
        console.log("GenerateUUID", d);
        let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            let r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === "x" ? r : (r & 0x3 | 0x8))
            .toString(16);
        });
        if (this.RemoveDashes(uuid)) {
            uuid = uuid.replace(/-/g, "");
        }
        return uuid;
    }

    public static RemoveDashes(str: string): string {
        let newStr = str.replace(/-/g, "");
        return newStr;
    }
}
