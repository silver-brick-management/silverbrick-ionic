
import { IBuildingBasic, IBuildingSimple } from './SilverBrickTypes';

export class OrgBldgUtil {
    static Transform(value: any): IBuildingSimple[] {
        let keys = Object.keys(value);
        let bldgArray: IBuildingSimple[] = [];
       // let newKeys: IBuildingBasic[] = keys.map(k => value[k]);
        keys.forEach((key: string) => {
            let newBldg: IBuildingSimple = {
                id: key,
                name: value[key]
            };
            bldgArray.push(newBldg);
        });

        return bldgArray;
    }

    static TransformBasic(value: any): IBuildingBasic[] {
        if (!value || (null == value)) {
            const newBldg: IBuildingBasic[] = [];
            return newBldg;
        }
        let keys = Object.keys(value);
        let newKeys: IBuildingBasic[] = keys.map(k => value[k]);
        
        return newKeys;
    }
}