// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.text', which can be found at the root of this project.
//
// ****************************************************************

import { UserChatRoom } from "./ResponseTypes";

export class ArrayUtil {
    static TransformFuck(value: any): any[] { // turn object to string[]
        let newShit: any[] = [];
        if (value != null) {
            const keys = Object.keys(value);
            newShit = keys.map(k => value[k]);
        }
        return newShit;
    }

    static sortChatRooms(rooms: UserChatRoom[]): UserChatRoom[] {
        let chatRooms: UserChatRoom[] = [];
        if (null != rooms) {
            chatRooms = rooms.sort((room1, room2) => {
                if (room1.lastMessage.date > room2.lastMessage.date) {
                    return -1;
                }
                if (room1.lastMessage.date < room2.lastMessage.date) {
                    return 1;
                }
                return 0;
            });
        }

        return chatRooms;
    }

  /**
   * Checks if signed in user is included in array of users
   * @param  {string[]} users - array of users to check against
   * @param  {string} uid - uid to check for
   *
   * @returns boolean - true if signed in user is included
   */
   static DoesInclude(users: string[], uid: string): boolean {
       const data: string[] = (null != users) ? this.TransformFuck(users) : [];
       return data.includes(uid);
   }

}
