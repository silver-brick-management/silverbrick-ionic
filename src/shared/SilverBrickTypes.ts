// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.text'; which can be found at the root of this
// project.
//
// *********************************************************

import RRule from 'rrule';
import { CalendarEvent } from "angular-calendar";

/*
  Account Related
*/

export interface IUser {
    firstName: string;
    lastName: string;
    uid?: string;
    isAdmin: boolean;
    isVendor?: boolean;
    email: string;
    phone: string;
    fcmRegToken: string;
    osType: string;
    password?: string;
    photoURL: string;
    fullPhotoURL?: string;
    createdAt?: Date;
    lastSignedInAt?: Date;
    orgs?: IUserOrg;
    role: string;
    lastActivity?: number;
    clients: IClient[];
    stripeID: string;
    address?: IAddress;
    propertyCodes?: number;
    unit?: string;
    isFirstTime?: boolean;
    isClockedIn?: boolean;
    taskCount?: number;
    completedTaskCount?: number;
    todayTaskCount?: number;
    todayCompletedTaskCount?: number;
}

export interface ISilverBrickUser {
    firstName: string;
    lastName: string;
    uid?: string;
    isAdmin: boolean;
    isVendor?: boolean;
    email: string;
    phone: string;
    fcmRegToken: string;
    lastActivity?: number;
    osType: string;
    password?: string;
    photoURL: string;
    fullPhotoURL?: string;
    createdAt?: Date;
    lastSignedInAt?: Date;
    orgs?: OrgToPermissionMap;
    role: string;
    clients: IClient[];
    stripeID: string;
    address?: IAddress;
    propertyCodes?: number;
    unit?: string;
    isFirstTime?: boolean;
    isClockedIn?: boolean;
}

export interface IClient {
    id: string;
    name: string;
    photoURL: string;
    email: string;
    active: boolean;
    scheduleInfo: IScheduleInfo[];
}

export interface IScheduleInfo {
    Address1: string;
    Address2: string;
    City: string;
    PostalCode: string;
    State?: string;
    coordinates: IUserLocation;
    note: string;
    time: ITime;
}

export interface IEmployee {
    id: string;
    name: string;
    last: string;
    goesby: string;
    address: string;
    rate: string;
    type: string;
    email: string;
    active: boolean;
    log: ILog[];
}

export interface IVendor {
    contract: boolean;
    name: string;
    address: string;
    contacts: any;
    services: IService[];
    clients: IClient[];
    employees: IEmployee[];
    information: string;
}

export interface ILogSchema {
    user: ISilverBrickUser;
    time: number;
    request: string;
    params: string;
}

export interface ILog {
    time: number;
    coords: IScheduleInfo;
    action: string;
}

export interface ITimeCard {
    startTime: number;
    endTime?: number;
    location: ILocation;
    notes?: string;
    authorID: string;
    authorName: string;
    id?: string;
    year?: number;
    month?: number;
    day?: number;
}

export interface IBooking {
    date: number;
    time: ITime[];
    services: IService[];
    complete: ITask[];
    notes: string;
    assign: any;
    status: string;
    finished: string;
    id?: string;
}

export interface ITask {
    authorName: string;
    authorID: string;
    clientID: string;
    clientName: string;
    clientUnit: string;
    active: boolean;
    icon: string;
    status: string;
    deleted: string;
    notes: string;
    date: number;
    id?: string;
    type?: string;
    category: string;
    subCategory?: string;
    orgID?: string;
    buildingID?: string;
    buildingName: string;
    subject: string;
    description: string;
    messages?: IMessage[];
    completedDates?: string[];
    photoURL?: string;
    files?: any[];
    lastUpdated?: number;
    updateAuthor?: string;
    updatedRole?: string;
    buildingAddress?: string;
    progressStart?: number;
    availability?: string;
    permissionToEnter?: boolean;
    progress?: ITime;
    havePets?: boolean;
    photoGallery?: string[];
    recurring?: boolean;
    startDate?: number;
    endDate?: number;
    scheduling?: boolean;
    scheduled?: boolean;
    durationAmount?: string;
    durationQuantity?: number;
    startTime?: string;
    endTime?: string;
    clockInTime?: number;
    clockOutTime?: number;
    repeats?: string;
    repeatInterval?: string;
    address?: IAddress;
    repeatDays?: number[];
    repeatQuantity?: number;
    monthlyConvention?: string;
    monthlyDays?: number[];
    monthlyWeekDays?: number[];
    monthlyNWeekDays?: number[][];
    assignees?: IAssignee[];
    skipDates?: string[];
    start?: Date;
    end?: Date;
}

export interface IAssignee {
    name: string;
    uid: string;
}

export interface ITaskFeed {
    type: string;
    taskID: string;
    message: string;
    timestamp: any;
    orgID: string;
    category?: string;
    subCategory?: string;
    buildingID: string;
    buildingName?: string;
    clientUnit: string;
    photoURL?: string;
    photoGallery?: string[];
    authorName?: string;
    authorID?: string;
    status?: string;
    role?: string;
    audience?: string;
    assignees?: IAssignee[];
    approved: boolean;
    id?: string;
}

export interface ITime {
    startTime: number;
    startChecked: boolean;
    finishTime: number;
    finishChecked: boolean;
}

export interface IService {
    name: string;
    author: string;
    active: boolean;
    icon: string;
}

export interface IAccountInfo  {
  metadata: string;
  name: string;
}

/*
  Org Related
*/

export interface IUserOrgBuildingBase {
    id: string;
    name?: string;
    isAdmin: boolean;
}

export interface IUserOrg extends IUserOrgBuildingBase {
    buildings: IUserBuilding[];
}

export interface IUserBuilding extends IUserOrgBuildingBase {
    audiences: IUserAudience[];
}

export interface IUserAudience {
    id: string;
    name: string;
}

export interface IOrgPermissionData {
    buildings: BuildingToPermissionMap;
    isAdmin: boolean;
}

export type OrgToPermissionMap = {
    [orgID: string]: IOrgPermissionData;
};

export type BuildingToPermissionMap = {
    [buildingID: string]: IBuildingPermissionData;
};

export interface IBuildingPermissionData {
    canCreate: boolean;
    isAdmin: false;
    audiences: AudienceToPermissionMap;
    isDBA?: boolean;
}

export type AudienceToPermissionMap = {
    [audienceID: string]: string;
};
export interface IOrgBasic {
    name: string;
    id: string;
    buildings: Array<IBuildingBasic> | Array<IBuildingSimple>;
}

export interface IOrgSummary {
    name: string;
    id: string;
    buildings: IBuildingBasic;
}

export interface IBuildingBasic {
    [buildingID: string]: any;
    id: string;
    name: string;
    landlords?: IUser[];
}

export interface IOrgSimple {
    id: string;
    name: string;
    address: IAddress;
}

export interface IBuildingSimple {
    id: string;
    name: string;
    orgID?: string;
    address?: IAddress;
    landlords?: IUser[];
}

export interface IInfo {
    readonly address: IAddress;
    readonly name: string;
    readonly phone: string;
    readonly lastModified: number;
}

export interface IOrgInfo extends IInfo {
    // Room for extras
    id: string;
    owner?: IProfile;
}

export interface IBuilding {
    info: IBuildingInfo;
    config: IBuildingConfig;
    id: string;
    orgID: string;
}
export interface IBuildingConfig {
    isEmailSendingEnabled: boolean;
    isSMSSendingEnabled: boolean;
    propertyCodes: string;
}

export interface IBuildingInfo {
     address: IAddress;
     name: string;
     phone: string;
     lastModified?: number;
     type?: string;
}

export interface IProfile {
  firstName?: string;
  lastName: string;
  uid: string;
  photoURL?: string;
  address?: IAddress;
  phone?: string;
  isVendor?: boolean;
}

export interface IProfileUser {
  uid: string;
  address?: IAddress;
}

export interface IAddress {
  city: string;
  line1: string;
  line2: string;
  state: string;
  zip: string;
  country?: string;
}

export interface IUserLocation {
  latitude: number;
  longtitude: number;
}

export interface IMapLocation {
    position: ILocation;
    title: string;
    animation?: string;
    icon?: string;
    label?: string;
}

export interface ILocation {
  lat: number;
  lng: number;
}

export interface IPaymentCard {
  name: string;
  number: string;
  exp_month: number;
  exp_year: number;
  cvc: string;
  address: IAddress;
  default?: boolean;
  cardID: string;
  customerName: string;
  brand: string;
}

/*
  Message Related
*/

export interface IMessage {
  text: string;
  authour: string;
  date: any;
  authorID: string;
  photoURL?: string;
  id?: string;
  type: string;
  role?: string;
}

/*
  Push Related
*/

export enum NotificationType {
  FOLLOW,
  REPOST,
  LOVE,
  POST_COMMENT,
  SUPPORT,
  BUY_MERCH,
  ARTIST_FOLLOW_POST,
  CHAT_ROOM,
  SHIPPED_MERCH
}

export interface INotificationData {
  type: NotificationType;
  date: any;
  authorName: string;
  authorID?: string;
  photoURL?: string;
  title: string;
  message: string;
  trackingURL?: string;
  id?: string;
}

export interface IInAppToastData {
  message: string;
  data: string;
}

export interface IDeviceInfo {
  osType: string;
  fcmRegToken: string;
}

export interface INote {
    deleted: boolean;
    message: string;
    date: number;
    authorName: string;
    authorID: string;
    buildingID: string;
    orgID: string;
    id?: string;
    title: string;
    updated?: number;
}

// export interface ICalendarEvent {
//     time: number
//     isToday: boolean;
//     title: number
//     subTitle: any;
//     selected: boolean;
//     isLastMonth: boolean;
//     isNextMonth: boolean;
//     marked: boolean;
//     cssClass: string;
//     disable: boolean;
//     isFirst: boolean;
//     isLast: boolean;
// }

export interface RecurringEvent extends CalendarEvent {
    title: string;
    color: any;
    rrule?: {
        freq: any;
        bymonth?: number;
        bymonthday?: number | number[];
        byweekday?: any[];
        bynweekday?: number[][];
        byyearday?: number | number[];
        until?: any;
        interval?: number;
        byweekno?: number[];
    },
    task?: ITask;
    id?: string;
    completedToday?: boolean;
    displayTitle?: string;
};

/********************************************************
                ---- RRule Options ----
*********************************************************

export declare enum Frequency {
    YEARLY = 0,
    MONTHLY = 1,
    WEEKLY = 2,
    DAILY = 3,
    HOURLY = 4,
    MINUTELY = 5,
    SECONDLY = 6
}
export interface Options {
    freq: Frequency;
    dtstart: Date | null;
    interval: number;
    wkst: Weekday | number | null;
    count: number | null;
    until: Date | null;
    tzid: string | null;
    bysetpos: number | number[] | null;
    bymonth: number | number[] | null;
    bymonthday: number | number[] | null;
    bynmonthday: number[] | null;
    byyearday: number | number[] | null;
    byweekno: number | number[] | null;
    byweekday: ByWeekday | ByWeekday[] | null;
    bynweekday: number[][] | null;
    byhour: number | number[] | null;
    byminute: number | number[] | null;
    bysecond: number | number[] | null;
    byeaster: number | null;
}


export interface ParsedOptions extends Options {
    dtstart: Date;
    wkst: number;
    bysetpos: number[];
    bymonth: number[];
    bymonthday: number[];
    bynmonthday: number[];
    byyearday: number[];
    byweekno: number[];
    byweekday: number[];
    byhour: number[];
    byminute: number[];
    bysecond: number[];
}
*/

export interface WeekDay {
    badgeTotal?: number;
    date?: Date;
    events?: any[];
    inMonth: boolean;
    isFuture: boolean;
    isPast: boolean;
    isToday: boolean;
    isWeekend: boolean;
}

export interface BeforeCalendarLoadMonth {
    body: WeekDay[];
    header: WeekDay[];
}

export interface BeforeCalendarLoadWeek {
    body: WeekDay[];
    header: WeekDay[];
}
