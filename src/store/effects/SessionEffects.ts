// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/fromPromise";

import { User } from 'firebase';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';

// Local modules
import { SessionActions, SessionActionType } from '../actions/SessionActions';
import { UserApi } from '../../services/UserApi';
import { AccountApi } from '../../services/AccountApi';
import { OrgApi } from '../../services/OrgApi';
import { ISilverBrickUser, ITask } from '../../shared/SilverBrickTypes';

import { ICredentials, IGetBldgHelper, IPaginateQueryStrings } from '../../shared/IParams';
import { FirebaseAuth } from '../../services/FirebaseAuth';
import { StorageHelper } from '../../providers/storage-helper';
import { IServerResponseLogin, AddMessageHelper, AssignTaskHelper, ITaskFeedJoin } from '../../shared/ResponseTypes';
import { Constants } from '../../common/Constants';

@Injectable()
export class SessionEffects {
  constructor(
    private actions$: Actions,
    private _userApi: UserApi,
    private _orgApi: OrgApi,
    private _accountApi: AccountApi,
    private _sessionActions: SessionActions) { }

  	@Effect()
    getOrgs = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_BOOKINGS])
        .switchMap((action) => {
            return this._orgApi.GetBookings()
                .map(serverResponse => this._sessionActions.GetBookingsSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetBookingsFailed(error)));
        });

    @Effect()
    getUsersScope = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_USERS_SCOPE])
        .switchMap((action) => {
            const queryStrings: IPaginateQueryStrings = action.payload;
            return this._userApi.GetUsers(queryStrings)
                .map(serverResponseGetUser => this._sessionActions.GetUsersScopeSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._sessionActions.GetUsersScopeFail(error)));
        });

    @Effect()
    getTasks = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_TASKS])
        .switchMap((action) => {
            return this._accountApi.GetTasks()
                .map(serverResponse => this._sessionActions.GetTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetTasksFailed(error)));
        });

    @Effect()
    getBuildingTasks = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_BUILDING_TASKS])
        .switchMap((action) => {
            let helper: IGetBldgHelper = action.payload;
            return this._accountApi.GetBuildingTasks(helper)
                .map(serverResponse => this._sessionActions.GetBuildingTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetBuildingTasksFailed(error)));
        });

    @Effect()
    getLandlordTasks = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_LANDLORD_TASKS])
        .switchMap((action) => {
            return this._accountApi.GetLandlordTasks()
                .map(serverResponse => this._sessionActions.GetLandlordTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetLandlordTasksFailed(error)));
        });

    @Effect()
    getLandlordTasksCompleted = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_LANDLORD_TASKS_COMPLETED])
        .switchMap((action) => {
            return this._accountApi.GetLandlordTasksCompleted()
                .map(serverResponse => this._sessionActions.GetLandlordTasksCompletedSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetLandlordTasksCompletedFailed(error)));
        });

    @Effect()
    getTenantTasks = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_TENANT_TASKS])
        .switchMap((action) => {
            let id: string = action.payload;
            return this._accountApi.GetTenantTasks(id)
                .map(serverResponse => this._sessionActions.GetTenantTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetTenantTasksFailed(error)));
        });

    @Effect()
    getAllHistory = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_ALL_HISTORY])
        .switchMap((action) => {
            return this._accountApi.GetAllHistory()
                .map(serverResponse => this._sessionActions.GetAllHistorySucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetAllHistorySucceeded(error)));
        });

    @Effect()
    getStaffHistory = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_STAFF_HISTORY])
        .switchMap((action) => {
            let id: string = action.payload;
            return this._accountApi.GetStaffHistory(id)
                .map(serverResponse => this._sessionActions.GetStaffHistorySucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetStaffHistoryFailed(error)));
        });

    @Effect()
    getTaskFeed = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_TASK_FEED])
        .switchMap((action) => {
            let helper: ITaskFeedJoin = action.payload;
            return this._accountApi.GetTaskFeed(helper)
                .map(serverResponse => this._sessionActions.GetTaskFeedSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetTaskFeedFailed(error)));
        });

    @Effect()
    addTaskFeed = this.actions$
        .ofType(SessionActionType[SessionActionType.ADD_TASK_MESSAGE])
        .switchMap((action) => {
            let helper: AddMessageHelper = action.payload;
            return this._accountApi.AddTaskFeed(helper)
                .map(serverResponse => this._sessionActions.AddTaskMessageSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.AddTaskMessageSucceeded(error)));
        });

    @Effect()
    approveTaskFeed = this.actions$
        .ofType(SessionActionType[SessionActionType.APPROVE_HISTORY])
        .switchMap((action) => {
            let helper: AddMessageHelper = action.payload;
            return this._accountApi.ApproveTaskFeed(helper)
                .map(serverResponse => this._sessionActions.ApproveHistorySucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.ApproveHistoryFailed(error)));
        });

    @Effect()
    updateTaskFeed = this.actions$
        .ofType(SessionActionType[SessionActionType.UPDATE_HISTORY])
        .switchMap((action) => {
            let helper: AddMessageHelper = action.payload;
            return this._accountApi.UpdateTaskFeed(helper)
                .map(serverResponse => this._sessionActions.UpdateHistorySucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.UpdateHistoryFailed(error)));
        });

    @Effect()
    getStaffBookings = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_STAFF_BOOKINGS])
        .switchMap((action) => {
            let id: string = action.payload;
            return this._accountApi.GetStaffTasks(id)
                .map(serverResponse => this._sessionActions.GetStaffTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetStaffTasksFailed(error)));
        });

    @Effect()
    createTask = this.actions$
        .ofType(SessionActionType[SessionActionType.CREATE_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.CreateTask(newTask)
                .map(serverResponse => this._sessionActions.CreateTaskSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.CreateTaskFailed(error)));
        });

    @Effect()
    updateTask = this.actions$
        .ofType(SessionActionType[SessionActionType.UPDATE_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.UpdateTask(newTask)
                .map(serverResponse => this._sessionActions.UpdateTaskSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.UpdateTaskFailed(error)));
        });

    @Effect()
    assignTask = this.actions$
        .ofType(SessionActionType[SessionActionType.ASSIGN_TASK])
        .switchMap((action) => {
            let newTask: AssignTaskHelper = action.payload;
            return this._accountApi.AssignTask(newTask)
                .map(serverResponse => this._sessionActions.AssignTaskSucceeded(serverResponse.data.task))
                .catch(error => Observable.of(this._sessionActions.AssignTaskFailed(error)));
        });

    @Effect()
    progressTask = this.actions$
        .ofType(SessionActionType[SessionActionType.START_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.ProgressTask(newTask)
                .map(serverResponse => this._sessionActions.StartTaskSucceeded(serverResponse.data.task))
                .catch(error => Observable.of(this._sessionActions.StartTaskFailed(error)));
        });

    @Effect()
    completeTask = this.actions$
        .ofType(SessionActionType[SessionActionType.COMPLETE_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.CompleteTask(newTask)
                .map(serverResponse => this._sessionActions.CompleteTaskSucceeded(serverResponse.data.task))
                .catch(error => Observable.of(this._sessionActions.CompleteTaskFailed(error)));
        });

    @Effect()
    completeRecurringTask = this.actions$
        .ofType(SessionActionType[SessionActionType.COMPLETE_RECURRING_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.CompleteRecurringTask(newTask)
                .map(serverResponse => this._sessionActions.CompleteRecurringTaskSucceeded(serverResponse.data.task))
                .catch(error => Observable.of(this._sessionActions.CompleteRecurringTaskFailed(error)));
        });

    @Effect()
    reopenTask = this.actions$
        .ofType(SessionActionType[SessionActionType.REOPEN_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.UnCompleteTask(newTask)
                .map(serverResponse => this._sessionActions.ReopenTaskSucceeded(serverResponse.data.task))
                .catch(error => Observable.of(this._sessionActions.ReopenTaskFailed(error)));
        });

    @Effect()
    reopenRecurringTask = this.actions$
        .ofType(SessionActionType[SessionActionType.REOPEN_RECURRING_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.UnCompleteTask(newTask)
                .map(serverResponse => this._sessionActions.ReopenRecurringSucceeded(serverResponse.data.task))
                .catch(error => Observable.of(this._sessionActions.ReopenRecurringFailed(error)));
        });

    @Effect()
    createRecurringTask = this.actions$
        .ofType(SessionActionType[SessionActionType.CREATE_RECURRING_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.CreateRecurringTask(newTask)
                .map(serverResponse => this._sessionActions.CreateRecurringTaskSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.CreateRecurringTaskFailed(error)));
        });

    @Effect()
    updateRecurringTask = this.actions$
        .ofType(SessionActionType[SessionActionType.UPDATE_RECURRING_TASK])
        .switchMap((action) => {
            let newTask: ITask = action.payload;
            return this._accountApi.UpdateRecurringTask(newTask)
                .map(serverResponse => this._sessionActions.UpdateRecurringTaskSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.UpdateRecurringTaskFailed(error)));
        });

    @Effect()
    getRecurringTasks = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_RECURRING_TASKS])
        .switchMap((action) => {
            return this._accountApi.GetRecurringTasks()
                .map(serverResponse => this._sessionActions.GetRecurringTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetRecurringTasksFailed(error)));
        });

    @Effect()
    getRecurringStaffTasks = this.actions$
        .ofType(SessionActionType[SessionActionType.GET_RECURRING_STAFF_TASKS])
        .switchMap((action) => {
            let id: string = action.payload;
            return this._accountApi.GetRecurringStaffTasks(id)
                .map(serverResponse => this._sessionActions.GetRecurringStaffTasksSucceeded(serverResponse.data))
                .catch(error => Observable.of(this._sessionActions.GetRecurringStaffTasksFailed(error)));
        });
}
