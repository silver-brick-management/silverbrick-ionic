// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/switchMap";

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

// Local modules
import { OrgActions } from '../actions/OrgActions';
import { OrgApi } from '../../services/OrgApi';
import { IOrgBasic, INote } from '../../shared/SilverBrickTypes';
import { IPaginateQueryStrings, IUserActivationParam, IGetBldgHelper } from '../../shared/IParams';
import {
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseMessage,
    IServerResponseGetOrgInfo
} from '../../shared/ResponseTypes';

@Injectable()
export class OrgEffects {
    constructor(
        private actions$: Actions,
        private _orgApi: OrgApi,
        private _orgActions: OrgActions) { }

    @Effect()
    getOrgs = this.actions$
        .ofType(OrgActions.GET_ALL_ORGS)
        .switchMap((action) => {
            return this._orgApi.GetOrgs()
                .map(serverResponseGetOrgs => this._orgActions.GetAllOrgsSuccess(serverResponseGetOrgs.data))
                .catch(error => Observable.of(this._orgActions.GetAllOrgsFail(error)));
        });

    @Effect()
    getAllBuildings = this.actions$
        .ofType(OrgActions.GET_ALL_BUILDINGS)
        .switchMap((action) => {
            return this._orgApi.GetAllBuildings()
                .map(serverResponseGetOrgs => this._orgActions.GetAllBuildingsSuccess(serverResponseGetOrgs.data))
                .catch(error => Observable.of(this._orgActions.GetAllBuildingsFail(error)));
        });

    @Effect()
    getOrgsScope = this.actions$
        .ofType(OrgActions.GET_ALL_ORGS_SCOPE)
        .switchMap((action) => {
            return this._orgApi.GetOrgs()
                .map(serverResponseGetOrgs => this._orgActions.GetAllOrgsScopeSuccess(serverResponseGetOrgs.data))
                .catch(error => Observable.of(this._orgActions.GetAllOrgsScopeFail(error)));
        });

    @Effect()
    getOrgsInfo = this.actions$
        .ofType(OrgActions.GET_ORGS_INFO)
        .switchMap((action) => {
            return this._orgApi.GetOrgInfo(action.payload)
                .map(serverResponseGetOrgs => this._orgActions.GetOrgSuccessInfo(serverResponseGetOrgs.data))
                .catch(error => Observable.of(this._orgActions.GetOrgFailInfo(error)));
        });


    @Effect()
    addOrg = this.actions$
        .ofType(OrgActions.ADD_ORG)
        .switchMap((action) => {
            console.log("action", action.payload);
            return this._orgApi.AddOrg(action.payload)
                .map(serverResponseAddOrg => this._orgActions.AddOrgSuccess(serverResponseAddOrg.data))
                .catch(error => Observable.of(this._orgActions.AddOrgFail(error)));
        });

    @Effect()
    updateOrg = this.actions$
        .ofType(OrgActions.UPDATE_ORG)
        .switchMap((action) => {
            return this._orgApi.UpdateOrg(action.payload)
                .map(serverResponseUpdateOrg => this._orgActions.UpdateOrgSuccess(serverResponseUpdateOrg.data))
                .catch(error => Observable.of(this._orgActions.UpdateOrgFail(error)));
        });

    @Effect()
    addBldg = this.actions$
        .ofType(OrgActions.ADD_BLDG)
        .switchMap((action) => {
            return this._orgApi.AddBldg(action.payload)
                .map(serverResponseAddBldg => this._orgActions.AddBldgSuccess(serverResponseAddBldg.data))
                .catch(error => Observable.of(this._orgActions.AddBldgFail(error)));
        });

    @Effect()
    updateBldg = this.actions$
        .ofType(OrgActions.UPDATE_BLDG)
        .switchMap((action) => {
            return this._orgApi.UpdateBldg(action.payload)
                .map(serverResponseUpdateBldg => this._orgActions.UpdateBldgSuccess(serverResponseUpdateBldg.data))
                .catch(error => Observable.of(this._orgActions.UpdateBldgFail(error)));
        });

    @Effect()
    addNote = this.actions$
        .ofType(OrgActions.ADD_NOTE)
        .switchMap((action) => {
            return this._orgApi.AddNote(action.payload)
                .map(serverResponseAddBldg => this._orgActions.AddNoteSuccess(serverResponseAddBldg.data))
                .catch(error => Observable.of(this._orgActions.AddNoteFail(error)));
        });

    @Effect()
    updateNote = this.actions$
        .ofType(OrgActions.UPDATE_NOTE)
        .switchMap((action) => {
            return this._orgApi.UpdateNote(action.payload)
                .map(serverResponseUpdateBldg => this._orgActions.UpdateNoteSuccess(serverResponseUpdateBldg.data))
                .catch(error => Observable.of(this._orgActions.UpdateNoteFail(error)));
        });

    @Effect()
    getNotes = this.actions$
        .ofType(OrgActions.GET_NOTE)
        .switchMap((action) => {
            let helper: IGetBldgHelper = <IGetBldgHelper>action.payload;
            return this._orgApi.GetNotes(helper)
                .map(serverResponseGetBldg => this._orgActions.GetNotesSuccess(serverResponseGetBldg.data))
                .catch(error => Observable.of(this._orgActions.GetNotesSuccess(error)));
        });

    @Effect()
    getBldg = this.actions$
        .ofType(OrgActions.GET_BLDG)
        .switchMap((action) => {
            return this._orgApi.GetBldg(action.payload)
                .map(serverResponseGetBldg => this._orgActions.GetBldgSuccess(serverResponseGetBldg.data))
                .catch(error => Observable.of(this._orgActions.GetBldgFail(error)));
        });
}
