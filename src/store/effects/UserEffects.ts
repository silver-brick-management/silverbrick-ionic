// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/switchMap";

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

// Local modules
import { UserActions } from '../actions/UserActions';
import { UserApi } from '../../services/UserApi';
import { IUser } from '../../shared/SilverBrickTypes';
import { IPaginateQueryStrings, IUserActivationParam, IGetBldgHelper } from '../../shared/IParams';
import {
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseMessage,
    AddTimeCardHelper,
    GetTimeCardHelper,
    IServerResponseAddTimeCard,
    IServerResponseGetTimeCards,
    IServerResponseImportUsers,
    IServerResponseGetBuildingUsers
} from '../../shared/ResponseTypes';

@Injectable()
export class UserEffects {
    constructor(
        private actions$: Actions,
        private _userApi: UserApi,
        private _userActions: UserActions) { }

    @Effect()
    getUsers = this.actions$
        .ofType(UserActions.GET_USERS)
        .switchMap((action) => {
            const queryStrings: IPaginateQueryStrings = action.payload;
            return this._userApi.GetUsers(queryStrings)
                .map(serverResponseGetUser => this._userActions.GetUsersSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetUsersFail(error)));
        });

    @Effect()
    getBuildingUsers = this.actions$
        .ofType(UserActions.GET_BUILDING_USERS)
        .switchMap((action) => {
            const helper: IGetBldgHelper = action.payload;
            return this._userApi.GetBuildingUsers(helper)
                .map(serverResponseGetUser => this._userActions.GetBuildingUsersSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetBuildingUsersFail(error)));
        });

    @Effect()
    getBuildingTenants = this.actions$
        .ofType(UserActions.GET_BUILDING_TENANTS)
        .switchMap((action) => {
            const helper: IGetBldgHelper = action.payload;
            return this._userApi.GetBuildingTenants(helper)
                .map(serverResponseGetUser => this._userActions.GetBuildingTenantsSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetBuildingTenantsFail(error)));
        });

    @Effect()
    getAllTenants = this.actions$
        .ofType(UserActions.GET_ALL_TENANTS)
        .switchMap((action) => {
            return this._userApi.GetAllTenants()
                .map(serverResponseGetUser => this._userActions.GetAllTenantsSuccess(serverResponseGetUser.data))
                .catch(error => Observable.of(this._userActions.GetAllTenantsFail(error)));
        });

    @Effect()
    getUsersFromFullUrl = this.actions$
        .ofType(UserActions.GET_USERS_FROM_FULL_URL)
        .switchMap((action) => {
            const url: string = action.payload;
            return this._userApi.GetUsersFromFullUrl(url)
                .map(serverResponseGetUser => this._userActions.GetUsersSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetUsersFail(error)));
        });

    @Effect()
    getUser = this.actions$
        .ofType(UserActions.GET_USER)
        .switchMap((action) => {
            const uidOrEmail: string = action.payload;
            return this._userApi.GetUser(uidOrEmail)
                .map(serverResponseGetUser => this._userActions.GetUserSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.AddUserFail(error)));
        });

    @Effect()
    addUser = this.actions$
        .ofType(UserActions.ADD_USER)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.AddUser(user)
                .map(serverResponseAddUser => this._userActions.AddUserSuccess(serverResponseAddUser))
                .catch(error => Observable.of(this._userActions.AddUserFail(error)));
        });

    @Effect()
    addTimeCard = this.actions$
        .ofType(UserActions.ADD_TIME_CARD)
        .switchMap((action) => {
            const helper: AddTimeCardHelper = action.payload;
            return this._userApi.AddTimeCard(helper)
                .map(serverResponseAddUser => this._userActions.AddTimeCardSuccess(serverResponseAddUser.data))
                .catch(error => Observable.of(this._userActions.AddTimeCardFail(error)));
        });

    @Effect()
    updateTimeCard = this.actions$
        .ofType(UserActions.UPDATE_TIME_CARD)
        .switchMap((action) => {
            const helper: AddTimeCardHelper = action.payload;
            return this._userApi.UpdateTimeCard(helper)
                .map(serverResponseAddUser => this._userActions.UpdateTimeCardSuccess(serverResponseAddUser.data))
                .catch(error => Observable.of(this._userActions.UpdateTimeCardFail(error)));
        });

    @Effect()
    getTimeCard = this.actions$
        .ofType(UserActions.GET_TIME_CARD)
        .switchMap((action) => {
            return this._userApi.GetTimeCard()
                .map(serverResponseAddUser => this._userActions.GetTimeCardSuccess(serverResponseAddUser.data))
                .catch(error => Observable.of(this._userActions.GetTimeCardFail(error)));
        });

    @Effect()
    getTimeCards = this.actions$
        .ofType(UserActions.GET_TIME_CARDS)
        .switchMap((action) => {
            let helper: GetTimeCardHelper = action.payload;
            return this._userApi.GetTimeCards(helper)
                .map(serverResponseAddUser => this._userActions.GetTimeCardsSuccess(serverResponseAddUser.data))
                .catch(error => Observable.of(this._userActions.GetTimeCardsFail(error)));
        });

    @Effect()
    getAllTenantsLandlord = this.actions$
        .ofType(UserActions.GET_ALL_TENANTS_LANDLORD)
        .switchMap((action) => {
            return this._userApi.GetLandlordTenants()
                .map(serverResponseGetUser => this._userActions.GetAllTenantsLandlordSuccess(serverResponseGetUser.data))
                .catch(error => Observable.of(this._userActions.GetAllTenantsLandlordFail(error)));
        });

    @Effect()
    getTimeCardsPrevious = this.actions$
        .ofType(UserActions.GET_TIME_CARDS_PREVIOUS)
        .switchMap((action) => {
            let helper: GetTimeCardHelper = action.payload;
            return this._userApi.GetTimeCards(helper)
                .map(serverResponseAddUser => this._userActions.GetTimeCardsPreviousSuccess(serverResponseAddUser.data))
                .catch(error => Observable.of(this._userActions.GetTimeCardsPreviousFail(error)));
        });

    @Effect()
    importUser = this.actions$
        .ofType(UserActions.IMPORT_USERS)
        .switchMap((action) => {
            const users: IUser[] = action.payload;
            return this._userApi.ImportUsers(users)
                .map(serverResponseImportUsers => this._userActions.ImportUsersSuccess(serverResponseImportUsers.data))
                .catch(error => Observable.of(this._userActions.ImportUsersFail(error)));
        });

    @Effect()
    updateUser = this.actions$
        .ofType(UserActions.UPDATE_USER)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.UpdateUser(user)
                .map((serverResponseUpdateUser) => this._userActions.UpdateUserSuccess(serverResponseUpdateUser))
                .catch(error => Observable.of(this._userActions.UpdateUserFail(error)));
        });

    @Effect()
    updateUserPassword = this.actions$
        .ofType(UserActions.UPDATE_USER_PASSWORD)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.UpdateUserPassword(user)
                .map((serverResponseUpdateUser) => this._userActions.UpdateUserPasswordSuccess(serverResponseUpdateUser))
                .catch(error => Observable.of(this._userActions.UpdateUserPasswordFail(error)));
        });

    @Effect()
    activateUser = this.actions$
        .ofType(UserActions.ACTIVATE_USER)
        .switchMap((action) => {
            const uid: string = action.payload;
            return this._userApi.ActivateUser(uid)
                .map((serverResponseMessage) => {
                    const activationParam: IUserActivationParam = {
                        uid: uid,
                        activationStatus: true
                    };
                    return this._userActions.ActivateUserSuccess(activationParam);
                })
                .catch(error => Observable.of(this._userActions.ActivateUserFail(error)));
        });

    @Effect()
    deactivateUser = this.actions$
        .ofType(UserActions.DEACTIVATE_USER)
        .switchMap((action) => {
            const uid: string = action.payload;
            return this._userApi.DeactivateUser(uid)
                .map((serverResponseMessage) => {
                    const activationParam: IUserActivationParam = {
                        uid: uid,
                        activationStatus: false
                    };
                    return this._userActions.DeactivateUserSuccess(activationParam);
                })
                .catch(error => Observable.of(this._userActions.DeactivateUserFail(error)));
        });

    @Effect()
    deleteUser = this.actions$
        .ofType(UserActions.DELETE_USER)
        .switchMap((action) => {
            const uid: string = action.payload;
            return this._userApi.DeleteUser(uid)
                .map((serverResponseMessage) => {
                    return this._userActions.DeleteUserSuccess(uid);
                })
                .catch(error => Observable.of(this._userActions.DeleteUserFail(error)));
        });
}
