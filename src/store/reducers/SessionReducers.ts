// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from "@ngrx/store";
import { getMonth, startOfMonth, startOfWeek, startOfDay, endOfWeek, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from "date-fns";
import RRule from "rrule";

// Local modules
import { SessionActions, SessionActionType } from "../actions/SessionActions";
import { IUser, IAssignee, ITaskFeed, RecurringEvent,IUserOrg, IUserBuilding, IUserAudience, IUserOrgBuildingBase, IService, IBooking, ITask } from "../../shared/SilverBrickTypes";
import { SessionStateType, IStateBase, ISessionState } from "../../interfaces/IStoreState";
import { ArrayUtil } from "../../common/ArrayUtil";
import { RouteUtil } from "../../common/RouteUtil";
import { UserUtil } from "../../common/UserUtil";
import { IGetBldgHelper } from '../../shared/IParams';
import { IServerResponseGetUsers } from '../../shared/ResponseTypes';
export declare type WeekdayStr = "SU" | "MO" | "TU" | "WE" | "TH" | "FR" | "SA";
declare var google: any;

interface IPartialSessionState extends IStateBase {
    isOrgAdmin?: boolean;
    isBldgAdmin?: boolean;
    username?: string;
    org?: IUserOrg;
    userPosition?: string;
    uid?: string;
    orgID?: string;
    buildingID?: string;
    user?: IUser;
    type?: SessionStateType;
    services?: IService[];
    bookings?: IBooking[];
    tenantBookings?: IBooking[];
    staffBookings?: IBooking[];
    tasks?: ITask[];
    taskFeed?: ITaskFeed[];
    allHistory?: ITaskFeed[];
    allTasks?: ITask[];
    recurringTasks?: ITask[];
    activeDate?: Date;
    allRecurringTasks?: ITask[];
    searchTasks?: ITask[];
    selectedStaff?: IUser;
    allEvents?: any[];
    usersScope?: Array<IUser>;
    allEveryEvents?: any[];
    allUsersScope?: Array<IUser>;
    completedTasks?: Array<ITask>;
    startingLocation?: string;
    // inProgressTasks?: ITask[];
    travelMode?: google.maps.TravelMode;
    // newTasks?: ITask[];
}

const initialSessionState: ISessionState = {
    error: null,
    loading: null,
    username: null,
    uid: null,
    orgID: null,
    isBldgAdmin: null,
    isOrgAdmin: null,
    buildingID: null,
    userPosition: null,
    activeDate: null,
    email: null,
    user: null,
    org: null,
    services: [],
    allEveryEvents: [],
    bookings: [],
    tenantBookings: [],
    tasks: [],
    allHistory: [],
    taskFeed: [],
    allTasks: [],
    allUsersScope: [],
    usersScope: [],
    completedTasks: [],
    recurringTasks: [],
    allRecurringTasks: [],
    allEvents: [],
    searchTasks: [],
    selectedStaff: null,
    // newTasks: [],
    travelMode: null,
    // inProgressTasks: [],
    staffBookings: [],
    startingLocation: "68 4th Place, Brooklyn, NY",
    type: SessionStateType.NONE,
};

export class SessionReducers {
    static Reduce(sessionState: ISessionState = initialSessionState, action: any): ISessionState {
        let partialState: IPartialSessionState;
        const defaultAudience: IUserAudience = {
            id: "all",
            name: "All",
        };
        switch (action.type) {
            case SessionActionType[SessionActionType.SESSION_INIT]:
            case SessionActionType[SessionActionType.SESSION_USER_INIT]:
            {
                const user: IUser = action.payload;
                // // // console.log("SESSION_DBA_INIT", user);
                let org: IUserOrg = user.orgs;
                const buildings: IUserBuilding[] = org.buildings;
                let isBldgAdmin: boolean = false;

                // Looking for an 'Any' isAdmin relationship
                for (let building of buildings) {
                    if (building.isAdmin) {
                        isBldgAdmin = true;
                        break;
                    } else {
                        // remains false
                    }
                }
                partialState = {
                    error: null,
                    loading: false,
                    userPosition: user.role ? user.role : "",
                    uid: user.uid,
                    isOrgAdmin: org.isAdmin,
                    isBldgAdmin: isBldgAdmin,
                    username: `${ user.firstName } ${ user.lastName}`,
                    org: org,
                    orgID: org.id,
                    buildingID: buildings[0].id,
                    user: user,
                    type: SessionStateType.SESSION_INIT_SUCCESS,
                };

                if ((!user.isAdmin) && (user.role === 'Staff')) {
                    partialState.selectedStaff = user;
                }
                return Object.assign({}, sessionState, partialState);
            }

            case SessionActionType[SessionActionType.GET_RECURRING_TASKS_SUCCESS]:
            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS_SUCCESS]:
            case SessionActionType[SessionActionType.GET_RECURRING_STAFF_TASKS_SUCCESS]:
            {
                let tasks: ITask[] = action.payload;
                let allEvents: any[] = [];
                let allEveryEvents: any[] = UserUtil.SetupEvents(tasks, sessionState.activeDate);
                // // console.log("tasks", tasks);
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                });
                if ((null != sessionState.user) && (!sessionState.user.isAdmin) && (sessionState.user.role === 'Staff')) {
                    scopeUsers = [ sessionState.user ];
                }
                let scopeTask: ITask[] = [];
                if (null != tasks) {
                    if (((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) || ((null != sessionState.user) && (!sessionState.user.isAdmin) && (sessionState.user.role === 'Staff'))) {
                        let assignee: IAssignee = {
                            name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                            uid: sessionState.selectedStaff.uid
                        };
                        if ((null != sessionState.user) && (!sessionState.user.isAdmin) && (sessionState.user.role === 'Staff')) {
                           assignee = {
                               name: `${sessionState.user.firstName} ${sessionState.user.lastName}`,
                               uid: sessionState.user.uid
                           };
                        }
                        scopeTask = UserUtil.ScopeStaffTasks(tasks, assignee);
                        allEvents = UserUtil.SetupEvents(scopeTask, sessionState.activeDate);
                        // // console.log("ScopeStaffTasks", tasks);
                    } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'none')) {
                        allEvents = UserUtil.SetupEvents(UserUtil.ScopeUnAssignedStaffTasks(tasks), sessionState.activeDate);
                    }  else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'all')) {
                        allEvents = UserUtil.SetupEvents(UserUtil.SortTasksByDateReverse(tasks), sessionState.activeDate);
                    } else {
                        allEvents = UserUtil.SetupEvents(UserUtil.SortTasksByDateReverse(tasks), sessionState.activeDate);
                        // // console.log("SortTasksByDate", tasks);
                    }
                }

                let sortedTasks: RecurringEvent[] = [];
                if (!sessionState.user.isAdmin) {
                    if (allEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, allEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = allEvents;
                    }
                } else if (sessionState.user.isAdmin && (null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    if (allEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, allEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = allEvents;
                    }
                } else {
                    sortedTasks = allEvents;
                }
                // console.log("sortedTasks", sortedTasks);
                let notUser: IUser = <any>{ uid: 'none', firstName: "Un", lastName: "Assigned", taskCount: 0, completedTaskCount: 0, todayTaskCount: 0, todayCompletedTaskCount: 0 };
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(allEveryEvents, [notUser, ...scopeUsers], sessionState.activeDate);
                newUsers = UserUtil.SortByName(newUsers);
                // // console.log("newUsers", newUsers);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let matchDate: string = "";
                if (null == sessionState.activeDate) {
                    matchDate = `${new Date(Date.now()).getMonth() + 1}_${new Date(Date.now()).getDate()}_${new Date(Date.now()).getFullYear()}`
                } else {
                    matchDate = `${sessionState.activeDate.getMonth() + 1}_${sessionState.activeDate.getDate()}_${sessionState.activeDate.getFullYear()}`
                }
                
                let newSorted: RecurringEvent[] = []
                Object.assign({}, newSorted, sortedTasks);
                let completedTask: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return ((null != event.task.completedDates) && (event.task.completedDates.includes(matchDate)));
                });
                let finalEvents: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return (((null != event.task.completedDates) && ((!event.task.completedDates.includes(matchDate) || (event.task.completedDates.length === 0)))) || (null == event.task.completedDates));
                });
                finalEvents = [...finalEvents, ...completedTask];
                console.log("finalEvents", finalEvents);
                let allBuildings: any[] = [...newUser, ...newUsers];
                // // console.log("scopeTask", scopeTask, allEvents);
                let tasksTab: ITask[] = tasks.slice(0, 50);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allRecurringTasks: tasks,
                    recurringTasks: scopeTask,
                    tasks: tasksTab,
                    allTasks: tasks,
                    allEveryEvents: allEveryEvents,
                    allEvents: sortedTasks,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.SCOPE_STAFF_TASKS]:
            {
                const user: IUser = action.payload;
                let tasks: ITask[] = Object.assign({}, sessionState.allTasks);
                tasks = ArrayUtil.Transform(tasks);
                let scopeTask: ITask[] = [];
                if ((user.uid !== 'all') && (user.uid !== 'none')) {
                    scopeTask = UserUtil.ScopeStaffTasks(tasks, { name: `${user.firstName} ${user.lastName}`, uid: user.uid });
                    // // console.log("scopeTask", scopeTask);
                } else if (user.uid === 'none') {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(tasks);
                } else {
                    scopeTask = sessionState.allTasks;
                }
                let allEvents: any[] = UserUtil.SetupEvents(scopeTask, sessionState.activeDate);
                // let sortedTasks: RecurringEvent[] = (((!sessionState.user.isAdmin) || (sessionState.user.isAdmin && ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')))) && allEvents.length < 25) ? RouteUtil.CalculateRoute('walking', allEvents) : allEvents;
                let sortedTasks: RecurringEvent[] = [];
                if (!sessionState.user.isAdmin) {
                    if (allEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, allEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = allEvents;
                    }
                } else if (sessionState.user.isAdmin && (null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    if (allEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, allEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = allEvents;
                    }
                } else {
                    sortedTasks = allEvents;
                }
                let matchDate: string = "";
                if (null == sessionState.activeDate) {
                    matchDate = `${new Date(Date.now()).getMonth() + 1}_${new Date(Date.now()).getDate()}_${new Date(Date.now()).getFullYear()}`
                } else {
                    matchDate = `${sessionState.activeDate.getMonth() + 1}_${sessionState.activeDate.getDate()}_${sessionState.activeDate.getFullYear()}`
                }
                
                let newSorted: RecurringEvent[] = []
                Object.assign({}, newSorted, sortedTasks);
                let completedTask: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return ((null != event.task.completedDates) && (event.task.completedDates.includes(matchDate)));
                });
                let finalEvents: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return (((null != event.task.completedDates) && ((!event.task.completedDates.includes(matchDate) || (event.task.completedDates.length === 0)))) || (null == event.task.completedDates));
                });
                finalEvents = [...finalEvents, ...completedTask];
                console.log("finalEvents", finalEvents);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: scopeTask,
                    selectedStaff: user,
                    allEvents: sortedTasks,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.CHANGE_ACTIVE_DATE]:
            {
                const date: Date = action.payload;
                let allEvents: any[] = UserUtil.SetupEvents(sessionState.allTasks, date);
                // // // console.log("SCOPE_BUILDING_HISTORY", helper);
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                });
                
                let scopeTask: ITask[] = [];
                let scopeEvents: any[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(sessionState.allTasks, assignee);
                    // console.log('scopeTask', scopeTask);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, date);

                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(sessionState.allTasks);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, date);
                } else {
                    scopeEvents = allEvents;
                }
                // let sortedTasks: RecurringEvent[] = (((!sessionState.user.isAdmin) || (sessionState.user.isAdmin && ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')))) && allEvents.length < 25) ? RouteUtil.CalculateRoute('walking', scopeEvents) : scopeEvents;
                let sortedTasks: RecurringEvent[] = [];
                // console.log("scope", scopeEvents)
                if (!sessionState.user.isAdmin) {
                    if (scopeEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, scopeEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = scopeEvents;
                    }
                } else if (sessionState.user.isAdmin && (null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    if (scopeEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, scopeEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = scopeEvents;
                    }
                } else {
                    sortedTasks = allEvents;
                }
                // console.log("sortedTasks", sortedTasks);
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(allEvents, scopeUsers, date);
                newUsers = UserUtil.SortByName(newUsers);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let allBuildings: any[] = [...newUser, ...newUsers];
                // // console.log("scopeEvents", scopeEvents);
                let matchDate: string = `${date.getMonth() + 1}_${date.getDate()}_${date.getFullYear()}`;;
                
                let newSorted: RecurringEvent[] = sortedTasks;
                // Object.assign({}, newSorted, sortedTasks);
                let completedTask: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return ((null != event.task.completedDates) && (event.task.completedDates.includes(matchDate)));
                });
                let finalEvents: RecurringEvent[] = newSorted.filter((event: RecurringEvent) => {
                    return (((null != event.task.completedDates) && ((!event.task.completedDates.includes(matchDate) || (event.task.completedDates.length === 0)))) || (null == event.task.completedDates));
                });
                finalEvents = [...finalEvents, ...completedTask];
                if (finalEvents.length === 0) {
                    finalEvents = sortedTasks;
                }
                console.log("finalEvents", finalEvents, completedTask, newSorted);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    activeDate: date,
                    allEvents: sortedTasks,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    type: SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.SWITCH_TRAVEL_MODE]:
            {
                const mode: google.maps.TravelMode = action.payload;
                console.log("mode", mode);
                let allEvents: any[] = UserUtil.SetupEvents(sessionState.allTasks, sessionState.activeDate);
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                });
                
                let scopeTask: ITask[] = [];
                let scopeEvents: any[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(sessionState.allTasks, assignee);
                    // console.log('scopeTask', scopeTask);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, sessionState.activeDate);

                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(sessionState.allTasks);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, sessionState.activeDate);
                } else {
                    scopeEvents = allEvents;
                }
                // let sortedTasks: RecurringEvent[] = (((!sessionState.user.isAdmin) || (sessionState.user.isAdmin && ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')))) && allEvents.length < 25) ? RouteUtil.CalculateRoute('walking', scopeEvents) : scopeEvents;
                let sortedTasks: RecurringEvent[] = [];
                // console.log("scope", scopeEvents)
                if (!sessionState.user.isAdmin) {
                    if (scopeEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(mode, scopeEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = scopeEvents;
                    }
                } else if (sessionState.user.isAdmin && (null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    if (scopeEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(mode, scopeEvents, sessionState.startingLocation);
                    } else {
                        sortedTasks = scopeEvents;
                    }
                } else {
                    sortedTasks = allEvents;
                }
                // console.log("sortedTasks", sortedTasks);
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(allEvents, scopeUsers, sessionState.activeDate);
                newUsers = UserUtil.SortByName(newUsers);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let allBuildings: any[] = [...newUser, ...newUsers];
                let matchDate: string = "";
                if (null == sessionState.activeDate) {
                    matchDate = `${new Date(Date.now()).getMonth() + 1}_${new Date(Date.now()).getDate()}_${new Date(Date.now()).getFullYear()}`
                } else {
                    matchDate = `${sessionState.activeDate.getMonth() + 1}_${sessionState.activeDate.getDate()}_${sessionState.activeDate.getFullYear()}`
                }
                
                let newSorted: RecurringEvent[] = []
                Object.assign({}, newSorted, sortedTasks);
                let completedTask: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return ((null != event.task.completedDates) && (event.task.completedDates.includes(matchDate)));
                });
                let finalEvents: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return (((null != event.task.completedDates) && ((!event.task.completedDates.includes(matchDate) || (event.task.completedDates.length === 0)))) || (null == event.task.completedDates));
                });
                finalEvents = [...finalEvents, ...completedTask];
                // // console.log("scopeEvents", scopeEvents);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    travelMode: mode,
                    allEvents: sortedTasks,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    type: SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.CHANGE_LOCATION]:
            {
                const location: string = action.payload;
                let allEvents: any[] = UserUtil.SetupEvents(sessionState.allTasks, sessionState.activeDate);
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                });
                
                let scopeTask: ITask[] = [];
                let scopeEvents: any[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(sessionState.allTasks, assignee);
                    // console.log('scopeTask', scopeTask);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, sessionState.activeDate);

                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(sessionState.allTasks);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, sessionState.activeDate);
                } else {
                    scopeEvents = allEvents;
                }
                // let sortedTasks: RecurringEvent[] = (((!sessionState.user.isAdmin) || (sessionState.user.isAdmin && ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')))) && allEvents.length < 25) ? RouteUtil.CalculateRoute('walking', scopeEvents) : scopeEvents;
                let sortedTasks: RecurringEvent[] = [];
                // console.log("scope", scopeEvents)
                if (!sessionState.user.isAdmin) {
                    if (scopeEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, scopeEvents, location);
                    } else {
                        sortedTasks = scopeEvents;
                    }
                } else if (sessionState.user.isAdmin && (null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    if (scopeEvents.length < 25) {
                        sortedTasks = RouteUtil.CalculateRoute(sessionState.travelMode, scopeEvents, location);
                    } else {
                        sortedTasks = scopeEvents;
                    }
                } else {
                    sortedTasks = allEvents;
                }
                // console.log("sortedTasks", sortedTasks);
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(allEvents, scopeUsers, sessionState.activeDate);
                newUsers = UserUtil.SortByName(newUsers);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let allBuildings: any[] = [...newUser, ...newUsers];
                let matchDate: string = "";
                if (null == sessionState.activeDate) {
                    matchDate = `${new Date(Date.now()).getMonth() + 1}_${new Date(Date.now()).getDate()}_${new Date(Date.now()).getFullYear()}`
                } else {
                    matchDate = `${sessionState.activeDate.getMonth() + 1}_${sessionState.activeDate.getDate()}_${sessionState.activeDate.getFullYear()}`
                }
                
                let newSorted: RecurringEvent[] = []
                Object.assign({}, newSorted, sortedTasks);
                let completedTask: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return ((null != event.task.completedDates) && (event.task.completedDates.includes(matchDate)));
                });
                let finalEvents: RecurringEvent[] = sortedTasks.filter((event: RecurringEvent) => {
                    return (((null != event.task.completedDates) && ((!event.task.completedDates.includes(matchDate) || (event.task.completedDates.length === 0)))) || (null == event.task.completedDates));
                });
                finalEvents = [...finalEvents, ...completedTask];
                // // console.log("scopeEvents", scopeEvents);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    travelMode: sessionState.travelMode,
                    allEvents: sortedTasks,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    startingLocation: location,
                    type: SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.UPDATE_STAFF_COUNT]: {
                const date: Date = action.payload;
                // // // console.log("SCOPE_BUILDING_HISTORY", helper);
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                });
                
                let scopeTask: ITask[] = [];
                let scopeEvents: any[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(sessionState.allTasks, assignee);
   
                    scopeEvents = UserUtil.SetupEvents(scopeTask, date);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(sessionState.allTasks);
                    scopeEvents = UserUtil.SetupEvents(scopeTask, date);
                } else {
                    scopeEvents = sessionState.allEvents;
                }
                let notUser: IUser = <any>{ uid: 'none', firstName: "Un", lastName: "Assigned", taskCount: 0, completedTaskCount: 0, todayTaskCount: 0, todayCompletedTaskCount: 0 };
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(sessionState.allEvents, scopeUsers, date);
                newUsers = UserUtil.SortByName(newUsers);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let allBuildings: any[] = [...newUser, ...newUsers];
                // // console.log("scopeEvents", scopeEvents);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    activeDate: date,
                    allEvents: scopeEvents,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    type: SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.REOPEN_RECURRING_TASK_SUCCESS]:
            case SessionActionType[SessionActionType.COMPLETE_RECURRING_TASK_SUCCESS]:
            {
               
                return Object.assign({}, sessionState, {
                    error: null,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.GET_USERS_SCOPE_SUCCEEDED]:
            {
                // // // console.log("GET_USERS_SCOPE_SUCCEEDED", action.payload);
                const getAllOrgsResponse: IServerResponseGetUsers = action.payload;
                let notUser: IUser = <any>{ uid: 'none', firstName: "Un", lastName: "Assigned", taskCount: 0, completedTaskCount: 0, todayTaskCount: 0, todayCompletedTaskCount: 0 };
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(sessionState.allEvents, [notUser, ...getAllOrgsResponse.data], sessionState.activeDate);
                newUsers = UserUtil.SortByName(newUsers);
                // // console.log("newUsers", newUsers);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let allBuildings: any[] = [...newUser, ...newUsers];
                
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    type: SessionStateType.GET_USERS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.REFRESH_RECURRING_TASKS]:
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    recurringTasks: sessionState.allRecurringTasks,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }

                
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    allTasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_BUILDING_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    allTasks: tasks,
                    allRecurringTasks: tasks,
                    recurringTasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS_COMPLETED_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    completedTasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_TENANT_TASKS_SUCCESS]:
            {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    allTasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS_SUCCESS]:
            {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDateReverse(tasks);
                }
                let allEveryEvents: any[] = UserUtil.SetupEvents(tasks, sessionState.activeDate);
                // // console.log("allEveryEvents", tasks, allEveryEvents);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    allTasks: tasks,
                    allRecurringTasks: tasks,
                    recurringTasks: tasks,
                    allEveryEvents: allEveryEvents,
                    allEvents: allEveryEvents,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.GET_TASK_FEED_SUCCESS]: {
                let tasks: ITaskFeed[] = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskFeed: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : []
                });
            }

            case SessionActionType[SessionActionType.ADD_TASK_MESSAGE_SUCCESS]: {
                let tasks: ITaskFeed = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskFeed: [tasks, ...sessionState.taskFeed]
                });
            }

            case SessionActionType[SessionActionType.CREATE_TASK_SUCCESS]: {
                const task: ITask = action.payload;
                let newAllTasks: ITask[] = [task, ...sessionState.tasks];
                newAllTasks = UserUtil.SortTasksByDateReverse(newAllTasks);
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                })
                let notUser: IUser = <any>{ uid: 'none', firstName: "Un", lastName: "Assigned", taskCount: 0, completedTaskCount: 0, todayTaskCount: 0, todayCompletedTaskCount: 0 };
                let newUsers: IUser[] = UserUtil.MatchStaffTasks(sessionState.allEvents, scopeUsers, sessionState.activeDate);
                newUsers = UserUtil.SortByName(newUsers);
                let allEvents: any[] = UserUtil.SetupEvents(sessionState.allRecurringTasks, sessionState.activeDate);
                let newUser: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }];
                let allBuildings: any[] = [...newUser, ...newUsers];
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allTasks: newAllTasks,
                    newTasks: newAllTasks,
                    tasks: newAllTasks,
                    allRecurringTasks: newAllTasks,
                    recurringTasks: newAllTasks,
                    allEvents: allEvents,
                    allEveryEvents: allEvents,
                    usersScope: allBuildings,
                    allUsersScope: allBuildings,
                    type: SessionStateType.CREATE_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_ALL_HISTORY_SUCCESS]: {
                const tasks: ITaskFeed[] = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allHistory: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : [],
                    taskFeed: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : []
                });
            }

            case SessionActionType[SessionActionType.GET_STAFF_HISTORY_SUCCESS]: {
                const tasks: ITaskFeed[] = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allHistory:  (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : [],
                    taskFeed: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : []
                });
            }

            case SessionActionType[SessionActionType.SCOPE_BUIDLING_TASKS]: {
                const scope: string = action.payload;
                // // console.log("SCOPE_BUIDLING_TASKS", scope);
                let tasks: ITask[] = sessionState.allTasks;
                let scopeTask: ITask[] = [];
                if (scope !== 'All') {
                    scopeTask = UserUtil.ScopeTasks(tasks, scope);
                    // // console.log("scopeTask", scopeTask);
                } else {
                    scopeTask = sessionState.allTasks;
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: scopeTask
                });
            }
       
            case SessionActionType[SessionActionType.SCOPE_BUILDING_HISTORY]: {
                const helper: IGetBldgHelper= action.payload;
                // // // console.log("SCOPE_BUILDING_HISTORY", helper);
                let scopeTask: ITaskFeed[] = [];
                if ((null != sessionState.allHistory) && (sessionState.tasks.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeTask = sessionState.allHistory;
                    } else {
                        sessionState.allHistory.forEach((task: ITaskFeed) => {
                            if (task.buildingID === helper.buildingID) {
                                scopeTask.push(task);
                            }
                        })
                    }
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskFeed: (null != scopeTask) ? UserUtil.SortByDate(ArrayUtil.Transform(scopeTask)) : []
                });
            }

            case SessionActionType[SessionActionType.SCOPE_STAFF]:
            {
                const searchStr: string = action.payload;
                let userVals: IUser[] = sessionState.allUsersScope;
                let search: IUser[] = [];
                // // console.log("SCOPE_STAFF", searchStr);
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IUser[] = userVals.filter((map: IUser) => {
                        let fullName: string = `${map.firstName} ${map.lastName};`
                        return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = sessionState.allUsersScope;
                }
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    type: SessionStateType.SCOPE_USERS_SUCCEEDED,
                    usersScope: (null != search) ? search : []
                };
                return Object.assign({}, sessionState, partialFilterState);
            }

            case SessionActionType[SessionActionType.CLEAN_UP_SEARCH]: 
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    usersScope: sessionState.allUsersScope
                });
            }

            case SessionActionType[SessionActionType.SESSION_NO_ACTION]: 
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    type: SessionStateType.LOADING,
                });
            }

            case SessionActionType[SessionActionType.GET_TASKS_FAIL]:
            case SessionActionType[SessionActionType.ADD_TASK_MESSAGE_FAIL]:
            case SessionActionType[SessionActionType.CREATE_TASK_FAIL]:
            case SessionActionType[SessionActionType.COMPLETE_TASK_FAIL]:
            case SessionActionType[SessionActionType.COMPLETE_RECURRING_TASK_FAIL]:
            case SessionActionType[SessionActionType.REOPEN_RECURRING_TASK_FAIL]:
            case SessionActionType[SessionActionType.REOPEN_TASK_FAIL]:
            case SessionActionType[SessionActionType.GET_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_USERS_SCOPE_FAILED]:
            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS_FAIL]:
            case SessionActionType[SessionActionType.UPDATE_TASK_FAIL]:
            case SessionActionType[SessionActionType.GET_ALL_HISTORY_FAIL]:
            case SessionActionType[SessionActionType.START_TASK_FAIL]:
            case SessionActionType[SessionActionType.GET_RECURRING_STAFF_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_RECURRING_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_STAFF_HISTORY_FAIL]:
            case SessionActionType[SessionActionType.ASSIGN_TASK_FAIL]: 
            {
                return Object.assign({}, sessionState, { error: action.payload, loading: false, type: SessionStateType.SESSION_STATE_ERROR });
            }

            case SessionActionType[SessionActionType.GET_TASKS]:
            case SessionActionType[SessionActionType.CREATE_TASK]:
            case SessionActionType[SessionActionType.GET_USERS_SCOPE]:
            case SessionActionType[SessionActionType.GET_STAFF_HISTORY]:
            case SessionActionType[SessionActionType.REOPEN_TASK]:
            case SessionActionType[SessionActionType.REOPEN_RECURRING_TASK]:
            case SessionActionType[SessionActionType.COMPLETE_TASK]:
            case SessionActionType[SessionActionType.COMPLETE_RECURRING_TASK]:
            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS]:
            case SessionActionType[SessionActionType.UPDATE_TASK]:
            case SessionActionType[SessionActionType.GET_ALL_HISTORY]:
            {
                return Object.assign({}, sessionState, { error: null, loading: true });
            }

            case SessionActionType[SessionActionType.GET_TASKS]:
            case SessionActionType[SessionActionType.GET_BUILDING_TASKS]:
            case SessionActionType[SessionActionType.GET_TENANT_TASKS]:
            {
                return Object.assign({}, sessionState, { error: null, loading: true, tasks: [], allTasks: [] });
            }

            default: 
            {
                return sessionState;
            }
        }
    }
}

