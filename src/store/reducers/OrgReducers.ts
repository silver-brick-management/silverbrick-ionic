// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';

// Local modules
import { OrgActions } from '../actions/OrgActions';
import { IOrgBasic, INote, IBuildingSimple, IBuildingBasic, IBuilding } from '../../shared/SilverBrickTypes';
import { IOrgState, IStateBase, OrgStateType } from '../../interfaces/IStoreState';
import {
    IServerResponseGetAllOrgs,
    IServerResponseCreateOrg,
    IServerResponseGetOrgInfo
} from '../../shared/ResponseTypes';
import { ArrayUtil } from "../../common/ArrayUtil";
import { OrgBldgUtil } from "../../shared/OrgBldgUtil";

const initialOrgState: IOrgState = {
    error: null,
    loading: null,
    orgs: [],
    orgsInfo: [],
    allBuildings: [],
    allFullBuildings: [],
    selectedOrg: null,
    selectedOrgBldgs: [],
    searchBuildings: [],
    notes: [],
    selectedBldg: null,
    type: OrgStateType.NONE
};

export class OrgReducers {
    static Reduce(orgState: IOrgState = initialOrgState, action: any): IOrgState {
        switch (action.type) {
            case OrgActions.GET_ALL_ORGS:
            case OrgActions.ADD_ORG:
            case OrgActions.GET_ORGS_INFO:
            case OrgActions.UPDATE_ORG:
            case OrgActions.ADD_BLDG:
            case OrgActions.UPDATE_BLDG:
            case OrgActions.GET_BLDG:
            case OrgActions.ORG_STATE_LOADING:
            case OrgActions.GET_NOTE:
            case OrgActions.GET_ALL_BUILDINGS:
            {
                return Object.assign({}, orgState, {
                    error: null,
                    loading: true,
                    type: OrgStateType.LOADING
                });
            }

            case OrgActions.GET_ALL_ORGS_SUCCESS:
            {
                const getAllOrgsResponse: IOrgBasic[] = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    orgs: getAllOrgsResponse,
                    type: OrgStateType.GET_ALL_ORGS_SUCCEEDED
                });
            }

            case OrgActions.GET_ALL_BUILDINGS_SUCCESS:
            {
                const getAllOrgsResponse: IBuilding[] = action.payload;
                console.log("GET_ALL_BUILDINGS", getAllOrgsResponse);
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    allFullBuildings: getAllOrgsResponse,
                    type: OrgStateType.GET_ALL_ORGS_SUCCEEDED
                });
            }

            case OrgActions.GET_ALL_ORGS_SCOPE_SUCCESS:
            {
                const getAllOrgsResponse: IOrgBasic[] = action.payload;
                let allBuildings: any[] = [{ id: 'all', name: "All" }];
                if (null != getAllOrgsResponse) {
                    for (let org of getAllOrgsResponse) {
                        if (null != org.buildings) {
                            allBuildings = [...allBuildings, ...org.buildings];
                        }
                    }
                }
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    allBuildings: allBuildings,
                    searchBuildings: allBuildings,
                    type: OrgStateType.GET_ALL_ORGS_SUCCEEDED
                });
            }

            case OrgActions.GET_ORGS_INFO_SUCCESS:
            {
                const getOrgInfoResponse: IServerResponseGetOrgInfo = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedOrg: getOrgInfoResponse 
                });
            }

            case OrgActions.ADD_ORG_SUCCESS:
            {
                const createOrgResponse: IOrgBasic = action.payload;
                const newOrgs: IOrgBasic[] = [...orgState.orgs, createOrgResponse];
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    orgs: newOrgs,
                    type: OrgStateType.ADD_ORG_SUCCEEDED
                });
            }

            case OrgActions.UPDATE_ORG_SUCCESS:
            {
                const updateOrgResponse: IOrgBasic = action.payload;
                let partialUpdateState: IStateBase = {
                    loading: false,
                    error: null
                };

                 Object.assign(partialUpdateState, {
                        orgs: orgState.orgs.map((org: IOrgBasic) => {
                            return (org.id === updateOrgResponse.id) ? Object.assign({}, org, updateOrgResponse) : org;
                        }),
                        type: OrgStateType.UPDATE_ORG_SUCCEEDED

                    });
                return Object.assign({}, orgState, partialUpdateState);
            }

            case OrgActions.ADD_BLDG_SUCCESS:
            {
                const createBldgResponse: IBuilding = action.payload;
                const bldgs: IBuilding[] = [...orgState.selectedOrgBldgs, createBldgResponse];
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedOrgBldgs: bldgs,
                    type: OrgStateType.ADD_BLDG_SUCCEEDED
                });
            }

            case OrgActions.GET_BLDG_SUCCESS:
            {
                const getBldgResponse: IBuilding = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedBldg: getBldgResponse,
                    type: OrgStateType.GET_BUILDING_SUCCEEDED
                });
            }

            case OrgActions.UPDATE_BLDG_SUCCESS:
            {
                const updateOrgResponse: IBuilding = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedBldg: updateOrgResponse,
                    type: OrgStateType.UPDATE_BLDG_SUCCEEDED
                });
            }

            case OrgActions.IMPORT_USERS_SUCCESS:
            {
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    type: OrgStateType.IMPORT_USERS_SUCCEEDED
                });
            }

            case OrgActions.GET_NOTE_SUCCESS:
            {
                const notes: INote[] = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    notes: notes,
                    type: OrgStateType.GET_NOTES_SUCCEEDED
                });
            }

            case OrgActions.ADD_NOTE_SUCCESS:
            {
                const note: INote = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    notes: [...orgState.notes, note],
                    type: OrgStateType.ADD_NOTE_SUCCEEDED
                });
            }

            case OrgActions.UPDATE_NOTE_SUCCESS:
            {
                const noteNew: INote = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    notes: orgState.notes.map((note: INote) => {
                        return (note.id === noteNew.id) ? Object.assign({}, note, noteNew) : note;
                    }),
                    type: OrgStateType.UPDATE_NOTE_SUCCEEDED
                });
            }

            case OrgActions.SEARCH_BUILDINGS:
            {
                const searchStr: string = action.payload;
                let userVals: IBuildingSimple[] = orgState.allBuildings;
                let search: IBuildingSimple[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IBuildingSimple[] = userVals.filter((map: IBuildingSimple) => {
                        return (map.name.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = orgState.allBuildings;
                }
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchBuildings: (null != search) ? search : []
                };
                return Object.assign({}, orgState, partialFilterState);
            }

            case OrgActions.CLEAN_UP_SEARCH: 
            {
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    searchBuildings: orgState.allBuildings
                });
            }

            case OrgActions.GET_ALL_ORGS_FAILURE:
            case OrgActions.GET_ALL_BUILDINGS_FAILURE:
            case OrgActions.GET_NOTE_FAILURE:
            {
                let error: any = action.payload;
                return Object.assign({}, orgState, {
                    error: error,
                    loading: false
                });
            }
            
            default:
            {
                return orgState;
            }
        };
    }
}
