// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';

// Local modules
import { AuthActions, AuthActionType } from '../actions/AuthActions';
import { IAuthState, IPartialAuthState, AuthStateType } from '../../interfaces/IStoreState';
import { IServerResponseLogin, IServerResponseMessage } from '../../shared/ResponseTypes';
import { ArrayUtil } from '../../common/ArrayUtil';
import {
    IUser,
    IUserOrg,
    IUserBuilding,
    IUserAudience,
    IUserOrgBuildingBase,
    IService,
    IBooking
} from '../../shared/SilverBrickTypes';

const initialAuthState: IAuthState = {
    error: null,
    loading: null,
    token: null,
    profile: null,
    authRefreshInitiated: null,
    isAuthSocketConnected: null,
    isMessageSocketConnected: null,
    type: AuthStateType.NONE
};

export class AuthReducers {
    static Reduce(authState: IAuthState = initialAuthState, action: any): IAuthState {
        switch (action.type) {
            case AuthActionType[AuthActionType.INIT]:
            case AuthActionType[AuthActionType.LOGIN_FIREBASE]:
            {
                return Object.assign({}, authState, { loading: true, type: AuthStateType.LOADING });
            }

            case AuthActionType[AuthActionType.LOGIN_SERVER_SUCCEEDED]:
                {
                    const serverResponse: IServerResponseLogin = action.payload;
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: true,
                        type: AuthStateType.SERVER_READY,
                        token: serverResponse.accessToken,
                        profile: serverResponse.data
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.INIT_AUTH_SOCKET_SUCCEEDED]:
                {
                    const socketResponse: IServerResponseMessage = action.payload;
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: true,
                        type: AuthStateType.AUTH_SOCKET_READY,
                        isAuthSocketConnected: socketResponse.success
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.INIT_MESSAGE_SOCKET_SUCCEEDED]:
                {
                    const socketResponse: IServerResponseMessage = action.payload;
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: true,
                        type: AuthStateType.MESSAGE_SOCKET_READY,
                        isMessageSocketConnected: socketResponse.success
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.LOGIN_SUCCESS]:
                {
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: false,
                        type: AuthStateType.SERVER_AND_SOCKET_READY
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.CHANGE_PASSWORD_SUCCESS]:
            {    
                let newAuthState: any = {
                    error: null, 
                    loading: false, 
                    type: AuthStateType.CHANGE_PASSWORD_SUCCEEDED,
                    profile: Object.assign({}, authState.profile, { isFirstTime: false })
                }
                return Object.assign({}, authState, { error: null, loading: false, type: AuthStateType.CHANGE_PASSWORD_SUCCEEDED });
            }

            case AuthActionType[AuthActionType.SOCKET_DISCONNECTED]:
                {
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: true,
                        type: AuthStateType.SOCKET_DISCONNECTED,
                        isAuthSocketConnected: false,
                        isMessageSocketConnected: false,
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.SOCKET_UNABLE_TO_CONNECT]:
                {
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: true,
                        type: AuthStateType.SOCKET_UNABLE_TO_CONNECT,
                        isAuthSocketConnected: false,
                        isMessageSocketConnected: false,
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.FORCE_AUTH_REFRESH]:
                {
                    const partialAuthState: IPartialAuthState = {
                        error: null,
                        loading: false,
                        authRefreshInitiated: true,
                        type: AuthStateType.AUTH_REFRESH_INITIATED
                    };
                    return Object.assign({}, authState, partialAuthState);
                }

            case AuthActionType[AuthActionType.LOGIN_FAIL]:
            {
                return Object.assign({}, initialAuthState, { error: action.payload, loading: false, type: AuthStateType.LOGIN_FAILED });
            }

            case AuthActionType[AuthActionType.LOGOUT_SUCCESS]:
            {
                const partialAuthState: IPartialAuthState = {
                    error: null,
                    loading: false,
                    type: AuthStateType.LOGOUT_SUCCEEDED
                };
                return Object.assign({}, initialAuthState, partialAuthState);
            }
            
            case AuthActionType[AuthActionType.SESSION_INIT_SUCCESS]:
            {
                return Object.assign({}, initialAuthState, { error: null, loading: false, type: AuthStateType.SESSION_INIT_SUCCESS });
            }

            default:
            {
                return authState;
            }
        }
    }
}
