// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';

// Local modules
import { UserActions } from '../actions/UserActions';
import { IUser, ITimeCard } from '../../shared/SilverBrickTypes';
import { IUserState, UserStateType } from '../../interfaces/IStoreState';
import { IUserActivationParam, ICredentials, IGetBldgHelper } from '../../shared/IParams';
import { ArrayUtil } from "../../common/ArrayUtil";

import {
    IServerResponseGetProfile,
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseGetBuildingUsers
} from '../../shared/ResponseTypes';
import { UserUtil } from '../../common/UserUtil';

const initialUsersState: IUserState = {
    error: null,
    loading: null,
    limit: "25",
    next: null,
    previous: null,
    prevCursor: null,
    nextCursor: null,
    users: [],
    searchStaff: [],
    buildingUsers: [],
    searchTenants: [],
    tenants: [],
    timeCards: [],
    timeCardsPrevious: [],
    masterTenants: [],
    allTenants: [],
    allUsers: [],
    credentials: null,
    createdUser: null,
    type: UserStateType.NONE
};

export class UserReducers {
    static Reduce(usersState: IUserState = initialUsersState, action: any): IUserState {
        switch (action.type) {

            case UserActions.ADD_USER:
            case UserActions.GET_USER:
            case UserActions.GET_USERS:
            case UserActions.ADD_TIME_CARD:
            case UserActions.UPDATE_TIME_CARD:
            case UserActions.GET_ALL_TENANTS:
            case UserActions.GET_ALL_TENANTS_LANDLORD:
            case UserActions.UPDATE_USER:
            case UserActions.UPDATE_USER_PASSWORD:
            case UserActions.DELETE_USER:
            case UserActions.ACTIVATE_USER:
            case UserActions.DEACTIVATE_USER:
            case UserActions.GET_USERS_FROM_FULL_URL:
            case UserActions.GET_BUILDING_USERS:
            case UserActions.GET_TIME_CARDS_PREVIOUS:
            case UserActions.GET_BUILDING_TENANTS:
            {
                return Object.assign({}, usersState, { error: null, loading: true, type: UserStateType.LOADING });
            }

            case UserActions.GET_TIME_CARDS:
            case UserActions.GET_TIME_CARD:
            {
                return Object.assign({}, usersState, { error: null, loading: true, type: UserStateType.LOADING, timeCards: [] });
            }

            case UserActions.GET_USERS_SUCCESS:
            {
                const getManyResponse: IServerResponseGetUsers = action.payload;
                let sortedUsers: IUser[] = UserUtil.SortByCreatedAt(getManyResponse.data);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    next: getManyResponse.paging.next,
                    previous: getManyResponse.paging.previous,
                    prevCursor: getManyResponse.paging.cursors.prevCursor,
                    nextCursor: getManyResponse.paging.cursors.nextCursor,
                    allUsers: (null != getManyResponse.data) ? UserUtil.SortByCreatedAt(getManyResponse.data).map(user => user) : [],
                    users: (null != getManyResponse.data) ? UserUtil.SortByCreatedAt(getManyResponse.data).map(user => user) : [],
                    searchStaff: UserUtil.SortByName(getManyResponse.data),
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_BUILDING_USERS_SUCCESS:
            {
                const getBldgUsersResponse: IServerResponseGetBuildingUsers = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    next: usersState.next,
                    previous: usersState.previous,
                    prevCursor: usersState.prevCursor,
                    nextCursor: usersState.nextCursor,
                    buildingUsers: (null != getBldgUsersResponse.data) ? UserUtil.SortByCreatedAt(getBldgUsersResponse.data).map(user => user) : [],
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_BUILDING_TENANTS_SUCCESS:
            {
                const getBldgUsersResponse: IServerResponseGetBuildingUsers = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    masterTenants: (null != getBldgUsersResponse.data) ? UserUtil.SortByCreatedAt(getBldgUsersResponse.data).map(user => user) : [],
                    allTenants: (null != getBldgUsersResponse.data) ? UserUtil.SortByCreatedAt(getBldgUsersResponse.data).map(user => user) : [],
                    tenants: (null != getBldgUsersResponse.data) ? UserUtil.SortByCreatedAt(getBldgUsersResponse.data).map(user => user) : [],
                    searchTenants: getBldgUsersResponse.data,
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_ALL_TENANTS_SUCCESS:
            {
                const getBldgUsersResponse: IUser[] = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    masterTenants: (null != getBldgUsersResponse) ? UserUtil.SortByCreatedAt(getBldgUsersResponse).map(user => user) : [],
                    allTenants: (null != getBldgUsersResponse) ? UserUtil.SortByCreatedAt(getBldgUsersResponse).map(user => user) : [],
                    searchTenants: getBldgUsersResponse,
                    type: UserStateType.GET_ALL_TENANTS_SUCCEEDED
                });
            }

            case UserActions.GET_USER_SUCCESS:
            {
                const getOneResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;

                return Object.assign({}, initialUsersState, {
                    loading: false,
                    users: [...initialUsersState.users, Object.assign({}, getOneResponse.data)]
                });
            }

            case UserActions.GET_TIME_CARDS_SUCCESS:
            {
                const getOneResponse: ITimeCard[] = action.payload;
                return Object.assign({}, initialUsersState, {
                    loading: false,
                    timeCards: getOneResponse,
                    type: UserStateType.GET_TIME_CARDS_SUCCEEDED
                });
            }

            case UserActions.GET_TIME_CARDS_PREVIOUS_SUCCESS:
            {
                const getOneResponse: ITimeCard[] = action.payload;
                return Object.assign({}, initialUsersState, {
                    loading: false,
                    timeCardsPrevious: getOneResponse,
                    type: UserStateType.GET_TIME_CARDS_PREVIOUS_SUCCEEDED
                });
            }

            case UserActions.GET_ALL_TENANTS_LANDLORD_SUCCESS:
            {
                const getBldgUsersResponse: IUser[] = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    tenants: getBldgUsersResponse,
                    searchTenants: getBldgUsersResponse,
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_TIME_CARD_SUCCESS:
            {
                const getOneResponse: ITimeCard[] = action.payload;
                return Object.assign({}, initialUsersState, {
                    loading: false,
                    timeCards: getOneResponse
                });
            }

            case UserActions.ADD_TIME_CARD_SUCCESS:
            {
                const timeCard: ITimeCard = action.payload;
                return Object.assign({}, initialUsersState, {
                    loading: false,
                    timeCards: [...usersState.timeCards, timeCard],
                    type: UserStateType.ADD_TIME_CARD_SUCCEEDED
                });
            }

            case UserActions.UPDATE_TIME_CARD_SUCCESS:
            {
                const timeCard: ITimeCard = action.payload;
                return Object.assign({}, initialUsersState, {
                    loading: false,
                    timeCards: usersState.timeCards.map((card: ITimeCard) => {
                        return card.id === timeCard.id ? Object.assign({}, card, timeCard) : card;
                    }),
                    type: UserStateType.UPDATE_TIME_CARD_SUCCEEDED
                });
            }

            case UserActions.ADD_USER_SUCCESS:
            {
                const addResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;
                console.log("User", addResponse.data);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: [...usersState.users, Object.assign({}, addResponse.data)]
                });
            }

            case UserActions.IMPORT_USERS_SUCCESS:
            {
                const importedUsers: IUser[] = action.payload;
                // console.log("IMPORT_USERS_SUCCESS", importedUsers.length);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: [...usersState.users, Object.assign({}, importedUsers)]
                });
            }

            case UserActions.UPDATE_USER_SUCCESS:
            {
                const updateResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: usersState.users.map((user: IUser) => {
                        return user.uid === updateResponse.data.uid ? Object.assign({}, user, updateResponse.data) : user;
                    })
                });
            }

            case UserActions.UPDATE_USER_PASSWORD_SUCCESS:
            {
                const updateResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    type: UserStateType.UPDATE_USER_SUCCEEDED
                });
            }

            case UserActions.DELETE_USER_SUCCESS:
            {
                const deleteUid: string = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: usersState.users.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    })
                });
            }

            case UserActions.ACTIVATE_USER_SUCCESS:
            case UserActions.DEACTIVATE_USER_SUCCESS:
            {
                const activationParam: IUserActivationParam = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: usersState.users.map((user: IUser) => {
                        return (user.uid === activationParam.uid) ?
                            Object.assign({}, user, {isActive: activationParam.activationStatus }) :
                            user;
                    })
                });
            }

            case UserActions.SEARCH_ALL_TENANTS:
            {
                const searchStr: string = action.payload;
                let userVals: IUser[] = usersState.allTenants;
                let search: IUser[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IUser[] = userVals.filter((map: IUser) => {
                        let fullName: string = `${map.firstName} ${map.lastName};`
                        return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                        // return map.name.toLowerCase() === searchString.toLowerCase().trim();
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = usersState.allTenants;
                }
                console.log("SEARCH_ALL_TENANTS", search, userVals, searchStr);

                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchTenants: (null != search) ? search : [],
                    type: UserStateType.SCOPE_USERS_SUCCEEDED,
                };
                return Object.assign({}, usersState, partialFilterState);
            }

            case UserActions.SEARCH_ALL_STAFF:
            {
                const searchStr: string = action.payload;
                let userVals: IUser[] = usersState.allUsers;
                let search: IUser[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {

                    const search2: IUser[] = userVals.filter((map: IUser) => {
                        let fullName: string = `${map.firstName} ${map.lastName};`
                        return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                        // return map.name.toLowerCase() === searchString.toLowerCase().trim();
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = usersState.allUsers;
                }
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchStaff: (null != search) ? UserUtil.SortByName(search) : [],
                    type: UserStateType.SCOPE_USERS_SUCCEEDED,
                };
                return Object.assign({}, usersState, partialFilterState);
            }

            case UserActions.SCOPE_BUILDING_TENANTS:
            {
                const helper: IGetBldgHelper= action.payload;
                console.log("SCOPE_BUILDING_HISTORY", helper);
                let scopeTask: IUser[] = [];
                if ((null != usersState.masterTenants) && (usersState.masterTenants.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeTask = usersState.masterTenants;
                    } else {
                        usersState.masterTenants.forEach((task: IUser) => {
                            if ((null != task.orgs) && (null != task.orgs.buildings) && (null != task.orgs.buildings[0])) {
                                if (task.orgs.buildings[0].id === helper.buildingID) {
                                    scopeTask.push(task);
                                }
                            }
                        })
                    }
                }
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    type: UserStateType.SCOPE_USERS_SUCCEEDED,
                    allTenants: (null != scopeTask) ? UserUtil.SortByDate(ArrayUtil.Transform(scopeTask)) : []
                });
            }

            case UserActions.CLEAN_UP_SEARCH: 
            {
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    searchStaff: UserUtil.SortByName(usersState.users),
                    searchTenants: UserUtil.SortByName(usersState.allTenants)
                });
            }

            case UserActions.ADD_USER_FAIL:
            case UserActions.GET_USER_FAIL:
            case UserActions.GET_USERS_FAIL:
            case UserActions.UPDATE_USER_FAIL:
            case UserActions.ADD_TIME_CARD_FAIL:
            case UserActions.UPDATE_TIME_CARD_FAIL:
            case UserActions.GET_TIME_CARD_FAIL:
            case UserActions.GET_TIME_CARDS_FAIL:
            case UserActions.DELETE_USER_FAIL:
            case UserActions.ACTIVATE_USER_FAIL:
            case UserActions.DEACTIVATE_USER_FAIL:
            case UserActions.GET_BUILDING_USERS_FAIL:
            case UserActions.GET_TIME_CARDS_PREVIOUS_FAIL:
            case UserActions.UPDATE_USER_PASSWORD_FAIL:
            case UserActions.GET_ALL_TENANTS_LANDLORD_FAIL:
            case UserActions.GET_ALL_TENANTS_FAIL:
            {
                return Object.assign({}, usersState, { error: action.payload, loading: false });
            }

            default:
            {
                return usersState;
            }
        }
    }
}
