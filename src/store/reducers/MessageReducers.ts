// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from "@ngrx/store";

// Local modules
import { IMessage } from "../../shared/SilverBrickTypes";
import { IStateBase, IMessageState, MessageStateType } from "../../interfaces/IStoreState";
import { AddChatHelper, IGetMessageHistoryHelper, NewChatRoomHelper, UserChatRoom } from "../../shared/ResponseTypes";
import { MessageActionTypes } from "../actions/MessageActions";
import { UserUtil } from "../../common/UserUtil";

interface IPartialMessageState extends IStateBase {
    loading: boolean;
    messages?: IMessage[];
    hasMoreMessages?: boolean;
    cursor?: string;
    type?: MessageStateType;
    chatRooms?: UserChatRoom[];
    isInitialLoad?: boolean;
}

// change error & loading to base & extend when needed?
const initialMessageState: IMessageState = {
    error: null,
    loading: false,
    messages: [],
    hasMoreMessages: false,
    cursor: null,
    chatRooms: [],
    isInitialLoad: false,
    type: MessageStateType.NONE,
};

export class MessageReducers {
    static Reduce(messageState: IMessageState = initialMessageState, action: Action): IMessageState {
        switch (action.type) {
            case MessageActionTypes[MessageActionTypes.JOIN_MESSAGE_CHANNEL]:
            case MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM]:
            {
                const partialMessageState: IPartialMessageState = {
                    loading: true,
                    error: null,
                    messages: [],
                };

                return Object.assign({}, messageState, partialMessageState);
            }

            case MessageActionTypes[MessageActionTypes.ADD_MESSAGE]:
            case MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS]: 
            case MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY]: 
            {
                const partialMessageState: IPartialMessageState = {
                    loading: true,
                    error: null,
                };
                return Object.assign({}, messageState, partialMessageState);
            }

            case MessageActionTypes[MessageActionTypes.ADD_MESSAGE_SUCCESS]: 
            {
                let message: AddChatHelper = action.payload;
                const partialMessageState: IPartialMessageState = {
                    loading: false,
                    error: null,
                    messages: [...messageState.messages, message.message]
                };
                return Object.assign({}, messageState, partialMessageState);
            }

            case MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_SUCCESS]:
            {
                const serverResponse: IGetMessageHistoryHelper = action.payload;
                // console.log("GET_MESSAGE_HISTORY_SUCCESS", serverResponse);
                const partialMessageState: IPartialMessageState = {
                    loading: false,
                    error: null,
                    messages: [...serverResponse.messages, ...messageState.messages],
                    hasMoreMessages: serverResponse.hasMoreMessages,
                    isInitialLoad: serverResponse.isInitialLoad,
                    cursor: serverResponse.cursor,
                    type: MessageStateType.GET_MESSAGES_SUCCESS,
                };
                return Object.assign({}, messageState, partialMessageState);
            }

            case MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_SUCCESS]:
            {
                const serverResponse: NewChatRoomHelper = action.payload;
                // console.log("NEW_CHAT_ROOM_SUCCESS", serverResponse);
                const partialMessageState: IPartialMessageState = {
                    loading: false,
                    error: null,
                    messages: [...messageState.messages, serverResponse.message],
                    type: MessageStateType.NEW_CHAT_ROOM_SUCCEEDED,
                };
                return Object.assign({}, messageState, partialMessageState);
            }

            case MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS_SUCCESS]:
            {
                const serverResponse: UserChatRoom[] = action.payload;
                // console.log("GET_CHAT_ROOMS_SUCCESS", serverResponse);
                serverResponse.forEach((room: UserChatRoom) => {
                    room.users = UserUtil.Transform(room.users);
                });
                const partialMessageState: IPartialMessageState = {
                    loading: false,
                    error: null,
                    chatRooms: UserUtil.Transform(serverResponse),
                    type: MessageStateType.GET_CHAT_ROOMS_SUCCESS,
                };
                console.log("CHAT ROOMS BITCH: ", UserUtil.sortChatRooms(partialMessageState.chatRooms));
                return Object.assign({}, messageState, partialMessageState);
            }

            case MessageActionTypes[MessageActionTypes.ADD_MESSAGE_FAIL]: 
            case MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS_FAIL]: 
            case MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_FAIL]: 
            case MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_FAIL]: 
            {
                let error: any = action.payload;
                const partialMessageState: IPartialMessageState = {
                    loading: false,
                    error: error,
                    type: MessageStateType.MESSAGE_STATE_FAILED
                };

                return Object.assign({}, messageState, partialMessageState);
            }

            default:
            {
                return messageState;
            }
        }
    }
}

