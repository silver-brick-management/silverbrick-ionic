// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';

// Local modules
import {
    ISilverBrickUser,
    IMessage
} from '../../shared/SilverBrickTypes';
import {
    AddChatHelper,
    AddMessageHelper,
    IGetMessageHistoryHelper,
    NewChatRoomHelper,
    UserChatRoom
} from '../../shared/ResponseTypes';

export enum MessageActionTypes {
    JOIN_MESSAGE_CHANNEL,
    LEAVE_MESSAGE_CHANNEL,
    GET_CHAT_ROOMS,
    GET_CHAT_ROOMS_SUCCESS,
    GET_CHAT_ROOMS_LISTENER,
    GET_CHAT_ROOMS_FAIL,
    NEW_CHAT_ROOM,
    NEW_CHAT_ROOM_SUCCESS,
    NEW_CHAT_ROOM_FAIL,
    NEW_CHAT_ROOM_LISTENER,
    GET_MESSAGE_HISTORY,
    GET_MESSAGE_HISTORY_SUCCESS,
    GET_MESSAGE_HISTORY_LISTENER,
    GET_MESSAGE_HISTORY_FAIL,
    ADD_MESSAGE,
    ADD_MESSAGE_SUCCESS,
    ADD_MESSAGE_LISTENER,
    ADD_MESSAGE_FAIL,
    MESSAGE_NO_ACTION
}

export class MessageActions {

    JoinMessageRoom(roomID: string = null): Action {
        return {
            type: MessageActionTypes[MessageActionTypes.JOIN_MESSAGE_CHANNEL],
            payload: roomID
        };
    }

    LeaveMessageRoom(): Action {
        return {
            type: MessageActionTypes[MessageActionTypes.LEAVE_MESSAGE_CHANNEL]
        };
    }

    GetChatRooms(): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS]
        };
    }

    GetChatRoomsSuccess(response: UserChatRoom[]): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS_SUCCESS],
            payload: response
        };
    }

    GetChatRoomsListener(): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS_LISTENER]
        };
    }

    GetChatRoomsFail(error: any): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS_FAIL],
            payload: error
        };
    }

    NewChatRoom(helper: NewChatRoomHelper): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM],
            payload: helper
        };
    }

    NewChatRoomSuccess(response: NewChatRoomHelper): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_SUCCESS],
            payload: response
        };
    }

    NewChatRoomListener(): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_LISTENER]
        };
    }

    NewChatRoomFail(error: any): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_FAIL],
            payload: error
        };
    }

    GetMessageHistory(roomID: string, cursor: string = null): Action {
        console.log("Actions: GetMessageHistory ", roomID, cursor);
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY],
            payload: { roomID: roomID, cursor: cursor }
        };
    }

    GetMessageHistorySuccess(response: IGetMessageHistoryHelper): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_SUCCESS],
            payload: response
        };
    }

    GetMessageHistoryListener(): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_LISTENER]
        };
    }

    GetMessageHistoryFail(error: any): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_FAIL],
            payload: error
        };
    }

    AddMessage(helper: AddChatHelper): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.ADD_MESSAGE],
            payload: helper
        };
    }

    AddMessageSuccess(helper: AddChatHelper): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.ADD_MESSAGE_SUCCESS],
            payload: helper
        };
    }

    AddMessageListener(): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.ADD_MESSAGE_LISTENER]
        };
    }

    AddMessageFail(error: any): Action {
        return  {
            type: MessageActionTypes[MessageActionTypes.ADD_MESSAGE_FAIL],
            payload: error
        };
    }

    MessageNoAction(): Action {
        return {
            type: MessageActionTypes[MessageActionTypes.MESSAGE_NO_ACTION]
        };
    }

}
