// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { User } from 'firebase';
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';

// Local modules
import { ICredentials } from '../../shared/IParams';
import { IServerResponseLogin, IServerResponseMessage } from '../../shared/ResponseTypes';

export enum AuthActionType {
    INIT,
    LOGIN_FIREBASE,
    LOGIN_SERVER,
    LOGIN_SERVER_SUCCEEDED,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    LOGOUT_SUCCESS,
    UPDATE_JWT_TOKEN,
    LOGOUT_FAIL,
    SESSION_INIT_SUCCESS,
    INIT_AUTH_SOCKET,
    INIT_AUTH_SOCKET_SUCCEEDED,
    INIT_MESSAGE_SOCKET,
    INIT_MESSAGE_SOCKET_SUCCEEDED,
    SOCKET_DISCONNECT_LISTENER,
    SOCKET_DISCONNECTED,
    SOCKET_UNABLE_TO_CONNECT,
    AUTH_SOCKET_RECONNECT_LISTENER,
    MESSAGE_SOCKET_RECONNECT_LISTENER,
    FORCE_RECONNECT,
    FORCE_RECONNECT_FOLLOWUP,
    FORCE_AUTH_REFRESH,
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAIL,
    AUTH_NO_ACTION
}

// Packaging the action to ensure cleaner code in the components
@Injectable()
export class AuthActions {
    Initialize(): Action {
        return {
            type: AuthActionType[AuthActionType.INIT]
        };
    }

    ForceReconnect(): Action {
        return {
            type: AuthActionType[AuthActionType.FORCE_RECONNECT]
        };
    }

    ForceReconnectFollowUp(): Action {
        return {
            type: AuthActionType[AuthActionType.FORCE_RECONNECT_FOLLOWUP]
        };
    }

    LoginFirebase(credentials: ICredentials): Action {
        return {
            type: AuthActionType[AuthActionType.LOGIN_FIREBASE],
            payload: credentials
        };
    }

    LoginServer(firebaseUser: User): Action {
        return {
            type: AuthActionType[AuthActionType.LOGIN_SERVER],
            payload: firebaseUser
        };
    }

    LoginServerSucceeded(serverResponse: IServerResponseLogin): Action {
        return {
            type: AuthActionType[AuthActionType.LOGIN_SERVER_SUCCEEDED],
            payload: serverResponse
        };
    }

    InitAuthSocket(token: string): Action {
        return {
            type: AuthActionType[AuthActionType.INIT_AUTH_SOCKET],
            payload: token
        };
    }

    InitAuthSocketSucceeded(socketResponse: IServerResponseMessage): Action {
        return {
            type: AuthActionType[AuthActionType.INIT_AUTH_SOCKET_SUCCEEDED],
            payload: socketResponse
        };
    }

    InitMessageSocket(): Action {
        return {
            type: AuthActionType[AuthActionType.INIT_MESSAGE_SOCKET]
        };
    }

    InitMessageSocketSucceeded(socketResponse: IServerResponseMessage): Action {
        return {
            type: AuthActionType[AuthActionType.INIT_MESSAGE_SOCKET_SUCCEEDED],
            payload: socketResponse
        };
    }

    LoginSuccess(): Action {
        return {
            type: AuthActionType[AuthActionType.LOGIN_SUCCESS]
        };
    }

    LoginFail(error: any): Action {
        return {
            type: AuthActionType[AuthActionType.LOGIN_FAIL],
            payload: error
        };
    }

    Logout(): Action {
        return {
            type: AuthActionType[AuthActionType.LOGOUT]
        };
    }

    LogoutSuccess(): Action {
        return {
            type: AuthActionType[AuthActionType.LOGOUT_SUCCESS]
        };
    }

    LogoutFail(error: any): Action {
        return {
            type: AuthActionType[AuthActionType.LOGOUT_FAIL],
            payload: error
        };
    }

    SocketDisconnectListener(): Action {
        return {
            type: AuthActionType[AuthActionType.SOCKET_DISCONNECT_LISTENER]
        };
    }

    SocketDisconnected(): Action {
        return {
            type: AuthActionType[AuthActionType.SOCKET_DISCONNECTED]
        };
    }

    SocketUnableToConnect(): Action {
        return {
            type: AuthActionType[AuthActionType.SOCKET_UNABLE_TO_CONNECT]
        };
    }

    AuthSocketReconnectListener(): Action {
        return {
            type: AuthActionType[AuthActionType.AUTH_SOCKET_RECONNECT_LISTENER]
        };
    }

    MessageSocketReconnectListener(): Action {
        return {
            type: AuthActionType[AuthActionType.MESSAGE_SOCKET_RECONNECT_LISTENER]
        };
    }

    ForceAuthRefresh(): Action {
        return {
            type: AuthActionType[AuthActionType.FORCE_AUTH_REFRESH]
        };
    }

    SessionInitSuccess(): Action {
        return {
            type: AuthActionType[AuthActionType.SESSION_INIT_SUCCESS]
        };
    }

    ChangePassword(pw: string): Action {
        return {
            type: AuthActionType[AuthActionType.CHANGE_PASSWORD],
            payload: pw
        };
    }

    ChangePasswordSuccess(): Action {
        return {
            type: AuthActionType[AuthActionType.CHANGE_PASSWORD_SUCCESS]
        };
    }

    ChangePasswordFail(error: any): Action {
        return {
            type: AuthActionType[AuthActionType.CHANGE_PASSWORD_FAIL],
            payload: error
        };
    }

    UpdateJwtToken(token: string): Action {
        return {
            type: AuthActionType[AuthActionType.UPDATE_JWT_TOKEN],
            payload: token
        };
    }

    AuthNoAction(): Action {
        return {
            type: AuthActionType[AuthActionType.AUTH_NO_ACTION]
        };
    }
}
