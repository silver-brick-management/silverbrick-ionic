import { AppPage } from './app.po';
import { browser, by, ElementFinder } from "protractor";

/*
    Note: by.binding and by.model aren't working in angular 2.
    https://github.com/angular/protractor/issues/3205
*/
describe('SilverBrick App', () => {
    let appPage: AppPage;

    beforeEach(async () => {
        appPage = new AppPage();
        await browser.waitForAngularEnabled(false);
    });


});
